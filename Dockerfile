FROM cloudron/base:0.9.0
MAINTAINER Girish Ramakrishnan <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y python-pygments subversion mercurial openssh-server && \
    apt-get install -y --allow-unauthenticated php5.6 libapache2-mod-php5.6 php5.6-curl php5.6-gd php5.6-mbstring php5.6-mcrypt php5.6-mysql php5.6-xml php5.6-xmlrpc php5.6-ldap && \
    rm -r /var/cache/apt /var/lib/apt/lists

# by default, git account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN adduser --disabled-login --gecos 'Git' git && passwd -d git
RUN adduser --disabled-login --gecos 'Phabricator Deamon' phd

# https://secure.phabricator.com/book/phabricator/article/diffusion_hosting
RUN echo "git ALL=(phd) SETENV: NOPASSWD: /usr/bin/git-upload-pack, /usr/bin/git-receive-pack, /usr/bin/hg, /usr/bin/svnserve" >> /etc/sudoers

# (stable) Promote 2016 Week 45
RUN mkdir libphutil && \
    cd libphutil && \
    curl -L https://github.com/phacility/libphutil/archive/40e473e057885dab5de4fb42901f7fff2d340a6a.tar.gz | tar -xzf - --strip-components 1

# (stable) Promote 2016 Week 43
RUN mkdir arcanist && \
    cd arcanist && \
    curl -L https://github.com/phacility/arcanist/archive/e17fe43ca3fe6dc6dd0b5ce056f56310ea1d3d51.tar.gz | tar -xzf - --strip-components 1

# if you update this, make a new db_seed.sql as well
# (stable) Promote 2016 Week 45
RUN mkdir phabricator && \
    cd phabricator && \
    curl -L https://github.com/phacility/phabricator/archive/111c63955155046bbd57c3836c78d460ebe13dfb.tar.gz | tar -xzf - --strip-components 1
ADD local.json.template /app/code/phabricator/conf/local/local.json.template
RUN ln -s /run/phabricator/local.json /app/code/phabricator/conf/local/local.json 

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache2-phabricator.conf /etc/apache2/sites-available/phabricator.conf
RUN ln -sf /etc/apache2/sites-available/phabricator.conf /etc/apache2/sites-enabled/phabricator.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod php5.6
RUN a2dismod php7.0
RUN a2enmod rewrite
RUN sed -e 's/^upload_max_filesize = .*/upload_max_filesize = 512M/' \
        -e 's/^post_max_size = .*/post_max_size = 512M/' \
        -e 's/^memory_limit = .*/memory_limit = -1/' \
        -e 's,;session.save_path.*,session.save_path = "/run/phabricator/sessions",' \
        -e 's/;opcache.validate_timestamps=1/opcache.validate_timestamps=0/' \
        -e 's/;always_populate_raw_post_data =.*/always_populate_raw_post_data=-1/' \
        -i /etc/php/5.6/apache2/php.ini

# configure supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/phabricator/supervisord.log,' -i /etc/supervisor/supervisord.conf
ADD supervisor/ /etc/supervisor/conf.d/

ADD sshd_config /app/code/sshd_config
ADD phabricator-ssh-hook.sh /app/code/phabricator-ssh-hook.sh
ADD preamble.php /app/code/phabricator/support/preamble.php

ADD start.sh /app/code/start.sh
ADD db_seed.sql /app/code/db_seed.sql

CMD [ "/app/code/start.sh" ]

