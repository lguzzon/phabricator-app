Phabricator, an open source, software development platform

This app packages 2016 Week 36.

### Applications

Phabricator includes applications for: 

* reviewing code before it hits master;
* auditing code after it hits master;
* hosting Git/Hg/SVN repositories;
* tracking bugs or "features";
* counting down to HL3;
* expounding liberal tomes of text;
* nit picking pixels with designers;
* "project" "manage" "ment";
* hiding stuff from coworkers; and
* also other random things, like
* memes, badges, and tokens.

### Serious Tools

Phabricator applications are serious, heavy-duty tools that scale to organizations with thousands of employees.

* Infrastructure is serious, scalable and secure.
* Performance is a priority.
* Even has a serious business mode, for the most serious businesses.
* See how Phabricator compares to other serious businesses.

