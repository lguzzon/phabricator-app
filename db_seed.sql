-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: mysql    Database: 
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `dbprefixgoeshere_almanac`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_almanac` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_almanac`;

--
-- Table structure for table `almanac_binding`
--

DROP TABLE IF EXISTS `almanac_binding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_binding` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `servicePHID` varbinary(64) NOT NULL,
  `devicePHID` varbinary(64) NOT NULL,
  `interfacePHID` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_service` (`servicePHID`,`interfacePHID`),
  KEY `key_device` (`devicePHID`),
  KEY `key_interface` (`interfacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_binding`
--

LOCK TABLES `almanac_binding` WRITE;
/*!40000 ALTER TABLE `almanac_binding` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_binding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_bindingtransaction`
--

DROP TABLE IF EXISTS `almanac_bindingtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_bindingtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_bindingtransaction`
--

LOCK TABLES `almanac_bindingtransaction` WRITE;
/*!40000 ALTER TABLE `almanac_bindingtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_bindingtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_device`
--

DROP TABLE IF EXISTS `almanac_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_device` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `nameIndex` binary(12) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `isBoundToClusterService` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_name` (`nameIndex`),
  KEY `key_nametext` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_device`
--

LOCK TABLES `almanac_device` WRITE;
/*!40000 ALTER TABLE `almanac_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_devicename_ngrams`
--

DROP TABLE IF EXISTS `almanac_devicename_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_devicename_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_devicename_ngrams`
--

LOCK TABLES `almanac_devicename_ngrams` WRITE;
/*!40000 ALTER TABLE `almanac_devicename_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_devicename_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_devicetransaction`
--

DROP TABLE IF EXISTS `almanac_devicetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_devicetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_devicetransaction`
--

LOCK TABLES `almanac_devicetransaction` WRITE;
/*!40000 ALTER TABLE `almanac_devicetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_devicetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_interface`
--

DROP TABLE IF EXISTS `almanac_interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_interface` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `devicePHID` varbinary(64) NOT NULL,
  `networkPHID` varbinary(64) NOT NULL,
  `address` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `port` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_location` (`networkPHID`,`address`,`port`),
  KEY `key_device` (`devicePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_interface`
--

LOCK TABLES `almanac_interface` WRITE;
/*!40000 ALTER TABLE `almanac_interface` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_namespace`
--

DROP TABLE IF EXISTS `almanac_namespace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_namespace` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `nameIndex` binary(12) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_nameindex` (`nameIndex`),
  KEY `key_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_namespace`
--

LOCK TABLES `almanac_namespace` WRITE;
/*!40000 ALTER TABLE `almanac_namespace` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_namespace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_namespacename_ngrams`
--

DROP TABLE IF EXISTS `almanac_namespacename_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_namespacename_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_namespacename_ngrams`
--

LOCK TABLES `almanac_namespacename_ngrams` WRITE;
/*!40000 ALTER TABLE `almanac_namespacename_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_namespacename_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_namespacetransaction`
--

DROP TABLE IF EXISTS `almanac_namespacetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_namespacetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_namespacetransaction`
--

LOCK TABLES `almanac_namespacetransaction` WRITE;
/*!40000 ALTER TABLE `almanac_namespacetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_namespacetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_network`
--

DROP TABLE IF EXISTS `almanac_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_network` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_network`
--

LOCK TABLES `almanac_network` WRITE;
/*!40000 ALTER TABLE `almanac_network` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_networkname_ngrams`
--

DROP TABLE IF EXISTS `almanac_networkname_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_networkname_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_networkname_ngrams`
--

LOCK TABLES `almanac_networkname_ngrams` WRITE;
/*!40000 ALTER TABLE `almanac_networkname_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_networkname_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_networktransaction`
--

DROP TABLE IF EXISTS `almanac_networktransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_networktransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_networktransaction`
--

LOCK TABLES `almanac_networktransaction` WRITE;
/*!40000 ALTER TABLE `almanac_networktransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_networktransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_property`
--

DROP TABLE IF EXISTS `almanac_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_property` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `fieldIndex` binary(12) NOT NULL,
  `fieldName` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `fieldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`fieldIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_property`
--

LOCK TABLES `almanac_property` WRITE;
/*!40000 ALTER TABLE `almanac_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_service`
--

DROP TABLE IF EXISTS `almanac_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `nameIndex` binary(12) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `serviceType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_name` (`nameIndex`),
  KEY `key_nametext` (`name`),
  KEY `key_servicetype` (`serviceType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_service`
--

LOCK TABLES `almanac_service` WRITE;
/*!40000 ALTER TABLE `almanac_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_servicename_ngrams`
--

DROP TABLE IF EXISTS `almanac_servicename_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_servicename_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_servicename_ngrams`
--

LOCK TABLES `almanac_servicename_ngrams` WRITE;
/*!40000 ALTER TABLE `almanac_servicename_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_servicename_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almanac_servicetransaction`
--

DROP TABLE IF EXISTS `almanac_servicetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almanac_servicetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almanac_servicetransaction`
--

LOCK TABLES `almanac_servicetransaction` WRITE;
/*!40000 ALTER TABLE `almanac_servicetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `almanac_servicetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_audit`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_audit` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_audit`;

--
-- Table structure for table `audit_transaction`
--

DROP TABLE IF EXISTS `audit_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_transaction`
--

LOCK TABLES `audit_transaction` WRITE;
/*!40000 ALTER TABLE `audit_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_transaction_comment`
--

DROP TABLE IF EXISTS `audit_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `commitPHID` varbinary(64) DEFAULT NULL,
  `pathID` int(10) unsigned DEFAULT NULL,
  `isNewFile` tinyint(1) NOT NULL,
  `lineNumber` int(10) unsigned NOT NULL,
  `lineLength` int(10) unsigned NOT NULL,
  `fixedState` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `hasReplies` tinyint(1) NOT NULL,
  `replyToCommentPHID` varbinary(64) DEFAULT NULL,
  `legacyCommentID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`),
  KEY `key_path` (`pathID`),
  KEY `key_draft` (`authorPHID`,`transactionPHID`),
  KEY `key_commit` (`commitPHID`),
  KEY `key_legacy` (`legacyCommentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_transaction_comment`
--

LOCK TABLES `audit_transaction_comment` WRITE;
/*!40000 ALTER TABLE `audit_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_auth`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_auth` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_auth`;

--
-- Table structure for table `auth_factorconfig`
--

DROP TABLE IF EXISTS `auth_factorconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_factorconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `userPHID` varbinary(64) NOT NULL,
  `factorKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `factorName` longtext COLLATE utf8mb4_bin NOT NULL,
  `factorSecret` longtext COLLATE utf8mb4_bin NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_user` (`userPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_factorconfig`
--

LOCK TABLES `auth_factorconfig` WRITE;
/*!40000 ALTER TABLE `auth_factorconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_factorconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_providerconfig`
--

DROP TABLE IF EXISTS `auth_providerconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_providerconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `providerClass` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `providerType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `providerDomain` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `shouldAllowLogin` tinyint(1) NOT NULL,
  `shouldAllowRegistration` tinyint(1) NOT NULL,
  `shouldAllowLink` tinyint(1) NOT NULL,
  `shouldAllowUnlink` tinyint(1) NOT NULL,
  `shouldTrustEmails` tinyint(1) NOT NULL DEFAULT '0',
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `shouldAutoLogin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_provider` (`providerType`,`providerDomain`),
  KEY `key_class` (`providerClass`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_providerconfig`
--

LOCK TABLES `auth_providerconfig` WRITE;
/*!40000 ALTER TABLE `auth_providerconfig` DISABLE KEYS */;
INSERT INTO `auth_providerconfig` VALUES (1,'PHID-AUTH-bakr7fx2mwkl6xno6n4z','PhabricatorPasswordAuthProvider','password','self',1,1,1,1,1,0,'[]',1478565136,1478565136,0),(2,'PHID-AUTH-cayoqyeiyw2m7zfvclvs','PhabricatorLDAPAuthProvider','ldap','self',1,1,1,1,1,1,'{\"ldap:port\":\"389\",\"ldap:version\":\"3\",\"ldap:host\":\"\",\"ldap:dn\":\"\",\"ldap:search-attribute\":\"\",\"ldap:anoynmous-username\":\"\",\"ldap:anonymous-password\":\"\",\"ldap:username-attribute\":\"\",\"ldap:realname-attributes\":[],\"ldap:activedirectory-domain\":\"\"}',1478565169,1478565169,0);
/*!40000 ALTER TABLE `auth_providerconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_providerconfigtransaction`
--

DROP TABLE IF EXISTS `auth_providerconfigtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_providerconfigtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_providerconfigtransaction`
--

LOCK TABLES `auth_providerconfigtransaction` WRITE;
/*!40000 ALTER TABLE `auth_providerconfigtransaction` DISABLE KEYS */;
INSERT INTO `auth_providerconfigtransaction` VALUES (1,'PHID-XACT-AUTH-adgj3m5crwndvby','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:trustEmails','0','1','[]','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(2,'PHID-XACT-AUTH-usn5ry3jzrc46h3','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:host\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(3,'PHID-XACT-AUTH-g32lp472vv6hfld','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','389','\"389\"','{\"auth:property\":\"ldap:port\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(4,'PHID-XACT-AUTH-f5lwkux6gtuoajy','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:dn\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(5,'PHID-XACT-AUTH-fjdp2gvxtrshsch','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:search-attribute\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(6,'PHID-XACT-AUTH-w43bdbt776llkzr','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:anoynmous-username\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(7,'PHID-XACT-AUTH-fgqhlor73wmxgey','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:anonymous-password\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(8,'PHID-XACT-AUTH-i7r3l4fheu2u4bx','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:username-attribute\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(9,'PHID-XACT-AUTH-6dazcebbnavyhgx','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','[]','{\"auth:property\":\"ldap:realname-attributes\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(10,'PHID-XACT-AUTH-42d7yob3y3n5eup','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','3','\"3\"','{\"auth:property\":\"ldap:version\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169),(11,'PHID-XACT-AUTH-pda5t6mwxx57nfm','PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-AUTH-cayoqyeiyw2m7zfvclvs','public','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,0,'config:property','null','\"\"','{\"auth:property\":\"ldap:activedirectory-domain\"}','{\"source\":\"web\",\"params\":[]}',1478565169,1478565169);
/*!40000 ALTER TABLE `auth_providerconfigtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_sshkey`
--

DROP TABLE IF EXISTS `auth_sshkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_sshkey` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `keyType` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `keyBody` longtext COLLATE utf8mb4_bin NOT NULL,
  `keyComment` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `keyIndex` binary(12) NOT NULL,
  `isTrusted` tinyint(1) NOT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_activeunique` (`keyIndex`,`isActive`),
  KEY `key_object` (`objectPHID`),
  KEY `key_active` (`isActive`,`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_sshkey`
--

LOCK TABLES `auth_sshkey` WRITE;
/*!40000 ALTER TABLE `auth_sshkey` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_sshkey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_sshkeytransaction`
--

DROP TABLE IF EXISTS `auth_sshkeytransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_sshkeytransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_sshkeytransaction`
--

LOCK TABLES `auth_sshkeytransaction` WRITE;
/*!40000 ALTER TABLE `auth_sshkeytransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_sshkeytransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_temporarytoken`
--

DROP TABLE IF EXISTS `auth_temporarytoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_temporarytoken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tokenResource` varbinary(64) NOT NULL,
  `tokenType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `tokenExpires` int(10) unsigned NOT NULL,
  `tokenCode` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `userPHID` varbinary(64) DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_token` (`tokenResource`,`tokenType`,`tokenCode`),
  KEY `key_expires` (`tokenExpires`),
  KEY `key_user` (`userPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_temporarytoken`
--

LOCK TABLES `auth_temporarytoken` WRITE;
/*!40000 ALTER TABLE `auth_temporarytoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_temporarytoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_badges`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_badges` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_badges`;

--
-- Table structure for table `badges_award`
--

DROP TABLE IF EXISTS `badges_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges_award` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `badgePHID` varbinary(64) NOT NULL,
  `recipientPHID` varbinary(64) NOT NULL,
  `awarderPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_badge` (`badgePHID`,`recipientPHID`),
  KEY `key_recipient` (`recipientPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges_award`
--

LOCK TABLES `badges_award` WRITE;
/*!40000 ALTER TABLE `badges_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `badges_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badges_badge`
--

DROP TABLE IF EXISTS `badges_badge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges_badge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `flavor` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `quality` int(10) unsigned NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `creatorPHID` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_creator` (`creatorPHID`,`dateModified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges_badge`
--

LOCK TABLES `badges_badge` WRITE;
/*!40000 ALTER TABLE `badges_badge` DISABLE KEYS */;
/*!40000 ALTER TABLE `badges_badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badges_badgename_ngrams`
--

DROP TABLE IF EXISTS `badges_badgename_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges_badgename_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges_badgename_ngrams`
--

LOCK TABLES `badges_badgename_ngrams` WRITE;
/*!40000 ALTER TABLE `badges_badgename_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `badges_badgename_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badges_transaction`
--

DROP TABLE IF EXISTS `badges_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges_transaction`
--

LOCK TABLES `badges_transaction` WRITE;
/*!40000 ALTER TABLE `badges_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `badges_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badges_transaction_comment`
--

DROP TABLE IF EXISTS `badges_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges_transaction_comment`
--

LOCK TABLES `badges_transaction_comment` WRITE;
/*!40000 ALTER TABLE `badges_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `badges_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_cache`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_cache` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_cache`;

--
-- Table structure for table `cache_general`
--

DROP TABLE IF EXISTS `cache_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_general` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cacheKeyHash` binary(12) NOT NULL,
  `cacheKey` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `cacheFormat` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `cacheData` longblob NOT NULL,
  `cacheCreated` int(10) unsigned NOT NULL,
  `cacheExpires` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_cacheKeyHash` (`cacheKeyHash`),
  KEY `key_cacheCreated` (`cacheCreated`),
  KEY `key_ttl` (`cacheExpires`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_general`
--

LOCK TABLES `cache_general` WRITE;
/*!40000 ALTER TABLE `cache_general` DISABLE KEYS */;
INSERT INTO `cache_general` VALUES (1,'K3VntSub8LL2','phabricator.setup.issue-keys','raw','[\"extension.apcu\"]',1478565143,NULL),(6,'hC_tvsuw87Qa','phabricator:celerity:/res/defaultX/phabricator/46d588e4/core.pkg.css;def-_jdxg_.SlMso','deflate','\��k��8�(�~_�b�\�\�\�]\�cL\�6�3ֹ\�\���\�}V\�\�e���\r�9����\0�\0�=FU�\�\'n�k\�iC*��R�T*�)\�\��]R/\��.Y\��~\�\�u\�]S\�7���y\�d\�7\��&��ɽ\r�}����*k�)[\�\�wA\�\�\�\�[�y��w\�lY�\�s\��z�I�:��M\�\rI6~R�[��ɟ\�\�Q\�4�h\�?\�\�\�<�]�w�:u�|zX\0^C6\�w�?Z�~xU@��K�>@b.��\�ևi\�5�\�P\�k�\�\'WYU6��\�w\�޳����z�Gx�ͧ%߬^S�\�OM^[��f��ٰT�\�\�7�~�=\�g.\�C�IS\�\���&�.���~ҙ\�\�\�\�cY\�\�i�\�\�\�׷�,��d\���x|\�\�I��t�OC\�.2�\�\'�忿{�ߋ�\�k\�&0?L\�P׿$ՐEغ��e�\n5\�\���\�~�TI�)\�Y�%�\�N1��A��\�5>\�/\�a�Zvi\�\�^ڞM\�\�_:蝵�_?\�?M\�\0C�;�\�\r�wZ�6�\�\���\�翜.]�\�\�\��������\�	���\��\�\�ԃ���\�6Iu��\�k?��?�Jgr\��K<&�y\�Ճ%��?7��\�pI\�vـ\��l(�\�Q\�\�?m����p6\�G\�Tmеz�}�����\�o�{�ᷫ*Y��|\�8l\0���\�Z�\�l�\�s\�?�9X����`�\�ۏ\�\�~�1:|\\T��u��\�Q�~�kОO\0fɠ3��_�h�\���\���\�\�\�ӥ��\�-������\Z���a\�\�Pl\�o���?\�\�,�]=��\\�����\�\�\0\�c��U�\�\�\�?.�yF��*%��\�m�\�ϖ\�b���\�\�N�\�$*W@�^��\�\��O\":i�\"8��݁P�r�t(ݬ�O\����xaY�!\0�\�\" V@9��E�\�H&4Pl�-g˿*\�\�\�O\�\�ۇ�\�%\�\�\0��f(�\�f�w&�}�\��\�\�4t\nU2P8�\�& \�8I��\�N�:oHk]<�\�\�D`�#�\�\�H�Ee\�̼`\0)e\�\�o�[C֎}i銢\�\�.0M\��ѝ�rX�\�e��x�C�D�\�g�{ٖ;{�(�\�~�\�y\�\�\�\0L\�z-\0�\�H��ߋ��\�`	\�!�]S���|\�\�JD}ۍ�`\n\\�\�l���,��`��o|H�ݡ�X��|��6A<�\�í�2i�jZD4\�{�t\�U\��\�����)\�f��o��pXj#�@\�\�A�\�\r\�\'0*@�>~<;\�j�\�Hf\\)\�c\�#�Ն\�^�G`�`pm�\�M `\�\�ǞWxa\�\�;ț@J��f�\��\�5��\� e�M���\�1���*k�hR�a\0`�X݃�j:u��n\��	�l\�\���@�(�|�#\�\\\�<��\�?�@ٺ\0\�\0q!v\��gЀ=\�A\�\�+��!6�;\\{�-\�<+\Z�?2�1\�-u\r�C�\�\�ݨ@oД�,P\�!0T\�@\�tp$M\0�\�#\�a\0cZ\�\�Df��%/�\�e�\�&0\�igV����Q\�\�Tr\�<k�#cO\�	\�\�gk\Z�\�?�w�����\�	�\'�\�\�\���H\�\�m�I�3�\�Լ\'�\�NF��%\r\�0?hX��5\�¤!\�#o����p)}&�,����V\�\�8gW\��f1(`\�`��;\�6��X�5\r0�,l�g�\���k�z$tQ�&+9>OQ�;��w�����o\�3 }\�\�6�:\�\����\'�ʛ@M@ g\�\�d\rnGᶵs\�S3�0��m8i���\�\��\�?\�h\�\���Ӧ�\�F\�\�}\�\�4\�\�ޗ�@\�I\�\�\�x���`���,\�U\�/��\�kA�\�\�8J( �P\��^�޼~	[1�	WO 0\'�����7g��\�\�U\�n\�\�\\\����.E��M�\�\����`\�]�\r:d%\Z��`C�o	E\�\�mWho\�\�\�\�j�G�Q�4�MX�T@\�a��D��\�꒞�i\�\Z��w\�\��7S�q*p^l�=�\�!�3�,��\�[��_\�~&S*eK\�n\�\�0r\�Dnp\���?	T\��\��5~\0;���\��oQ���<�\�偎O��`\���-� h\\�M\nYr\�w|��\�\�5��\��\�.\�</�\�L���N7�\�,�\�jNd?h�\ZL�\� \�V���B��\���`��\�5 ��\�n��3K�\�@ϗ���\�X{-��*\r-G@�; \��l>���\�	\'{�}\�D��h��S\�G��x\������e`\�p&6��6=R�\�Ӌ=���,�eY�\�\r�@�\���H�|��}�\Z\�\�3���J�\�r\�+y�c\�\n]^\r��?KX}�B�]$�\�ؓ�%\�\"�\��\�\�+�\�MR!X\�̉vIny��W�&7\� \�\��\�!C\��(|��*Z*?�\\\�y�n	j\�l��\��\r�\�Ю�\��\�I\�|�\�\�@hk\�`��\�h�|�T2p\�!n.�(�r�6\�2�\r5\�cg	\��B\�r\�\�Y\���\�\\\�f������\�\�\�1���\�xL\�gDL����a���\r��\��h���Á�%F.d���p\�7����\�h�\\\�J\\T�C���i7�\ro�O[��t��hz��t�H�x�\�\�*a`�qb}���\��i\��w5و]9zB�\��.\�7�*j�]\�`T\�\�]\���ݥ\���=z��$ir�ּO���D \��gd����\r���\�p:8�!�t>mlrC\�\"\�\�&R\�\�t��m6�dR��s\�\"\�#@>���@Q�\�\�b�2\'Q�i0�\�\�<\�\��e\��I\�\n=\�\�Z\�\�ˆ\�c��ܮCCY`\n�B�X�gm\'\�\�qr�\�\��a/�1|�Y\�#J ���\ZG�%\�^��Ɏ\�\�I���lxӆ��l�Hy�Yd\\vvh\�9`�v�\ZoZ\�6��8\�\�����в�G�]��U�\Z�@W�O|::\�N�L�\�hi�\���z��₪`}ꮝ��\�ʱ~�\�����\�+\�\'\�\�P�\�;;~��(��\�u2&���)KptӇ\�FM6M4b�Et��\\ew cxOx�@D\�	�\�4l�g�\�\��?\�\�^A�iz\�8��x����j@\��0�I\�G$Q &�\�r\�)�e�\Zf`a�u\�Ogɍ�~�\nGS�v5Ѐ/����T��\�V-g���=�6V���\�sA�V�U�s��my,�\0�Ri\�\�8DDh\�#U�\�r7�r�z�TE.`\�y㇏!`\�c\�,�b���_\�B\�i	�X�E\rS�+l1�\�p\�}_@�\�R,ņ:\�,Ee\n�l\0�mg�j\�0l�.�����\�VҋQ�\���lv��e6�\n�C\�\�P}�i6-WK��ϸ�Ó\�F!�?�\�\���\�|�\�\�\�\�CL�\�^<�׺n�-��z\�*\�O\�^�s\�_\�f\�>\n�p=7dd�7����e�\�\���B���危}�H�2I\�ֱ༹{uC=\�_/`@\�-�\�^�T�\�.��Ų\�u�&\�\��!\�R/7Ǒ\�C\�v�*�N��/a�\�;,�`�X\�\�j�v|���\�WOo��(q��w�I8K1P�U��\�R��\�G-<��;\�\�\0���b�����1W\�ڂ��Y\� \�ry���c�\"\�qn>�«\Z/��j\�?���k8`1�l�\0^0\�AwFЃG�����\r\�u\��?Ωg���\�i\�\�\��!��\���>�ޔs\�:+�GW�]W�\"��7�n�S~e\�_�rq	��Lk�,\��ڼ1�̊��\�}5,�V���\Z�Z¼vX\�Z\�r>ʊ��\r`�\�5�֦r都\��\�\�\�*\�}iR-3�\�j�\�U�ߤ�Q*�X���^L�\�\�EY+\�jw�X�j�\�,��[�1L\�\�\�\�u���E�Ħ�r\�O��\�rX]�\r.7/틩�\�0\�^�-��4\�k,�]a5���\�I:�\�iP�얭�5V9T�\�Y���n\�h\�_ިIjX,�\�\�f�z�\�\�I���8�\���v��l6�󷘾G\��/\��\�h�7jJo��\�\����l\�\�\�hχ�kW\�qn<U��\�\�\�l\�v�\�0m�+u�.6�E�Q�\�T�-��\�\�|\�\�O��\�tԪ�M��)+ep\��|�2NϭA�2b��:ӯ\�lod\r�\�ڭ\'+\�T</\�\�u_k+[S��E�-*i���(�\�\�<��j��\��\�h��\Z���(�;h��k�A��쏧�V\�WfJV\n�BS\�N�|Ƭ�)�ԙ/\�?\�\�\����OQב��GР\�\�\���f`B�\�\\��[�\n��9\�cQ�#.\��R�N\�IMP]��7�h�\�\�\�Z�=?f\�&&ВMR5v��e+�GF�iͫU��v\�4M\��|\��=�\�9`�\�V��\�\�\\�G�쁎�L�G |\�9$�w=�cZ�\���1ߜ�e�\�g0�s,\�3�\'��71\'\�i���\�8�B�\�AG]F�\�~��\�w\�\�\'�\�ӎ{\�kϺ,��\�wO���6�\�\�ǾqS&k\�O�\�.��q-k��и\�\�Ţ:,��DN)�V���\���W�L�\�q�C?o�6\���b\�ݍ�5\�p\�#�/\�g`c\�\�N\�A�^kgs!\�\�V{\�d�l�?\�?wprT\�\�!س�\�t\�\��0\�>\�U�TԦ\"|�\��sgk�\�\�q�\�,��\��\"oC�\�/�Èsa\��\�J�\�Uk	f\�L�A4�\�gC�2\\�C\�T2L\�\��\�a\�ˎ�\�̳\���g�p\�!��sU�M��.��\�u[�\ng�4Wm+�b�t.ҝ\�eٱ\��d�\��@w�G\�0=��nq\\nnکluI��n�*�j`��\nL1e��\�j��P�\�x��JQ�^\�)�\���+\�땅ީ\n��Z\Z\�+��2=\�n*�:\�\�r�J]\�\��˩��(m~�hv�\�\��\�z�f���|pe\�6�ު\�\�\�q\�T\�bg\�ϲ\�ã�8�ҥ[����*%\�\�\�eW\�i��>e\�Aj\��\�H�\�\�m��SУ%���zl��X�\�\�\��`U�Mv\��p_.����\��>\\M\�e�_�?�s�\��	tŨ�\�u˛<�	\�/\��\�9\�\"\�$\�\�\��\�wK��!\�\�\0\�\�;��q~/�\�O\�jZy�b\Z0\��u3}����/G׳�\�؈	\��U����A���}\�\�\�a\�~�O\��5�k�A\�i��x\'�\�vɀ3�ѐ?�	\�5J�& ʸ�c$DEyQ�f\�@+�\�J\�Cǐ�\�\�O\�\�HT\�ގQ�\\bzJ\�^�ظ�\��y4i�y�\0і�\�\�\�\�\�\�\�}�\��\����\�q+l<��+�b9k�k��h9\�H\�[����xV�-X%A\��&O`��x\�%\�JTTް�(TL�1\�5���\"\�-�,Ɗ��j��\�ă�i� k��s��g�D\�3�L+�\r��\"\�\�.o 3l)\�r\�n\��\�9�\�\�nx�\�\����&d\0�\��\�\�\�R�\�l{���R\�^<@*\�\�\��F\�{D��߽9��A`$b?����	�\�v�<B]�\�\�O4\���п��b_\�ǎ\r��汘\�F�\�BHǠ�[\�o\�c�>�J�\�3\\\"�)0BtV�&\�\�&|#U��L\�\�\�	�\"��l\�/1��ıMy��\��\�P�\�}��o^H�/��m�\�	\�^^�\�hސOgՐ�O�o�;\�a\�\��iG\�R���?���g`G\�4�`1�\�}\�\�K�\"\�%\�^�%�7/�I�\�1h\�\�l��c��i��\�:Sc\�k�\�D����\�\�\Z\�L����\�ZVd�@�I\�48U�O�D�a\�,\�qE�(D\�0�	5D\�N\0\Zoo\�=0�\�\�\"w\�X\�]a�\��U|\�\�!t\�4\�\�Z\�B�\�V�\�\�\n\'@\�g\�1��\�\�*�f��.G.�$\�9D�\�D\�K\�j�nb��=]$\����ˉ\�5	Ep��xD��}v#���eAZ�Q�d\��ܵ��\�yMG�\�C��J���W��\��\�J=\��@\�j��sM�)\�\�Ӭ�s\�MH;,�#O\�$�?Wn�1\�\�\�APwc�\'��Y\���\�ŷ��(\�/V���x�.���K\�˂��_oF�\0\�E���\�P\�-�I\"d�\�x7����@�\�>\��jy�\�`�ЭߐFeW�L�!��t9�:�=��\�<�\�{s\�C&v�8e�,[�`mu��.��O��5m��Q!�WF�\�KH �+;ȧ�\�\n������p\��\�mC׿,\\p�\��\r�\Z$YfJ�Z\�dۮ\�J}\�9�	V��D\n+޸���\"�k*�\�\�x�ɕxs#z�5��l4�\�A�@U�XyY��{�\�a�4�\r�m��\\[�^\'y$t�9|L�Ym\�\�\r\�\\�s�8�\Zn��q�ȿ΅�޾�$\�=凓��6��k���I�Ri{\� \�-\�>𝒭��z$��\�D.\"\"B�_\�hd�i\�y�By�e��Ӿ\r\\�\�Gt�\�\�S�Lk\\G\���\�\'oq\��\�<v~n\���a�.�v��[\�H�\��\�Qx@�ᣤ�KY\�y)\�ۏ=(���Ց�8�ΉC\�-pN\�\�P�ZS�\�V$B-\�\�xmc�sR>�k@T�$�3Ȣ�^gC�\�}���6�C3\\Y\�\�\\\�\�\�\��\�#�\"�\�k�o\��-,\�rN.\�rx\��ld!\�!�v\�v�\�~qQ}�=��C\�4��B�\�#8\�=/5\����g�\�W\��ǯUr�\�X\�>�� \�\�|�\\)\�-^o\��>�J�_b�84\�\�lZ\Z_�\�\��π\nR-U\�\�j��E��+Q�\ZU\����{o��9v)A�R�\�Ձ	\n|�\�;B\�-�b7�p�\�]緰\��\�r3Y\�G\�\��L%��\�^8���as|�\�>\�?\"����G\�A��\��0�)Xxcu��\�]ӝ��\�\�\�����o�\�\�K�?�\�s�.�L[��\\\�o͐\�VT\���\�/�\�\��\r��u\�:\�ل^Rv�w\�5��l�\�?%v\�\r�A!B�`_\�r,\�ަl\�\�\�@g����/#�_��\�&�\�\n\�}�F���<|\��eD��@�6�0z�l�k+9\�Gh��+Ox\�^ze-rj�&GC�]B�E=a>٧_qR\�]��8g�x�\�Z\�ߙ*\�1C�v��\�\�@.A>[�xN�!\�%l\��9\�ۧ\����̏\�=Ȋ\�\�r���@\'�a;~}:�\���\�C�p\�WĶ\"r\n%;g�A\�_6\��t�\��C\"\"\r���8^�I��d\�O\'q�8Ro\�rA��A�\�J��j\�߭\riLx \0T��Q�b\Z�ƿǽp��ž��\�_\�\�a)�X,�\���w0$4\�w�@\�\�F���m�)!{	�\�m՘���vh08\�I��BNR/\�b}j^��\'�\�x��`k\�\�J[|M俷9H��ݩ�� O$c�\�\�.�6��Ǭy\�r��}K��ۆ�$~��\�+lp\�\�W��$�V�\rIhY�@^�%�?�\�8�;~�J.�$��N���M8��[��\rFȑ:l>�\�\�KrF\�\�\r\����\ZɃ(0f?\�\�M^�$\�%A�\'C\���\�v\�\�U\���/]����_\�\���\�\�N�\�\�\�D��X�g	�]|�7�\�\"\�-͘��Zj�\�\�9B�K|��\�\���F�`[\�)\��lG�\�;v����\�ؠ\�\�?\��\�D\�:\�\��0]��[x�KD\�.��vG�L�v-_B\�Nǭ#��<�q��E�\�I+\'8E���-�\�!�\�\�\��ү���M`S�\�\�H�\�s�\�C���\\�}�\�\r\��Z��.\�Cgg/\�V\�8\"6`�\�YA\��\�uAf֢�����+	�\�|2<����\�ѳ���	\���}�\rE�\�ơ>\�\�0\�㊑��[�j\��x�\�\�\��\�\�\�`\�;�ngα�ݐ^.a	Fɗp�\�\�و��}x���h\�D�p\�\�p6��\��\�|F;\nwֲ-Uq\�n~8%\�\�k\�m���S_T�P\'�::�\'�\�\�~[n\��$�w@\�~\��\�\�Aw`�\�h��*�\�ٗɣW��Poa�ꐳ\n\�\�%r7�`\�\��\�{\�\\\�\�0]��C�肋�\�ᵗ��%�`[)\��\��q\�(�X��\�/��h/[�*ƚ�;<7�ƃ���\��; q\�\"��\��o�3���3��h\�����Q1!\0{l�v��.�\�xIs\�\�\�F5�Ƹ\�p%��~<��\nc>�^��\0u�8pd�������ԮՒh\�\n���\�[\�4�\��O�s��I��_s���E\�=���Y1\��(�\�\�\�(�cT>/d��\�_��ד��=?\�\�`tD\"�\���\�\�32+saz2��OABR<\'D#?������\Z\�\�\�\'31>�(�)r�])���W�9��\���\n�9Ý��\'\�*\�\�׾l&�\�\'�$6O�!-\�Ć��\�\�%rY>\�Y��\�\0*\�:f�@�\�¯q3�\���;9\�QyA+D\�C�R\��\�\�̩�7���\�0{�p�\�\�v�r\�\�\�Q5�\�x�N��q�\�e\"2\��pr\�IJ@P�c�\�q�����rP\�~X��?i\�p�\�!����*\�{�\�9�\�CI��\�\�\roH��@`���\�a�\�3`� SZ˴\\�[�k+��/2��\r.px�������	���k\�\��F�-��׀X\'M\�\�\�\�\�!�i�\�\�tZ\n]\r�&\�8��Re\�]��t\�RH��:�qD�%q�\�B�ƹ���to\rc��!m8\�U����\�/h(��\nn\�1]yf�&F�N\Zh�8�v�]�GХ?r\��lC,�f\���\�W1�\�J��\�ߎj\�$\�s|�i����\�r��\"U�\�\�\�?V\�n6i�k.dy�j\�|%��h\�.>���؎���7rQ>.\�>���j�%8��\\+�fU�\�a����Oc \n����t		j\�o_�\�?t��΄\"K\��\�@�V\�\�\0�@d�P`�\�\�\�nAn�\�\'D}�瀜�r%�|�y\�3�{8�.�s99\����Pi����=։-�^A���\�)ОSP\�7׉���R�l��\Z�D\�\�\�\�\���\�w��l\�b�H)^gB��(S5�WݔyCܸB�\02���\�]#����\���ĸ\�����r���GnE\�h�;���n\�\�y\�\�P���ZL\�!\�ÿ=O@\�Ʌ\n\�4\0Uw\���q1�t\�\�a�&�5XQđ��£�����&\�)HP�p\�B5x)��	U=\�Ȩ\�\�?���qd3���w\��`\�3�&2p/uf\0���\�\rى:�W�#K\��_0�\�\r\��\���|@.��)g�;>\\��I\�\�\�?з|��\�\�#j�9�mO��t僦�\��/e�\0\�\0o��	@-\�y\�:�U=�M/5qtE\0� �\�W)}��\�5�\�\�\�k�Bb \�\�\�\ro����n%�����|\�4&�Up�N;Kn�\��)g��\�7\�ɾLl#a\�u\�\�`3\�Ϗ��\�:\�+���;E\�]\���\�e\��gg�@�\�#D�t\'H1@\�qw@L\�*c\��C�\�\�s��u\\�g�`�~\�`IBXmߪ\�8�\�>ibW�M\�Z4l\�:?��s�\"nXAo}lu��-\�a)\�/<\r\�\n���ʽ�\�c�d�gɆܒ\�f�o>��W���O\�3�r�y:Q\�\����@����\�\�\�n R\�l�\\����ƒ�d�\�\�I\�dQ�L�U��H\�G�X�\�@\�$�8��x\�h}\����]̾�e��OM\\\n0�%\�jOp�86�`{�\�m\�r���v�X$\�?�G=�^\��\�H\�ts�\\�AL�0�W닔�L\�@X;\�\�a�ҽ.\�J�n�XVS\���֙��!(��l�5\�ΨWtC���\�c\�yq\��8c�\��8\�Lv*�\�M=ÝI�m�\�\�\�俄�M�`ǻ�f�!\�&!\�F�\�8v\��\\�Ȉ���K���zu:\�0Vg|\���\�\�:M&N4���\�\�ކ{F\�\���	�\�A�j\�)���Yi��Ą\�t>���\0�\�9F0�>HT�R�\�CC>jw\'�I!�۷��hu\�\�\�\�<�\�+1�	�\'B�ᡛ&DRԽ\�\�4�\�ݍ2��\�Ra\�xU�?C\'\�\��\�\�{O\�g\�x~�q�a0\0=�\�NJ�\�C�\�F@�ߍ�\�7\�N�@\�\�yڶ��3j�Yg\�	`#�<�DT��|D�!���)���;\�W\�>G\�J�k��I[�qz%x\n�٘l\�Y�\�\��b{Մ77<�#;���em\�I\���\�\�Leg\�\�y�&���畽�?�P\0\�<O�\�\��9��/�\�\�I�W\�8���z�kZd���\�J\�\�:#�Y�P�Z�t�ᅮ\�~\�_7?�Q`�U\�\�bb\�\�.K��??��\�.\�6\���\� ��-]D�\���ptrNp�>9\�\�ȏ~��-�\��a\r�)֙�\"-\��\�2�\�.|ӌ\�\�#Fn�&\��(�FS\�M�4�DC(j�\�{(k�z4U�p�>�ny/�ݤq\�#\�\���\�F��u̇!kbbA�G]j	�\�Q\�8U:X=~Y\�ÿ9�q�7҉�\�\�WC\'�Xqͥ�o�SQV\�\�t���*]�\�\�v�\�&\�i3/\���m9\��G�|�M��r^��\���\�V\�lm8-\�b�R_,\�1\�-ۭ�~�j�\�\�o�mU\�l\�_��\��v\�nl&s\�\��\�j�{h�\�\��02�L㲚\Z\�hHV��\�ZUʚ�m�z�\�~լl�\��د\�s\�eH��sk��\�\���C+sKUS�ܲ@��̵�kY�\�W\�l\\^�4VY\�4\�>\�`?X\�ն\�,N3г�evt;\�z\�8(\����\�7�,ϖ�\�,}�;x\\/�-{\�\�B�^\�\�\�{zǕ+\�If_)5S��X\�f�`L\�\�y�xnݣٻ\�z�������\��\��V���Te�/P�|��0Kܲ�\�6��L�V�.Ee�08>_�wz\n%d\�-�<5}��\Zʕ^�(o��.5[�J����Υ}jؔ{Y~D)���G�lm\�\�神r��J�a{�\�9EӝҀ\�g\�LW�6����S�~�n\�V1\�l>*Te$�j+{+��U�Nw6�\�Z\���mC\�L{��tE/�\��x\\\�O�\�Q��\�,�e\�]v\�\�VS\�\�x_Ժ�\�ꕫooZu4�\'Sn\\�5׽��\�ʒ?vÉ��ݎB�\�l��T̮\Zf��o\��aS�\Ze��֨����\�K�Q�1�\�\�\�\�\�\�yϯ���!��k�$ujk\�q�)�ˍ���z1<�\�⽤����Qʦӥ�R�<\Z�`�\"�����%��\���\0\'�\'\�\��5�@\n�8�\�T\�\�;\\�c]f����Վ�O\�!`�=��+~\�J*nW�n\�^�վA�\�\�z�BL9I\n\�D>��T�\�1\�\�\�Mq\r%\�_��G�S\�D\�\0�b�\"��+\���h@!\� �/�\��\�޼{_\�\0@Y�x��I FIENUpoBO-w~��\�\r�\�A��X�\��\�ϵj \���\�DN�Ro�#@or׼�\�OUz\��L?�7n|C�1/P�\�2J~\�&Q%��g\�\�`^V̑���{���T�\'\nb_��ma\�a�\�\�\r��\�1\�~\�\�O:�e�b�3*zZt!޵\�\�a�-\\9\�\�q\�L&$,�[\�i�ݐ�^\�π=�ak�6~G\�C\�}D�v¥��Yz�!�� .(������\���ΒzC�vG^����\�\�\�2�s�?�벱�\�\�\'���?&��\�\�k4)�$v��.y6Q\�*�oq6�~�N�@�\�M��ݹ����_M��T➝{PkU\�\�H<�0�\�5\�_;\�\�\��qQ\�\�]\�{\�7Ĝ~��u%\��h�sW�\"�h\��\�c��	q?�k_�w��ֻ�aG�y�\�\�@+�\��\�\�fW4\'/~Q�p_~��\�]y=tԭ=/\Z:ᆎl���a\�D5ͷ�0�Ng� \�NȟW���\��\\w�\�\�瑿DM\�\r�:\�\Z[�Q�b�pi���fS\�^\�E]q��(\�R�zGGZ\�\"���I\�D�/�\����mT�Sb��\�C\�\�(��+��\�@�_*w�\�vV	yMAx�h�\�\��>�2&Dv��#)\�\�=\�\"e�G�;��cXB`\��Q_Q\\xrf� E\�!\"!�ԋv�px����\�\�T\�],��7F�0\"xdq�$V$�CB1\���\�IV|�xL�gW9]��$\�\�+Լ\�1(�\�89\�7NT	\�E\�%\�\�\�\��W�	\��c�J��MZ�\�E\�B�\�F��a��[L!	\�h\�I\�N�v:\�5-R�!\\�7��j�\0ʄ��\'��!�\�]�;�{���\Z(Ɖ�!hgr-��$Uщ \�\�`� �-\�t\�<��!�e.\�o\�\��\�n�vx\�E\�i\����\���\�\�\�D6A�6�v�P�썞\�\r;\�0\�s\�P!\��iC�ю�Ȉ<:���k�b�B�9\�h]t�\�>�\�G[�H>�S�I�l\"H�#\�Kq�2���\"��+GqD\��\�5��\�\�g)�\0p��)\�h���Q�\�\"���\�~}���\Z}\�>G\��}�(\�H\�G�\�#�_x�%��.\�GL>���9x;\�L��L��Ԋ<��~=y�\�h�M�u|�YGx\�UbX:C��\�\r\�����X\�V�\���\�ۇ�_�\�K�g�\�#l\�L��.V�\"�\���7��Q�a2\�GB�Qt�M�W�\\����[s��]��7�B.:H�\��o\r�-y\�G.\��ȾY\�*�\�V�d\��k�z� č�vL�Q\�8s\�{�>_ \�\���y�\0?a=3�{�Uv�e\�\���_[�?3>\��\�_��\�(;�O��b�2	1��3�߂V�p?\�c�Q\r�\�10o\�aV��lȞ�\�s$�\�w\�v�%h��\'&S1�\n\�X�p�Y�i腎\�D�\�pLS\��\�\'\�ޫ@\Z��N&IE\0��\�у}\�\�U��t\�֚�\�+��\'h\�$����\��b�\���9j�e�G~*\�\��L�\Z5\�\�Ac�G�(a��\�\�\�S��\���c�\��C(�9�U�\�s� eX*\�>\��\�۴s�.\�Ջ���J\�=\�몛\��f�Ӧ&\�j��\n\�љ\�O�\�f�ϫ\�ݾ���\�\�v\�9�}�\�TZ~�e����y��i�\�à/\�3�L�j����*�\�\�R\����}J]p\�\�$�P��^�\�\�-e\�~\\�&s���\'�x(\�\��\\\�\�a�s�k\�K\�F���C:-wL3_�����\�\�x\\\�[�ha�F��7�csv=\��\Z�7�fn^\�\���%�\�yZ��\�\r�ɤ6�zj�\��\�_4\�\�>Z\�\�V�h,Om��\�T\�\�6��S\�+��\��>hJR�$�,�+ӂ.\\\�\�ގJw�۸~�\���\��)��Rf���\�B�q͋��7W�p\�T5[�T��\�Rr5uꦶ�F��\�U\�1\�\�\�inW�VW�^�kS�r��6�J����\�yI�t\�Je\�>V\�\�d��-Z\�\�\�f\�K\�ri�\�=\�\n���E�:2�[\�T.\�WU͉\��}�5��c\�\\�m�6Y0\�K���\�\�9\�P���\�\�զ\Zߜ�rE\�Z����\�v۷���톲kt\�%�\�$qґ\Z+n�\�\�W�y^\�f\'�V]3\�#j�\n�.7�\�HL\�ԥ04;�c\\O��r��\�!��e~�Y\\oVFM�f�2\�\'F�$W��Q��`��rj�4�vn5�V*��IaX�\�\�r�\�2\��J!]\�gV\�b�\�VM�\�d��ࠍ�F-��:�_\�\�h7����Q鏷�R�;\��HԎzv\�\�0���<KM�\�rxN7ݒr4�\�v\��\�B��4{=>\�ڤ\�\�Bu_,n�\�꾲�\���=Y\�\�Y\�Ζ�\�\��+\�)�p�1}JU�郰�\�ƽɼ\�+���\�NV\����JՂ<�\�\�~ač+\��>M��|)Mn�A�*L��\rG\n�Q�*5ڱ�δ�T�I_�\�*�m2����\�]�f\�x]���\�.���Y\�\�\�\��\�\�[�\".$\�j�\�\rM�O�\�Ti\r�\�G��_�։�?\�\�f1\�Z\�92�~�\�:��ˌ~�s�\�\�Z\�~g,{fѺ�Ӭ��3�\�]�\�a��\Z\�\�\�n\�7�6O\��d\���K�2\�q�ᔝr�^*���R\�?��5�\�y:\�\����\�Ki�̬�r�S\���x/J]ݳ�k\��SK�b��њ�?箻kuXZ�&\�|E\�g\��T>ƺ8��u�r\Z�\n\�\�\�Jo�e�?�\Z\�\�v\�\�Wn�Vc��\r�\�.ב�l&�*\�kYA鵊�G��h��$\nL�-]V�Ao\�[M�ہrU֋ӹp���kvQ\�ow�T��ю�\�P]t\�i\�ј��3�5�\�\�r\�\�n�mkI_\�T\�~Zuf%~Tn��<\�lw��}TF�k�\�?=t�cye}df\�+O�{Fy0b}�IRe{ы\�j�R)R\��:m\�y�g��pPjg{�\�>\��Z�rMsB�\�=6�A\�|\�lU��\�YJ*^\��NK\�\�2\�t\�!\�wk��*\�z��Ր��_?�\�\��$ʕ\�c\�\�Z��ҩqy�\�\�y+7���\�h.VSYj<\�\����\�d\�P3����\�r\�P�tc֗��@4�\�S�0�n�Ǽ\�)\�\�Ԫ[\�\�\�~�mz��u\�_��XK\��\\��Tr|�Lږu�s�Xj��٪�k)��	S������\Z6�-�V�\�\���zT7F��d\�\�֜�J\�vk*Ս�9�\�\�\�\�\�m���9\�\��^j\�i�U�ժ�&]���f�X+��GQ߭2f^ٛ��\�r\�\'�U�Z<�\�\�dv�\�\�<�\�V*��\�_��7�\�a2���j�\�y�\�yu\�>LK����v(�<lg\�>������7G�Jo\08n\�\�\Z\�\�,ݘ�\\4��b�f\�M�\��L\��{�ҽvȹ�ެ.��t\�\�bo���n7�\neqX2\�9���\n7�_\�̢����\�׆�Ҫ�ty[*\�\�m\�\�(�i!\�\�\�\�/�,\�\��&{\�NjKi�d�G]�s�\�=v�\�\�X��\��\�?\���\�\�]&��\�z�q�Z�u[Z͙�ɥ\�Lj�5�ڌ\�\\Wt�*�\�I\�\�W1=\�w�V�ˈ/�\�9��J@.*�N]���\�\�T�B��͸.��N�ѭ㠖H\�l\�_=�\��%\�*�+QfV-�\��Lo�#;�\�O���f�\�n�:ߍζ}��ij�5��,�\�\�q\�\0˃��J\�C�?\�o��bCZ�\�b?���Sa\�\�\�5\�\�y9\�2�L{gWմ9Kgj\�#ۧg�6�hr�r�x�/5s���\�\�\�2\�\�ue\�׫Sk\�)jw����\�N6���j\�Ҙ]pܖ�\"�\�G��\�M\�貛�E�J��I\�t\'+��:j2\�r#�\��\�وwV/�����\�k\��0]T�\�\��Qe�n6\�ҿ��g����a\�Oc�`\�+��\�Tn?kޛ\���\�>�k\�\�\�\0�\0)XқJf$\��ֽ\'�\�Z}\�6ձ5fv\�\�p\�\�惱Bk\�0�wf\�o\�Vָ�\�K_8�\�\�\�v�\�s��\�d�3gv.}\�\�\�\�M4*ͻ�i�\�ck�{[\�\�,uN�R\�j�ۻ\�k�k)��R�\�xç��/\�yq\�\�4F���Sְ��_$%\�-�Tm��5�\\wy`\�\�v���ۂ6\�ˋk���n\�2���7Zf(��\�K����\�\�B���J\�r\�k��Ʋ�\�fM�}f��2Z\�\�b.�)\��Ҡ�s=z}�\�\�C��v�E�\�\�k\�\��\��/\�W)3�^��\�mt\ZJ=\�T9����إ[�n�P\�fk\'�4{�F;}\Z�h�f�騔E�d\�\�\�iE9��q?\�\�w\���h\�u��sf8Y<&\��\�fdn�j���\�\�\�D\r:ue\��[�T_\�\�\�>\�\���L��\�M��b�Y���I}X\�ͭU�\�M5�\��n�s�լ��a��ϧLEY_�����C��\�/Ւֺ�z�V\� �\�\�\�\�\�$_\�_ӊ\\\����>�\�\�\�v~�Jۻ\�X�VWe�mT��P��{��u.3֨]�7�G��}pY�s\�\�\�\Z���j��bC��l�\��ݛ\�y�Z�\�z�\�25{zzAq\�AO��\�\�\���6\��\�[m(\�E}gI|5�۵�\�\�\\�+��\�a7��Z]�Ii�\�hV\�\��Jjp\�jY[�޼�q�kn2\�߹�\�|}sVt�ng׆��QFa��ҭ�R�o���\���\�\�~A��Z�`㉂Xс(�n.�l7��\rg�=e�S�T=\r��\�D<\�*\�Y*\�\�`s;\\��\�6�G4sz\�\�\�i����kC�ݵw\�f{0�ֻ�y�#�{+kQ��ϟ���Y��T�\�8d�kV\�\��\�B\�\�\�Q\�\��� ogs9?}P�q��sE��h���e�\'r˳bV\�m\'�(��\�ɔb��\�\�\�e\�UsC�]��t}]�\�ieR��\���!ϩ\�A\��ۭ�\�\�\�\�\�.\�\�\�j��P\�1\�ޠ��w%L\�GM.\�\�lX��\�\�C�ұ]�Ss�/���b�۰\����\�	����Lew\�)rֺV��۲6+�\�!\�X\�V�<\�,�T�\�\�n+�՝ֺ��BQ�\�LcVR��h�\�Ψ�\�p�\�\�r\�r,d��ҖJ�\�C\����\�</\r�eqW\�n\�\�QX\�����w\�̲�\�ߧ&sYӧ\�\�ndf�b�:�7U�ͫ�TZ\�\�۝��;\�l��\�\�aU�\�\�!O��7z\�ѵZݰZWN<�7�v~j��˭R\ZW�\�j���U\�\���\�m\�ٱ�^s���-��Í\�a��c��\�e��x�\�/\�|s:Y�͸UZ\�Y\�<n�\���ݕC�J�g%�s\�m��\�}P��#ͨn\�\�q��hn\�A.\�\�GK�,��Vl�*ýnn�\�V����4��>�\�|ݨ\�\�s~Z����P,�$Cζ\�\�x�Kw��\"N\�Z�\�kd��ƴZ[�n�Ag5\�g\�Y�{Y��<k*\�|\�,ѝlQz�\n�F\�\�U\n�\�fP}\�\�=�g�\��o�J�f��\�zfX�봖R\�)�qmv�VuM�ʝݒKE.�Ċ�쿔8\�k�8�h�JW��t���\�ʔ=\�zk���u]ٗ۵r�]V�\�\� �fe�tO�\��Ju�ܚ\�q9j?�R\�4ڔ�\�\�-G�Ǣ\\[m�\�d޲4S*\0�?\�\�����xf�~\�\�\�>\�E�\�CȍJ\�\�\\$� \�S�~JR\n�q\�\Z֕bvɔ\��J�\�n�íP2�\��F�\�\n\�X\�\�xFgh�+�\�ǥj\��\0g��fu<>Zt�\�a$��d�#�Z3y�Z\��r1\�\�\�\�M�{p��8�2uK�7��0.�\�\�7�u�_\'�%\�\�M��\�ʭ��\�0�͛.���cuĲ����\�\�\�\Zg\�Kw��Z�\��L?���D�\r�v.�\�\���▭i�޼ދY.\�\�k3WN=\�\�\�\�/\�[m�l\�\��^ku�ե.hC��V\�-���\�\�y�/\�\�ɾ*\�n\�ǂ\�\��jf�l\'\��0UKzYY*�a;\�\��6�_iS9{)ͅš\��`\'\�&�cV?oUޠV��l�\�{<U��J�R�\��@c\�����,��`��\�~��5�[\�(�h\�\�U�\�\� �;m{�1\�\�e\��:\�5:U�X�m)�ٟ��?;\�%v\�2�u\�2\�u�\��[ip��\�L�tA�sw9ES\��2W�+�\�\�xe\�z�%n�\�q��U���tg�\���&ժ�zڜ\�\�\�lw.S�ɺ_*\�ǹǾb�\Zk�Zl`wE\�\\\�\�R\�/�tK,n\�՝��̀H\�ta�ޮ)�g�r�Q��zw\�\�\�{\'W�Fʘ�\�\�K�Ra3\'!\�]\�\�QK)��lG;dZ�\�\�S?�/5���u\�\�l\n���p4R\��:[\�\Z|\�T)�NE��ʧNm_\���Q��\'��*�s�^gܺW\�t��?��|\�(�\�\�~Z��3u^\�N\�ir\�ҳT>K\�W�a\�0\�����v�l汖8V\�\�kY]J��\�7�\�aB\�\�Y5v\��1;��*\�V,N2��1�dj�ݼ>��\�B\�\�C\���X�F�\�]F���_R\�dUVK�M�|/�X\�\��le�\�ӛ|[w\�f\�zN�ֆ׆�m�m��ӝ�\�ttn\�c9��\� OU�ͪ�\r+2\�+�\�\\\�!�z\�܀�/�\�c\�m�o\��Tȥ\Zg��\�y\�4\�Ee6\�/�\�T\��\�t.��M�xR*\�ٹ:4ʍ*\�\�>/>V\�b-C�Š~M���\\��3=&OV7�ڹr�V�^\�7\�\�\\�v���]�W��\�ǵ\�\�2]U¸�m�\�\�\�M\\R��R�7zQ\�ZiY\�\�R�?.э\�}D7Ԉ\�\�e-�o�3}՜G\�\�N\�\�\�\���9^\���zb\�v\�U�:�!=;Z�Z\Z���]c@��\�Ng��Ry\�\�[�>d������\��`=�\�\�\���\�\�7{K�/χ꽿���T\�lӧ�T;�\��\�HU�GA��v�Aa�\�-;�R�oP\�\�Kg���\�k\�n/=f3u6�\�\��a1Ru\�3%K��Α^�B�H\�\�$�Vm\�\�]V�_Nm����\�N��ԚN��n�\\�������ǲX\�g�v�߮\�����\���P\�\�E�\�j�*5n�/wF�{\�\�\�\��p<��â|�\r\��\�ھKkl\���u�:�\�}*{\�ZifȂ�\�ɭ��l-6����\��K�	�\n�)S]��\�C�r�5�\�^}�Y�\�鸪�\�e�;m9�Z=\�sC9�:�7u:\�TR\�:g�:\�&\�\rg��\�δS��R�\�\�2R�zͦ��c����9N\��eQ<g�� �R�\�֩�x\�>4�]�|~yT僔\�Uu}Yz(Եe\�Ts>���\�x\��\�l+�\�lY�2�\�@=�\���\�\'\�b5�Y=�\�JG��\�n�ح�λ5{�\�ii\�ù^W\�\n͌V±��7\�>_l�kN=v��>+6g\�b\�׳g\�\Z�\�`ƨ\�\�^���\�\�:��!��֩A&e\��L���yq�\\T\n���]w46j���\�k��1�K\�(fW-*\�\��qYfv�\��P�\�t�x��M�A�\�Nn\�\\�\�B�ܡ�\��\�Y�\\C\�VF\�j>N\r:��oeV.�-Ϻ�n�͉T9�O\�8�\�25񺿛F�\��Uj17�v�<�{[}%K�	\�\��\�!\�_-��+<W|lR��2\�G��k\�f\�6��WG[�ǥ����SkbNm�V��g�Yn�k\�-\�Sz\�R���^\�\�B��3\�\"ۨ�\�ή�h7�\�e�vl7\r9\�/s�=�\�\���8)ޕ�\���͵�\�e\�ң�CK��o�vI*��Q�7�\�\�,�k3\�?/k`�Z\�S[Տ:Ӿ-���\�8���j^�XU\�\r&\�\�\�:�wUC[\�g�\�ץΠ��B�V��4j�=w��\�p��\�\�*q]�}\�R�]u+���RX5��\r7V�\�1��v\��џݏ�<*��Vwu��2;,̦E�\�MT�]\���\�;w7�[܏�\�F\�\�\�\�nP̦�̝dK�\�a�\�c+\�ֶ]I�Vl��1W�\�\�\�\�\�%E\r\�d�>\�\�b��w�Wċ�\�M\�_J�H�\��nj\�\'0$Ã՝��gzZ�Z��|\�\�\�5y{Y���\�طw\�ֺY\�6\���;V�C�7v\�!@\�\�*���ҝKW\�\�{vV\�\� \n\�\�%;SwӲ�����$\\$�Y�-\�b\�WٙT�\�EC\�>j����ݹ�\�\Zה�-�\�Hc]\�\�\�E~;m\�zM\�\�nwZ*R��ԫR��:5\�X^�\�\�E̖*Ymә���6+\�̜]�\�\�\rf\�zn�jMnp\�]�UYݸ�\�F�vvҼ��^g��\�tYI7\����3��M\���\�\��}�pR�LZ.G}�\�Vy\�Vk0��^\�\�]\�\�\n�,_m{\�\�Z)\� �\Z%�1>g�\����\�v\�\�n�\�\��C0�I:\�\�f��\�\�S�nԳ��b��\�=t��E.�\�:EռV�n\�X�ҙY��/�)�\�Tu\�\�/�R\�[�ɬ��O\�\\�ק\'�RDi\�9��\�bٚ�MqP\�g\'�r*�a�8H����\�E��\�\�w̾qe\�\�K��M�C)���\�3j��\���0�MuR���ڝ_�u1�(O\������J>?�2[�H\����F\���]\�\�\�[i�\�,\��\��\�\�<f�W&��ݳ\"x�\0N^�\�m�\�]�;���F2\�\"3�2�\�QX ek�\��\�\�ȷѥE�\�k4\�BL��\�V1W�\�@�X\�R�VՊb�BT��ȵD�r\�ݙ\�\��=�ZF�	\Zw67x(&\'��,D\�ދ�\���)�c�\"\�\���7\�\�coڞ�\�\���2���#ٙ\'\�L\�m\����{��HhP6\��U��}o,\�B��x�XvC�J\�%DJ��:�\�\�r�\�xG\�\�\�olo�\�ц�2\�XMP��s_�=�\��:�\���\�Q!\�u\'�+����H\nVC�X8>�\�WZg$>��my&�Βs\�-;1{\�8\�\� �\�kl	\�DsR2ǔ�zWa�Ua\�h(��+�\"a)#%̳(ʦI$\�}\�a�\��t\�n���>A�.�09)\����$�\�t\��w!IX7(\����q\��3#2%\�%R ��i_2\'�O H,^\�\�Ù����\��\�\'���\�3I����\�T��\�\�x�Jc�}\�n��X�$���H�\�\�Ĵ\�\�AqZ#^\�.\�\�r\��\Z�\��!߽�n4&G\��|�\���\��J�����\�\�vEC\�w�^\��\�\�J��U�j\�\�_\���hR\�>#Hg�\�C\�\�\'�l\�ϥ�48\\39\�<\\��l*�˞}��\�;\�.�\�ty\�m~\�����g�\�B�y\�9���w�F���\�\�.u�,g\�>;۷g��\�f��ӎ���c=�ʥ\�r^?\���f\06ɵ�qӻ��E\�V���\�\�\�\�\��n�tI5�R�W�\�u\\\�]\�	��XY�/[\�cq01�\�<Ů�\�]\ZjG�+�\Z]�N\�ԮfT�\���\�[��c�\�\�\���,\�EY؍/\�\�v^��*k�k�\��\�n�\�\�k�v�_o�}�^�Ԫ�\�-?\�.���-?\�HlIX\�*̭CMu�Z�\�\��r���tfs\�\�\�[s&?Tst\�U%��\�\�\�A�˅\��؝\�ΥN�\�_��\�\�\�Eg\�\�\�]+Ϫ\�y\\�v\�*�\�U3��v�㍬`u)�<�\�}fP\�\�\n3�\�2\�\�I�if�\'uY,\�i�\�Η��N�\�\�&%e3�)\�\�VW#�8\����D.\�ӥ٨�\�LMW{�tWF�\�Vl\��\�\�,�\�\�1}M�zV8\�3����\n���+m��U��5�멼��z3~T����\�V�f�\�7s\�R\'_3��Lje��\Zu)�:k-\�����\�f1�\�\��\�\\,	=P��*Y\���\\\ZH��N\��l�L�\����Z�թ�T2+Z]n\������7��D�e1\�Ͱ*un˓a���P\�\�kߪ\�\��]���\�Gݤ��8b�\�x\�FM�HTgߙ�n�LOS\�\�Z�6�[�\�,��[\�ᾪ�L���\�\�s�\�	;���\�>\'��mw4]\�R�\�7=U\�լ�\�f�UVm�y\���B9?�\�\Z\�i\�(�\�\�*}Z�\�N�w\�\�Je\�$>r�6x�\��yݘ:ߟX�QF��\�unԭ\�]ثlC/Ωq\�Z\�\�F�h�ƙ\�`�-e��|]�0&Wd.:C�c1Qꅕ&\�s;��U�\�\�t<2*\�+��r�>\�o�;V�6ا�b\�Z�R\�K�t\�t�\��\�\�g\�\�\�Sb��$\�7�zu\�\�sL�Nj��Jݻz^/�z\�r\�\�t�\�Z�L\�p�\�\Z�\�97\�^ҥ\�5�0��Im\�\\�z\�F��oQ\�P�U\�5�v\�\�bm�R��\�liݵ��gכ`\�\�A\�-��M\��3ͤ+\Z\�W�}�\�6�l}u��fo3��\����\�P�]g9���iN��m�\�\�SF�I�Nq[L��ir\�Y�6`V�x\�\�\�\�W\�9��NSyM4V���\��G�ۺ�/U>�\�Jf��vW�-.�m��7�zy�t�ĨV�.��\r\��7�P8���\Z�x�t4���G~\��_�\�\�\�Vpw�7$Z$G4�\'�}r��\�\�\�[s.T�A�U4ÐV쟴����8�5l\r�AV?��	��#�۫���\�Y#:�|;����SB\�\�I@n\�C[\�\"��}\�znq�O\�_,\�V\n\��=ͩ\r*���I	L��S�\�&ao���\�-\�\�)�Q\�	W`Pي1P�\�\�\�?�\�\r\�\�P��\�\r	�}����쫐TL�eL\�n$��|%>��F\n�\��c=\��U?x��Pʳ\\�]�B�EG�N\�7\�����\�\�l��)��\�Ef\�˸\�\�\�ܸ���B\�\�p{/ ���}�~	\�V��\�*Q�\�p�1�.�>رn(1�9\�Y��%\��Q\�A\��Q\�\�\Z�\�\Z\��\'���y���\�͢\��raV(��~* \\\�!\�\\s2\\�P��\\H�ر�������/�9y^K�~�`n�h_~�=G\\�>s�� �؅㯉��x�\Z���\�}U�~�\�\�Xn��\�I�\�f:\��hnZ�� z�!�A�~\n�\Zfn�0�?k�\��\�\�t�j��5�x�M}/\'�6\"\�y�\�\�JT�B2�}�\�`L��ϡГ9\�\���!yY��Q\�\�Y���	�4�n0��\�\�ݏ\����+s\�P�>\�S\�/i[v\�\���\�f*��NvP5���y�Cc>\r�9L\rEG�����\�W����\�h��TmtS�l(��\��\�`?�\rh�6\�~��8>\��8�t�\�&эOc�����\Z\�Q\�\�P\�(`�uˍ��Ӌ�\�|���?�^m~�\\<��U\���x�41/5�+ûMw�\'K$\n\0��˜3�(9\�P�z\�ׂ?�\�\�\���F⍝�.�e�K�j��s\�ן8\�&\�4y\rL%��\�J�vfv)�p\��8\r!��պ�\�\�?��J�����\�\��\�\��\�D�,�\�\�1{\�4a|\�$S�`�b_tҘ贿�s�\�\r\�oX���X\r85x�\�m�쟎���b���\'�ņ\�6]8\�=B\�\�]\��^gr\�ĪЮ5K�5�\Z;g|�\\\�!�_	l�2�ǜZ��\�&\��%��%�\�\�v7�\�3\�\�G\�V�De�C}r\�Z,�Z�� \�� ��}\�\��:�\�$�\Z�N�\�)\�\�=�ܽ\�\�\�\�v���<Y�ޟ,�|xe\��:�A\�\�\�\�\�^׭\r����Rg�,}o��}\�	b/0Г\�\��x�\r\���\�;\\ˤ\�ic�\��9��fǥ�\Z\�v\�$�����¤�L\�\�\�\�\�ת&��0\��\�.N\���׍�O b�=��9�\���I&�7�����\��]\��B܅&��>���+�{;���~\�.!.u\�\'@OQ6@Q;\�\����H`���C=�\�\�\�����\�I\�\�y���6=\�1��^�H����\�c��XWj���_ú\�?�\�\�쏘�\'i�>Ě\r��X\�\��^\�o�\��uB5��#/\�`\�G�\�\�N�p\"���gB��Sg�_\�\�ٸ&Tȉf�Q����S~D�d!�.�\�l\0�\�\�\�oS�\�!T�E�᠖\��8��o\�w\�q+\�\��ȼiw\'��ma�++�\�R��zx|�_\�#d�>�H�L\'\�Z��,�\�T�/�_\Zl�u|\�͏X�r\�o$i\"��\\�\�tC=8�<\�#\Z��{YR�7�\�\�+\�\��&�D\�ɠ\�\�\0aB7sB\�?�.\�fl\����=�B\��@\�EDH���\��Yv��E\�r�{\�\�g\�\�BVb	��i��\�\�($Y8+2/\�\�y�����`���,I�\�Rt��e3QN��@;XI��\�(D*S`�(\nS��Z�`S�|k��)�,/E��\r�~Ђh�~x�\�s\�<����y`\��e+2�3EJ�m�O�wœ\�` o,9p�ő�i%�\�\�n`�}F\�q\�J\�\�bM���Z<�i:\�	t|J��.�nOF<��\�$ss&�ü-3����K\�u\�\��#3\�E�N\�y�,�8\�_\�W;\�s��]}!\�g\��\�	g$\��\�_\��ׄ\�s�(�\�\�/\��	\�{ޗH\�\�9a0�;D1=E\�;\'�eZ.��㠇�^��\�<\�6���\�\��\'Dl�ī���\�cS�#�㐝���,0��\��\�!&�\�ܣ�\"pg�E<���&�CTљ\\�\�\�\�o�\'α׺n�\�7�\�\�$@�����D�6ݷN\�\�5��#4\\���C���\�L��ڽ}�� ��\�c�\�92\�	����\�0�\�\n��\"�PO@ydg)h\�\�\�N\��޲xq#K��$\�!\�I)\�2��{ɍ�\�pHI\"b|S�\n2s�5\�\���R(|.��3$4?�\�B��\�n���\�\�\�b\�\�\�\��	��3\�pGz\�2\n>+)\Z~�\�Dޟ�\�V\'W�+�G|X7,�\0T�\�?\���/\�8��6Q\��\��#�\��r\��\��\�DC���\�?�N�>�\�d�\�&j�)\�VDƷv0�\n\Z`�\�.\�\�Er�#�\���NH��q�{��R�\�g�\r5�\�\���w\�ٍ�e�E\���rMˍ\�f\�X�T.���t���ϟ\�M�t\\U(u����U�:df:?\�\�\��l\"\�\�e\�h,ڮ�\�R\�ʍ\�6�X�5�^U\�:m\n�nN\�M�\�~������Z�9�\�Pr\0\�*\�\�BsW\�	LKk\�6�i�T2\�{ez\�\�\�m�R�5+K���R�ɒ鎯Jw[�uU\�\n��T\�֫\�to�\�{��\�,���2h�v�\�q<�.=z\��h?�t�1��U\�\"e�L\� >:�\�}u\�n�\�.\�y\�G�\�Y��*��\�Yj�P,P~�᭮���\�@����\��2���.\�bXh�M�@;\'S�\�\�o(�Q\�ux�\�b+0\�E�׮�G�\�-,�\����\�\�Q\��,@3�yv���d\nV\'�وe\�\�\�/\"\rƿ>��2!3�/�\�x5_n�X�\��vG8�/\�\�\�!V\�^W��7�5sT/l�9�	0�>_ؾ\�~xE�v\'\n�S\��*\�\'�Ư_)�]jz\�n�\�~uJ\r�Zol�\�\\<�c�_]^��Ym8�^G\�\�;�އ\�f��\�]��Zu���\�G7�\�ϳ�^}3\��i|���w��j^�\0ON�`�q@sn\�\�\�yO2EeHw�fy\�[\�{GЎ�\�(G�WP��4�+\�j>����\�l8\�\�\�kg13\���6���s��=��a�[Y���{�\��h^{[\���_\�\�\0\'޺�M�W\�\�G��\�\�|��Y+�������2�\�\�^.\�<=�\�\�L\�(6FG�a�\\�^\�<\�k�\�hk\�\�s�\�e�&\�!Mu�\�_g\�ZJ8�r0\�mN͙ʰx\�N�t��V\'�roW\Z�m&;\�V\�\��:\�*uj��\�{+�\�boY\�߇fT���es;g�{uC�.\��\�\�\�ri\�Э\r\�\�0\��`\��\�ǐ{J�\��ص�\�z�\�\���֑ʵf���\"��ͦ�i(�I��^\�sk�����|]?\�s\���Y�tK�S��k\�\�W\�\�\�Y5\�\�LR��@\���n�ە\�e��\�N�-�f�ݔ��m�fDq��2+I�4E�^\�\�J\�p��x�0\ZO�[o\�\�-�\\\�\�֤2�7�]iT�Ju5{Y\�\���nO/桵\�ܘ�b�k��kl:�\�j)�!w(������՚�����\�.\�|G��\�\�\�\r�\�\�}��\�\�mV\�m5n\�m�\�\�츒UJ\�ݿ�K\�UmU=���)\�\�ܢ1�q��2�9�\�Ge?(Nv]-��\��\�\�\�dd����s\�կ9U��\��jZ\�\�]���\��\�f_,\�\�k��(\Z\'k�\�\�u3ճd�]KJc�)\�,;Vs\�}�\��\�m/�\�3\�\�(\�1�\�d�Sc\�c[�\�$�{�Sfd6�\�w\�\�gw�4d��\�� ��E\��\�Jh�\�k�Ң�TZj+�{\�̅pRk��N\�;�lf\�.\�\��c�\�)�\�hY�\nyib�}�5$\\�~I��� �\����[��Z�\�O\�F�k��Z\0ˈ�æ�\Zn8 \�ͅQ�\�-^k�J�qV\��樵,���>Q�V�Z*\\�\�\��Z����c�!4\�t���%\�\�ӓeMwJ�ST����!\�\�,\�y��[�ڸ\�+T�f.�\�\�f�:fz�45\�wg\�}Zh+Ձ^]7\�\�\�4�\�E8�\��D�����Y�\�J{�\�\���6�h��e�9O�[9Ik��;�˝�9j8_e\�3�\�k|g\�ke\��N�ʜ1k;k�t�5\�\�.���6;\�~?���\�-r\�A��\�S\��h.���^\�U{�\�\�ֽ��\�.o��~~8ڍg�is\�\�\�E\�ѓ�&S\�7{U�Sz\�!�`�X\�m�\�O�\Z;�P\�I�3��t?�\n\�W\�=��zхK\�\���\�AS�\�r�s���U\�`.�\�*O\�\�n�]#%�g\�Z$\�/��\"�䫋\\p1+�&J\�#J�\�@\r\�:��\�\�\�Y\0�x\�\�k���\�ݯ^�8��0?f\r\��\ZƠM��\��\�J\�:�d\�Xc��\���^�`L��ʴ���\��`\\�\�ޤ��ɼ�\�A����\�\�U\�N[�.5�~e1LX\\}|ۧ�l\�u�\Z�i\�(\�g�ѡ����j��G����\�<�]M�*K8\��\\\��w\�\�p7}�*�JwR�\�\�h\�*\r�;�?)f�\�Q�Ԃ\�C��\�\"|`{4�g�\�\�[\���\��q��a�w\�%��\��\�u2�\�pr\�\�)k�m\��Afs���Z��\�\0w8�SZf \�mB���wS4g���\�T+\�K�y]��ˆԭ�*�Z\�l�`�\'��`5�\�\�\n\�17�g��\��\�����\��6�׫�#/.�d7+�{tv1�gS\�d\�#\�!W܊\�c�\�/��\�u�Q�խ<\�j\r�(��qQ\�a5\�\�ˊ\�\�̦r\��©�9\��\�qz��W\��\�x�Ն\�nmޟ>��0\�3�~x\�O�\�4�pf��\�T��k�k��]���Q�T���;>����lQIUλԖ��;�j�j\'-\�/��z:S�\Z)v\�+͟�����\��e���\�M=vt��\�\0\�|M{����\�T\���\�n���\n�j\�\�M�L,\�ݬ2\�5�\�n*�\�dS��z\Z�ʳ�N\�a�vSu���\'k�2P\�\�Aan�T˧�^#7\ZW)�m0xz}\��Łz7�L��ܫ\�)\�\�\��z\�j\\\�O�}~���әR\�8˙�5=9Y\�y�*m�͘)2{��\�yM\��J\�Tϲ\�_\�	�\�8����t�a�T,\��\�ڪ\�\�]��z�b\�\�\�\�}�\�\��JN\�\�\���A��7kZ�\�\�9���n�M�-m7��7�\�\�hę\�z\�*�vY��\�\�Q�m�K��Z���\Z\��\�q�ܸs�֋�u*m(��\�:~Ǧgz۝/\r�{��\�e\�\���?a��#�W�;\��~g;��\���Z\������oT)^�O\�w\�\�5-C\�\�G�Ќ����\�~g���fEJ\�sF\��h�\�\�\��ܜ[\�\�r\����\��+\�})%\�2XO6�\��\\�nǍ\�\\�\�\�Y�d��~\�\�M汿\�%>G��V�el\�4?\�\�\���\�r�\�\�ʱUZ$�Y�\�\�u\�zȦҔ\�\�mɥ�õ\�?v��<9_G\\�\�\\�݋\�-g��Xxڤ\�^7W�ӑ˚��^V�T�Vmїm\�8\�\�t]�*��^�.\�\�pz^/ʣ\�Q�\��zEc6l�Z�ȩb�n\�g�b/�\�Ӆ��:P\�+=?9ܛ�\�HkF3Ƌ�ީ;��{#�\�\�\��?m#�V�[��l�||/n����S7B�\�bmKf\�n��M�W餌\�i�8T�􎩋\�zЭ��EvT\r�FqU�\r��b{\�\�\�L\�Uk6F�ͱ[<5\�\�j[�U*\�����N\�)\�+\�Z{�7�\�\�bW�ާ�\����\�\�f\�\�\�l�C~r\�[ݕj\n\�ms\�iv*�k}JO�G~�-+�j�ӗ�vt\�p\�n����L\�j)C�\�\����<\Z���\��|\�G�ѬZϪin\�M\�g\�\��T\�S�i`Vzx����\�כ\nUzt)h\�2\�:�\�\�\�\�\�\�5���n4\�\�n�x�:Y�=\�\��&\��\���v��?̍�iO/��-���Ѝ\� Vۗ\�\�;N]\�\�C��ID\�5:#�c��;\�\"4,\���o�e7\�\�[a\�e�\�\�Y!D�R`C�;�����\�dԱ\�DB�`t�ɘACd	]I!G�y\��}+KJ�z�\��k����&�\�8��\�\��H~*:�\n�\�?\Z�\�ݹ���+I\�^���t�l�ɛ\�-�@ :�8���\"�m ?�Y8�R\���6�3;nA �v7ҽ�v�\�\�\�&9�j�\�k\��Z7��\�MX��\'%?�q\�I*�]�q{\"z9\�,		�P/1�\n\�\�rR\�\��y\�\�h\���\�|�(0rl�nQ������;W�\�\��/R���\�p�/\�\\�횋j�����\�(	��<t�\�*\�Ti�B\�2\�\�\�\�t��0�\�RE�=����T1#��;�pi_\�B	!Ѭ�cicp&�\r\�.�\�\�8}�\�\"\�\�\�\���6bS�z\�\�\�dI\rG\�K�~~e�h��(\�Ba��V�v9؀�-38\�\�t���D/\��\�H�+Wﰕ\�,\�|����yb\�5\"=<���\�\���Ѽǖ\�x�4 \���!�\�\�\��@��>�Lv\��Ċ�\�cK��\�H\"5\��\�ϵQ ͥ\�\�w.�N3�i/���\�ƅ)t\"(`\�-\�^�\�M�c��\rr��x8wG&ӿ\�)/mϦ�+P�C7����_@\���%F^Ei8_\�eZ�~P\�9mQ�\�/��V[�\�\�\�x�k_\�&�\�Y,\�A�d�\�8�\��4�A��\�\\�~��۽.�ؔ��%\�U��܊���:^\�MS�����\�W��FP��.\Z\�3��� ���cj�\�`�j���K��y\�*\��\��=��\�_��:�\�\�p���\�ڌ\�\\^k�Z�o�E\�\�\�\����\Z( \�P�UWQ@K�����1:uLq\�X� ���Z�\�T��!v\��\�\�Su\�B�z~!�R	_�t�\�\�����\�\�\�EM\�������\��۴�}}�\�\�\�HV\�\Zo\��\����k\�I��`ц�]dh\�y\�\�g�\�\�\�\�����ڻ	V�S6T<��Ϸ\�\0�\�yD[R\�Vj\0�F�$U\��\�4�K2l=AC�n�W�l\0P��0��}OJ��1�`ڑ@\�	��\rh�wJBBރ<V7<�=�Ok�JAX\�\�\r$\�\��\�\�|��	��Cw�;���N����G~B4DC\�\�\�\�TEC\�pމ\�*BQQ\��PO|\�F�SR\�\�\�?�d\�;���0i��B�@!\�?	ZN��<F�\�\�1y ��\�	\�f�J��\�o-\nbA~\�\�!\�ԃ)[o�������ܴ\�N&^\�_\r\�ȺR�j\�ɷ���%\�i\�\�m�f�T�GUK*f�T�T攁�\�j\�p?Vשr�������b�s�\�޴\n��\�P\�]\��n�a*\�e!�ٮ�\�v{6\�G��\�{�\�m�6֒7&S6�^.���l�\�\�\�v��1\�\�o\�65\�ϥf{�V\��B>P�:\�ո\�:�J�k��ZÝp��\�g�|��\�&��y�/��\�q��.�\"\�=7+[4�Mb�u�O���v\r\�^E��\0�\�مW�۹g�ŋ��]C�gT�4Vh�\nG�~\�\'��`�1�2x\"\�\�ԃߐ��o�Kȷ%9!�#U0��z���Q\�`�\�\�d\��9\���\�ۇ�K��\�\'f\�B��*i��\���?HPh%\�\r��ר�\�\�\�A�\��Ї\�k_[\�0W���Hn\�pU1\�M:W���q\�[W>?\����ť?\�bǅ���z��/4\�ɞ�\'�г�\�\�\��(\�(ڑ=�o�����i\�{4FS4D2�\�\�=/9/0<ˍD\�{�/\�\�`w���z4U�M]\�T\�\�\�+,2�w��RuEX�\�KyQx�#�Ph\�?З\��1��Y\��\�Q\��+b6���$�\��o�����\�K�\",.z���\��A\�%7C\�\��Õ4&D��<Py4P�q\��#�\���B��Iv�\'�aq}.\�H��8�B\�C4��\�_�G�����c�6\�P��\�\��:\�^?\�t�>;H����_�K�(\�}�\\����7!�ҋ}\��ǝ%��xC\�R�$<Xq\�\�v��O��\���I\�H\�i\Z(��\Zf��.\0\�\��#�\�x�Q6��\"\�\�/\�1r� �\�b{B_kxJ�ԄGw�H�?�4\�Ü\�U��\'&�Hdz��ц7N���T�t�%}�GIy;Ȑq�\��J49\�N\�h6/R�\�\�v\���jÇ:጗�\'���\�O!�Fu����u��\�l�\����\�CI`��\�g��Z1{\��\�3���\�VO\�:M\��I\���/�+\�� H���\�\Z{\ZY�)ur�\�9~l�&Al\'\���\'\"\�7:�xe?]kgs\�\�\r��8\���ɋ��$\�!Sk=(H�S��ŝ�y6�\�Wg�8��Fm�\�S�[��\�PRV\��q��V��\��\�\��,A��\\�kC�\�\�$\�=9�\Z�gLا�!Hȥ�\�ώ߸A\�@�<ڟO\"ܻ�Kї�3>��̬O�.�\�\� \�~njR��L\�[\�\�\���0�ۇy\�\�S\�V�	l\�\Z\r�\�\�I\�l�q�\'\�u3�$[>�{�\�S\��\�^U��Y�n\�2`P �\�aG�����wܧ��(t\�[�J=�s\�.�ƐD��b�\�#ܝ�S��~	{\'%\��\�.�11�\�����\�5-\�\r\�]Su�-�;Z���\��;ZA\�P�\�=�\�h[���\nկ�;��2�v\�\�I3ӥV\�ޤJ�˯\�Q��ջ����`��a��\�Ի\�Y\�\��*Nt�\�+\r\�[w+>F�&ݝ\�עx�j\���s�\�p\�\�\���:�WS�tzp\��\�f�Tڭ\�kne6��.��v�\�a�^�\������\�\�jX(W�2WW�\�0{��g��\\���\�1/\��d*�\��o\�ҍ��N\�E�3�\���:Y0�(��MZ<^\��+9���ح;/3Q��ma�;ǝ\�~�\\g�ȭ\��\��a\�\�[\�^�\�\�j>\�]\�\�ϡ�\Z���\�d��B��M\�^k��\�ykh�.R7)\�1�{\n�#\�YT+�/��3d\�*y�0��p���|\�[\�p�,4\�ZG0Ƚ\\q\�$3+NF\�v��\�c�\�\�{���ry��%f\�\n2יִ\�ʿ.�G�u�FF\�x-{U�\�BA\�\�q[�\�\�~A��+`�SA�}��\�w*\�\�U�l/�9E\�\�k\�(�ýD�Rџ\�1\�F\��S\�X̮Ј{\�!<dy�W1\�Z\�8\�2T0��ܝ��Y`&\Z��C��A������Q\�\�eZ!��پ���\�c��R\�	7�f�x�\�\�.\�*D+��`ܱ�y\� \'`t�;���\�&a	6\�4G`��Z�\'�O�\�W�_�����3!h�c��\�!ߞ�\�,��37�Y\�]{(P��@wP�턒���TX\�\�e�I,|��~%Fi\�\n+�,HeQv�\�\�N*\�8\�$��3����\�\�\�\rRat�#��E\�5\�\"F9\�\n���PT\�ق@\�I�\�Z5bF�Y\���]�\�ӹR�\�ʉŝ\�k\0���\�p�(�\�ߟV�\�*	�#ĵ(p��H�A\����YW\�k\"l[�\��e����&	%\�\�E\�\�\�D2\�\�v\�m\�|k�je�J��6/\��\�ki���\�M��\�z��\�I\�L�})\��5�e\�\">rϨ\��BV 	b\����r\��\Z\�8>�;��]1�Fҽ\�]��t�\�\Z��\�-�?�_RZb+���\��\�]�\�?Pk���\�\�\�h\�sT��\�3��nZ��#�-+/\�\�yB�G�ڸ�x�*�\�\�\�\n���	\�T�R`vd��SC�`�\�;S>H�W荨�ֽ;\�]Ù�B\�߇|[/�\�\�׽\�\n�{��̓�4x�\�Iĳ�h�\"-��\02������\�X�!��҂\�|]\n�~醈\�-@\��\�>\�ໞ8��D=rMP�\�U�j�_krh4|��\�\�����j��E�ޯ�,\Zz+�\�]O\�\�g�\�pߜ�Sm-?O�`�\�\�\�?�\���\��\�3A�Q忻6⢉P\�H9\�\r\�\�	�\��\�BN_�7\�B��:lk\�@��?k���3W�;\�\�|���w\�?:�a\�k�<`b�\�\�\�\�\�%\�x�)�la�\�Wص(\���\�\�W�|Q�S-v�?Gt�\�\'���m{��D�\�\�[�\�|\�\�郈(\"\�I����3�db��W�Y=�J�`\���G\0�$aR�烸q�\�}F�HɇL�a\�#�g�M�\\�\�`˂�r�u$�~m��H�%c�\'���\��YJ}�\\�\�\�L�풫	�X1QǶ祣*\�ו/��*/�9�P���f��Θ�cA��1�\��\��GP��d�_/R�7RT,&\�(�\�Il)�La�	�J�\�ɦ\�a�\"\�\�\�\���\�\�S�\\8 p\0��\�	��x�<\�\r\�|�_e(bid��@쓓�/!\�0�J�ϡ?���OF\�W�\�S�\'��4�J��\�\��l\�%����qF�/������\�f�\0b���w*D{�\��7�����#ا\"�È=:&h\'�[H�q�\0ka��=\��\n~�v�\��^\�*~�Ǧ!�ib.��Fr�9\�C�ݪBO\�\n��\Z��\r>u+>tI�N�\�F֥!�\�%\"�إ\��#2B�]:BO]B3\�Ϲ�D^��D����i��	?w�	?vɱ\"�� -��.!��.^T��~x\�\�x;\�\�w8YxC\�\�\�\��l�/x;�?��\����iB�\�\�c��dK\�\�1������G{07�g�WYޅ9V�\�Cyϫ\�Z\�F��)��U�\�<�ᳶ7\�;�\0d�ak`<���x�!��\"i�\�db�i(�\�\�\��)(\�>\��\�?�W�h\�9�\�\�O@��\��h�\"��(\��M`͡�\�\�~}\��\�\�J|\r�K�}G\�Yh�\� \�Cœʚ\�>J��C\��J\�\�+��\��~Wp=�\�\r\�\�ȫ�.:�xU�QXW]���\�ϵ.�M\�\�#��\�?	^��B|\�NK�q\�9S��MT\�+w�\�ؓ��M]b���\�\�}?�?l\0\����1\�t�{g29G�\�\�d\'�?~m\�3\��%�Q�\�}ir?G�\'\��\�M\�\�zXPndq*�\�}�!�`��O!�x�w�\�\�?댤��P�j%Y\�B\�r!�<�\�\�v^�\���w�\�7��\��v^\�1%�\�#$\���\�y����\�o\�\�T\�xC��\�/\�Pz�\�<���\�`D\���\����Yh.���\���ț�{*6��[\�k\"����l�0!�\�8!/\�w�o�{�}�綡k��Rd��Ϛ]7\0��;\�H�Gw\�\�<\�\�\�\�\�&\�4/\�x\�\��$�@���W\�y4\�ɹ-\�LRXm.\�w\�u\�\��\���\�Y��	O�>�2X���xv?\�G|1\�\r�\���F\��\�s�L6�\nFـ,�S�\�0U@d\�ȇ\�}!�N�\�.(���y�6L(/S�ēs���7Ȟ�ΥJ�4?J\�\�P�g|õ�5t�v��\"1�c&5:!ߓ\�!_�?BC08{�8��\"��z7&�u\n8��\�B�>\��\�\�X�#\�\��9淗\�\�\�Ǒ\�Z�bq@w\�\�	t�(_&\�GG�\���v|}�>\��e�0\�ql��\��i�\�`u�\�C�3RLc�\�,G̔�m:^vv\�G��r��h�?��_b|K�İ\�p��x\�Q�\�ȴ\�%2\�>�9�Ŭ�\�\�Bc��\\\�>+��\���N.E깵�C�,���ur\�W\�⮀}!!�%�m���\�\�ag�!o:��p����!ļ$D��	\�XX\�+cM�G��r񘠋\�\�i\�@�\'�c����{�Xo2\�\'E����@�L�g}@:\�A4�`���b\��<�gQ��\�L�/E_�\'����\��\�\�fJֻ�\�\��U�6h1-$�߿>q�U�py�ϳ\�z!�+�Er���y\�5�n��X�K���\��Pp:CE�:�jc�^\�^K%|�I�\�8�~1�\�g�o\�\�\��U�ϭ6 (d\�Б\�\�\�K&�}G�\�\�\�u\�\��i@^\�\��ׄ�+l� � \�\�`ן党}+>�\��:/#HSV\�l\�oݹ\�c��:���\�w��p���z_du�2�\��l�v6\0\��\rB��\�	|;�	c��[7V\�\�uc;p�U�x7\"\�;M�#=I�M�k��\��Qމ6r��hB\���>�W\�\�C�裶ϏW֡Wq\�Y��\�\�+ܪCꍗ�_\�٣;�AW�/�\���\��\�BO�r_\�\'\�8�\�\��3\��\��\�S���wľyJ\�;4G�}�6�\�W\�o\�#M�}��\�h\���\�\�\��i�EA>��y\�s�\�w&�7켎�=�\r���\�ш\�\Zr\���\�$�\�\���`δ\�K�\�ݟ��}\�Ho}�\�tc��~�$�x\�\ZX\��\'<�\�\�l<�\�\��d�\Z���-X\n�c$��r��=_��pz\�$\���\�q\�iD��m�X��?���-Ǩ�V�>�X,\�*\�\"b\�]�$\��\�G\�ԡs���/t\�\�4�d\�u\�VPV\�Ⱦ��\�i{�;^ʾ��-9Y�w�\rK׸1�>\'!\�*#��A슺c\�˅O45����zĳ�8��\�G\�a\�q{w\�\�~\�G�@W�_}rb;=io��%5\�\�y�}��\�%\�\�Qm�\\�W�sC��I\�)G\�2\�C?h��T�\�AH�\�S?\�{0\�c��\�b32\�G�|tI�`�%�/-]Q49&w���g\�\�\��-F\�\�\�j!ƛd(��\�S7�1*�9\�	A\�Ca��ŷ��̅<��\�o�\�\�j�?;\�\�4\\��\"�\'�D\�7\�\�V\�k\�\�U �q_�����s݌f�Lh\�\�_\�\�QC���\��ɕK�s	@=ldC�B\��WK���\�{r\�<��0�X\�Aod3N�\�NGd(��\�\��P�\�	N4ʩ��Q��\�\�\�VĬ\�&$z�Fw�\�4�<�V\�C˗���IWKN��\�\�{\�$t\�kY�<�D_�\�Q�\�Tn^S2 p\�;\�+��\0�t�a��WX\�L4f$|\�s�O\�-�)2�Ā/`�c\�\'�J�s�Co`��*?x\��\�,&)	/\�}I,��Ɠ\���T����Y-U\�\�\�\�:\��#\�\�\��D;�\�\�|17�a�g\��\�/��\�\�\�I����\n\�Q�\�0�k�	�[��\�~A�v��,�O�c	FJE<�ȚK6\�J0¯\��锪��\�\�t-3\��V�[�ƛ͚ګK�RڮT�3\\\�fOf٢��rݦP\�wo�v\�<�\�=�Yn\�NU~N\�R��v�c�0`.봜e\�{\�Ƶ\�h���]qW-�\�\�\�aU\��\��\�6j\�|��\�\�\�\�\�Ŏe\�j� T$�atS?M�W�4�\�2�\�OF\�vܳ���ζ�BW�>8fa\�j�K��\�H\�z\�n��%׼j\�bw}�\��\��\�즹�)��\Z�F\�\��-R\�Cs�)ws\���s\�G�\�a��\�a\�懼�\��Mw�\�\�Y�8\\\Z\�N\�hpw�\�=��;eu\��\�#\�dv������\��2�V�\�YM�ů��VENe��\�e\�)�e\�+S��h�\�|\'*j�\�mj�r�V\�j��7��ڱ@\�ϳ\�=5Ux@oẄ�NiR�L*�c�\�miU�\�]�o:Ӓ6\�-?\�S\���~��\�ǽ\�[Ʃ�X.��\�AfT�\�>{\�s�\n3��j\�c�g��\�\�:os�	�T.�A���U�he\�\�^\n����/\�\�\�\�6�R�I���ͅU�6ٍ\�\�}��ۛ\�\�	�\�(�(ϙ綍\��Q*T�\�Ir��ݔ�X\�\�(���I\�\�H��j��\�`uF�g�\�u�i9)^D�\'}I��\�\�s\�\�Km�5�\�`�0�\�#H:ߤ)j2o8_	w\�_�ްy������\�_�l\�9��lca�?m\�/?|c\�\��0�D�\�2>A�k�M\�@\n�\�`��W夥����Cqp_��{�F�\0�h�IR��?e�q��\\��a���rH��\�\�\�\�\�\���tsOF\r	67\�y\�j�,������ܕy���\�d�$�\�\�۔!�?��\�@$\"ԡ1\�_)\Z\ry�9�\�\�쪧\�_���]�\Z托�A�  \�\�c��\�6\'d��!�\�\�t��bW�/!E\��A \�o\�09�\�\�>�g��YȒ\��,�z\���\�ys&�{�o�\�-\�j W�cPRo/�e\�%�3,^	�c\"�0�����\�����x\��\�Ds&-q�+R\�\�H,\�RhX܈N\�4��(GE	\�b�\�\�p�f��ptM�\�\�\�g�;*�s;�u�A�3�����^�Uu�x\\B!\���{U���8-��\�r�=�\�ګVJ^2�Ac��{g��;ow�\\]�m_��]\�7��\��\�\��\��C�lay��ڽb�Z/��me���*�4$$�{I#��V\�\��`�^���K�\���N�]�\�/�۸o\'6Uߗ�@&�*��Rl��\�\�q-��\�\�.T7q]�\�z:�\n���FMd��Zd\�C�:�x���_⃵\'\\r\�\�\�e\�\Z�HJ���p���\�B�zͮ��N�\"\'���\�)!!�\��Nر\0�eƮ\�S>��P�%`�!o�\�t=�\�K\�J(�H�tR�}\"G\�\��c\�p\�\���9>\�\�U\�\�L6\�\�\�\� p�g3MQ\�\����~�\r�n��0>\'&\�;��\�?�\�&!�����\�.��\�\0��	��N\�Ĳ�\�r\�\�a\�F �F05Rh\�ȗ��\�\�H� qR$���&�HP��蹛�*)�$�?\�G\�d/��p��P2\Z�(P�\'�QI ���\�\�\��\�\�U\']`�\�@�\"H܈�0�\�	8`x�P��L�\'�Q��sB*86�Q\�\�H\Z\�<��G\�~\�/��	����0\�#H\�\�A\�/Pi��\�C�\"\�Y(Д��*F� k\�QĤ�jn\�X\�s\�\ro\�H0\�ГX�\�D�eዥ��,�����Z��zw�A\"��z��ajjb�:���<?�\�	yC~\�0���0\���rN�!y-��|6�\�\�*��W\�\�p�\�7�<�\\�\��df�~?�=@pм��\�:k\�+�#�\�cA�d:,8I3��>Gѹ\�j��\��\�Ζ�\\&C�\�\�_\�r�B� G\�\�\�\����(Y��� z�Cy&�\�F�<Pmr?~�\�\���\�\�׺��\�B\��@�4�4>\�b\�E(AOX�[K���|\�Ĭ�Ter~�\�\�9!\�r\�k��\� vr &\�w�W\�\�97�.�R5�V�N�&wܗ\��x��\�_\�]G\�~qD3\�f�V� {~\"\r�Pmr�}�\�\�\\.\�H\�k��\�n!v9J>\�m\�N^A\�ƀ��\�\�{�@!�^`=&\'e\�\�J�;�ː\�\�fE>Ͼ(��\�#d��D��#\�vvq���\���Z\'s\��FM�$Z�z;W`�|tV�M\�\�o�\�,�L�\�cv8�[b؛\�l�\�0\�+����A���,�뜔�=\�:�Q�Xer~0F\���(�\�\�_\����\�eQ\"M\�V��	T�u\�&��Y�C��\�F�\�L�\n ���Y�yYv�\�d6f\�\'\�b;ΰ�\�\�\��9!��փL�7읯\��`K��F\�?no\\:x�T\��\����1�+\�)\�\�$[�E\�V��^2��\'v}2#}���\��x$ȅ��d\�%��\�\�Z~\�s=?��\"?\�\�D�\��EC,�A%F��\�2<�L3�\�O�\��\��d\�3t�+��o\0x�t�\�;��m�q^$5\n̋\�\�~���y=��;~]�EC�e\Z{��p�\��=7�\�p\�_�\�)��{�ύ����ƞEd/�\�꣡Cq����7T0jo�k{>F�\��\�\�cv�y�\�DĈ�/�]�?�8�Q�9H�פPb��X�OG/\�B��eՄ\�(�v[�Kĉt!\�/���\0\'�\�:���\�\�\�/bo����Fj�<��3 9��k����G��t0�}CBUN�\\�c\�ⷽ��\�S\�j�Z�\�\��e\�\������HI�\�H�\�e��\��8&\�\�}\�G��X\�=̧\�\�1\�\�\�\�9\�@\�Y�jt�\�\�\�\��}i\�#\�O,\���\�Y����)��\�\� �\�J�!�\�\�%RE\��?\�Zꬔ��8o��\�qH</\�{P\n�\�X�\n�L(�\�ړp͋�\n�\���v���\Z�Q�ʦ����\�4D{��6d3�\�\�lA\�P���6\0H\Z\�\�8𚙆�\�x�*��\�Y�����d\�~P�����*�\�\��\��o�\�?�.\��h҇~j\0Й��\�;�J��5�\�E�U_���o�r�q\\�k.G\���\n\��\�U\�fy)/g�ߪβ�Y\�Y�\�7�!�+�\�p�=�u���F�@\�.���y-�l�2\�\�\\�1�\�?\����\�.?2\��>��S?�Y�(t�����\�\0	sÐ0�\�~��f܇,��uf�Y�\���\��`�l�f�������ƛ&j�\�\���/�Ϛ���\�\0����D\0\�p�\�!B>\\dM�^wD����E`݊,��!�\��\�c�\�9gS&c\�;\�ת�\'�\��\�l�\rE&\��-\�c\0��\"8 ��� :\0�����n�.���O�<�>hG\�L�`{�@��]&9\�W��\0)BB�X�׈.�(�7\�N�t�\�r�e���<\�\�30Y�\�ވǰ%\���\��m���\�]��Q��8�mx\�ЯnL\�\�.B\�\�\�#\�\�(� \�7\"�\�QP\���%\�R\�\�j\'\�@9ן�%eĴk��\�\��s�C#�a\\��A|\�\�Z\�,ð���\����+.�v\�\�q�\�\�PqC\�\�`\�!b\\9�.9�q��� H�Ĵ]�^�[\�r��\�侕��@{$ø\�l\�\"�\�\�ăU�\'Ad\\YbGt\"��\Zq&e\\Q-�\�W��p%t�g��9)	\�\�{Eu�yT�r\�]\"\\�3bI\�p�\�ٴ\���X�\��\�2@Õ����-\�Q��\'Ob!\\V�o�%��\�R`*Z:^�}R�?8��u6b\�Rq�|U\�\�\�\"-�?~@\�%�>\�/�.%B�\�eŰ7벖,��\�t(s���\"�fA!�����\�.\�I\�\�\�$?\�l��ӊuYn͛V2�\'�`x�!�w9\�ȟM��cED�N������K��5(.Ka=�\�2�l\�(!0ް]=^Vd3A�X)�eQ/�Mw\�&��\�bkoIp9\�\�@iL�\�\�\�\0�ִ�l:\�8\�AH���$0�mDC7�4Q\r\"Kg�h;\�\�#+F[�\�>��\�ge\\�\��C\�k�\��\��B��a\�T�\\I= {���\�\ZbA���0\�D$���(tā\�\�|�$�\�c�I@\��{�j5�\\�*j�*=��r.)j\����\�<Q\�ˉh�\�q.�\�w�k\�{�a�7Q�Ȼl\r�v��FZ�\�r3�\'_d\0$�xM>H\��t\�\���N\�\�Y�	�1�@	^9\�d��7	mqGf�|>\��y\�3��Sa-���h\���18\�y���\�\�D��m�D0_�?.D)\0C\�\�\���\0T�&\�1�,\�r�uU-��\�\�YJ��\�\�(\�P\�O�\�jx�\�\r��\�\\V\�\�\�\�c�}���l����\�����\�y/���c.�\�,�\�\�K^#\����\�K	\��f�ea�R;�J��@\�\����}�PH.�\�U3PƨS�Q@�d\�\�\rq<4���*!���\�?\�c�]��Ayf=�CH\r��y\"\02&hʅ\\ps�ǪJj�lD�Bh��q\�GO�%y�w�.��qv�\�|�S��\��H��\�\�`���\�<I\"�ﲎpִ�nًG�{Y#n\�\�H=���5<� �\�\�`��:V\�\�\�\0\\�\�\�g`dY\�\�@q��G�|\�x[1G\0�o\rɌC,ɄK�\�Q��\�\�v\�j��.q�\�%d~2wD\�\�цL\�b挀\�\�P\�5�\�E\�\�g3ΐ( S3����6< �\'RH\�=����4��K��\�g\�\�]\Z\�@$c@�~LQ5M=�JO%;\��2\��m�\'��E�m3MS�\�\"��m��#����{bd[����\�\��B\"Fv<5��\r�͘�\�\�L\Z\"��\�d��\�\0%D@IW@�\�\�q�@V\�E\�S�\�VJ\�[f\�d.!%\���\�+�	%��䲛�\�`_\��\�4m\0��IT$���z�X$\�q(�D�8&N�J*V�J\�\�\0\��rӼe\�F+�B\�\'\�2\�p\�\�}�\�&�$�dPfُ��bh\�\���`d*�na\\ns�~�\�}w\'Q\�\��FV�3?��	q\�.z5x)�\�£���A\�G\�RI\�b�r�l\�\�:yA����\�B<�?e�?��n\0\�h�H�V@\�B��wG��\'\�\07\�J�i�\�\�J6��r\�᪕xd)K8d�\n/�W֏=d\�\�X��n�1|�F\'\�gՊ[�׌�>\�(�hӪ��^Kā�\�li�A�Zk�\�\�ɹDCۊ��\�\�\�d(�w@ӏ�\�*\ni�{0\��A$�\��\�^�v*�Q\�Ȫy�Ȗl%Y�\�sm\��\�&a�X�\�\�d���МX\�,\�\�\�4�G	\0\�Iq\Z9������T�1\���U\�XZY*�\�,C].H]Y.#�\�D��4��<X�\�B&�Gk�\0\�\�x��ω%�sK���F�-��n%���C\��-�g\0\�@\�d\Z9�\�Z�i\�b\�\�\r��Tk�A�\�c8f�N�F^,ȆH�a�0G�l��?���sh��7\�ڀI\�\� �!�\�\�\��D\���(�i\�t\�E�\'#B�t�\'\�F\Z��\��\�9�;\�@�\�U��͠�&�P\�Ld���i�\�\��3hsm���h�\��>1�o��\\[`f\\\'\�\�\�!\��e�\'8��;�v4�n9�����{px��X2�f�s\�G�q��\�GOu�`Ȗz���쌉D0��;	@.+\�Ov��\���x@��\�\�5�FN0{�@�U�F~.>T\��|^̍*�^\�L\�@�\�C\��_A[0\�HΪ�!7��:�s\�.\�x�;7Q\"P&t��l\�3y\"x�tG~0!x��|aB�qk)r�\�X{-K@k�if�\�ѩ\�\�Y\�=,ys:\�,Ȼ\ZMM��\"��[P�O\����[<(\�I�|\\3Ai���\�3HCNTj����\nsQ82�#�M[\�f@\�,L�>ˈ	\�`�\�ς���v��\��S\�\����\�\�M#\'�d\��\��CP�\���q�\�!\�G~2.��iI\�\�G\�4�\����\�Dpd\�\�T�|*��\��\�\�$\n\\\�Uc��\'���r�H�\�\�^�\�\���l�D\�y\�g߸\�`\�#O�\'D4�͹��;��P �l����[�HV\':\�m2\�b�\�x\�g\"n+\�㆏1r\�Y)cF�������\�\"4-rڱa\�l�b,^�\0�9�:\�\�\�9��u\\��ɋ�v\��\�:I��D�	o��J�=\�O���)\�%�F�;0\�1ٍ�\�y|�\�+\�$H��\0V1x�w��v`\�\"\�i\�\�Ó�\�\���p�[�����\�W\�k\���)B>;D*\�&�\�$z�\�ȅ%s!�\��\0\�=\�΃�\�\nz\�\��l�\�\�\�U=Hd7W\Z����?*Q�\�}��3����d��@>ա����M�4r\�Y\�g#~t<gh\'&\�8����\�ɤ ߜ\��hAT-K\r,\�\n�\�}$OC\�c�cl�w\�\�As�S��Q/d\��H\�%�\�s!\�\\�\�UV2.3�� oa��͑Wdx\�G\�)\�y㈛�:\�\�]�m�X�GN8!��\�T\�dᣙ�B��F\Z��h�C>;׍,k\�@D\�o\�\�e=A!\�\�l\��N\�>呂s\�C4�\�ȥ�,\�P�έa ~�F�-�<�k���]�h\�\�\�\�bLh\�@ݐ\�ci\�\��yd��\�x\�*�Zg\�\��\�2\��M��Z\�Ѽ4��)r�&�tv\�g�\�W4r���\�\�.\�:\'\�$\��cȒD<������\�Ǽgj�K�|>&��\�\��&��]\0����O�i\�\n$�\nQ:!/��*K\�#ٺ���|���?�\0��V�l\�D^?[]\��i\�\�u\�\�R\n���y\��<|\�QmC7*\�L�||�<Q/C>\�Q\'r\�c�R\�\�\�OpA�71Gh��@xP�n\�dR\�a\0I�ІN\�1, -\�l\��$㉅<_��.\�\�o*] 4�碴 x\�s\���$_T�`���i�ۯ�R�-�\�c\'\n\�\�^\Zye\�ɣc;G�X6����\�```A�\�\'����+�w�\�0M1�,/����\�\�m�\'3~�P��Ү�S>������\���)�\�\�\���J�߹H.\�a\�\�\�2X�dC�\�y\�H2\��F�][sm\'/\'H�_\�`y<\�4\�ó}\��\"�|<�\�?>�#3����\�t�\�AI#����d\�ё�Fd�����H��B#2%qݕ�CM\�kwjܡ)RPN`9�c\�	70/��q�\�{8\�p����=r9;��osh{\0�\��0�7����\�L�P�l�\�\�[64rH�łS+Hfz䥶QMЃDQ\�y�9��@�\\\�wBe!\��ހƐ#��<�3\rF�$2�\�\�\�m��y@	��|\�}O�bʞ!Ma4$s�q$\�\��!��\�,���|\�\0\�\"&Cl�\�\�C����ٽwݎF�c�\0y\�#ױ��us\�6\�\�[\�6�\�R�&*\0\�AL\�5\�f�|l%{�r�QH�k�\�zC�~!\�0Q\�\�$��\�7�\�틀1�h\�\0%\���ۑ�\0\��2Q\����	m��\�D\�e�l\�Anb�7Z\��\�%\�A����\�O�\0\�<T?\�c�\�E���F\�a�]��֘\�\�\�3�@�G0\�1��=�jX	�;r?y���\�F#G1�g�b(ߙ5	\n,�&\�\��s \��|�\\�9Z�|�IV3\�LP\��\�y��<�T��щ.\��\0�\�*-&0���Dҡ��̍���N,�|�jdB]\�\��\�\�ck0��弛\�aV\"c�\�>Y�$ٶ\�!�\r���\�A2�~\�8\�\�>�A�`\Z��x�\� \�/H�IZ`\��en�\�0Ճp\�Hv\'�~�\���\�\�\�8`�\�\�\�\�[�g�0���\�\�\�y(�d����^\'\�gh\�-�^	�<\�l\�`S\r\�:� o�=\�>�s��,�`\�\�9$�I�FdX9`��\�.2�r:㻊U��\���u&jpr\�\�f�\�Z\�\Z>���\��\�\� /,F\�b�hF�%�c+.\�7(	,���m=�!\�\�>���00�S\n�<�p@�\�@�^�L�%� G/�\�4�y�A\�^\��L \�UQ%\��\��o\���q]�\�\�\�dOpd�AN\\����\�\�y\��\� ��n��@t�d�;\�E\�1G� ���\�\0�M\�Y�Ϙ��\�î��k3�ͺ�8\����X\�e\r�h�x!Ɍ\�\�M�1�\�%ߎ�\n�,�&�5�;��&���\rt��s\rFn].C*lm����\�Eޘ1\�\��`Hhb��3\��Ѥ\�n\�\��`y�Tw�\r�f�\���\�FC\'���~��\�I0.��Q�A�|\0\�|����\�\�M7D1��\�00��\� G2��؊D\�y�ٻ��Q#� \�wL\�\��r\Z�f\�\�-���\�:�\�φ(�L<�\r���\�?6c\�\��\r�A2�8fENa>d��@&�+\�K\�$\�W\���F��-�A�c6�_E(��g��st84��j\�#\��A\�`\�c<\�Ν�1�<)óW2�\�d�T�A~`���Pd�\�&\�TH;	�}�O\'\�\�d��\0�A~_��\� �/��祋\Z#r���.�d���#�^\�\�\0L\�u\"�/(�\�8my|]2�N�oD0\�v��7\�\�y�\�9�*���xI��0\�\���\�x�r�9\�\0ZVW\r\0���\��?@���h�/0\��>\�����u$79g\���\��\��\�\�^\n\�ڐ��\�x7�a\�.�N�32l&\�x0�aY\�\�\�\�$�[0~�#Tr� 9F�!N<\���\�\�x�\�z�\\�\�˻\�#&χ�b*C�q��x�8�{��-�\�kqf�>_�1F$q\�ٓx�\� \�/Y\"�\�3^H&?�K\�\�y����\�G2휧I\�ـֶܽ���\�\�&\�*\�\�\�h\�\��\\��L2b�wl\� �/x\�G C w}^2A\��n`>��|_0��\' �\"`1���7F�@�\�\���� ��\ron\��\�\�33[0�(� ���I6\�i\�\�E�4���\���\�\\\�d�\�\��^퉳s\�R\�K�\�r��`�~Q��\r \�-\�.n�!�PL�#-AS\�\�)��r\�\�\�8�\��\�!o,7\�4|e\�Zl�,Ƌ\�6\�\Z�}�s\�N�\"u,h>����|#w>r\�\�M\�Yُ�l�\"�<p�!7%x5\n!�\�?Z�&\r�ݵm\�\�&�\�;2\��K�\�uD�,�A&	\�R�!�jm#��ܢ��Jr��@i&r_�!�H\�f�#v��0J\�\�r� *��l�\�\�A�FS��y+��_(G��2kQ�\�\�{�i8���\�\0��P\��%)�Iy5�\�J<\�\�~�\"d\�w\���Oش�\�ۥza!\�\�\�^2@�8~\0�C��͢\�\�-=!}\�r\�Z�eb�xHk��\�\�<�\�l\�|��\Z\��x��[59N6\�\�y�y�V;\�\r�\�Ą8tF\�\��W*��#�\n\��t.+�X�ek�$�	\�\"\nRv-\�^�ż(�\�\�ݤ7�\Z���\�L�\�Tf���\�\�\�£_\��g�h^\�$)��^��{�\�s�\�\n\��\��c\n\��~�\�\�OJ�\�Y�V	bO\�\�w�\�)�]\�,ւ�\�\�\��|�aJ�\'��ELgf��de�\�t���_��&0T�~���r%\�\�i�^\�_��M����\�\�+U ��?^\�G;W�&+�B&\��g�V�����?�3%*����3A�U�=���0\�\�W�\�\�N��24�}�?`�|�W�,\�ȴ��n\�9�E����-j2`\�\�ϋP\�\0����\�\��^��_\���\�\0ٵ�\�0dU�=N.d�,�\�ɒ�P\�Wy�(\�Ȣ ���\�\�f�BA\�\�;�P�o�_��v�S[\�وge� \�(+>��ힾ�\�\�`K.K�<%?\��?�{3y�\�\�*�*s泴�g�V�]�dx.�\��/\�\�\�\�S01��)\\=؎�N�\�`j\�(�X9��Ll�S-�sͫ\�\��,nޔ�_{�񡛷0�b�w��´�O7��\�\�U�YY4ᅢ	t�\��х\�L}m�(�*\�o�a�V�\�ҿy$Y�\�O�?\n�$+�9�Ŀ4\�\�鱯b�\�-��?\�4&�i\0yB\����\�y/	�C\�\�^\�\�c��}���\�	\��\�\�\�$���{a�o��Ǐ�n�����\�\ZI�\�U\�\�\r2�\�\�\��;ij��F�yA|\�7e����B\�\����ơW\�ƹo��s^�ڍ�Q4?)�Au#��\�o���\�\�A�\�7(\�x\�!\�0�����O���|_�^6\�lb��}��\�aH��\�d6�\�6��:n�\�\�\�`�\ntn\�\�\�^c���z�0�Ax\�#�\"LB�\�MN/\�\�E��\��\��\�����!\�X��g�r\���`\�\�u��%�Z�2F�@\����\�\\^f�\��e�z\�\"KE\��\�Cg\nY6Cla\n%\" \�\�ku�-pR�d� ��\�u���\\��\�\�\�9=w��3k:���&[��Y��\"@Nrx<�/�\\\�J�,�}\'t!j���?�\�w\�\�G\�hg*6+�̓\�c� }<\�*\�\�c�h��b\n�B�P!�l��\�@\0\�2a�H?�\".KF�\�p�\���\�q�\���@\��y��1|\�]V&��A���bx�ٜԵD�\\��A���2���\�\'2�\�\r<\"|Z\�;�a\���$\�\�P\�[\�v�Tx�\���t�lgI0e��o\Z皰�a;�����\��۴�}}�\�\�\�\�\�@\�\��=\���\'�E��\�\rY�\�P3z\�\�g�\�\�\�\���68\�&X2\�\�u��RE�C\�\�\�$��ڿ�q4{\�T ��\������\�.\�\�_(\���?R��#���p.\\��\�!\�q�t\�\��?�\�\�+L�O\�;\�a��7�\�\�s�\�0�\�\�\�!\�\�+\�\�u����k���\�R�\\����\����0xI=�?38\��\�\�mnxI�Z��,@�\�8\�n��+x��\r�&fM�\����=!)?q̚U-W�5�\�V*U�Z�se�{=&|y6$\n�Q�\�֌��\��)b;@�S\�6TX\�?\�l:<�\nz��_xjv�.W���|>\�U�\��b�Q�\�(��B\��:wm�y�M����\���B\��mj\��\�\�P�\�\�4�s��Z�\�\'�\r\�t\�2Z\��l>��`�)\�\�{�\r\����c\�\�\�9$yr\�)j\�\Z|u$�����\0x/okǺ�`\"$��<������Gb[\�1��,\�\�\�\�ZD\�]w\���\0����W�:\�So4\�.A��@\�;�|����o\�2�?���o�>\�֎_�@}���%\�T�O\ZN	\�+YyS\�Y\�\�n�\���1SBB���\�\�h?#,�G���\nD�(\�h�\rY�/Fy	G\�^[R�/15��hG�\0ogb�G\�\�z�p�R�\Z\�ӖT\�$�y��#\�Z;��O\�t�a���^�l�w\�;\��l\�\�\�A��$\�s�E/\\\�tH�\�\'v8��\�Ĥۙo��C\���v\� ��9i��hqt\�7�0�H�W�^s\�\�~�]�K:I��]\r8�&V3\�>\��\�\'��l>\�\�[`7�d:�\Z��\�\�0�g\�\�˒�~�\�Q�#m����o\���$������e#\r�KǷ\�\�H��f%\�3�a8��\"@`\��7z�\�\�2;�v$\�\�&(���4\�X�\Z\�k�/�\��\�\��\�i���\Z�.|8�&�\��\�\rsb\�FȻ*�\'��\'T��T`�q��V���}���\�/\�q���mߑ�2L\�z^֎k{3�}\�O�g�xP~���9�]����+ծ+z�\�O7թ�V\�_�rq	�]\�\�q~�L�\�p6l\�\�}V>6\�k��+��լ浡]�5�f�Ʈ�(\��\�o81�|�ᖜ>\��+OM-`�\�[r[��e=X�T\�\�\�1uIe|��Wl\�ou\�C�\0	\�hGAlk��4��\'Ӟ��.=żƬo�ӀP�6!Q�$7u���\�\�V�\��j�\�)�#��\0�\���J�~X;��&\�Q=��kqDD��MV�\�nWb�\\��|\�\�.FЦ�+�������{ߍyƔXG�\'s>>$.;-\�|�g������2��q6c�K�4�w��\�f8OHK���� \�,�B\\�F{�Lyo�g\�1:�`�\��|F:>aTTp0�\�JB?��l3�K\\~�tx>X\�^B>�R���\��M0Z�坕����9U\�ޔ��&d)\�\�\�\�]�\�\�\�+#߁xMp,PML�\�F��;.\�\�\�\�_��\��fl�0L\�\Z�K\�W)�4\�0�!c�>��\�V\�Y0���\�\�k\�Ө\�\�\�}\�U�Нݏ�s�\��}w/\�\0\�\�\�8&�\�\�H��^>��\�:O*��rcN<A�0\�\'�5t�v��_#=AM�\�����w��\�l��\�\n��\�\�)\�\����\�\�=�`�;DQ+B�s֌\�a��j\��\�}�I���[�_���\�H�\�Ž��\ZeXh\��Wo%�b#\�\�\n\�`���}:��߹�;\�P�x,�.G&�oÂ\"\�\�\0��	���Í\��\�Ȗ,}F�#\�h��\�I�L�\�JT�\"Zpp�@\�h1��tq�Q\��{\�\��F\�OOR\�\�\�_H\�\�\��F\�\�DR�7��g91\�\r-<��\'\Z_\�u/q�Gg\�j�Fa�:I�y0�\��b@��\'\rt|��py&I1Dwk��������_d�ph�#�T<I\�_Bv\�y42�\\Lv���.H\�\'��p����6 �L@�\�B\�.[�N�\�\���\Z��\�\�\�\�T�.E�\��\�Brx3�\�F}��(^�\�$�zͷ�P�5OB���<	\�^p\�$�z\�G�P\�5\�OB��ޙ	\�Bj\�qI���B����4@�\�\'�;أ�4x��uVbq��ֆ 历�Y(qD2%gd1�\rX\�L�o\�\�\�ݙ;\�\�oP}�P\�$�2�\�U\��u\�v)s�#BH:����_a�l����ZԱ�\�p\��\�\�H<j~R8�#8\�\�ӡ�q\���KLjDi�LtP�xU\�\�G��s�%Qړ\�H�r\�\�2w�\�)::\���B��\"붽���0\�\�\�Z0-�\�v\�d������c:\r�\�sc8\�g>r5�(X��wW�E�C\�G;H�\���,fa9�\�\�N�\r3\�ׇ�\�\�P�N��7{j�����\�,č��K�l\���� -6\�x�%�9\r:�?��E�\�wC.;s\�A��A\�s�\�|��VH�\�F�Iة�Cx\��;�4\"�	^�j\�\��-l�f�ۘ)	\"\�\�B~\�f�,]�࣢\�H��7إ��5����/�g\�׆%ɣ\�v\�}���j�\�\�#��n\'_t\�\�L:�\Z�%5\��C#wbG\\*�y\�\rT�a�\�S(\�h\�/�\�PO*�\�\'�;|ϫ���?��ו�0�\�?���\�P\��/\�6a�\�\0n{�G\�\n*\�\\,$I\�\�\�<�x-x\��&$Vm\�iL.\�\�x�\�a�`\�	\r:�\�.\�)�\��\�Y5��@��\�Z�5bS\�\�u\���l\�\��0��d9LBM���m\��M�\�\�Z5L\�\�`�\�q�@W���q�\�\�(^\�\�xD\��V��PHK\�\�O\�g8{Q\�:���3\�\�bS\�7�y\0B�\�v7��\�5�/ȫ`3\�l\�\�ǀ��-��C(�C\�\�\Z�H �\�\�J!ל��\�\� ��o �\r\�\"\�0:��\�\rK<[�/`�	\�\�}\��LD����/2���\�F]\�(:�u���&�ⱹW�\�\�\�\nY�*ý/4\�\�:<�@��S�dQ7�\�6.��\0Q\�oH�u6��{�ٍ�$��\�8׽��\'�JA�\�\�Ξߝ�a*���LͳI�\��4�_�8{�E/!�\�FU���`_ϱ�xy23\\a\�\��!\�\���c\��v�g�4A�|����ɦw\r0��\�H�\�{\�D6�އ;��*��\�ݘ\���~�IA�+b�\�P���\��/�\����O\�_{\�\�ۅ19��.\�\'����\�\r�\�\�$�?\�d�T�Y��c�\���%E�\��m\�q\�\�_%���S��\�\��˜�E�?p\�\��ϗ+�\�\��a��\�\��x\�,\��o�\�t���\�\�=�\\���\���\�w�[*�?�;�\�\���\���\�\��{\��m���\�{R���+7�\�5\��>�\�.\���\��\�\��\\��\�x�_i�o_�m���t?�#���cb�\�ob1w�׉\�m\�\�\�.d�o���ȿ揑�\��^�\�oұ��\�M\�\�;%	�\�s��\�U���խpj\�\�?\���]\���s����\�7������\�\�T�\�	}8��г\�*�̤�w]�\�(X�\�\�&W�(�S\�J���� �	\�\�\�9!0��s,�\nFb\��R˞%��ؘas�	/ƣ\��C\�\�[��>-�O_��Q\�:��\�\�w\�S�*O���(\�y-�gi\��>ɺvk\�\�H$\�^\�XE�	�\�p����iS\�q[�j\����\�J�~n��\�{�\\�\�Ҽ!r?�\'�����	�V8��$\�\�U��\Z��˅\�=\�MD�\0��\�֔�ˆ�n\�r3	�\�D&_�\�\�b_��\0�\�F\��`E{�r�j\�I�EbY��k���S���$�SÇH߮\�a�\�z�\�9���*!�p�f�2�\���9\�l\�`\��\�\�K�H(\�.S\�\\�4�pb�\�f;�\0�ز��\���\�?P�VV\�pF\����g�\�\�\�X\�؈N\�v�r[Wg�@\�\�\�Z4\�\�ʏ�\�1Qڶ\Z�;\�\�:j@%�\��\�d$qJ�\���z�\nfn�\�\'\�T�ǈ\�|�е]s�D\�\�a\�J�b��\�I%�A\�Z4Z�/��u\�l:Ypdd\�R/�:��5?i0\r$\�7!Vep�4[-\�\�F\�\�ݟR!fX��Ƹ{�DH�&fS��\�ɮ?bsMD\�\��\�{�� �=+\�\�	\�\�	�0%���69\�= tD\�8�U���ch��2\�b���\�	U\�\�V���J#�T\�$����0\�\�\�!Ӟ�75\�7�7\���v��-���l\�Z\�~\�G\�aJp\�g�R1(��P�i�>�\�\"Z�~����oUU\�2�(M\�\n�,o����w��\�`\�S�\�\�\���΍ӊ|ALM\�q3\�!L\��\��j�*n�DGpJ��\�fx�u:�<Φ��7\��9;��T��!H���ʙ^�\\�6\�\�\"�\�,�7�\�̡\�*\�{\�G�\�Nu?d\�@�\0\�=݄\�iS\�\�B\�\�m�\�$��\'`ppJ�}.�\�8�Ro�\��؀`\�\�=�7\"��\�(F�*�c`9}��\�ź�\�\�<\�a�	\��*9�\�\�si_\�Aw�Z\"*�a#N�P�*İL\�(��\�\���\�7�W8��\�\�5|�l��\�_?\�Y;��\�S�\�<\0\0{��R�\�\�S\�ē.�}km;\�\�y�\��\�N*M\�Ji�I-�\0Q`\�Z�Wj�� I)��r\�^�D��K\�O����=�(6\��f�6������Վ�\'\�\�ՀC;��\�\�h\�g�\"\�|8N�o\�\���\�zL�\�5\�\�TN\�7���ԍQ�ɮ\\��\n��\�[H�\'�\�r#�f|ॊ[I#\�\��\�gp(��\�H�\'�P�!	\�%�\�#s\��\0\�8Z�E=k\�`5���am\�\�up�o�T-p\�\��\��(\�\�\��KxO��\�>(�қ	�\�0�\��\�:F\�No\�r&_P��\�`�a�4A�\�-���F\�\�|=f%�e��\�cM�ɘl��3<r\�㶕\��[f>��o\�0�9D\�\�\�`����b����u�k��B�H\Z�2E�H\��w�$P\nr\�b4���ɂ��Y\��\�\��\�\�n���\�\����ť#q�f/����?8�\�Z�o��L�]󚓮��\�\�>̶�ԬHWT�*p���A.�5C�rb!�DC7%R\�\�,���Ψ�\n��\�@\Z�i����츱K\�*\Z�\r�\�\�!\�}\�q��\�\"*1`����MNH\�Vfq9\�zj0�)l^�\�\�bXl�\'M`wTL\�,�*d�!�B\�YȄ�^n\�C�bm_TRq+tMz��\�u\"4\�$?��U��\�֧\��r���\r\�GYEa\Z<K��x\��]m,M�7�\�i��5Ԝ�׸7H�Xr�\�4\���^W\�\�\�\�Z��8`X,��}\�9V()F\�F[\�WVY�ˮTF�\r\��z�5\�\r��;f�w1\�߸�P��\�h�,��9>��0�V�\���h�\�\�ap\�sD-$6\�P��O\�\�\��,	�\�T\�g�i-�\�o.�\�nT��3ű՗�U\��%�x���k�aLyp�G�\�S�y\��3-ض�\\��)mC3o�gm\�C��l�(�J���՟\��4��f\���ל��\��>\�%6��\�M�\�\���\�I,�3d�=\�\�K��,��	u�\�n�\��l\"�h\�U\neU�SD\�\n���\�R���\\\�~�^֗\�@L�K�3\�\�*J]�|��\�~Đ6j\�0��\�i�b\�-K\�\���\�\�ii�\\ B��V\�\�\�\�*WU<��\�S�j \�\�;�3�\\��G��\��!���qH�J c�\�_I�1�P\�v��Ct3��45ᗣ͍E��r]�����(�LF�\��:f~�\'�F�t\�S\�֓=\�\�\�PB�\�ӭor_\���\'X��i\�\Z1\�BǟO\�\'\�z�Uy�\�����1odN\�\r�\�	*ۏH��\�u	|\�T�N�\n\���)��^gJS��k�\�V�\�46\�\�\�l\�M5\�$\�D{!\�ĝ����q��G�?]�-޶-طj羏ʀE`%��P\�I\�\�A�}@�o�vƿ^A\�JߢkѧJ\�nf\�I��0|\�^\�\�g�\�&mg�[\�çjп3�\�l\�u����\�A�-��`=%;R�m%�\�$���(!�EmW��@\�1E#�p˯�O~\�ʛ\�/�.jk\�r\�����i��i\�\�-PIB�R�f�7|�zjr�kps�\�&��\�\�L(5w\�s���F!\�t^�h�:�lN!K-a�)���9\n�<����?3\0\�V���S�\nO�4;�\�o|\�X��	�\�Z�}D۟f^\�\�1P߳�/�V8��85�deA���L:�i�\��zq\�a�\�\�\��#`�g��\�oƱ�ϔ�P[�_\�vo���\�\r/\��J˯�A�f\�.\�\"B��\�aF\��I`>Sd -W��K�C�\�ȴ�\�e\��t\�=>&�	\��U�n{�S��9\�\���	\�TH�\�׭M�C�04��p��\��U�i�t\�\�;\�\\���P\�r\n������Ш	�@�͍\Z�4�E��	�V�\���:�#i�>\�.\�N\�M�ܿ<��+9\�P�Q���\Zf�b��!�0�wm�B\�w\�܆h��\��\�\�P#�\�/\�Z��\�\\0���Z�X�%=>AB�\��[:\�,!JF��\�!�_���w�=y@\�<\Z\�z{�x�|F�ʖ)Az��\�г�\�\�\�\�V:�x_Q�\��\�f%#\�w�g~L_��\�\�\�5(��\�\�$\�\�\�\�3A\���1_B�.1�ݍ��\�\\~Y����U\�\�,U(8��\�d�L�ϧ\�c\�F\�p�m�\r\�\�\�1�o���\�o%�?�g%Z�A�վwA�!�-7%v-їsΌ\��*�\�\�,\0�A\�3r�\�\��,�\���\Z[\Z\�1\�=�\�\�\� p��\��\�b�Y�M�Ҽ�(\�.�\��ƀ6�t�\�\��:B�Zl���	_�\�3�\�X\�\�\�\�\"~�\"\�,{�\�n�(\n1 �8\�#g7\�\�1\��H\nd�\"�����\�\�l�$W2�)\n�ٽ \�\��~�fR�ʏ6q�>U�\\8�\���p\��$^\\��ܲ{\�\�\�8�j.,ȁ!Vqe\n��4*>\'\�09E��p������M\�R\�N��[Z-m,\0�\�Q���l�3\�+1^\\8\'�\\\�ew�Ï\Z\�B�V��\\\�=/u�Bú��S�\�eAQȆ���v_��T0\��ꇷ�w,i\��Bp��2\�E\�jH+$�JM\�:��l\����<\�f��\�\��\��\�\�\�]`J6�Z�y5���~Mq�k/v밿ʗ8 ቘ`�\�>4.\�?�r�\�=�>�k���5]\�MZw\�\�\�;>\�T,IA2l�,��l\�~)\�\�e@!\0�yX���|�����\��ʊ!�T��}��S\ZDT�d;�\�\�\�3\��o�V^V}\�\�\�\�xVQ_-\�o�\rfM*۫�Zd\'E#�%\�=��+h��B��\��MY�\�\�\�\�dY�\'���y\��\'��X�\�\��v\���B\Z	\�\�[r�p2�\�T�\�(\�\�\��:g���Z0h\rb�hqBL�u\�\"\\�\�l\�F�X�8o(\�\\�r1\� �{\�5\Z\'\�5��^���u��\'hH)�ڋ(z�x�\�\r�\�O���)0�\�[\� ��e�)Iꕔ���\�ِ����z2�\�u\��3\0�\ZFO?ȫW�&��\�F\�Ⱦ}��,<��$벅M�ޡ{b\�[\�c��\�fv9���\�=�\�Z��b\�8�).\�t�:�~�\�\r/ \�~�}��$p۬ג�4�~]KW\�\�n�����\��x�I�J�\�c<&�\�f��\�C�*p0\�\�,eNpX��Y\�\�\��X�{\�F3�\�/�׋E\�e�\��ܛ��\��\�>��ۮi�ja�3h�g�z�3,\�t\�i��̠\�\�ĉ]R{.ct�x&���S<Q�}@��x�\�A\�Hr^\�Q\�ȿ�~ɏn\��\�\�f2Rd��\�\��o:��Uq.Q\�%^^���M\�/p|\�q\�	�7ز��R�)�\�`q�\�\�4�\�?�;w\�\��\�/w\�Beey�\�J>�;\�T#i�d�\\��\�\�<wj9O	�(}�Q�&c1!\���\�~H,0�FGo�\�7\�q�v��8\�F|�3�Y��\\d?�Se�U�p9��I{\�ͦ�sn㒯\�ϕ�w>�8ˋwd�Vx�\�\�}�G��%�b��C~�q�!$��(I�3��4i\�t��[y��EY\�a�˦�q�5\r�\�W��H6Z\�@�����\��}L\�\'\�A��\�\�\�9y\�\�щ��#\�C>��\�`~n�*Q�Jl�\�\�\��Ԩe\�l�)%\'\�D\�R�d\��Ò\��\�im�C\��\��\�o�wlPzo\���/o@n\n�cCK�\�z�\�gi\n3��_\nb��AL�g�\�W|\��H�V�\����T����҈\�\�A\�\����\�_a\�\�\�\�=\�\�}�	�Z\�ܡW]�1�t}Ѥ\�\�\�~�>�\�6\�֮\�n\�fL+�\�Wث����\0��ɻ�v��ζ\�V�%Ƈ���:�\'`a\�\�\\\�m\�7�,Q�|�\�\�\�T\�,��\�OXn\�jj��\�\�\rQ�Ny%x<�\�a�\�@\�g�\�\�U\��\�}��>	H\0\�s�%�|[Yp{�\���f��\�g�2�*\�5���\�V\�7}\�՝\�\�wE�r�\n;߂�\'$�:���ή��\�\��`vwn�\���%%L��\�iw\��^\�?\�\�ZU�8:\�\�\�ٞ\�P1���\�a\����\�ɴ@\'�Ь\�O\���\'(K]P���lڈ3\�%w+Ob^�y[\Z&Z�vWQvo�v&\�i�\��\�V\�=`7D\Z\��>Z�O��\�0��3���Z�D�?�Q�r_\��DX\�(!i�T�\�Ц-\0\�\�v�;L[a|g��\'���� \�1�\�s~˿�\�H\�ۜ\�\�o��\�v�\�R�\�\�\�n�Mw��g��l(�\�\�BX\"O&E\�i)�l1\�>IW<�ҕ_��\�{h����<�&�=Or�ङ`YƜTn\��\�4qЖrI��\�\�؎�:\�O	��@l۹g�\n������7��\�;Q\004�C�y\�\���c2�\��A�s|CR��zژ\�@��9NX��p�$\�{\�3�M[z���c�,w�̉�6^�1\�Ɯ�\�F<�\�g�>.8\�\��1N\���@8��еv�p\�$L��\n�\�型M\Zck�\��Po�?\���\0��7-\�\�gMeR�����g�\\�\�\�\Z\�?��	\�v]I�\�c8��m�㍇\�9:\�\�d�/\�����\����I��\�C��$8�#\�\�$.r^\0wX(�|�W: mL7\�s�h���+E�����mm�;h8�}m�\��ҵ_A\�\'��\�˻((Β���3x\�+\�\�x�z\�\�\�n���\�n��`�rF}\rC	b�\�7\�D\�\�\��3\�]\�.\�u�u\'`R4mh!D�\�\�w��k\��?�8K��\��)\����\'\�5\�\�\�\���\'~0he���!C�g��\�Y����D���n�\�v\\�\�_��3}\�h��v\�\�j\�\�s=�c\\�_x8���O��\�9�v�)eS�\�.�~��i9��4�\r%Jb\�\�G|$\�\�\�\�s�g�5\��2\�\�Qf\�\�9���\��4Ck`�㫥�\n�\�>�\�ݎ\�\�\�;��V1\"/O;�M�Fߎ\�ᔬ\�\�Wc.��;}\�=�ȼhjZM���>���ʆ\�\�5��\�\��\�@]�\��\�9g\\�l�\�{ͼ{\nΆ\�\�\�\�\�\�ϣ���\�O\�\�\�\�.(�,d�\�ϩ7(�\�F񂜋���\'\�B�ډ�A9g�\Z��sȿB_>���\�M�ͷT\�g����5X_)>�0}�@P~\�\�aG��\�\�v�\�8W��>Q��j�]�$fV�1�y�����7�A�h\�u\��1]\�T%=CH&ߠ��By\�&2\��W Jxq\�&K�K�\�Ԍ\r�3�� w���qo���\nT�*���HoD�����\�|6,��\0��QfV��ڱ$�1i�|Z7]�mLD��yb\�C�u�\"��L���g??X�^<�\�i�\'Q��f�HJ�Gj�G7�ʩ\�G\�V�\�_\Z`M&Ǆ\\Ϸ�\���Q\n�QXP\�A�e\"R��o6~]�\'���?fښ�+I�����\�8\�\�\�؞\���R\�W���M�(�����X�DGC��m�VW}�1z����s�-di\�ՍԖ\�H�u\�\�o:\�I�U|\�\�\����a�ו�#]L,\��Aʃ\�F�MOSُP�E��n%8a՚�B\����1��\�W�yϱ\�<k�ܴ̆{����@5$�\�&|\�u^\�~��!=�3�b\�Y a�\�\�C0t�q��\��P�\�w%\\��(�0�VXq�~a�.\�F\��t\�\�>L�S\�!\rs�Z)\��>�g�\�e\']�B`\Zuem(�〻>�?�au�*O�eT\�:ƀ;hf\�\��ݫxǶ\�y����\�4nHi�\�X\0H�(\�S?K2��Ua�F�c�]�S_XL\�\r\�\�\�A�I�$��x� \�\rĶ\�倛��J�ƅ7\Zw�D\�iq�\�z�|�\�9�\�=\�+$�ԏ\��UC��Y�KG\�i�\�&F-T`�6\�\�K\�l\�V\�\�\�/|��\"7ԳvK\�ׇ?�\�p��K\�c�qzB;T��㣚\�f=��)5x�m\�<�\�\�\�y��z�7\�B\��\�8� \��d5�B�|W�\��(HibR{_:�Sk��$A���\�\��\�j�^ܗ\�\�|\��)t\�Y8�B#I\�g���S\�\��y�~#,�my\�RE&�� �g�e`0�\�p����L~��[2\nC��=\r^^��\�B\�\��X0\�1�*řj!��H�\�2W�BQ\�X\�Ȧi6U#_=�߽�\��Y\�.�r�F\�J2�A�\�\�T��п�!�Y*\nd��r!�����\�/�+rY�#\nR�	\�\�t_\�\�P\�\�{�SS�=���\Z8\�(\�\�\�cS�D-�\�\�7�π�4�\�\�N%<\�xY\�F\0,\�V�\r��\�߄j��D�\�dv,��\�`m_\�_q��\�3��f�Qo\� �l\��^`��g/,\�\��i\0\0`>\�\�\�hK#\�y��\�%�}�\�\�<\0�\'���P\�\��\�\��:�q�~~��\0#R\�\�d(\�=I\�\�x;y>�1�y�\�,Q�\�5t�\�z�]n�.\��\�\���G �_]T\�\�[/8�:3D�%wJ\'\�7\�8T~p|�S\��`w\�O��4\�\��9�<K����\��x\�%r\�M\�\�0s�L{U\�!�:~c�Ϳ9�^����Z�T\�4o�\�\�xҢ~<\���i�NiU-\�\�2ly\�^\�/ ˬ>z��.�� \Z4��2`����q�sz�a�e�\�ks\�x?�,�����\�Y\�\�����s\�4CRf�%%�A�S�Zz]\�J}�MB���\�A\���%\�i/�C�o\0\�\�,�\��\�\�,y�C����J��\�y^	�i�3\��z\�dۂ��\�g\�z>l{��)�b6I\�3����\�\�\�߼��%I�k=�ޭ��y\�=\��@��^�\�\"��xPJ[	��W�YF��T\ZҜ3-���,}l\�\��\�Y|J\��\�LL����\�1��L��\roo�<�upҪ\�H���X\�7�T\\��2\�\�,i\�\��\��>jbC�\\��\�H9�L�rzt;�wj�!笁\�\Z� \"��~\�\�|�\��\�\�/Q�=y��F�����8Nt���$\����\�¬I���Dtw�#6S\�Ot\�5b]���0,d����\�L�����\�y�\���\Z�\�R\����L��y\�g\�d\�\�eZ�Rx�l�\�\�d\�\�m%PB!X�1rl�����^��\�a|�\�\�_+��\�Y3D:NJ�\��P;�mJ����~��E��\�Hej$M\�`LA�\�=y2n��	�Z�\��\��d9��6	}{��1\�\rw�㌶�ő\�\��s\�\���!E8�\�\���\�{z�}�6F���Pw�\��\�?bl\�l��b�S\�Ec\�X��g\��gV6�X\Z\�{9�3T�g�u�_�u����Fr\�C=_��a�ג�,��T\�\�rH\�R\�s\\����v\�2\0��3\�W3�\�\rrZ���g��\�\�\�.|^�K\��\�\\\�J^2�\�\�6\�4ZP/=cID�\�o^ޖ�͌\"����/�gj�Aږ\�L\r��?�\�@\�/5qOQ?���EjU\Z��u菭\�\�)�\�2���S�Z��I#�AN�R\��\�<W\r��2���o\�wK)+l\"$\�\�\�\�\�\�\rÄ\�iB+��%�?;P�d�\�\�\�.(�ٳ\�d�\�\Z&�\�׌\�\';ʿ�\'�\"���H\0\�\��1\�\�M\�Ŕ��ʄCj�d.\�\��D�d=�d\�F6\'\�B�\���\�}�)�\�`]�S��vDޘs\�\���v\���\�$	T��M4߯\�p\����\��7M�e�y�j��d\�Ϥ\�-,�\�\�\�B�\�!�W`L�\�\�\0�W0_\Z�\�\�`�\�e�$�\�kJ�\�\�O̳c\�~��K%ƪF_�\�\0Wզ �L\�ti�\�/lS�\�@�\�)�U��z�]\��\�\"�M����сq��\�R��FC�}�\�\Z� \�3j\'�1��\\g� \�/���-\��c]̅M���̆v�+�1��L\�\�)�\��/�\�\��ാ�\'Fx�-\�X9\�ie�a?uDj+ĠsE=�$�&,\�\�#�P�|��P���_�D߂{.\�E\�\�c\�\�����Gq\�\��\��L�\'\0B�.�⺥˫>P\�.\�r�\�|�\�;;�;����p{Yژ*\�+g\��A�:���\�b3rܲ�&�p��F\�\�\��Zr\�:i�j(\05�\���X\�����!�%_%\�\\�O\�ڎ�0\��\�wФ���n�(x�\�|���\��::%�\�X?\�\nJ9\0:OyR���I\�(Mw5��QZ���ݞ25�W�\�=6���<M�\�|8\�:�\�LpH	,{	k��\�\�%\�,\�\�EK\�h�[�=�q�\r�\Z\�\�\�&\0K\\�\�Nx��R\0\���4|�g?l8&��\�X\�D�7(6K5�/��C���TA�v�}����:�V��.��\�ͼ\�ݦ8�]\�#q_\�D\�\��\���\�i\��e�L��\�H�#�.$3�\�]���ϖ&p*�M��࣠\�\�\�ɆSY\�\�VF\'��(1B&��1\���\����C|>�7�b͌L~?C����\'�\�3�\Z�e)\��\�\�ٱ\'=&�(��\�Yt\��;\�f\�4K<��\'ȿ7�V�G\"�<�����j^CXH�I�	C�����ed0K��^3so\���I�x�\�?�\�\\�\�\�\�g\�H3\��v������C�B�n&e 2z*�i2\"�\�1��D7!g\��}az!�\�\�)\�79\�ZI�\�i9��\�=\�!Q�O+\�\�Zۤ�k\�\�r�R>�\�T\��Co�\'b��Z\�\�v���R�\�\���P7���\�\�\�F\n\�؟\�r\�k\�:�^|+I쓉*K�)\n\��\���\'$/>���K\�!\�-(�\�\n�N_�\�\�t��F#k{�\�>���\�+I���\�\�\�\���\��\�\�<�\���貞��\�<Ѥ\�0�\�zF�^,d%�Xf*���\�\�!A%��fg�� �Y��u�\'�q\�\���ޗ\�2�4�\"�\�\�\�X����kH\�7h$�/��؁�\�/�I��\�[�6�\�\�L3\� P�\�FK�\"ĉY\�%���[>���.d�J�\'>)f�\�k��\�Phގ�k(\��A��vG\�5�ϑ�;���ZCh�\Z^�\�<�c\�\�\'%J[f�c�\r�lY�r�/���@c�b�ڳ��\��\"%K�\�`#�:\n�\�^I�\�ؾF�\�ɋ����\�\�mb*��;��$sHe⦸\�\�ZF�\�\�\�\��\�`�}�7lW1�W\�Y\���(�3z�\�0c_ǋ+�1\��$����5�\�tH�ӿQ \�\�9p3\�c�]6\�BlM\�`�q��ڇ�\�\�\���\�<�W\�\��\�\�_1_\�\�Qj=��cN���|ik\�\�OuH{8�\� \�ea\�\��\��t\�eu5�~!kiҹ����׶��ҁ&.?kv7;M\�d䛟\��]=�p�\�$\�N\nP8��ݩ�o^\"^�ް+;�\r0�I�\�b�\�:dC�z\�\���)}\'�3\�\�\�s\�7\�\�\�#)p\�9\�\�[\�	T@%�|Z���\�ŧMT�l7��q�#t���\�ݮ \�\Z��?\�\�*��cY\"ǥ\Z�t^|��	\�\�\�\�I=_7|\���/q��7���N\Z ^r:\r�G(�r��0\�ﳽ�F�\ra\�\�\�Y��n^2L�\��L��|\�b�\�\ZB�=nYw��S�C|\�E�+X�跩�\�\r\���sG�\\\�\�d\r��m��\�\�\�֠�W�d@i�iz �\�s@�y]bJ\��Ml�$\�܍�L<\n\Z\�c�]/\�<\�`��o�\�\'}>_W\�~�ƛe\nQ �\�\0d��I�\�NE}`�vESXq\rk+b�y\�\�w���m9�38�\��\�gg�����h�͜���mj�AƔ�٠\0�10���L6.\\H��\�=k�2X/2Jpx�9\����_qZ� \"�Q\�\���	�n;~�Z*x\�\��1\�\�c\�s�oV�ZY�\�\��~��<?\�G\��{0K\�5E$\�ֈ4a9W\�?\�[\�\�9�HAhLz��<<GJ�/	7��2�z^N\�|y�\�4n\�Ҩ�ٻ\"�lt@\�\�@H^z�z1w4{/��\�\r�\�\�n	\��88N\�S���	Ñ��\�\�@�\��VAt+�%,\�+qU\�\�o�\�߽*PL�\�c�`\�nt7�\\��j{�R\�#\�=�\'�\�\�A?^>��\�U��]w����\�V�ˀ\Z��\�q~q��Q�8૚��H{\�\�$\�\�$+���\Z-�\�(\\�2s����{\"����q��VRP��T{g\�Mba�\�M\�\�C\� ˒0q�\�g\0t\�1Ng�\0�Q0��\'g���,�k�\�[\��4��.q-x����&>�|B\�\�^�D�&\�n\�\�۝\�Y����G��\�9��G��\�v�\r��\�ݘM\�u)~g\�Kg\�es�S�	�BCbڒ�\�\�\�x\'�\�\��\�Nj\�\��֤;�\�Ws�	�^�\��N\��N\��\�U��xY\��\�\�\� �-T\��%�櫑�*z\�9d��z\�Br1���T���L/\�dnjI�\\�\Zݽ�,Ua���\�>\�]���,=\'Š\�\�\�;B�I\���V�E���\�\�!�\�ŀuy�Xq\��\�\�Ϡ\���9�8\��������+��\�̛G\�#��M\'��Wa��\�oj\�\��l��ΨM��\�`�K��\���Kį\�Ƽ>��@V�������4\�,^\�nn\�\0C1�B\n�\\��\��gBѢ��nVG�Y\�\��\0!\'(�:aݕC/\��+��|}\���\�����|f�NC���f^\�X\Z\�7\0h��\�\'%\�}�P��\�\�~\� D[NxS�vf\�WO��_�N\�eo��\'�\�e\�1�\�n���\�\�NSQ\�+j\�/��d7\�w�P��\��;;\�֚mW\050G��\�$\"��E�.\�XA�H�T�\r�X\�c\�n�^\�on3s���\�K)�\�eÛS\�-\�\��C\�o2�\�_�C=�}��L�qA>y��8�\�8%<+���?\'�2��\�5Tg\�j\r\n��\�^\�<\�\�\Z\�Ы$֘{iW�m\�}W�ވ\�\�:4�\�^\�\�\\\�\�\�c����\������<�\�]�\�\�C\�+�<�A�\n\�mTv\��U��v\�-\�6��9\�7\ZGKG\��_s�\�^��/7\�F��\�O���!�\��#\��ԉj\�	R\�S\���\���eo��\�g\��@+��\�1|cE\�&��\�\�5\� \�2QyP�Q�ํ��\�3R$h�\�[$m�\�R���ND���Ԯ\�8\�nV\�s^�����g0��\�*[J<�]]-�@�\�d`01+s�F�Km1��\�ߥO�r0-�ݏI��\�z&�\�]\�\���5�A�lRh�t\�\�O�J\�0{+6\�\��çMML����A��\�\��\��N\�g09I�\�6\�,\'~e\�`� ��J\�g\�Ҝ嘒\�\�\���\�\'(X	}�\��ћ���&�4\�72g.\�J-\�U\��,Z�\�sM�Ҍ\�\�N\n\���.b}\�\�`ڑ��U��\�(q��|Λ`\�*Ã\�:�\�G�a \\\�l\�d,*\�XK5��\Z��\�O�-*�>J`T�\� \��\�C-\�ّ\�f�ޜ�	�\�\�\0�k�s���\n\��\�n{6J�\�2�\�g{VvҡzfD\�9\���^���\�\0E���mڼ�b���W�{iVq���KX��\���H��]\"\�1��g~�1��\�da���\�ڕɳ:��Y�\�\�ˁQ�k\�b>\ZF���\�\�j\�/LHvZ\�\�W\�L\�\nU�\�+J\n��َe;�\�=�b��\�u�ɲ/\r\�[u\�Hp�\�$`�hϟ&��fm�\�\�/\�bU�ߘ\�]����\�\�{zr�ǳ��\�/۶��ͦ�s+VT\�&�t\�C&\�)\�w�\�\�N�Ti��LI�gGyn\�o\�4��s�.\�Nr�º,\�վ/�JM��\\y,4\�\�J�Ja\�Ƀ���\�O\�r\�<vPO���O\�_��\�����مd|R��\�\�\�o\�fJm\�X\�PR!7��d_��k\�	.\�^A��k��8NR��\�;�Mۢ�s���h�W�Y\�ƞU\�G,�2<�~�<e\����4O\�A73�^5\�~i81\03�\���g��K�\�r�s�Y\�Ns1Q�Xz3?�UJbI�s�t硊D�P4�\�C�\�+\�\'\r[Y9��?i��\n`���PU\�I\��\�C\�\' �0Ϡr\�7u�	]�\���=4v�E\Z�Xdm\�B��\r /��i~uW\�@�!f\�#d\�6�SƓ\�,��\�<�\�E�\�/���P7�\�V�\�\�CrAf���H+p\n\�6\0�%P\�=����qD6�c�r��\�+��֩�JB\�n�W��\�3\\�\�H��_ŏwb>\��N�-��\�\�\�5�n幹\�DzƘ�!o�,gFt\�\�\Z�\�_���=zNʌ*�CH<;��/�i|ݡ�k\�\��c\�j�/�\�\�c\�o&���(e\�=�^�̼9��N���B. \�!�jo�n�E�6>\�A�j���K���ï�~54�鞃$Hk\�_V�4sԥ�p]+|^d\�\���\�\�\��\����[���u�SՇM7tl8\�c\� ӿo�Ms\�\�\�\�W^\"�q��d&���T���;2&\�\�32K\�+��l��&\�|���R�H�Z��I��7�w�\��\���k,���\�&�B�\�5g\�+\'\�\�\�\�H\�\�\�G̶*ߞ��Uq[\Z���>\Zy\�{�ɻ�Iӱ�֥XO� ����i��(``_z�1��\�\�\�ӎk�\�\�9\�\�\�Y\�#l\�/l\n�%Q\�9�\�@\�V�Dg�\�2ʪ~\�c�^�6͓zt��\�&U�\���0\�O�3�h\�\�MR�$\�C=*Y\�kio\�(g�\�\�ORB\�6\�ͺ��\�2�{�\��\r\�~})�\�c�\��<\�\�)?�\�\�;\0 T��\�\�@��&�t� F��D@]�\�u\�+.\�\�<uu��?KR>�\�PF#�#1��Y���5\�aBM�ELTx�>�~\�A`\�:$o�\'����>�w�\n�W��W�\�\�ML\��\� }��7�\�y\�\Z\�)/.\�f2\�\ZJ�w r���@|\�E\�T\��c\�H,��VQr;�GS}��\'?�-k�<REOC\�u�D�Ť]V\n�Qj�\�ϡ)Ɲ\�,\�\�\�\�hc�%�2�\�\'\Z��n\�:L%�ռ\�z�\�˘��˿cG������D�t_G_���v\����!w\�t}�GR%C��=�`�\�E�`Q*\�{P;\ZV���\�D�fc!�\�iW�f��\�o锘��j�|�4\r�\�\�{PPy\\;[:u0��V\��?�,I�bz����X>E\��\�\'\�\"�� �9��X\�6��cu�\�H(�\�F\�a�*\�\��\��a�\0z��*�_lL\�l\�\�3\�]�\�K��~B��a�Očip3�\��&KC����\�˓\�fٓ�g#w1X1K[�~^�^B�\�>@>q�Y-Y�doߓ9����7Ǯ\�\�\��/�\��%�ϧ\�vF_S\�x�-+=�ܺ���_,O�r�9$\�Z�Uk�IU�m$\�d\�\�C\�+s:�\��\�K\�5�\�J5\�\��_Б��Wq�+��\��S����Optˇ�\�H�雈�e\"\�`^	\�R��7R�\�ɺ��3؇E4=\�iC�i\\=HA?}N���P*p=\\o\'�\�-�H���]WW\�A�\�I\�m��SH\�&{�\�._�Za��1m�k����3L\�ᡳ#Q\�8&t��n�6\�v\�^+J~\�\�^<wc	9�J��\�\�\"\�\�Lp���8���\�ovk��0|�J�\0m\\j���=�F�����\�j/3.\r��d�V\�JH�S:\�$\�#\�AS�\�[���#b\Zy\�\�s\'��S�\�\"��3\�\�q�\�oVҹ��?\��\�\�\�\�\"\�	�\n\�\�N��0�R�8�:\�\�n��\�y�m\"\n�2`~{_�$1�]a/s\�r\��\���;Yu߇}Tہq�D\r/zK�͈b`�br��\�9�=�\�#�\��	�x\�\�\��\�c�\��F\�6�56��M-���\�E}\�Р\�lտ�-��$%1-�\��hhG\"�2\��FM�]�;9���Kר�@\��*\�$\�z0Lqjq;f\�\�C�9M�\�t�my\��5�\�s�(<\�N\�\�.�CbF��\�\�\�q�4\�p���\"C⭏z}S��\\�\�\�\�_+�FBC�\�V���\�zNO\�wS\�k�4M4�2\�\�1�l��aI�\��/dN9��a\�\Z\�\�Ւ_�\0�\�8���\�,j����\�\��\�\�^��u\�!�\�?x�!�1��\��[\�\�\�ڗ\�	�6I�\�}��@����<�\�6z}x\�\�W\�\�XpBa�0\�w\'�=-ٕ(TZl�Á\0\�!(�����w\0�r\�rVCݒ+f\�_Tj�{K�\��dp�}wM_\��\�zK\����x\�)[�Z�E:\�\�nxs�S\n��\�$\�J\�\��t\�\�|���Lf\�_\�\�>s=~�~XUnO�4B��q����\�ߕZ9��~\��\��\�S\�`\�5��\�\�^=�ش\�:�7��SߕHvu\�*$�>s\�?\�F1	\�3�\�:B�\�\�)�\��8�\�ג���2<�跌ܹ�\��.Ǟ\�%5���\�\�\nx\��$��\�9�h\�\�\�n����n��I\��].o�r8\�K\�\�\�kCG4~!��%<,�u\�(<g<_���\�s�Mt5��VC\�;�@w�\�>Z\�\�	״)	A\�=�f8\�op���G� ��:\�yt�O��g�lO��z%�\�\�b~��\�ՙ殸Ep\�WB��T!\�>$D/���W]7\�N-\�&U�\�O0��F\�!��\��AJ��,!4⏠\�G�\"\�\r�)S���3�E�\�Ԯ\�-\n�f\�\�\�\�~\�I�\�r���o�FTɖ�\�j>8\�\��\�&�g\�V^Tm-A����\�:�\�6��m\�Cm�@�\0\�x�\�W��� ׷g�g�B�Җ�	�\��U\�p!\�4\�\�\�U=\�l�3��2�F+��\�G����C��֓��\��c\�CE(V�ۯ\�_\���\�u|Mw�]���Ԧ0:����\�Y�߅�����P\�:\��&f�>��d��Iݕ\�~;dn�7�\��\�h��M\�MF�\�;ƕ[�O\�\���y��~���Ҡ\'/AG=��J�w�3\�> �q\�ԃ�{T֋��k��\�W\� c�抡l��\�/t=�\�>g��\�C\�i:���\��\��D�\�CV`1\08��6\�x��ha��7Vo�I_�\���v�q_��)ȿ\�	�4ї����+�\�\'\�c$7=h5ђ_lw�-�_��=\�Y@��=�*��@\�v%��{8^\�\�|\�S�(���pv��(#XE�\�\��\r/|[ȉ\Z\��ዷ\�在y\�p$,G��\��W�\��^�К�_a4WNeR�\��H�\�,��s6�-}���\r\�\�S�aK���1\�a\0��|_�Kk\�cc��\�\Z�g\�\Z>\\\�Y��\�C\�)z��+�tT�c�}�)r�D_O�|�����H\��6z�P��\�A�2�EIuz���3��18�\�#\�u\�غ\�\�Ū�|,P���\�D��\�l(\�(-�Ֆ��?��\��v�@�?<D5�D�G�\�\�|XH�\\�=\02Я\�\�DZ�ù\�`\�6_\��*\�\Z\�1	0}\�g��\�\�\�,K[_Th�We^���\r���}��Z\�\n<k��]�:�$�	\�\����j\�f�ԛ.\�\��|}�iL�\�\0~/��`�\�\�\�i�bp �nj\��]Tے\\b\��<(όjoF%\�%�H\�\�\�d\�E�\�2ԅә\�,\��x�*M,��1�\�\'�\�ζ�7���\'��\�Ui�&�y�\�Ԝ���*ۙ\�#je�S\�-L<)�\�G�\����*Z\��٢bڇE�\�\�K�*¾��.�?\�n�\�\�g\��$�\�2\��u@<sP\\�RS\\�\�2ېbS@0�[A¢��\�#=\\\�/M,��\\9G_\0.J\�g�x���\�iw.\�\Za��\"��)������R�B~*���8~�܊3�\�\�\�ݯX}[w�y�\�¨|�\'�y$4\�G�e�\�@4x�\�(�w\��\���\�p7H&\�(7\��՜wʙ\�t������\�fx[,	\�؅�O\�#�ε�@�\�8�d]Y5W�9��\�.y\'[�R&L�\nl�\'	a�\�\�[ *gVB\r�9�µS�(\��hh\�I�\r\�;B3\�\�\�\�zXs\�\nK�,K~\���¾�b\��M\�d�*Y\�TQ�f�\�e.V�g�\��O\0��\���\��Yt�\�\�\�h�\�\���\�\�=\�Nz*\�J\�VHPTl�Ϟ\0Ԅ\�d�\�]ʕE�\�\�5�\���]�l�8w~\�w0��?Z<Qؐ��a�ծl��\�\�U\�\�2�\�\���wF[�K\��\�ά�q���L%��~�Ժg렢��\�\�C�\�r-\��逼h\�g��\��Z�?~~e�jQ\�Oy�gˠ��X��=� \�N�v\�\�?}<81_xF\�\�Ba\�ڭ�ތTO\�\'�\Z\�Q\�70��h|?\�>�BF>�\�7E�a�|���m�@{O\�\�\'Y�\�\���.\\\�\�&~��\n\�\'�\�\��_\���029\�G�gNW\"D6y��kaL@\�SaJ\�\�~�n\�\�4M�.\�q��eYnmHj�ɂR\�!/޻\�\�\�\���#]\�\��~PȰ�x�^mK��ϲ*�_]褒��\�Q;*Nc*I�{�ξѰ�eL\'~�k�	\�F\���b����HYj���49h\�\�U\r\�(}\r�\�\�%\�`\�\�]lU�oh��O�0Z!_��x\��Df?o�HXe�m�%4�XZ\�s]H�\����\0\�,�;�\�\�\�\�0\">L0\�\��\�D)\�\�^]8_��h�d��\�^k-ELP{�\�\\牎/w�N�\��o�gn�H�wC$�7o\'\�\��3�01��\�\��\�+�[�\�\"\0\"ψ;\�#%F~ ��$\�\�	�kYǩF�)��w]�\��\�\�h\"\�2\�i*\�\�nr��ɳ,\�sn\�qy }Ҥ\\\�8�W�w����8�6C�#��\'����+h\�B\�\�G�}5a/G��	\�\�U=��=I�ئF\�\�\��\�@�_�}\�\\�\'�k}�!\�e�M�,\�\�,�\�\�H.\��\��;�\�a]-s��U�A�\�l\�&�S?K�~k4�7	��1	\�\",�߁10V�;b�OiR�+\�Jf�|��#���\"� �TZ7\��j��d�0T��n%Н9�\��Z~{�G<�a\�IZ\�\�+������~}�K2�\�0�\�#\�\�\�\�3�\�z\�I�I�\�\���\�ۀ�/uG:\�+(\�\�&Lm\�)}�K\�0QEd.��j\'\�qH^\����b�\�:[\"`�\�y̩Y���X^�\��_B�)8;���\�cz�AA�1\�O��\�pw\�\�p=\�[�H�yl�\�\�?\�.��G�\�\r��h�飇�Ա$>\�\�,+�&�_r;�\�H���\��H>\���,\���nh�dR=%\��t [j���0��=.&w��&�\�3�\�u�\�d푂\�S	R+\�h9\�0(\�\��cY\�/&�__\�_���eN����Y^�\�\�\0ae\�~���q�%^�\�\�O\�_X{g*r�\�^��H��\��z���Ϊ�\�)�\�#\�\�O�\n�zemZm\�\�\'�P�\��\�I$�j!����\�Z_\�ix~G\��p\�\�� \�Nx;\�\����[繎��\��\�?E\�q	��y�m\�Go)��\�?Y��\�\ZX+P�G�L�\��\�+E1Z\"=I�Yfk\�Bh\�)ly\�h*n���\Z-ܶ�i\�\�CX\�Y�T}į[��c�r��i�͛t#������Az$�!v�ו$�r �`\�J�ygB�Xa\�8h�(�[��Yk�\�K\�xX�\�?>&�\�\�\��A)����\�\�wt�e�b>Ӊ\�E�<DE��R�X�Ǩ\�۽�?��Z~��e�~,\�0U\�Q\�\�\�,ԮMk\�ch`(��L\�m\0�Kf��@�Hf\�lS\r\�ɿg\���ٟ�/c\�h��,4�&(1���8�\�Ծ�x	�\�\�wJ.l��\�\\\n�:�\�\�І�A\�z�\�\�B[�S�\�7�*.���\�K5WN-z\�kxd\r�b\"\Zfpģ*QW]�\�|��u׿\�\�k\\�ߥ�,�\0\�\rN�\�\�p5d�\�\�df^��\Z\�W$\�f|��Ĕd)+�\��\0�\'��\�\�\�\�b�<6\�U\'\� �@�}\��2.lI��!I#�\���\�$J\�\�)^� ^:\�l|���\��\�Ŀu�-I��>x�c�@3�\�o5\�ݫq\�<�����֬/���\��w��-�\�CF\��w�\�͸4B�-��\�Ӱ��V�S���O\�)�L�jpK?ݟ\�շp=\�\�%�g\"w��{\�\�\�\�\�\�\�J��\��\�5��ed\�\0=i�(JB�d�a\r��\��wA\�h^���\n�o�\�\'\�a�\�3 #\��f��Eba��5\�t\�\Z�P����\rگy���B�\��ܱD�R\�A���\�\�\�5\�\�$./tx��\�\Zm��5�\�\�|q\�A�V�\�\�w�\'u�X\Z+��hIy!P�N\"?3��\�J\�~��+a�T��\�Y�\rU)A\�\�w� ��B6\�i��ST�)P	z\�*��\\�\�gڥ4�s\�lku,��eH��\�5J1\�ׂQ%���\�\��\���\�\\���kK0 �Y\�I�D\�\�\�ԩm���4\��r6�\�\�l�t,�B\�bx>-�a��\��\����L\n���K(��C���*V��X��\Z�j9P.��\�9�\�*��\�F<_g�\�o�y���r\�-�\�˯�Zc\��\��\�z`m�P+ΰ\�\�\�\�O\\c\�W\�\�x\n�.\�s$\�Te\�\�+F��D�|�\��+_�Ȼ\�ͨ?	\�܇�\�gn`\�\�,�_��:!5���$���\����JW\�\�X\�\�L\�.źP��\�QJ\�A�.\�:�B+ح\�u�\Z|G�u\�2���z��S>\�xC孱��&�^W\�\�3%�\���\��QQ\�Rp��`%T�\�2��hs�����{˙��Q�ʐ���\�B�缥.V�\�L\�W ����TH0d�p���-��\�F�iV����%�3\�\�c/,��t\nm*��:\���֓~\��5��@\�\�4!B�\�-�}�%\�Sv�xc�Ҟ�\�U:�Թހ\�lϴZ\�D<6\�3��\r�\��T`�\�󢝜�\�\�\�J97�\�t�g\�hPi�!\n�X@\�P��\rQt�h˽.�\�\�O��|�\�\�垾��\�\�\�^\�!ۯp�EEv�xx5��\�\�\�@ʿ\�z/��\�~�>~\�\��*�_r\'�sA���>D\�*\�I�{\�e�O;\�S&HE���\�G\Z�1\n4/k\�V\�\\\�\�\�\�铙��&1S@nGS6FރX�}\�\�+�ݢ&�\��D-)��mk�ԒXV��%(\�iGDv\�Θ\0\�1�&/\�\�gM\�o�\�\Z�\�럜$�P��\�ߋ\�<_�oF�E2R�&6��:\�\�{u�\�_\�Y\�D1��>>]�\�\�~N\�V��4�X&\�ɂs���Z{�C���\�}8OȻ4\�﬉\�Sc\\t\Z7{l��\�%\�+3���@\'E��Ŧ\�\�G:��I-Z��y<�\�{��\�\n\n^���\�\�Os\��Y:\�2��I\�\�\n�:����٪Ć��^(\�yt�ɟ�[޽\��X�{u\�YQ\�\�9>��{������%PQ\�%=2\�\��q�\�`=�>L):,2\�\�\���\'ٷ���\�%�f+�~3U��j�\�γu\�O\�b�\�\�!.�髼t1�)\�\�\�\'\�\�	\�؇/\�ۮ����Bb\�\�{8�\���䕳\��6\Z\�`,\�t\�gm�j���uſOfШ0e��M�T�#�\�v�Ft���\�\�\�\�ц�u\"dT�\�4u�+J\�\�b�\"s\0Lw\0̯:�\�Z�C�ǝ\�b0̤�\�\�~xc|ʹ|�,\�3�\�\�(�\�-}�\�/\�HǙ(\"�0��lLd�\�h�\�of�\�./\n\�WU\�\r\�\\\�1)�0.)׷�l\�8�������\���D��k$���^~��h��;�\�ں\Zϖ\'��\�p_�\�,�-5�쨯&����\�`T??�<\�\�^t\�1���\��A\��|���\�\�w!\�\�\�\��*\�.!�m_-Cӿy�\�O0K�ڰOG\�^\�Rc�y\�5:#q�F��T.ߺ\�3EUc\�\�\�odL_\�\��7�y�ߖj��\�D���\�^\�l���|\����^\�믑��O�@*^�6w0e\�&(U!�\Z?X\��\�<�\�\��Yߪ�\�\�\�e�a�\�.]�\�� O\�\�;H��U�P\�d�b[\�=�@���\�AW)\�\Z�7{��\���ʥJ�>v> �g ��\�g�\���X�\'�\0�ҩ6?��}����!cŕ�^c�\�\�]��}/\�\�\�\�w��@\�X`\rk!r�\�\�\\?\�\'X��ER��bp�x\�t)��v\�I�\��\�2��\��!��W׉\��\�\r/�ˑ\0\�n~�LL�S��),\�t�\\|\���\�l\�酉�\�L����$\����\�\r\�\"\�	\�\��rb\��+׷I\�*R\�\�3�X\�m���O\�\�_��	�Z\�(ސ\�cC&?\�$�/p��H1,|cJ\� �\�2\�a��^�aa}�ӕjd8�OҴ`\�\�qU�F�\�bX��hk.�@�%���\�\��\�a\�,פN\n�\��>�\�bfg�\�l\�.h6E*\�\�\�\�b\'E�]ى��Z�\�	�\�o�lLAs\�n�>=��|h�Bٽ?,��HHݘ\��\�\�	؋in®��W\�Q\�x ؝�\�N\�\�s��\��꣄\�Fζt�B��\\9N˭\�\�:7\��\�#q�[�ܚ\�g3ݷ\�S4{m�6ؙ뚦\�_NE;[86�\�T\��g����̟�K|\�F�+�\�2\�ꐘ-�!�EZ?��\���4\�$�jW_��\'\�S\��uz^��p�𴣻\�%�J�x)�(�ح�v���^�;x\��j\�/��Q	*K`ܮ�s��5\�|�]x��g&8�1�##\r>0�\�\�o��]�_W\�H\�\0y\��\�!��E�(�\�KȥH	\�/6Q\'\�\���V\�\��r\�\�C�\�X\�;\Z-4�u0	�|������\�i@_�~n��@\�\�\01��CC�\�^����\\b\��~��]�(A\"�R�#�ςьҹnD��	��\�\�\��\��y\nN)���̺FZ\�O�o\�M1����s��������\�Z\�E��o\��_����ț\��1���_�l-��$c�_��C3�\���l�+��\�\��7�\����\��\��_�\"\��_�o�\�x-��?�mc\��\�\�����\��6\�\�\���{�%iQ\�0�ۚ���]׿�	��u�\�\�\��\��&��n�?�\�k\�\�3��\�\���\'\�\�>d��喌\�\��_���͞YW\��qs��\�\�|�-\����\���\�:\�[����\�ۤ��H_\��\���������x;��\���\�_�8\�\�?����/�I��\�Z�<\�\���?\��̿F�S���\�3�\�\��[x�ж��\\�\�\�������_m}���g1��̹���F�\�[\�m%�\�\����źM�4[�����\��?\�\�\�v�,\�h��\�������\�7��m��۩�?��\�1�\�W�g�������\�F�\��\�{B�,\�l=���\�\�ƙ�?�����z��I�\��\�v1ܧ����lc(R\����m9\�cO���6�H^�?NG�����������\�\�W���d\�^��\�\�\�$�j�?\��\�/��	\�mw<~�V�lU{Z\�\Z_G�u\�a�Wr�٨��1.m��\�q\�㖼K�L�c\�!4I~�L��\�\�\��b����\�\�\�)\�K�����f\�p�~x����Na�U4\\\�=Mf�O���*Ӹ\�$\����1���\�9s�L��\�F�:\�zCܶ�G��$ݑ�XB\��\�ϖ�\��̞\�%<]�ZVU\�w\��\�ƒ\�H�#��<A2h\��6\�\�\�%\� )�<\�Ҭ8\"M&��\�@����\�7dȋ=�\�.u�k�\��^��\�b�\�Z�\�0\�I�2z\���T�C�1H~�\�\�A,��\�\�o27\�7/!*\�K-B�n�\�\��<c�m\�\�2\0X\�1\�R�&\�ؗ\��1���w*vf*G�Ⱥ\���O+�|�\�\�z�֙�\�B-�؞C@Y\"P�^\�1\�]\�|98\��o�\�J�c��x\�ced�\�Ж3��9�\�f�\�\�ͣ\�q�\�,\�&�?S��ؖJ\�YP�r�\�s�:�cp�g\�c��M5�c(y��ߧ�\�\�xB��s.\��U\�NC\�\�dr�\�\�kv\�Ч�wv>\�?\0s-�¥��8L�f>n>\\\�̫�@�4�\�!��>��$��u�؍c\0\�&K\�[\�l��-Ck+G�Go��\�_�\���.;��v\�\�P&\'UƁ4L�#�����í�*6�x��\�\�X�bG��\0��y\�\��-\�,J�\�\�\�+�\n�\����\rA�+\�k\�\�\\�]5�\�i���\�\�\�~����\\�	rƇ�\�4\�^�p����C��\�\�\�\�ow?��󞤲�\�*ء��\�\�K�@b�\Z\��8?j\�B\ZQ4��M�OU�[R�wͺ_Iʩj\��r�A\�h\�\�\Z��ˎQ�\����\�W��1\�\�}GտM�\0\'��S�+�e�\����b�\�\�[�	��_�\�.A\�u��40\�b\�bq�!��*\�?\�<��$\�iݥ�\�e6t�HN_:����[�OU	��R\�h�\�(���a�|>ň�G�\�g\�\�\�z\�G+x\\���Z\�?=t�p���<�č�a}ɻ��5t9\n\��-\�Z��&ի�\�w�u�\�\\�\Z_\�i�lhbvBy�\�󺇗j\�`9�\�kj\�(\�c\��dz��:%\�&\�D\�z\�\�\�7���\�\�c\�\�/�5�-`|�~\����a�\�\�-���\�o�\�Eh�\�\���!�m�Em[ο�\��i�\��F��c�?ސ�Ѿ��3L\�\�)�\�\�kSv���y\�#Թw�-E���\'bcں�\�N7\�\��M#������@d|_�\"}?j��\�\���9e؛\�I?\�ɕ�e,jB�>zܖ\\�[��wh\�4j\n.�\�Y&��U.�9�HUCuL6�Ʌ��1De���\�\'�2�!�4wL&(݄�\�<�S\�\�Q\�\�ѫq�\�:\�n����Ȉ� יht���y?tR�>\�d(\�互C\�\0�\��\�>��`S�y\��	E午\n\Z��2\�&w\�a\�i\�T�r\�[[q�6?Z�N���\�\�\�\�\��:�7\�e7^\��N��\�Jt��TX�*M�\Z�����\0\�٥,�\�\�`\�:{�>­��\�ؒ<��{�\�T\�|��x\\\�\�>N\�%�V,����\�آ�\�MG��*\�#k�-}\n`�7\�\�\��\�b�Ϛҝߎ��i\�:��C�\�h\�x_\�M\�\�\�C�\�\�\�\'Q.�N�\�:�!\�\�ׅ\�\�\�\�`\�^Z�M&%�`Ж\���]~�J\�7O~J�@\�6�\�\�z��MC>*�\�:H2\��A\�V|\�\�/i.��\����ַp��u��\�\���\r���\�\�\�S�#}@+%\Z(�����\��1CS��֮\�/qW\�M\�\�&�8ڸ\���\�f�\�Á\�\�\�w�d9\�c4���+d`{V|ϥg\�\�\�z/�\�f�s��\��\��kq�4�o\�\�\��\�˅u}.\��\�$��IXf\�]%O��\�4,+1ko\�	��溥\�{\�&$\��0*�䶈=��J\�Y�Ǫ\�HE9�p*bT�\�\�.��͒�f����@\�X5���\��\��9�\�x\��\�f9p��\Z1ϕy�5Ǽ�em.>�\Z4äZ�\�C\���W���|\�x�\'\���G,�!���\�v�:����\�����\�E)si���t�P$���(z\0\�o.������� օ|:�q�\���\�\�<\�\�M�\��rI\�\�\�\"EL\�)\'��- \�`J�\�<\���\�2Y�!P�z+9\�3�c\�A=�i\���\�i|o\"M�\�$MFA=b�\03y�g���,w=R��N����.u\��M\�g�֧e&\�1Fj\�Y\Z6Ũ8&s[�f\����)�\�p�\�s\�\�\���o\�2\�\���c>^J��c2	iWi\�\�90t0\�61+�Y[�/_j�&��ㅍ�t%�x\�128\�`\�\�%�\�ޮn�\�\'nMK:_�q\��0hg���	#\�`>k_D�׾x\��]��y�>\�\��\�:��������\�r�\�73\'��)��{���G=>\�\�E(�/iƲ��3��-�\r\�Np��)8[h�@�e�ԝz%Y9�\�\�\�\�/��=4�\��@\�K�\��\�|\�\nTm_c\\�dS�S	�I�4eS��W\�\�;�P-�T\�W��7m^\"����2\�o�\Z�\�\�\�ߕ����\�`�!P�\�Q��\�aâ\�[\�\��c7��8\Z\�߸�\�\�RJ\��\�Q�,��/(��G�\�|�݄_��\�~\rCi\�Ǹ����K���Uu��\�д%ٹ\�\�Օ�������R\0\n-Nδy\�b�p-�\�.�7c[�7�m\�-��L�u�axJ̫o�T���\�#=C���\�x��Y<j�\��Uvp\�7N߰�BGqK��Pj\�\�<}K�6e\�\Z5�Κ���=�tv@N��\��lŋԸ\�^M�\�̌��N(���w|�+�\�[�\�\�B��0��M�z\�\�qD\�\�w�t<`Ir\�\�X<\�}�%\���@E�델U[9�Y3���Zk���b�)6Z��� �u}�20S���\�����\��Lk4T,}��\�\�5�4\�v�Z$k�Z�1T\�\rw���\�\��w_\�\�Z�y5\�%H��1��^�E\�j\�ưk[��l�d!̬ҡ��\nZ7Q��o�k�d9\�L_r\�y�1\�e�r\�� �U�b\�d{\�΃Q\�U�vJ�U��L3\r\\y\�B\�&��\��\��w��\�7\��d\�9\�G�qn�Ԡ\';͈aQX!��?��!\�a���I\�ï�:\Z\�e1�W�\\9j^�ȸ.\�x�e@\0̹�C\�uכ\0*\nmL\�q��\�c�T�\"0kP\�\�಑o���b5d\�~\�O�/BP�/�N��٠|��IUf�l-l˫,\�{�Ƶ#\0\��uM�6�l�/�\���&qgܙ@g��\�݌ܵ����\�5[M~N�f`Jh�~�@�v$Qݽ\�rS�f\��xG,;\�X��\�\'���?h\'\�8�!i���K\�a\���\�\�^�^��\�\�\�Ob�x\�z�׃$�%q�\��\�\�\�\�v-\�\�h�d��\�,\n��\�$�!\n\ng�k�ش�PX��Л���G�\�C\�ij���P�~~���b�\�B�[6\�Èn\���}\�=�\\��\�@^Lΰ�\�����\�3�b��\"]Z��G��7d\�h���`��	�\�;�C\�\�LF\�\��̊\�-��d�UGD�K�\�Jh;\0�\�̏H��d�L�x@��� �\�{�(r���|�\���<_f\�\�=\��o\�� $ޤ</\�:��>[��%^��zݡ{\n\�3)�7b���\�T3\�\�kb��;�L�\��\�D�Y\'\��\�\�k�t\�Kބ\�\�so,V�`9\�\�Ϋ!��P�\� \�\�k��m�A���U\�h`!F�w$�}�MN�_A�J\�3�ˇ*\0\�/�sp��/�SbB�3^\��\���BM�\�\�\"SAW͢�:�D\��\�`����\�pr�I\�?\"��\�Pgm\0\�@\�;�ΰqi�\�Q�\0\�ʾU\�*S3i�|�nL�U<P�+6GoD\�\��Iz\�\�xǰ�\�oƄ\�\�ֳ̑#���\�kE\�PX��e��P�\�t�dǇ\Z��leǉP�\��nv�\n�.\�`���j�\��E2���\�܏��C\�:�1\\\�Q�P�\�f�\�\�Y|�\r\��t@�n,Z�K\�\�\�\�pN�\�~{a:3�1+a�\���\nK��{1\�\�IC]A�\�\��fx\\p�aU\�\��6\��\�g\\�I�iV�[;\�\�\rG׉`P5�\�Iup7V4���J�%�\�v�\�FN \�� �M����SٓTj\�\��\�^I�b��\���\������_oHF\�Ͱ_u\�mR��k�r�� �=_q��\�\nʁVI\Zؘ~\�>�\�\�\�TxB�RF`�\Z,\ZēCJ$F\�O{��JdU\�	����m>�.\\��|\n�k3����g��+�O\r\�{P\�\�0(׮\�t�\��v��Z3_B�S�r1\�u׼\�^1��\"�\�\�<G\�e�����V�&���`ȟ9\rъm\�\"\�W�\�\�N\�ѥ�9��[(pu\��O6\��覀���\�\r:\nF��%*�ϐ(@T֊\���\0#\'��\�H3)�\�rL1�p\�\0I�\�\�D��1�9N�&w}�k�\\��r\�\�o��de�i��7�Ơ=�>��9o\�Q��$��g&\�k��(���\��*rG\�k�Ë\�-S��~e\Z�2�f*]\��\�\�gI|8;3\�!<�����u\�5̄5�S-X(]@�?���\�W Y�\�\�\�/:H�\�?�\�\�L\�>7\�qA(ux-\�}{՝p3\�\�\�/��\�\�^=��g\�\�$�F_�\n���\�֝,��jp�\"\�l\�ْyM\�i\�$�ϱ\�5tZ��\�i�C��\���c\�\�����(^��\�7\�=�%,R\�\Z����n�z�e�Dr���aN\��\�\'�	Ѥ���\"\�Jr;Z���u6��{�WU�/W\��w1���|�i�� �`\�}ǅ� &&^�\rc\���LǄ �2�F��J\�=�g��򮽪��s�d[Im�i�mR\�-�%\�\Z(�h��\�\Z\�EӰ���7����M��5�\��r\�\\8x8[\�ؿ�\�\�@Ϡ\�I	\�,<�x�˂]E�(\�xQm\����>����\rnr(��\�%�\�\�	\�jnJ�oN I\�r)��\�O�\�\�y��\�>�\�)\"K�Q9-DV~\�*}.\��w��TY!��	���+6@��J\Zְ�\0˽R�3k\�\���i�R3\�\���qB�\�\�|��\�\��d\�{�U%D.��oI�������\����V�b:�=\�:L\�qj\�\�߬�c#x��\�ش=m��\�7��@/�^�\r\�Gɡ\Z��Z�Ň;����!�K\"�glEfU��g���/e\�\�n�\�8)�YW*D������y�\�\�\��GI��T�(\�\0�J�g\Z.\Z�\�:\�\'\�ؚ�k\�\'��C��\�����֒U��\�\�]�-�7��\��\�+��@�E\n��\�]\�Up\�\�k��9L�Q��Q��&��2\�qĥ\���\0b5`�jئ<�8�K4N��\�T�\�8�;���:c\�wFD�\r����aX\�\�x\�JW?w	�\�z�R�+4��.e���\�iA\�o�	�|��<ݠh=Dqg���&?5��Y����\�c�\�����\�9S� �K��f;��F�l�5/Q�O����\"}}\� �5�Y:\�Y0\r��A���HX��\�mn&\�X����$�\�=\�\�?c��l��gG\�TOf\�or�;�\�\�\�\�Ǽ\�N+\\��\�\��	=�\�[�+Aa \�êX/\�w;\�{�S�5U�\��\�*�/�9/�_�%~\�^\�ԩ \��\�;<�\�®c�\�A\\�\'��C4,�s�\�2?&���N\�;\���A��&0�\�o5����G�\�\��y�o�OΣd\�~4@C\�|��#�誏��\�W��+O�舱/�!\���,8TL)A\�u\�\�	�Z��]�I��\�\0`\�<TI��J\r\�;3e�=no\�)�\�H�\�\�e�K����:�\�ϊ-<(GxR+xT�\�j���\Zz�\�\Z\nX̀�Fp�\�\�\"Ng)��I�#Ti\�\�\�\�\�15˘�Y\�~N\�Ka\Z�c@���i�1�0;����3͊hL�@E\0f`Ļ�G���9�\�bzc�z\���]G(<\�N$&\�?\�K�\�Wv��\r�\�\�%\�\�֛\�\�Wo[�C�\�2�\�\�\�m�7\0�\�ց\�ލ�XX�q���nY�K̰�ǂ_��\�~\�\�緆\�\�7\��h��\�7z\�\�AE���\�?x\�\�\�5�_;�g.\�@\�Ɍ�?hRt��#Z\�\�n<\��(�RQ�\�=*��m���Z|O\�>{�?07\�v�ltp�6�\�~��&9\�I\�MF\0\��e?r\��T\�*M��t�\�\�C\'k�\�\�s(`�\�\�v�\�\��\�K	W%\�װ@\�-/Ŏ_���{��\�tm�\\T\��H��n\Z\�0\�6DX\�}؍̐n�Evs0�a-m,>Z��A*�\n�9m�~ښRt�v\�\�\�X\�*\0�L�\�EPǁ�����I�M�\�\"\0\Z�\�T��W �����v\Z)\�\�\�p;\�k\�5�b쬅;��g?\�p�\��9g�K\�J\�I\�µ\�S_�gq\�\�T\0ڢߚ\��Đ\r]�S;�}�\�#\�\�1\�\��\Z33�t�\�w9��a\�\�k�ݪK��^5�Gv\�\�ʤ4\�`��}�k\0\�f��Z��Oodi��2�\�0�hW��\�\�<%�\�&_@�(�y\�\���\�S�?d��00\�lYC\r�RB�u\�\��v�{��%jEl46�P�\�\�3��\� U\"Q�bo20�Ő#\Z�)|�Ga�y\�HT��\�=\�7\����P�{56�}������\�:�T�lQSk�x�Pq�sw�}\�8F�\�O��$�5\�\�\�.|fC��\�\�\��};G\�V\�A\'Y%��\r\�M\r.��\��O׀����%D�\�8\�c�vvå~MY+�c�_Q��1s�}\'^XNK\�	쫏+�J\�V��V��\Z�\��\�\��\�\�,|͡ƕ~\�\"�Z߅ ^;�ϵ��W�����_\�=4\����}�3菺m3³\�7\'۪\ryi+\�\�\�w�Jf�3]�m�kfK�4o�\�=\"\�윆�\�0+\�n\�\�G�d��p\�\Z>.x��ĥ\�=J�s\�\�Ņ��G�:\�BT�\�^�\��q\�u	�����1T\��0�WC���\'^\�8�\�\'�9X�/��ߒB�CVȍ/�\�-A\�5	+*�\�\�5)�U�#�t(ֿ\0���\�.��ϔ�\�\\ި\�0�K7j\"�5\�C6ہ�aH\�j\�X�4{\�G/\��9\�?��\�Qa�\Z���c�����c\�ߴ~���\�腞J��D\�\�i�K�Z\�[���\'�o�#�\�o$��r\�Q�\�QJ,�n\�&��E�J��\�\�o�W\�;�2����\�\�O\�<^弟�lA�\�Z$�\�O\�k���-�\�*\�1ƾ\�\�wIE\�(�|\�J!R�}\�\�@Ȋ�C�5��f����\�k �\Z�Mf\�N3ib^ID]\�כ�\ZvJ\Zh_��*@S�\�s��Dxu\�F3\Z�\�k?����\�D\�\nɸP��U��88;2�)W��p-\�\�)�C\��\�\�T��&\�ӌ���`a\���u\�\�����\�\n\�&�\�\�0�|\�\�$Z\�n	c��\�o�)�,L�)\�/\'T\r\�\�=4l\��W�y\r\�Y�U�2�1+2֖?��4��F�Ir�\�ZS/�\�k\�x\�\�����ә�\\7�\nH�\�\�@\�c��\�d�Sv���^\�PR�\�f���/�y	_� U7W�W\�a0\�s.ޱ�ʊ�ng� \�?��c\�gY�\�\�\�ew��\�\�Zjt���z1B6\�\�L\�\�\n �ݠ{IBvLiT=1+\�&�/]\�G]m[N�&\Zo�D\�h��΢�\�\�\�\�{��e���\�\�R\��rB3�\�1�\\��ҍ,\�\�}Ģ[\��\�\���r��]>n�\�\��,\�}���K�\�*\�[�H?�\rw�c\���Z�&4�b\�\�\�I8�vw_��RYe!\�Bh��0�$��\�/Ed����(\�\'s\r�fA��ATe\�y�\�<z؃c�0�\�F�\���\�\�>{�oSP6x\��|��A߿ԗxJ���	�6\�>J��s��\�\�P,\���jG�\�^������\'\0@P;\�N�g�?0M��ږ�\�\�NM[\�bVm\�s\"�ǔ�%;x�M�\��a�ù��H�֭,,5\����pbPIL\�9�\�i�$��Ǒ\�\��[^Ǵx|\�jKc\0��p2�/4/\�t\�\0\�W\�\�Q\�\�9\"y���T�\�\�\"�5#�L��-\�{N,m�� h�\�a[\�E�\�\�v\�0a���[jn\�k�e5��6�4u\�\�r�)B�i�\��?���\�^��\\�	g\�0�\�n\�ǝ��\"����\�Z�쫍\�\�\�~b-6kd\�(7���\\\�o(�ڦ0X\�\���ĉ���!��Dɓ���\�\�n�;�w;\�\�g\�\����J�%�\�-\�\�TmU\�\�DKອ�R�u�x�u_P��y��I��:�\Z\�6b\�w	\�\�$�K�\��4\n��R�\�:c�JW\�����Ͼ\�$4�񓏩�<N��w_m�ʮ�/_=\n\�\��-\�U�=;������ ��\�ϽH<R�\0�C��K�t�-\�Sď�,T6y\�\�cW�\�Q\0n��\�iW\�#_Z\�\�빒\n�\�(�\�D4z��\��u�V����86�%v �S\Z�\��0D�\�)�!�T�\��\�\�w�\������\��2\'�\�\�6sa���\�mHKޭ\�}��\��a\�W�Dk<�\�,��6�^[,�.ZGÅ)�Zoҷ��\�F\���C��r���r>��J9\�ӚF(�b\r�	\�kN]:\�	��\Z\'\��\�*-�?\����f��@V�����Ve��\�a\�\�-,\�\�?\�\�?N\��@�\�\��\�\�Vc{�n\�\�\�zc\�\�%c8-\�0}�\�1�Q\�%��v#}�F\���P\�nK^}\�!\�\�T\n\�x٩)R:��r�\�#]��\Z\���c:�u)g&�y\�glܝ�N/\�\�[�p�k�\�\�\�w֑��	}L=-,\\\�L��A�qu9S5\�\�e\"U/\�l!�\���_V6:�m�Xx\�(\�M\��:l|��|n�Y\�Nc\�ﾛV\n�\�^l7\�\�ǁT5@LI\�Y�:�(=5��+ی\'RE\0�~٘2g6���ZP�pnҾ�\�P\�)q!Z���>�\�z\�ᜬL0\�_\�l\�\�\�>&\�M+ CV4+�\�QϒZ�\�E\�A\07P\��3�n%C\�t��\�A��pK\�=\�L.�ˬ\�V���\�1r=\�\�Ӂ��~-�/d2\�5\��\�\�5\�\�I�|\�#\�}�kw�F\�\�\nn$�\��|\�ͱ$�C��]ߺpWbq:_E�9�r\����\�#5\\\�z)\�z:�D�\�\�E Ϡ#h��Pb��\�\��D\�\�`v$pP�\��\�z\�\�\�lJS\�*+\�޾L:\�E\�9U\���1�T!����X5\�`�`\��eၳ�S���\�Ր|\�=\��\�\�\�dt\�<��[hW0��R\�*/a�d�X}ּ�q?�uy\r�\�<\�l��pC�����r(Ӣ>9��b��\�z�|��g[Q�E\�\�j�5ᐲ�\�\�\'�\�6�8*\�<�\� :��0��{_�-I?wOzꩳl\�Y�\� �~�\r��\�\�(c2\�R\�!�w\�W�ċv\Zְ!O\r�\�.���E1\�Ӥ!��%����\�\�L��TV>�[\�r)�\�\\�:\�q\�9��`Q\n\���@��o-�q[q*Kfɶk��ա\'�d���)^�;/\�Wy~\�\'��\�)g\�t�����r\�0�\�2Y�cT��ވ?��nv}�n�p\�r/?�	��\r���k\�쿋�\�D\'K�([���ƫ)\�\�ŋ\�*�\'��(\�,�{\�Dk�\�s��촯���q��\rj\�DC�\Z�9��\n\�Ɣj;�\�\n^3ud\�EW�Qi�)]ǒ\�0\�\�v-�^si3�ȓ@�\�g�9p\�\�m<��|i�����D/Cx_O^f!.�.@Ϙy-n˞ٖ[\'\�~g�?v\�N��4*q\�\�V3\rUF\�%-��6�Q�����S\�meY\�RQ땋\�!EC��Y�\�m\nd�fo]3\0\�v��	�\�~�F�̅\��֯P><�&���(\�C\�\�C@�(�B�%\���\�7˄m�:��\'�\�Bz�@��\�_\��^q��y\�\�k���ht&�N��H��\�\"V\'ݛo�\�_{\�H��%�\�\�\�m�*L!�о�t4G��3�;S\�٠\��\�.<�[N���\�j\�}\�)�oƋiK�0������.\'�\�X��,�=+A8<���\�\�lE߷ʋ\��>\��E(����\0M�\�Y`�\�u�Ϝ����s\0tvi8 /��nxYܙ�\��\�3x�?+\��-\�H\�N؂�h\�Nv��\�\�\'�r�c����G_�P��/�.\r��\�������B[��j\�W[��B���Z\�.=�\�7	�a�#1gg¼~J���x	\�7�\�������y�@�H�澛\�\�\0\��ъ��?E0�\��pf��\�\"s�ai\�\�\�\�U�Ƽ�r2\�d��QMK]��;�L\�\Zp\�\�\�.�Q\�\�7\�\�ώS\�xt@	\�Uj4��~���\��8ٛ\�\�f�$\�\�8\��\�mj��\�9\�M�a�\��\0B\�vuƔ\ZK+:\��gpLњ�ւO����/�ɮ�M׋57٫[7@+)��E\���Y\�\�\����\�g\��iRCB=��Ͼ#8\�*�,��L���b��ֶ\�}SI\\��d�6�7\�hsA7�]\"�ӾmbU:mt��Fw�+u$\�R\��3��P�k\�kj��T&\rFlE�]\�d��\�GoL߮�9znu\�o��^\�\�S���󃖽��T�i��\0gb�\�\Z\Z��p��3��\�R��\�^[��rN\r\�աzVq\�\')�ƛ(��\n\�T\�-݇K\�f\�A�\0��%��2w��vi;-\\%�\"\�\�U����P���̠E*\�D\�\�\�z���N\�G\n\�\�@\�ʮ���o�ؒck�w�\�\�N\��&\�\�lBk\�\��|���p\'�5�i7�\�A��.*\r	SG�	A&Q��\��H[궾B��P/u5H=W\�m��\n�3\'4�IR\�\�\��c�7i���v\�G;�܎~� \�<K\��d!\�\��+8�0�_�f�����\�YG�z\�\�\�\�\�\�\�\�\0Z�\�(:U=m���V�*\�5A�86�� ��4c\�\�\��g�R	��=�o��ǖj���ᏃԻD}�b��n\�8!�\�ҵ\�\�¡�菑��\�9e\�\�\�EU�o\Z\�\��w\�Ϸ\�%�v��2\��9W�\�4\�\\r��5o\�\�+\�\�h��ݝJ�f9@��\�\�Ӷ\�Ø\��\�^4\�\�8t~5��>щ\�7�H@#-3�����φc�pL\\��Cy�/b\��\ZĄQ*��Ǹ�)<��\n\�	^ߪ�@�~N�Y��.g\�D�\�\'\�u!Ԧ2F���l\�z\�Up*�z����t�.�2�hft\�\�=^\0gs	;7�7�z�x\�\�GQ\��8��1�&v�Kz6��ǩ@5�ཨQ�^�s���\��%\�\�@-�J\�Z�jE��\���d�s\�~#W�Yp�]�y�������m\0~�vZ�\n�of��\��?���\�ըb�0�uW���A������wW#� \nd7M\r\�Y�[��D_h��y��տ��,���\�N�o\�GJB�\�|�}$\�[U�=A�Ə�<JR~LR\�\�\"5� _\�\�\�\��j��\0�F��n\�0�^\�#��\�(�ن��o���E\�\����ԗmk\'�\�:�߮\� �L\��D����e\�	H-7A闔J\�ZW2gU\�\�\ZwR;\n\��\n\'�\�V0Gs\�\\ 4\�j\��#-	�BQg�\�TbÇ\�q\0zV���wo\��/��J�:\�l\��\�gȭ�RE\����r��\�鞝�X��\�>4_���\�p̼0�)�t�\\�h\\8%��,Htkr\�\Z�D52tZ%��\�\�:C�\�\"h\�/��M�#~��\�/3�o���MX\�M\���\�\�\�\��\�x][Ϛ\�\�\�<�Y\�i��%\�;�&�9\0�\�-I�U�\�\�`�g�g�>n}�\�/\�\�\�R+\�?�	J8I�2L(\�\�Cy~�!o\��p)�a[\'7ֻ\�\�Z:��\�:��\�\�[��\�g�%�\�\r%r\��\�s���\�{�ƛS�AP\�\�UV�ī��k\"B,)ꆻ\�\�gYe�\�\�\�.�Ѵ���+�D�)%�j.Ș`\�\�˃�\�\��]s��\Z��\�\�<\�oΦ`����`E\'|:\�X\Z����\�X���:�$	�_w8^�\�z�\n�j[\�ng��>8��K!���~�o�\�2�SS�\�\�p\�\�.�y\�/{b5��F\�˟����\�RM��}�LU^%�\�?�@�d\� \�~��\�౯�\�υ\�[<\�\�NL\\5|\�	{��\���̋oNg}��\nJ\�\�\�\�UQ�-`y�?�J\�\�\��l\�Z�\�R�\0\�m�7xr\� =�qgD`.\�\�N�\�)�\�w\��\�y\�#�-\\H:Ĵ��/�s\�\0��\�-��s����cX�<$�\�x\�n\��`*���%�c��K\�H�.7�\��+*q�#\�\"����\��*괊�NZi�\�#O�Z& [r\�\��\�)�Rc��g\�n�)&�\��\�A�\�\�\�cc�g�;�tc��1\�\�\�\�(���t|K\�H\���N{v��화�\�\�G�W՜f\�L�>��`\�\�χ�\�\�`3��mZ\�)qQ�5�\�����GҨ*/�ȵ\\a�����L���\��;�\n\�\��)����:]k\�	k\�o}��b��^Lv��\�\'\�9BŋE��\�L��ҡ$ø�cC�~֑k\n�hm\�\�\�qu�W6ά3g�Fh=.8|���q��4M�h\�[y�w\�\�ϑg]xž\�¹;V\�\���W.��yU�\����\�<\�m\r\�.�\�{(���%̓.^$i\�smŧcG,	�\�r3N�T\�(����y�\�\�\��\�,�SmP)axt\���{\'\�\n�\����\�5,\�\�\Z�\�V>\�y\��\�\��nB?\�w�\�\�D�\�\�`mhp�bP�\��ܬ��*\�5�NC\��gHn�Q�t)�\',%nt�\�\'ۢ�\�5+�\�\\\�fa\�\�Mv(R��	\r\�9y�JQ;��\�\n�3M\�;(_;�O\�n\�i }�\���W�\�哾��a�/Y\�m\�y?H\��f�P&�>�3z��W�ӻFĈ=K\�L\�%=\�ҁ\�\0$\\\0�XAF=\�˗;M��\�o�\�V�\\�\���aC���_.ޏo}�t4K�Yz�0�u�\�\�n\�5�HO\���-��ZʦjHf�}�r/i��\�,��܇x�C�׫����c��7\�x\�r{��I1C?\r���ܖ/\�Rf�\�\�E\�}�j\��\�\�i\'_����J\���N��\�OCo�T~H��܃�1޽\�/\n�Qs)��䶿\�,�\0!JdN�w\���\�_��F\�,\�Ka��\\\�\�QB[�U��}�\�\r\�\0y�����\�K\��K��Wv��\�NoS�4��Q�\�r�\�|��-\"�Q_7%\��O�~����%��,\�\�z�%?�\�\��Ԟ��xx.~;0\0\��l�p\�B[��$\�\�\0\�\�Io���\�\�\�\�U\�2CɲL��$$ĺ�UI3C�_!�^��\�3\�Ҟ�4�ۘ��\��YuS\�T]�,?̈́�j����/�:\��\�m\�1\�\�+1o\�\�a	�$ey4��t�*%\�eJ�\�q��+B\�\'o\�硴�\�\�B\��hn\�̴�!J\�7)�0�%\�\�_�.\�\r������G�\�Өk�خ|��7ZN\�YZ��\�~�h�I(�/1��\�\���P\�c8~KB�G`\�{`��.��\�]�-\�EW�	�P\0o|{�v)\�\'�\�N\�e^I9r>�v$�y7��7\�\��$t�?y5]�\�>n_\�\�\'ev\rz����n_6j\�L �&��*i~Reۓ\�\�c��nǞ%�{<y^�J\�n���~JR�0�d����?\�\��5>1\�\0)*h�\�J�B�+ߪp���!Y�/i\"!5��9Bd�~�K��&u	�\�˾����K$W X�\�x\�럒\�	�K��[y��\��\��Z�6�����2�j3�Ue�x;�\�GΌ9[��Ŷ���\��M\�M\�U�,\��\0�\��r(�߈m�{��\�\���\�2��!Ҙ��P��5�YM��o\�\��:6/\���p\Z\�{@��-��V	\�\�7\�y\�ǚ��\'N�;�F9H\"_�xi��V�{�n�qVc�r�1Q��j�\�%�btT\�;\�z��\��L\��?\�	Ґ\��\�a�\�O\�\�C\�&u\�	�V�\�\�\�B\��!�P��V?,btz\�V��}v�\�FI\�d\�\�����(�\0=�j\�\�%r\'��>\�\�\�&\�\�-��}\�ɴ�bb\\T��t��\���{/*�G\�\'\��\�y���\�f.u�8_��Nhvq���\�\'���Z\�\��\�y8�lT�\\VY�\�\�$��\�U\�&\�Nb$�D\�T\\\��j\�\Z\�\�|�b@xI���\��iO\n�?�?TT\�\�,C\���w\r�B�YUg\��\��\�M��\�e8\��eMay#8HR^��\"}*�%[�u\�_-Ͽ\�\Z�O�v���u\\��_\���\�ߥx\\>�\�G\�5\�/��Q�g��hj��s1��\�F\�\��o��r�\�k�\�\�G0D{tq�\�\"H\\�\�\�yӝO��oa�\�f�T\�o\�0[\�.r)W�`�3B\���d�y������M����\�\�C�/h�/\�Y�	>\����\�+d>t�5\�M��uS\��R�T�HVP@��\�`\��	�\�D\��5\�$;��\�\�܎?���efU�B���\����eRM���D\�J^u�\'f�����[�Q��,v\�@�/(���\��]\�\�Ul����_�\�%\�b\\\ZX\��þ%\�9%�-,\�V:k�k]h�%�D\�f�ѴI\�z_\�\���k\�\��(�E�SP��^x;��0�r��#�\�i��x3����\�or\�	k�|�\nV�P���\�0թ\�/�T��!h�\�X\�\��\�=h�AA�ȴ8.�^Ρl9\�z3\��}�\�r\�\��������N~ҤZ���>�\�Lbw\r[2\�\�\\� yB=%A�o7?�}\�o��E��\\���\�~\�\�Hf�Z[\�WI\n��w�$\�\�\��V\��\Z�j����!\�[\�n�����KmZ\�q\"5�0��%9��|�;��\�\�\�Mw=\��\0\�\�,�\��߳��\�P\�,�x�{P\��\�\r���m}o�w]\�\�\�4����&�\�Oh/�\�V!�\�	�Z	\��jJ\�\�z0TYF�-�Y\�}�ukT\�1��m�B\�\�{>�\n>�\�>*a!��\�Ǿ۽��\��k8��{`�.4S�Vga\�\Z��|+*\�{\��\�\�N�<\�\�\�<��Ѹ�`U�\�K\'�����\����~�i��\�\�׆\�m��ъR\�vK	@*�K�a\�\�\�7Vw\�Gu��:����@�\����*��(����E�\n\�����)F�˙�I����n�gb���i\��vx��\�Y���\\[�DgP\�{N\�@\�P؛r\�\�wԅ\�Z�_\�ɩ-|B�]\�_�\�E��4\�xu߅\�\�\�M\�2U\��\�\�\�\�sCZ*x*�-\�\�Ɣ(�\�P��n#\'\�\'\0}\�O��\�j�d�3�\�r\Z7\��\�\�\�E\�f�\�||�/\�Q�9�\�\��Bf�dIU�\�\n��C�\�ZU�\�2h?!H���\�0��6@ptY�x\�9�M\�l뫜4���\��]\�*SA\'gs�,\�<丂�4I�\�9b\"��A\'\�\0Mݬ�= \�o\�8�٨s�!��Vx	m��\�H\�o�`��6YB{�\�\��}	j��B{vu\����V�wr\�*W~&b܊7A�!�\��^����.\�Zd#��Ӫ���\�ot�kO��Re\r�\0֌�\�1�/�E`\�\�O{=��hstKz\��\�\��15�;u-1�c.�ְ\�StB�\\��y^\�	�\�*_{o\�S\\����BRG��\�\�\�A����[�|_�\�0�\�4T��A�]\�˼x��l-sG����\�>!�Ah>\�zzo�\��\�Y\�D�+��\�#o�4�l9\Z�3�d�\�����#o�b%\��{Ҥh��SVY\�\��\�\\�\�^-G�ܲ8��W(syO�	2n\�\�[G\�ϩ[���?���ъ{�\�}�\\��3a�Г��&\0��$O·�\�;]Mg�z��\n�\�\���\���\�=LB��\"z�\�wĵ�\n�\rq�\�\\\�T� ;a�\��\�\�:�Y�\�o\",9��\ru	�j\�֐k\�\��7�I��*q,\0]O��Mĺ\n��� l\�,|�!�On\�l!��I\'��;ΉR�t#쏑S�\n�nFJfכ�\�?\�`\r��\���V\�]�a!�R�\�J\�t\�-d�\�g\��\"�f^gXj��\�}u\��S\�\\]\�}\�I+�}Ǣ\�_?X\�[\��\��_$4gm\�*O��\�c\\���h+��\�L\�/�_^�=-\��-�Q����B\�.�\�2\�x��\'�jk*8��p��7�1\�\\_�I\�\�\�E\0-:;4[����s4MN\'����@\�\�\\~MM�\�$X\�G\�\Z,��5�$�Dw�6k~!�\�\�X/F\���\Za}�w�_\�;�P��l;\�WK�U�T?r\�7�~2S<-�[sa�w���ߛ\���r\�k\��v^h\�k_�w\�>�X�4�²��r��\�M�\�o�pm��\"\�1~\08��\�\�m}x�l���&Ӝ-ʾ\�\�T��\�-/D����\������<��\\���Go\�\\�ca\�QW[\�>!LH�	\�\�(Uv\�\�Ī\�\��,\�0\�S9�>\���)|ޭp�\��|\�&\�H߹\�O�_=�7�\�qWvg\�\���l�\�P\�|%)���iU����b�\�gr_������a<�1m(��\�ʆ�l��5u��5b\�{\�\�\'\'�\\u\r.\���\�j\r[E.0����o,4	:j\�\�hh�\��\"\�\�4�y��2���\��^���N�֪\�\"��\�&��(j��8�\�\�XW��\�6�Kug̔�����x�.�\�D���\�\�8V[*U�r�%s�\�f�I�Yf���&_��t*wn�\\!Z/\�O=M��,\�r�W,��\�Q\�!o|��\���8v�ʫ�Kq\�\���m\�\�\�Ʀ!\�1hd\"��ϸ��=ɨF@\�{\�g\nr�co&�%�8\�q�?۳\�\��Ǯ\�\�\�*��W\"�\�\Z\���H38�a���x\"�i\�#\�$���<$Y߿�HQ�&��\�\�\��z\�&b:v\�\�6v��$X\�LD_{��Pf��\�/,\�B\0Z\�2*~�9��\�\�\�J��+\�&0��L�:��w`ԁ&\�����\�(�9�O�\��%|W�\�\�\��O\�$\\,\r�&0j�\"L0�_2K\�x1������L��Dx~\�\�ӓ�B��_\���Cٮ��\�<b�p\�\�1�-k�cUܙ=�\�g/e�;#\�\���`����{]^���T8B)(����hDۓ%�+���\��9��\�W��jN冁<����sΟ\n�{*�xϡﵠ�Y�E��\�۰y��؄\����Q��lA\�#��\�\�x΢z����RR�U{�_��\n⨓TW;�(\�ŪɈ�\�\�m�e�a�i\�\�ɷ\"�\"��\�1$D���C��N	Mp��\�ڐ�[{��\'\�l?g\�0��\�n�i\�+\�\�\�i5���2:ɢs�gH��J�E+�����2\��\�e����\�L���^�\�Vt�/<a-1k\�\�i��\Z\�u\�b\'�t\0ߪ�\�#\�\���-Zǟt�uoo-\�m\��~�A&\n�\�\�\n���SW=�}�\��yJP\�\�扻�Gmt�\n\�	��\�l�����<\�^~��ضc_R\�B	��e��?\�o��|#7 �A���kSˎĊ2ht\\tMz�:BC2.�\�\�7\��*�\�p7��7h�\�H��#�rP��\Z�\'\r��8��\�?\�!P�3RU=Y�-�rt��\�_HM�ق�Ҫ\���ٙ.R�\�I~�=�m\�L\�\�z;��zk;\�t�����./m\Z\�[\�\0�D\�N[\ZT]��h�#\0,/f�c\�m�#�\r�G��ʕv\�θjT�7$��o\�\�秗S�\�p䱾�%Y�0C@\�\�f�\0�\�7�\�}[<T�A\�Q\"}]\�\��R����qm��N�\���N9|p;?�\�-�E�/�.\�v\�O\�,\�㬎�^��\�71�\��>o�z5Fq�_j�625�\�\�\�ʖ\�\�g\�1ޭI�\�k��\��`��jYP6�*\�-{~����\��\�N\"\�v!�M7}�.�(�\�	\�Q欻����\�c}\��Kgb�4dY!ř��-���s�،d���$�Y�t\�VcU�_G\'o\�\�\�\�\�\�7#\�\�G�\�84�\�c|\�p.E}7\�H*x\\*���\�r�\r\r|\n�@���\�v0.\0���\�hߖ0}waԹ\�\\\�SJ�<��\'�P�`�u\�ӯ�\�!�� ii2\�.�z���7�\�ɃO\�eh\\�-�l+\���?\��c*#:�\�d�\�̧�\�ۈx&T�o�q���F;\ZH��\�\�Cv\�ze>k;5�p��G\���󳤢\�G]e&6\�\�Cp�\�\�vq�*��\�2Q������!菦>\�Y���,b6\�=\��B��ۆk	\�T\�]B�\\���\�h\�\�*y\'�ߥ\�\�H!�| q���ZC��\n\�\��&[�\Z\�\�+\�U!�X����b����W�rՂ��\� �\�ܟyCw�>!��z�֔dn��\�\�\�\�s5\��l��j��c:̘0-\��K��ˡ�]t\��^\�NB��f�M\�\�啕-;>2�\"\�\�%\�xQ�\�\�\�|e\���5,�]�\�>�j�R��H�Ƃ+�J,�\��ʩ�� ��\�&�~\Z]\�\��`���-�\�\�\�\�pD�`/{4��}t0����j1Gnn�\�\�)/�:<��42\�I͡�U�\��0\�\0_\�\�Ԗ�\Z	���w3V�\�R�%���drY`�{ߺA\��ɋK\�d�\�\�L�딐z�&\�\��\'��?e�\r��\�\�r|_\ZI�j�}3��>�$�\'}��ęY���@8`\�B_\���\�\�x:��®N\0�l�n\�$K�r�^�a�\�,8�I�PǟA\�,�@\�\�_��DiI\0<��W�j�ͼw[����\�\�s�)2K�\��c��;\�>����3X�[�cᯂ祡<�\�,|M��\�]Z��gaX�\'����u*\�\"=��O�,��${YK\�v\�!\�be>j�!\��\�}п%�MR\�\�P<����4߷=�1\�\�*\�˓�x��}/q\�=\�\�MCD�2L\�\���tE\��)d�ѯ�9[0\�1p:?6���\�\Z�\�\�\�\�F��\�qq�\�\�E\�B�{��T\�X�\�^�+�02WbVf1Z謙rҋ\"XͲ�W�Ty k\�\'u8���	�JՄ��z\�v�~\�5���\���\�\�{\��J\0����Y��\�4\rP#\�`|�\�Z\�	���\0\"rݺ�S\�\��p,��9kK�q1$�,\�>����LE\�\\�Z�7�yZo,/�\�Q}#]�\�E`\�Ƒw\�F(2\�%\����^z��*D�ħp\�<\0�\�(n�)a��YĿsc�����Ӽ\�O�Ь���n\��\�@��\�a:��\�x��}\0:�\�?BIu\�\��q���9^�z\��\�4\�\�\�\r\�!H\�ö��\Z���pM_ �@/\�q�c\��\�v\�Sk����	)\�i\Zk�U�\��\0(\�!s�2���\� B\�i\0�\�xйy��\�\�>F�Oqd��\��\�7\�\�Ξ�	7&vNT+4��ȃ\�8.\�#�JV5\�[�\�-\�َ�Ĩ�\"o��^\�u�v@��[f\�ٵS��D\�@mL�\�\���9 ���1�\�TtQ��\�L����\�\�j�L[9\n�m�O��;��7�\�\�\�\�\�՘\�M2\�\r\�1�fXz�\��xfI/5e\�!\\��j@vsА�WqՒM_\r�\�d\��PMz�-I�֗1\0p\�M�yK�:f\�꽅-�|x�\�]�vI�=\�.H:\�k\�r�\���β���߆ZÚT]�x�%\\�Ji\�P��沊\�	i\�v��Hܬ\'a�\�\�%�bd#\�?U��2�_\�j\�d%;揥>��\�WG[a\�\�u�]�ۥ!�\�0I>Y�S�\��Pod\�\�\0��g 5dM\�11-�yr�\�X_�\���\���k\�\�\�pj�x��RX|.G\�)����m\��TUy�Gt��χ��!���\�iIm�\�;���s@\�tGƄ_�\�kp\�ަ��_\"��o}�ƶ;\�g{�z�)���\�\r��i$\�/\�-&��?\�s�p͑a5\�e2��<eե\�\�W\Z�?Ç!W��\���A<)n\�fz\�R��{�,@��\0�\n��5�+2Uh�\�\�]G��n�l#�\�X\�\�.�}��8t\���Y\�9\�N�WK\\ݰx\�\�<Y]�<\��;E�{B\\����B�4�.@q�\"\0\���{/�\r\�ɷK\���\�0񚪤�;�{g�\�J���}���\�Ƿ��-\�{�mM�5�\�w�pI\�wxS;r�Îc�\�\�տ6\�K	sK\0%�Շ\�\�8%)i!�_Z�S�\�\�ppzhi\�![�?7R:3i\�Ǒ\\p�����=ÖڟH}�3�y��\�A̤\� ���_�ȱu{C�Йk\�;\�59A\�x\�\�\�j����4����\�H:sܪ\�\�Nx/m���� r�\���v]4\��4Q3��\��\�$�o\�i��\�����ؕ�\�r���e߸b�4B:\Z�C\�U���\��/\����G�&äG��/$[?՗^�G����.�hC�a㭩\�xd�?�9r&����Mv�ML��`�D��9�å�b`�\"M\�dn�\�,ӥ��ġ\�Y\�ٖ.0�b\�\���\�\�\�DQ�ͥ.!�94Qb��\��3C}��O��\�T{lɗ�P�Hݖ���w7��$���3F\"�y��2\"�s�b\��t�M\�t��_�V*�\�bV\�C�4K��js\n�]�\�OЧ�~�u\�qT���l̰\�\\R\�bjYa7\�d�}s<)1�\�&�K��\�>�pUim~%)X�\�\'��,�0#��!~w҃9���\��\�2\�\�X\��]Pp\�:\�c\�\� \�Kn.�l\�ʬ�;A�\�	Ƞ��1\�Įd}��_:(k�x�s�8\�Y4\�\�tG���_��!Ep�\r9\�]Tى��\�l\�eCZ\�\r�@ɕQ\�(#8y/\�_w7����\�\�\�dJage��9ym�\��QA��x�F�\�[�f�\��<+*�*\�,�F�|���\�\� c�ai\Z\�v\'�H2ʴʹ\�6~5Bg\�\�Ĥ̫hHȪX���=TWt�\���\�\���(H\�i\���k�x0{\���G���c��Ǯ�\�\�����~Ys�\�L\�<a	���\� B6\�\�_RH �:�#�$m�81	+�\���eM�?�ܓ��=]\nѽ(��\�}�#W�\�h70ց}�/�FtU	x+�\�#��\�V<W_�~9m�\��}%%�߇�.��ġd�P��͔&D8!\��\�\�XY�%��ym�}?�N2|靾\�x\�IS`*%\�\�|u\�\�\n\�k�޺�)\�p\�gC*�j�\�y�>\\\�uҜ#��o���\�G��\�*\0\�Dz�\�\\1h���Z�2\��\0E9�\�f]ƩBy\�\�\0����g{�=9��rTu%�V;�~�\\M]\�\�~o�\�{}� T\�C�A�\��L�$�����6Xk�\�~���\�\�\�,�L��l�ߛ���s\�\����B=\���B\�\�\0\�`\�^\��;5\��z,�$�u��\�s22[Th�QR\�$��)�GY`����R�\�\'\�,W�`\r�\�%\�\�\�(�m\�\�L�u)pd\���a~\�\�/ \�\�ϨL\�\�E\'$\�\�EAX�>\�\�wLf�܁B0F\�\�j\�������aFu\06d@J*&A(ڊJ�kqUD�9\�\�\�˧z\�.�\�1�q��T��k�����0B�\Z��)V�_�B`�6ŋ�<\���F\"\0J��`>z���\�A�\�\�p�PyU��F\�_\�R\�t\�F��ζ&�*�^\�m��\�%���\�\�:�C��&���\�>�(\�e\r`\�7\�Ť:\�\�A	mwT\�X\�Iց�\\\�8��6#�ѱu�y�Ҫ\�\��a_\�\�C�\�\"wX93	$�\�|П�$\�\�<M�p�\�\�0��C52\�, �H��7A\�G@ϝ�|\�}�\�\�\�t��%���e\0:\��8\n��nʻ+�&����D,	��j̯A�|U\������ۙ�\�^^��q�t\'\�\��Q��Ƨ(��\��\ZH]\�dZ\Z��m�ob�Z\�\�\�\�ɮ���\�U�\r�\�p\n\�9,\�\ncp�\���>\�b\�K��$�>�*�a\�\�r�G	|���E�T\�m\n�F\��&�ʀJ�;��h\�\�#\�\�䕰\�N!B*�\�N���8x�H�k\n��ET\�@-uq\��f_���Io���\�o�^�*Ms\"�T�\�\�C|ۯ�穨f~.J3Ϥ\�zE\�\�/�R���J�!��V\�bV?}��3 ξ\0��I̬;��I�y\�a�큭���\�ǌ�ug=0ǆR�=�27\Z��&Ө�\�]k�$\nDqw��\�f��D\�$�H\�2\�	��`\����\�B~sW�\�\���Q\'�,\�2��$�\�7�[)|֎f{b�S�\�~���\�\�q\�4�\�P��S�9P�\\\���QS�k\��5O�y�\�y�7���J��ã\�\�YXe2}�T�Ř\�\��\ZQ7��\�WCٰ����\�Q&]N�s?�JmPA\�*�B7i�V(���\���\�\�1��k�V\�!���17.N-\�\�\��S�\�Ҽ%R�` C�\�\�Mo��	D,}��ɯ0� R�\0آ7��p?\�#�\�!Ò\�ϵ\�v��\n8U\Z�g\�\�Ֆ���\�\�.���*h�\�6J��1א�\�6Ƅ\�r[\�\�Z��1�m��8f\�+�r��<?��\��%Ѡ	ȡ\�dJ�\�\�\Z�\�d�H��\�}�Oi��ӹ��\�\�S���φc�P\��.9�<\�ChC�Mк\�<T��\�k+�P663�-�ik��K-��MB@�.Mз\�{�։\�#���ҽՑ\"e��^�7\"j\�5�D\�\�A\�wOC�9�\����L1�\ZH6��Z,(\�Sz\�/_2\�Z@[1��\�nҏı�r�lٹ���4y\�\�L�\�s�}H���\�;��s=��\�k\�6~�[�q��K�G^|P6v\�r��|	U�\�㬰�\�\�*ԃu���n࣎���\�Ď�5�I\�O�N=�ݟ�\�y.B\�33�(HXV��A�4m��\��O\�[���g\��ˊO�S-�\�n��8\�\�\0\�w�٣V�Ѵ ����l\�Pni�c�p��\Z���\��\�e�p8�ex\�\�\�J\�G\��q\���i/\�C��\\���I\��\�Z誦g�!3��@W���tk���쯊�)�\�)8bBZsϚ�����)��\\8n�\�^=��:\�\�@\�]�O�\�\�\��@�����$\�W\�fjy\�i����_E��!J\�B��Ⴡҏ���K\�\�X�\�3>�\�<D��\nPj\�\�s5Jc\��ys\�1�\0\�y϶\�+\�NX���&�\�޴<R$i<	0팮�0\��J�&OӨtҫ*v~\�H�t�1ߖT�;�\���֦�\�մ�d�]��R�M\�\����Y��\�1_\�d�\�+\�\�!�\�\��\�o�߾\�iڙqu�\�\�芿軈Q�%{��%6\�ː4U���[�7,�\�A\�Jҫ\�_ﲚ������!G,�0\0�;��޾��\�\�^q�2\�Q4㺨�3\n]�Z\���r��\�x\�D�O�iȥ,��vG���\�$�.&�?dK\�6�0��\�\�3\'��G�\��A�\�y�&9���Z�]xS�/\�\�\��b��D�[\��c�L]�p@Eb\�;��+].�\'�D��\�\���G\�\��\�g�f\�{���3�\��H��u�x�\�;\"\"�\�\rm�d\���\�4z\'�&L\���\�\�ewZz(�>�ʲ\�\�\�Vm�CB�WH�=�X�L��z*�\�H|\�l|\Zj�6·D{|nWC\���7��O\�Ĝ�f��8+B\�\�-�\�m���\�R\�\\Ğ�\�uͼƭ\�ڛ��°>-��q o\"��X�0Cg1^%�e8=�\�\�7I\�K�l5��\�\�	a�\�\�.Ш\�\�|\�Ψ\�%�\�L\ZP9����X�%ҍ�h�\�yn��\'�Q�\\>)`�7Q\�\r8\�\�߱\�&(����l�V\�\�\�\��Y\�\�5Fe\n�N��\�~v�?��_��ِ��o0&�5���t|�Sz	\�臺�R\�ز\�\0Kj�8\�(\�\�IJx�\�Q\�\�\�߽9��6�cl	�\�>E��P���\'O���M\�=�-�m��\�4��\�\�o#��e\r\��\�~(?x��Frcp.\�0%�x\�0�3�_4\�;\�xy�#&�*k\�X�\�\Z  T%\"b\�!�3T��op�zl�\�/\�b=6��,\���No��\�Y=�a\�\�\�~y��\�\'��?TF�\�0\�R\�](L?a(��$m�F��<�\�[%a�l7ź\�}�Y0\���-Gz�,�\�a�螎\�\"\�K��|��\���a��\�>\�F�Zr>�\�� \nQ�DŁ�ǒ�t\'�\nsDm�\Zc{<�\�.\'��A\�)H���6�f\�;t�`HF�,\�㕯\�ݛw��nQN\�\�8��G�_\"�\�i�i��\�imH\r\"�\�`[� ��f+W\Z\�liC(EG�\�\�\��\�|�\�5X\�;AS�\��\�dm�_����\�=\�\�|5|�\�\�0�\�^3��*��컾\�%v�l�1\�S �\�:�\�\�ä\�gN�\���\�h\�\�`��C��<�\���LM�fbM\�\'�U_�\�9��IpҤW��\��1%,\�\0BZ\�P\�S�]���\�\�q�#ƿ���<\��\�\�\�J�&�C��2�*\�$�\"�\�\�و-ӊ*\�\�[VD.%v{�M�\��R	˲�uuv�Y*)�yR\�?��=��d�A�\����U\�\�Vn��S���T�Q�5�iw�x��\�d$!�PS#J�M�U�\�RY�.T�G\r�M\�7�9\�\�|��;�	\�	���wҷ˚�C5��Γ�\0\�@ҳ�\�o<����a\��/��\�\��	s\"Lq\�o�\�IN�SR��)�s�\�\�\�2FԂv\�\�C�\�\�R\'�\�i�\"�?\�\���q�h]y��\�nR@ł�\�>G�!\�j�&:\�Zi�th��c	�\�\�9x�uݡ+\�ϔqn\��m�_\�k�\��}B*�f1a�x�\0�\\R_\�Wz��?�Ԋ�z1�i\�\�\�Rx,�MD\�7���\rčIg�\�*>F�\�X���\�:�m~�\���G\�sZ$�\�6דZ7\��\��\�5~\0v\Z�}fX�jke�B���J)1���\�}����Y��s\�����&��w\n�z��I㰌�5=�\�8v\���\��\n˘\�X|N�4��r?8\�\0WS\�q� WO9\�U�=0��\�;\�51\�Ǫ*�~c.4J[��vַ�\�:\\e��N��\n4\�M��4��\�\�\�o�t�� ��f��\�Dl�� �.\�\�5r=�\�\��5r�]��-�N�\07#�Oͳgx8{i�V\�غ�\�K�\"�g(\�\�\��\�\n�\�9\�2y&s\\R$\0Ңܯ\�U5\']�p,9�E\��4�\�\�ڗ*b�c����hў��]�\�\�\�\�ؗϧi\���>�t�gߦMƈ=��x\n.\�~��`��l��t�ЊadW�\rQ\Z\�\�\��\�#G��r\�W@WT�c�ی~\�\�9��HhS�G=���4G(1*\�̌J;\�\�\�Κ�z��F\�ٮ\�3�h\'\�?��]\�\�C�f�\��cz>D�J�!\�F-t/\�c�yf8\�*S$���I\��}(�y�L\�\0\\�bx9\�3�l��k٫9���I~�0\�~a�1��ɭ.�/\�\��c����b\n�J\0L��5\�6F���\�sL5D�{�\�\���\\	]S*)��\0\�\��=ԍa\���\�दH���B\���,JDq���}`r�\��\�\�3�v(i]����z\�\�u�:�\�\�\�^�*>\��\�v�\�\��)�k�\��]\�2#�K\"\�nz\�!�<Ц�)p�{�[���X\��*\�N���)�\�o��j�\���\r\�6\�}ss\�*\�\�c�obo�\r��Ȏ	\�:�2vyo\�\n�Ӳ�h1\�a[��û���\�\�[3);(K�3\�\0�\�b�\�#\�*����\�e17\�\�񓚄\�\�MW,\�\�-մf.3�\����LK\�b\�.Ĵ\�\�\�#�\�\����\�O*\�O\�JQ�����\�G>®�f�W\'EYV	�yy��5����\'�\�cc&\�\�ABb��;�$�G(e��\�\�S\�V\\k\�Ĉ�\�d1\nOv�a�\�\�)cx~\�\�\�\�\�\n��Mh,��\"�pإ��>\�\r��\�O/H��k�\�\�\�A{\r�L�\r�j\�\'o;����!8O�=��;\�WF\'I:\�\�Mj�\�:��d]\�\�%�	���\�{�[t�]\�\�o�<`�ŋ,el\��{\�Pi�V\�\�\�C��s���7=�\�\��\�6a$�\��8�9۩�o���3�{�\�[d�\nGw�\�\�R�Xwv��},lC��4$Ύo}8��\�\�~�_\�\�ƴ̘xFV\�\�e�/@gi���\�\�\�\�z��)_Ҍ�\�\�+Ut\�n�\�50��4%�?\�\rD��-���\�4��\�q�t��T\����\�\ZS�H��rT�/\��>�\�Ӽq\�J����a1<G\�C$\�\ZB\�7~\�ا|a�\�fc\�%\�\�Tu&w\�\�a$�\\e�|���G\��L�ߺ�6�<��\���1\r�X\��m�\�>\�\�pR\�f9��(\�6��ڏ�9qc:J�V����-�����g��\ZJ?M�m�^�:Hj�\�\�\�h	��\�\�I\�*����d��gВ�Mf���-�&��g�y\�\�\'w&Ei�\�\�\�V���+������:[0`R��C�羽\�JZ\�\Z\�\�8\�?bR\�H�xJ�\�^�p��H��\�\�\�NE���}�m_敲\�5S#S|\�	�\��CBV$6�\��?~1�\�%\�\�ڻo\Z]��3~b\�\�\�#]Z�\��\�\�\\\�هH�\�z9UR(,\�_�+�QތeΡ\�M�Ւ�=���۶\�̣)Q�\�\Z$\�2�]5\Z\�\�\�{\0&�4\�h�{���3��aitD��\�\�d\�Eu\�n4x\���U\0ݺU�d\�\�٫\�~[�������Fu��Ij\�M�R\��L��Ù���\�7�\��\\}�\�i�]*��Ȋ��]T))�yat\� \�&���a\�T\�\�U\�폮yFP|tv��\�]t\�_*���W��/�Z\�L�w\�\�LI�\�\� %\�b�\�\�ݥr��Bq\�|?�L?�WB�\�\rb~�=i#��H\��߲�,�]k��a�>\�;]\�CC��o,]{SJ�W6Q\��O\�\�5�3�\�\�\�\�%�\�Bi�5��)�ƚtݺ&\��f\�\�\�\���\�\0�?/S\�`v\�\�]�\�o�n��aWQ)��}TpS�\�믯l4\�q\�1Ql�$C3p�<ɕp\�ı\��}Ԧ\�\�V���C��\�N�\��N^|\"�2���\�p<m���\�q��c�\0��q�\�]�X�\\�v8X�����.�k{�\�q̅?ĺ,\�Ld\�<�\�9\�1R\�y�(��rѡ�PI\�\�~\r�M�^\�\�6@oЃV�_�\�fr�,JA�T����~j0\�\�\�=Fx�-�i��	> N3I��e�J�Q�(h���`\Z��\�G\�1M\�\�~e�}�\������\�<Mg�{ҧ�\�@O�f\�Pj\�{\Z�\��\��}f-Q	�\��U�櫲�X\�\\:{\�&\�;3aB�JY\0\�-�u�e%��K��9�]�\�\�g�,Y�\�\�kx�\�=��B\�U�\�\\�\�2\�\�d\� W#�rٚq����~�\�M9[�E4\' a\��n�\�,;�,�R|���\�8�]o9\�p��\�χ���ao	�4e/�\�I�\�\�r��-�\�k#\�\�az��ə\�õBC\�Ku��\�ԗ\���Q@�տ�P���?U��~Z^\���J��䔁\�o�_�X\�~ǘ�\�\�\�5\rEⵥ|[˘\�D\\.^\���*3�=G�\'��~�\�\�\�WH��P�5<\�f\�1���M\�\�x\\���0�o�.-\�\�\�7\�㻺\r�#\�`�\������\�Sh��M\�\�c���b�ꡯg�M�\�\��\'�W�/%�EF\��\��Cت�\���ձ8�W�\'\�:^\0�\�^\'\�ͫ\�w�\��Ov�\�D\�o�\�\�\'k�\"�am~��]~���fո\r\���&q\�`O���\�Q/��˿�=Y�\�j��]]\�W\�\�c�4}�r3�!\�j�\�ۇ\�P��X}cbթ�3\���v�pI�Jw�Tpk!V������>,\�H�9������_R�\�RA\�r�;\\lP�L!q���� �\�\�/�}�\�C��\�̮��U.|�\"yK�F\�~c%�O�m}\�q�b\�_\�\�\���o��˧\Z\��K{]\�\�\n�}�\�@SP\�ŁlЫ[dp\�\�\�.�W�}\�\�9M�c���H��2��^�\'�\�\��(�HE���O����2��mW\�%.\"�\�K֧��ß͏�\�P\�h�mt\\�#�� L\�t8\"�_c!淗{m\"]�\�?�\�L�aJ�\�MZ-�J~\��G-$Id\���Yv`�eu�i����\�+���8��]Q���\�je\�x=�\��`\�\�a\�!��QB=\�\�\�?�A����;�X5�\�~�N.EN\�o7Dz��-!p\�2Um�%�/�-	�\�j�~yoss\�\�\�upе0�\�?�\��7S�|\��\�\�d6Jj��\�\0;����y@^\��\�\�gǖB��O�t\"�|};L��HL�\�!�dg�\�>�Lz\�a_�(׌\�iRA�\�(aº\�\�\�g\�0�1t�FO�G2j�dx^64�W}s\�[|b�\�\�z�쳊��gg!�!/e,+d$�9�Z^�\�@\ZC�GJ^��Ӱk�g� 2�]�\��i����\�\np\ZPY� t�lsðC�K�\�)�����\�dT\��\�\�0N�Ȱ\0�\�\�-*m��94ϊ���)	l\� \�_\�a�\"\�,�ܝ)�F�ו\�B�\0\�\�\�cUe���8�\�\�Kܢ>��;��Q#�\����\�+\�\��r�[���P�.pK\�\�$}\�ST�\�P1~6��Q��B\�OG\\����嗔\�\0�\�!P�j�#�;���\�\�_��\�\�O�%x���o=t�\r\�O~�Ϸ6%\�݌	���K��Q>]@��i�c��T�`�=_^O�ب��_E\��\��Ve\�\��\�w|�<�\����IR\'\�S��Qb{!i\"a�d\�W\"h|\�=���\�}LdE��\���ĝ�\�K\�hT,:��K՘\0�}�ƌ\�Zk\�p	�\'<\�\�\Z\�߻�t�s\���\�\�\�DǱ\�\�r�oR�?�C7�\�\��--���k�r 	L#����\'e�YC|d\�ߨ��\��\�\�T{6\�o\�3�p\�Ԭ�ƄI\�ێ\�5�e�a^�[�ߥ�:\'w\�\�\�\��Kܷ_��Q\�\�pq�\�mO Wब�\Z�&ǾK\0\�\�bD�%\�Ҽ\\#o(a~Ag�\\�\�\Z�K\�C\�[+KW�\�^[��\�y�<�<\\n}�p�{2oz\�?�<лZ\�H�9\�x_�Q#癪\\��V\�@K�	��?��$J|\�\nt�s�a|t\�\�\��2\��\�~�\�Cw,\�o:5\�\�#�\�1ܻh;.\��;7x4kAa�\'��ߘ\Z\�WK\��e\���`���h\�M���\�H\��k��Em\�I�-�/\�5̌\�Q���/�\�\�\�3A\�9\�I\�6�_�����:�[\�νϘ`�\�ir��,�R����\\���{��\�����ah{(8\�D\�\"��\����׿���in\�5Ǌ�:~eEO�\��\�chkR\nk�%=k��}�7��Ο\�i2W�\�74lL\"�W�\�D$�\�O\�W\�Bg�?�EFI�\�e=Z�N;�<��P��\�f*C\�\�\��p]�N��\�Ych�\�\�\��.z\���\�k�ɃҠ}�$T>j\0\�D63N+\��K)����\�Ч��nq-\�\�z姶˳.K1J\"�-���-\�\�I7��׆@�;ݞ�Pkѡ>\�o�?�L�1�\�\�L%m���*���(\�\�\"��\�\�\�E@\rI�6ͯ���\�\�E�j�t���s\�D{�\"�\��\�rhጪ\�\n�Ώ�}�\�7?1}�e\�=G\��\�(\\���j�ŷ�\�,x\�6\�j�!cK}+�4S�掿;���\�\�?�\rO�\�X]ǌ\�\�\���eA\�f\�n|Ly�\��\\����By��T\���`}}\\\�w�w=�\r�������>�\�N\�\���g�Fۺ�h\�3�+9<ؤ��\rQ��BK\�,RV+\�,o.�\�3$�Q�\�3r��\�o�\�*\�e;\�8\�N,�\�H`̶4�}�\�N�\�0�F\����\��A\�,��|_��\�G8\�}\0���\�5�ܮ9\�\�P7d_�n\'1�A\�\�g\�S&\Z?�\�\�5h�\�C� ����Q\\\���5�Y\"xjb�Ǉv\�_m�VM\�n\���.��\�\�\�\��\�s�y\���\�0\�F�\�\�P���7\�x@���t�\r9L�����t�+!��(改[\Z\�Ӝp\�\r��(�\�!fk��ʎ\r��y�e\�h���9x\�ߚ\n\�^\n\0��)\�	\�N\0����hq�.�\��7X��o\�/~dgيz�#�\n\�6���F\�iI\�iӒ[w�`]\�Mc$��E:�\�\�,\�\�:�E\��\�$:\�\�\0Gr\�:�\�{)F�\�iU�r\�ȏA\�؁�-�q7�\�-p�A\��\'H\n<\�9(�|cm\�	\���\�Ն4|4\n�:�z���w.G&V>�.�,	�)�I�v \�\�2�\�\�\��\n�0�AspL�(�K\�O!g�\�O{;\�\r�ը�H4�-\r��iw)D?+Jp<H�\�\�B�|۬\�M#�뀇fҟu찜W\�ٸ]N\�e&ड[i�N�5c\��\��\�??˭�v?\�\�j���gh���y\�@ɔӄZ�Aml�����Niu�\�x��!�zT��\�1\�e-Qݮ�\�h�5��@�\�\�Ȉ\�Q�0z/\r�Ő\�%\�}��\Z\��$f\�\�>�/LcI�SI�\�(@\�L@;3m\�q�VSE[\�\\�I\�aP�a�\Zo8�`o\�Pj\��\��F���\�N:(I\��\�̛/�\�Oc��$^,���M�o(�*�߅\�\��i\�O�:J.1�T�ySOTn	�|��\�\�ɼ�	fN�;�fN\�C�Z\�95Ʒ�\�5׹n\�V�(�tN\�\�`\Z��3������\���\�\��\��n\�\�r\�L\�\��\�G�U7��\�Ǻwׇ0#�\�t�U1PK�ߥq���\Za. \�x/\\~��`J�{�{.$	$[]g\�\��5k���&��|w�\�s6�\�o�3�9�?6���\�\�\��\�\�d�\�e�w1�iէ\�ס��}IЇQ?�\�@s8F,}Ft\�`t>r9\�A2\�\�iJ\�i\�\�ͣVg\�E\�q\���sL�\�&��T��c�J\�0\��	�\�E\"�1\��\"����;\�I\�1}�\�V�}���\�E|;{�VX5VPz�qlF������l�\���\�p�Lo�\�{�:D)h\�\nAU�tqGU>�K�\�t��\�)�\�=\�\�V�5Wc\�!���=i\�P0�2�X�\��Qj�<0���$D�H\�\�.p��\�?t�\�P\���{\�L\�5��<��-�\�Pp���ΥIg\�\r�wf,��\�\�\�|}��5���C?�X黇97�ďGN��\�\��뿬|CI�ѕ\�r�\Z[\�W\��	\�i8p�\�\��Y\��u�ܵ��\r�9���(\���u��\�ߗ\�xpc���N�a�n���Q\�,��������\�\�\�\"�4\'����Gk\��!$[B8c�$�}\�;o`2�E�\�:\�\�zA\�\�\�gz�R%\�\�7���\��\��H\�K�0���\r�b3�\"j���\Z%.:\�LEy�W\�\�\�#F�l��\��\�#��;�}�O�b�fܢI|\�68���!Bn}=,t�%��+☽81\�\�U��<���\���\�?S�Uv�w�.D\��\�Ul	Ԫ�k\��^\�\�&\�\���oQ�W�`n�\�)�\�$�c�M{�u�\"\�\�#n�����񷹡gg$\�1y��_y($\�\�\0@��\�|{�y�&$�8.kbR\��04Q�\�\�\�U\�\�͟S�\Z�өA�\ZY\�\�\�A��>\rq6\�-�yY�ze\n\�$�\�\n\Z\���ވ�s��|9{���^=ӳ=M�\�=�\�N�S\�\�\�e\�Qk*\nC��@�O�8\�;C�\�!��\�Ĩ\�x\�p\�\�AԘ�(\�*Xg\�\"m��6P�\�f8�\�^G\�\���\r�Lqfҵ)߱�<�gN\�KD�/�s3e�e\'�)\�\�-Pe�\����ϿVM\�\�2SM�\�6.\r�\��:3馋u���;U�\Z?h/\�u���;\no>��[nR#�\�\�/�0s�z,�,w>\�ǊIf��=�7!�9�;�m\�w�<^�\�	fg��o@:o*l*\�\�ע�\0<U�\�=p��#WF����-�N$Bl�@m�,\�\�hϡ�X~��\�\�\�cr(\�z\�=�\�&kwg��`��\�W\�\�\�\�\��Ͷ�\�\�5\�C�۹63��7P��\\�Fuy\�B�ղH��ݏ\n\nA\�dT�\�\�)���,\�Γ\�)mp���H\�\�\�\�\�ĵ��4k�\�3t(w�\�R\Z\�v�,�\�+l[��(%N|d�q0��ςp\��\�\��y�\�Ǒ>Xd�9�dS_�ΰ�݂!��	Xԇ�{���\�j\n�s�)�O��\�%ݬq,\�\"���޿��X�4w\�˃�\�x	���\�\�	[\�O�\r���\�d4£�\",~ˋ��y\�C��:\�o�L\�Vr�(\��\��\r`�\�\��\�TL�m�X\"M\0��u뚱��\�>�\��Z\�S�~���#\�j�5�:�0�.���\�m�\�ʩ�\�MyA*\�\�/EL\�J\�!3�0�\�\�\�}�\0L��Ub��\�\0Ӿ:]\�k���m�\�tj�\�M\�\�\�\�\�V7\�+԰�[M�\�q\�g��$Q�K{�Ά\�U\�CKZ��v-�_\������\�]I��\��\0\�\���=���Nm\�\���ߠ\�$L	,�f�~��~�\�0\�\�[В\'��q���\�\��\�j5i��X�I-�#�\�\�\�c\�t�,�i�\��\�iX�j\\�b\noP �w٥T�V�u�\�&Z5}x�\�d\���\�:#��vDq\��\��\�o�\�SB����HW@�ٻ���	���΢�>���\"}���\r\�cI^;�V�\���ם4:��@x�#8vl�\"�YE�ݼz��\����ݚ��t8\�\��*�������܇,@\���E6�\0\�\�`\�\��\�H\�\�֠>�^\0JzO�X&-;��leɟ�J\�l �Q��Iٸ]�L��Xz0p`�\��׍�`%��-\�7�f��\�\�;N�#z%q\�4��0�43\�\�F;��%�\�@��O�V\'}u��=7�r2\�Ê����2T\�!�\�L�T�X��q\�\��h�=�\�VMu\�\�+\�\0\����\��k�\�Be<7\n��b��N�?��\�vD\�Y��������b��ߨ\�b�M\�_��\�\0\�쳴�A�[O\����������|c�W\�ؾ��\�L$4�U�Gs\�٣��&�}~EI��v\�\�uN\�Zi1�18���o�8\�[����\�@U\�n]ͻ�0�s�^Ռ\�\r��|�\�d$�\�r/��>��|�g$\�8x�\0\�M\�\� �b�0\�D\�\�\�|�\��t\�\�(_��\�rR\�P	\\tϣ�d=�|F$A�=�����Lx\�P\�娶Z�8|�\�\r&�\�%��BFC�xm��\� ��3P7.6�4W���c\�Nw\�|i\��0\�cN�\�e\�G��~	\����ej�$\�$E����\�\��\�z�,�Z\�<�\�p�S<\�#�J��.Љ��URj�\�\Z��\�\�s�\r\�p?��\�\�*�R�1�d\�o\�\�\Z�!\�\\Jް.��,�,ќ�\��NH�!X(\�f&z!\r\�b)�es\�鞠w�J�`\�Z�A�R妄\�M�)�j�	\�T=D��\�\�\�W\�}g�\�K-\"�\�%:*\�$�]K7ك�KA29ǚ�kκ~ے�:;���X�\�H��x`��X�\�M+\�\�=KAfG���\�h�7�i\�K�Y�E�vʮ����\�mXG���W�\�vfk�\�B\�\�I@OX�StrbL:������;!,�Y\"v�i��T-t\�|\�\� ə�j�\��=OhI�B�\"��V�?\�P!\r����\�eO�m�cmW�9ZI�\Z�\�9\�Հ^\�$��4#�cO��lmrj��N\�MNC����\�\�\�m�j݉\�L����z\�\Z�L\ri\�a��Pb#a�\�>mH��+�Yw�\�?[\\�OT�K�H�:>�\�qk-t,\�.!瓎�\�	ф\�\0�yY9��\��ق��6\��:7\�ϗ���\����fC-\�\��6FN[\\a.�j\�Ia\����hn\�Y�ի\�1\��D5٨�\�\�3W�\��t-\�9�Y8�\�Ok�\�䠋ds���\��$1��Ճj�\�=~�Y\�e��DOgә]c\�\�S\�d\�,q%�<~\"��\�ĜoL\�]���\�=�\�֖(��\�\�(\�\��f�\�B�\�H_���9��4F\�\�D\��\�R\�\���?\�\�\�b�hd`4�w�=�+��s�\��FlgX\�bY\�㺧bIj\�6n\�dn�y;\�UM\�\�\�x<�(\\+\�ʀ��\�\�q\n紗���\�J,���8Sh\����\�+�&\�\'Sv�f\�R�Ho�\�|k����\�\nTߜ0X�\�v\�ƥ\�g ّK˛\�\�7�#��jh�l�v¶\�Brk=�\�}9ӌ:C&�r#1�T66���\�R͐��گ-�\�\�\�\�t\�����.SK\'��\�$H`�eeZL\�V83\�lcVe!`���N��-\�R���0b*�\�8�N�\�V8ۛ���2�\�\�s�H�8��T+�\rget]�g\�\�׳q\�1�\�m�ί�b\�r�\�\��::L�6��r\�<I��6n\�Hq�x	�\r�\�	a�%\�o\��\�hײݣK���%�K\�\� Vk9\�M��;LA�F��r\'�\�\�r\�\�\�pt\�3dht�i륱�\�8:�\�\�R\�CA�\�%gjo{�׳�\'0^o^\�GA�v>\�ٖ\�r���\�\\\�\�i�G9�e\rێ\��AÇbm^r��MOéҞ\'\nCQ�/�dG��\Z!<�$BE�\0vX��5}�º�U����!�l�jG8�~\��>L&F\�=!�x+5����\n&Sc�Y�Rs\�Yq���\�;	U�����%.K�yfî�>\�*.j$)#�H�l\���A�M7\�LM��љ\�\�\�s`bF!�\�g����Y*�\�4\�e:Зj� �T\�i�旞0%+/:��P����{}�y�)SS;qٔ�A�MV}�\�[n�\nJ\\l\��`8\�6���\�+%Z\��l\��i4\�H\��=ω\n\�\�ˌ;�RK@��+\�z%,ӈ�\�U�	�X\�i,�f�EsB�ʋ���\��1y�ԫ\�X\��V�\n��V\�\Z�2��\��<�\�y��\�\\\\*\�z9\���)�sPc3v�\"���kK��S��l\�	\�x4����n+\�!izlO\'�VP�!��\�`�^¼AS�WEs�4:�*��An��\��\0��\�\���\���\�+`6�\�͆\�\�*\�P);,Im���ق\rv\�`\�J8�\�:C�nnM	��6�fAu�\�\'��\�\�\�D\��A5\��\�*�\\��ʾ>kT�\�suH��\�ӱG\Zo\�\�Ҵ��l�������e֔\�\'�m\�0cj�\�mW\'BRSH;�쬌\�%�m���d�\�\�\�&\�F\���\Z�x\�i�`|��w�\�t;R�\�~G,����F�3V��$*{b\r��.\�\�\�1�\��lDw+*L�ow��\�#ሪ gL\�a�w\�\�?�k��\�D^�;����\�D�9�@|\�ҏ5��CX�0��\�to���LX�\�I*D�\�r9-I	\�=�o<\Z-��U������\����7���oS\�\�\�\�\�\��jITk�\�:jfi\'&\�Q\�L��\�ħ�rj\�8Mw\�\�L�	\�\�m>iwF3RC\�L�1IhD-\rJ�n77j\�2�řS�kO��\�>\�{sZ\'�\�nGa�0=\�p\�o�\�|������\�)�ؗ\�9�4�\Z@{n9%Vgf\Z�\�(�\�l#\�Ѱ�\��\"�[�\�ʣΪ�m�d\�jf�\"\�)Rq�*W��\�}�XMi+_	�-\��|���:�\�ʎIŜ�Y�\�C�Xa_6ޱm\�b\�\�\�2\�R*�ө�\�7kA�BS��x?�i�� �<��Ն*�wir�\�t�#\��\�BVde�2n���\�\�Lӡ\"EIB��f|�<i��\�v0�$���UL+���U�W�\\4�ٱ�P)��s\�r�2\�7h�x\�\�x��\�\�tN\�J�\�p�K\�lC�B--�n\"��/\�\'�չekD�\�:�\�uܦ\�AJ��h�%�bX\�\�\�j�\�c#N#Z0y�\�s\�\��q\�:\�el�{��\�\�1Ǒ\'\�\��B\�\�4\��ΗA6\�I�\�ti\�d�P�\�j�\�\�X�&REY\�8���\�\�\�\"�WФQ4N\�p��q��Ou\�P�o�\��\�Nu�۔$�\�\r\�y\�q\"k��O\�EC3O���>\�\�V�\�:�\�M\�\n\�6�L\�ᨥ6o)$jx�cgsb�\r\�&/UF;�5u�\�K��Ω�˽��b]\�\�n1���w��1,�@�6�����;\�\�s?H��\�T�\�\�$J\�\�t�C�Ą��ޢe)\�C\�m�\�67�\�㺶0\�\�y5�p�*Bmڜ\�дz\���b�螑���[\�ĕhw@w���*Q$1�����s\\l\�C%��/6\"\�zc�Ȟ�\�\�ѡ\\�\�$mrC̣Sتu\�9\\\�3\�a�Xu�h�\�m\'�lH8R�j\��1$��ܟjz<J\��cz�\�׾U�q5�|��4��=MYIR��\n\�ɶ*��\�\�lt\�W0ru�����$\r�İ\�\�=g�v�r8\r\�t\�\�8Q\�,h\�\�Oo�֌�8�E�(q�\"z��q(\�8���5�\�I�`\�C]H\�mk��K]���!כ�k\�aU\r1cJ\�\�\��\�}\�,\�y=e\�Lv�\�u\�r\�zY�USYb5p\�$Z��@N\�\�>\�C�wh�spgnȳe^ޝ��\\޻�༠I\�	U�uIq$Xxv�\�	��#�\�\�\0s-�W\'a�(\�,�W�/��c�\�R$�h\�o#��Y�צo\�\�\�;�0ɷ�z��\�\���\�\�%�MrxRɱ�Ť\�X@�nHpi\�#�\�W<xegb�tM�am\�tH3Z�� �\����]����n!�1�1[,\�m��)0��sf����\�N\�e \�2隋\"M>sܔ;\�~H\�\�~�2g5\�KO�wf�c\�Y/��P���L`�؝�\�m�3�֠8.�f?�.ѻb�qq�\�8v\�C0\�7��$�x�\�\�8�^\�1\"�U��W0\�\�˽��9	\�j�۳g���匠[-�M�\�\r�|f\�2p\�\�\�\�ls<l�E�|�,��q\r�h��\�\�͂!�P\�\�#%h\�m�O��V�`Yk Fؐ<��\"zJ�m!�\�\�C�\�\�I\�\�r�YH�\\\�[B�R�Ӷm�\�i�N��R\�-�(FZ\�\�k�H\'\�UX�=������n6T(du��A\�;\�Di@uǼ�bK\�\�A>O\�!�?T��\�\�Dġ�tG?\�j��`G\�E�#1wP��������\�\��\�5�-�gt�\�G��\�\�n��u\�T�\�<0��ٵ\'�Zl�\�\�nq\�\'{\\\��r�\��P/\�\Zt\�\�	��\�͊�U_�a���BpK6k�NX^\�\rMf\��&JƹZ*\'�\0�\�\�ȟ\�J_m�ծ#~���\�G�Ԃ�t\Z��8k��߳{\� U��b4�L\�\�x\�\�yU�\�ʧ\�򷓄C\�q�>\�\�\�NҒ�e\�\�\�\�7\�D:=EЛ9\�U�\�+{l\�\��n9�\�\��=5�\'�7O�N�\�(�\�\�+�)��\�N�\��\�GT�\�	Zw��p�\�j�`�f�L?O�ݢ�rm��K�52�\�B\�CFFĨbX��0\rO\�\�GX��e���\�T\�u��wauj*\�λ�2�\�Iz�\�\�xz�۩5\����3n\�m6�N\�\�^\�6\�S�r���e3�������\�\�ޡN9�\�*jr\�S}P\Z�\�\�y\"�z�򎙯�!w�9��]��_\�\�\�\��?�󯯶\�ħ*o3��G\�C\�\�\�ϟ�\�䱗ՏNf�ȷ�E^�M�g_`\0~_��wA\�0v�\�\���	\�\r��\�Q�ˏy�>\�����������\�?\��*\�=x\�I\����\�z��X\�Oo�\�Ϥ}xV\�|����w\�?𘛴�\��0�>5\��O,{��8�\\+��M\�;��~����s����s�5<\�?�\�\�\���E\��\�b��g\�z�ۦɳۢ\�ۓk��ӧ.\�_\��r�\�\�\�y��d���M�~GZy�Մ���\�&��\0\�_/<\����\\7\�N_<I�\�f_pp\�xC�X���_�<�\0�\���p��u��\��z���T�\�++	O\�\\��\�!X�0yx�\�ۍ�c\��ͅ���eW�c5y�x�\�\�g5\�\0tx�IC���/\�<Xm���\�\�K<i��ůF\rr\�x��W\�M^|�\Z ���zxLۤ	ARN\�4{~H��y��\��g\�`���W�`z��;�@�#hҀ\�4�]\��/\���W_�)\�#\�\�8\��4/<]x�,7l\�/\�E݀�\'\"\���s�g�v|x�%�F}\�\�cQ\�~�x�aj��o\�f�\�\�c��/g�z\�\�\���WxV|�\�\�]\�F`��^zo�\�^�\0_�/�V\'\��o���\�\���\��,h�m�{�gV\�}�/&\�o�\�y\�~ b^�x]�weH������\�o=\���]yV�\����r\�\�m�X7V\�^y�dܧ\�~\0�p;}��W+�9�P\�\�.��k�a㥏\r\0�o\�3\0��c���ua9�J_Yŝ�Y\�x/���\n�(��\n�\�{|\�\�3\r\�\�\��\0N.<��q\�8�\���k��\�tոֳ�p���R�@\�w��A;��@�>\�\�\�\�s�	�\�F\�\�v\�{�c\�\�oO�\�9���}���/���\�a\�>a�\��?\�\�]��\�\�Y\�O\�È\�s=���\�%R��\��\��\\\\\�#\��5�O9\�m|_��\"\�^��\�yUk�t6\r]7�^#�J�x\�K\�\�.5�ٽ|�\�{�sp\��\��\�}{Yz/^�5\0��_@\�\�\�\�g�\�\�\��PxU\r\�}�d\�\�\�������\�o?��k\"�D\�\�?4���.�_/5�Yk0�^\���y\�|���k�1�\�\�\�\��u��{�G<\�4z\��$�ʋ\�����\�\�F�\�U\�z��\�?\�K��7��\�s��\�2\�E��\�j�\�Y�K}���/��G]���	�.\�\�ut��\�Eb�A��\n`\'����#��פ�\�7�^�\�\��\�\�K����\�(�/\�ڵ0\�\�\�\����\�O�_{\'\�h\�e�я]1d^\�\�Z�o/rὈ{��}��&\�\�+�\�NB\���@��>\�]#\�\���\�ɷ7\��/��t\0���kՁ�[ O�p�k���\�.\0��\�\�d�/=�-~\�^D\�u2��\�c\���ݾ�� �\�_C\�\��	9\�3�?+����$\�a\�>u\�������\�$	�L���hQXv\�}\n�\�\�\n+\�y\�e�%�\�9\0H+\��x\nn�\0�Zus3Ns.�\���\�\��\�?ް�<��\�x�5�|\�R���M/(<��Ң9�5\�\����d����\�\Z�ש\�\�\�zN^Yׁ\�ik\��0�\�)2%Y�\�B\�s���߿-\�\�<��\�\����y\�O\�\�Vi2?�\�^e���S\�%=}���~hך^�^�Ͽ`ؿ>�\�\�t��qq�*\��\��a�\�\�\�חh\�.�̫~���\�ϙ�=|�����i�� <շ0^��\�\�_\�\�y;��}\�n\��܁�\�pU\��\�y��)%?��2�U�o?�\�g(\��y\�����u��e��o����\r��3�\Z3��a\�^c&�>\��k\��߇�x��\�߆�|��o�!����a�x\�ς���*?\�~��[T�_�ri��\�\�\��J�\�W���\�\�_��K�������%R�(MP�\�\�\��Js\�~���\�?���V��m\�m#旅�!�r	\n\�\��\�J�\'U쵐�.�GU���W�|��G\�K�\����\�7��@S~\�I}��V\�b\�M\�\r��W\�^nU�\�Y{\�7����\�\Z�\�\��\�ۤ7j���.�\�PÛ\r\�닎\�d\���~�\�\�\nx�a	����\�\�\��x�V\���\�]|�\�0�-�-��f�\�B�G�T�4\�	\�m\�yx�(�]\�\�_��\�ǋ\���\�\��g\�^\�N�_\�\�3���',1478565070,NULL),(7,'iAz9jZrWu5tD','phabricator:celerity:/res/defaultX/phabricator/cea72e09/conpherence.pkg.-ADKWGvys4KDe','deflate','\�Yo\�\��}��H1�:-\�\Z-P��Aї�ɑŘ\"	��\���~s�s�\� p�y|\�|�\�\�\�CVD(�\�eV�\�?���Ri\�\�\�\�O8o\�]^\���,.��+�\�\�O\��2\�\�Ev>\��|�6i�\�\�N�Q\�4\�i\��\�\�\�\�\��e��&\�q����\��Ś����*\�y���p8�\�U����\�\�~���\\\�\�\�\�^�ڭ�>��x\�To���z�\�_W��\�l���,�Oi��>~��.3�:ȸ\n��P^��\�OO\�_ \�fGH�Ks)�\�V:�\�N~�~\�\�b+`Հ��{��nU�v�׏p���Y\�4�\�/l��QVď}\��G��E��@\�~R%H?�|�\�<�/��Xެ<��Jc\���\�2#�	�p~�D\�S?K\�\�O|�st£ק��G\0\��\�<\�l�\�5\��\�$O4TZ,�4sР�	UiN�m7�d�dނ�0h\\��Ҽ#��S�\�?���r(�ai��\�D\�0Q�!\����6?Q\�ȱ��d+|�H&�\�\�\n�%���%JȎ\�#��\\k���\r��j�\�*\\mכ(w\�\�	\�窆��y�<�W��͂\�Ǹ�埴�|B)\�Zim\�4�9z��M���lWH�T,1�5�t{rIW\�U���O��w8\�ҲN\�}{\�&@\���jテ\�\�B%{���9w-s,�\r���?�\�\�ʮ\�\�z@�]��v�0W�֓\�_���XZ�xC�I�D�*nk�\�\�o\�O\�\�~�\�)M��|���z\�p[��\ZӠ�F\�\�\�\���9/\�ً�\�ίn%i�3\�\�	\�Fb�\�8㋓�!3�1��\�vs3Ak\��Z3�J��\�\�bv\�\�f,bD]mrT$��\ZS�Łb\�{a�\�\�*\�I\n��\�\�^�_�n\n\�(¼�8՗v��û\�x�Y\'�96\�\�f�e϶k�\�]\� \�\�\"t\�l���WR@ّG72!�_��\�0�zi\�=R_\�\�i\�xu\�\\�\�5\��\'\\\�$n~���\�E�ߏ>���P\��ܠ4\�\����\�\�\"��Zo��}_� ښ��P:M9��!\�\�D�h�D��}��q\�>B�y\�P;\�S?\�q��J̀�\��\�z�\�\'\�\�4�\n�\��\�\�nt\�\�ȱޠ�)��\��� (^�:1��}U\�\��,=g\�\"k\�vX{�\�B�,n�+���j+r\�\�\� ���y\�ef�i<#�\�ċ\0�3\�\"�\n��\�+L5v0u�\�E�hqn`]pG���\����/�\�e�rzd֊�V\�R��C�\�N&\�\r\�h��&�@\�\�H\�\�\�\��G\��A\�\�\�f�F�n]γ\\/\�VwJ\��1B�\\����u�\�\��))�,\�?%�>�p[��\���j�\�\�M<D��\�G\�-w�\n��Es�5[M<�R-�\�,^�I\n�\�\��dnM?`�ݕ\�:7��P���O\��Η%Bu\Z�9zRH��)%����|�Ee�l��\��ڒt\�gt$5)	�j\� %$b\�h�!�nwmΝ\�\�\��,A���F�çݹ,q�\Z�\�\�\�՜�\�\�{EE�\�Qa��n�&�\�r}�ɠS=ՊN+��\�#I�9�8èV��^��\n\�\'k}W�ă&t%X^ɑN�j=\�\�m�\�S@EZR\�}�<�\�с�V#�!\�Y�r\�zC\�\�\�D\�]\�UE\�\�\�{=�\�/��ɽxV/zL���_ǀ #k\�\��\�C+\n�\0�ơO�ܣ�\�\�6�\Z����Q(��@neU8�4���i<[i����)0�\�eujQM/�\r�ศ�er\�!=�ò��\���\�=��`�ʸ�*\��,X}v`tnFå4�%K��XW\�\�M\����ȿ7\�n\�|��\�;��7Lh�J�#ie%\�\r\�u�5�E�.s׶\�\�\�Z\�\Z\'��k\�hl\�&\�v\�\�\Z\�ȔЍ�񐳿����7\�=��~�\�i�\�\�z9\�\�H���\�w\�i�?��n,�\�Ɩ�H6\�g+\�\�m\�j��\r3C�]afń+�\�ܮ;G^ɴ�x���i�\��z�\�]�hE\�ޕ��6i\�EIl\�\�\�)�s��\�\"{\�_\\w�\�4[ِs��-B/\�j\�a~��婑��$T\�\��\�\�i\�z�\�\�\\�\�w�ա\�WR&�\�~\'�+��yO�:�\�نq�w-\�Z[.\�o��۴���j�Vb\\\'\\\�1�D7�C��u���~^�\�\�gV\�\"O`���\n�>B\�{*�v\�j\�\�\�\�\�\0/�\�1(y�ӡ	9=\��Q?f�\'�p\�U��i��\��yH\���1�<ب���\��P\�*2i%A�\�\�UO}�k��7�}��MJ�\�\n\���f�n�9���D�IsB[\�B�e�!JO\�õ����g\�1��\�rP?\�	VӋ�}Y\�XZ�d\�\�\"\��Ӎ��$+�2)��nBE\�܅wD�\�\"��3?Jk|�O+��}ޮom\ryӥs\�턁{e�\�\�_	h\�)>���]�\��5�Y�)�O��X\�J2�L6.lbŞ\�Ƌ\� ͗�l4cЅ7Y�\r8� �R��-\�\�:�\�	�\"d8�ydV>�\�\�V\�����r�l)�\�oN]��okr�\n�\�\��\�\�Fܜ6\�+LL\��\�,ςq\�8\�\r禿c��\�AR,�IBJ@\�\�EuM*2dV�Rag(\�6ww_\�t�[5�e�\�lu\�\0�Q$Ɏl*���&Gw_\�~�_چ׺(�#\�@�\�~�\�%�H{�Z u�3.�Sz�q�\�\�\�Y��63�a\�o�*���p5ݍ�~�>馸\r\�f�\�ǘK\�u�Ĉ�juC���1��Ԇde l\�c}\�юE7�K\�	���v�\�\�%�ehC\Z-+cKM1O\'�/f(��\�j5�\�(Y}��d�\�a�,����$\�ۊZ��BTo\��\'H�ʁP�-а[0\�\�q\�6�V��\�d.�9�\�;\Z\�EQ\"\�\n�F�\�Q�+�e\���\�\�Dn&�\�\�v3�[yF�&\'Z�Z\���`\�\"[\��\"I�\�\0j^4\�\�9`��ﳥaTV\�Vt昴4�\�Mڙ\�\�\�\�\�HCWU�\r���\��s������\�,\�K	w�*\�(�W�\�Je�w\\cQ�\�>B�<�Ob]g\�Hs��\�\���b`a9����TDq�.\�<L\�u\�����j\�\�Z\�ĞU�/\�T\�p�\�m�)�L;\�\�y[ˮ�;�\�Nv\�%Z\�J� mR����\�\�ŋ���҈\�^\�\���J����+\�\�w��\�\Z��Y⫄tM.\�.�a�7<<\"���@��ȹ�,J��>ҧd�o���o,�HN\���a\�G\�P�i\�\�<\��\�]�`T+$\�#I*.>\��z\�q�7땡�\�{�F*㶒# po\�\�VK���Ƥ��\0¾�<��YmP��3\Z%!\�PM/��\\9\�/j\�\�i�\�l��xb��<�;��Y\��Em\�\�9Y}D�\�\��J�4\n\�\�{%1P�pU�\n�8\�\�롣��!�H�\�\�\�Lm��\�\�*.\�\n��d�\�ڜ\�J��\�\�\�\�\�9���ZZ\�?��\�\�a%%aꎼ\�(��J�\�{\�\�|\�*\"�T�1#���9kjKYPoX\��\�\�`�[Z��]\�hy��C\�.t7\��\���A�x˵Լ�nz\�R\�\�\�\���$\�5x\�*L���v��a/c\�\�\\�Ts�\�\Z\��r`�NS�\�\�U\�\�\���\�46�Ι��\�\��o\r\�\�\�)c4�\����\�Տ��6!�R\�m\�\'�h6G�\�m\�xŪ�C\�\�\��\��Q\�D\�&�`uG�pE�Y�>%�gY%a��}n@%U$��[A��;?T�7gÇ\�\�V�Lolƅ\� s�\��@2}�z\�-{j��+�Q�&-���[�rD?E�vnLmU	 \�I�q[,�u�?�W�X�l�\'\�i�B��+\�t{ID:\�;�&>\�[\r}\�pY@4B4\�]\�rw�ux�U�a��G\�rn�DtE,L\�#\�E�;�iuc\�\�$�����ϯ\�Gp=\�M��&�S@\�ohI��mB.\�6˭Oh��\���>V\�\�<Y�\�~�~QS.�Z��PjҖ㟽z�<\�\�iYX\�K%݉���#PUG��n��U\0ۣj3PD\�:JT�;�����\�tv;\�,�\�&\�|�vիrtTt:ͤ��\��J;\�1��ԙ�֌4\�UNO4�\�\r`8\�!:=�\�7\�\�c\�Y�Ʋ��g\Z�\n��G\�\�\Z�*Ժ�\�\�\�p�i\�.\��\�p\�ָ\�^lY\�5\��JfE�\�YrN\\t ?\�g�I��M\�w�<�\�\���:��>7t�`\�\�\�p���nľ\�1soc\�\���A	�\�\�n/�_\�z�#�sB4�B\�;�S��\�~�%\�\�O7?��\n\��\�ү\�q\��?�3Tɗ{=���+�����w�=aP\����O7\�\�7�T)\�nj\� �҃3ip�\���\�J\�\'5�\��\�\�\�!\�E\�3\�G�\�\�Å1G䄠\�\'�a$�+�MA\�Ϫ\n�\�F~󡠜�z�n���wfɄ\Zz\�H2x\��Ζ[n���\�#��Wl%g�%��\�^\�e���AM�\"ۏ�[\�\�>\�.. 9��2^g;�T�7\����\�*ΰ\�iQ>;��W��:�˝�3\�#*�o\�a��]-\�#u�\����o\��\�J\�\�Mx]L1\\\��',1478565071,NULL),(9,'gh5esCO0id8G','phabricator:celerity:/res/defaultX/phabricator/3010e992/rsrc/externals/j-1TBv7Es48.Gb','deflate','�V\�n\�6}\�W8~0%ث\�\�\�E�Z,�\0�\0�a\�ѡI��\�f�{�CҗX\�>͍\�9�C&�F\�Vh���X%;�\n�\��\�\�p\�5\��*\�v\�-3W���Pϋ\��\�ic��\r{\�x�\�Κ(�\��\�i\�\��!\�i\Zo���4zң\��|���˄�\�fÕ�\�i7\�����q\r�<.l�Ri�\�&q;�~�\nqۛ^r�lK0�\��2VU�5Q���>����®�E\���7D�\�2)I\�c[�m�6�\��I(a�! �X\�\�_,\�\�\�\�\�gDMjV�����9*�r*\�q��a�\�f��\�K_|��--t�m΢p/���`��E�2)n\���\��r\�=\0��C\�;�v%�i{^���\�5\�\�zC�-$~����F�(e��\�5m}r&\�ϗ���\�=_��D��\�B��\�\�#ǋ�5�h���2�Z�i�Cye�_\�\�ZY�`�6p\��\Z��#��\�\�I\�=K]��F\�\�w&\nJ�mk g�~��\�\�\�[|,|�v���\�z=O�\�ݾs��+,jq\��;��\�-u�d�\�\�\�P\�/A�^���ͥ\�_n�L\�f��\�K��F\�3�\�o\�\Z�l,\�U&�`�}\�4�\�Y����B��@kp?_�3\�x�lGue\�;�b���z����fz*ĥܞ�Ϲ>\Z\�s�\�r\���\�\�謫$\�a�\��\�d�\0~����\�ܩ\�-%���\�\�\�ugq�i�\'�\�qF\�s�{\�t{�\�\�p\�^0$.~r~\�\'.#�\��su�\�Z��\�vHy\�%D+2��\�\�gA\�\�cMg�E&�X\�(\�%S\�\�9^�+�_\� ntSs��*\�z\�\�^n�wS��\�(T��\�\�\n�\�3,�qN��my�C�\��\r�V7y	\�\���\�۽��\"�9S9��\���n\��\��0B<�\�\n��p\�\���\"�\�\�7\�\�`]\�Ƅ��\�n9�\0\�*\�D)\n-��\���g���<\�:?_qX\�1����Q{\��\�\�6�w1\�Vv\�}%{�\\(�W�W\�_!t\0x\"~ {r���팽hu�\"�\0P�g2��؅�\�\��h�#̬\�B\�kot=?\�ǩ\�O�\�\'\'1��E-6�����G�\�\�\�\�GL\�K��\�z�Q\�ޠ_\�s�\'pV!!?\�\�\��<�d\��3��\�r�\��\�\�u:��\�\�\��\�SS�5.\�\Zs<�p�h�D��-E\��\���!ܐ�\�\�AK\�\�\�*�a\\fˀgG7\�\�w�\��De�8(�\n�OC�:�V��Ԣ�Gݘ8�����R\� �_��9����p�\�\��Ew\�A�t�K��?',1478565071,NULL),(10,'VeSa.QADfCjJ','phabricator:celerity:/res/defaultX/phabricator/035325a7/core.pkg.js;defa-5q2z.DeamD1m','deflate','\��i{G�(\n\�_���\0�\0��\�\��\�cK�X\Zoc���\�`@�H�h,9$\�o��fFfe��\�=g��\�~\�\"*�\�-2222�W\��\�Eq���\�\�|־*W�\�yٹ}?Z��,f\�߼X.\�K�1���h:-�óY�\n���{P��]N\�\��b\�ݮ/���r� ��|\�\�\��;�,�Og\�:~TUq8��\'\\�7-g\�\�H88\�\�.{�\�\�B\�W\�Ig�]�\�\�r\�Zj5�ݜ�\��e��\�\\\nU���\�\�M����\�ۯ�-\�\n�W�\�\�\�F�\�Z��G�X\�\���͢\�\�\�\�jvN3T)�\�G\�\"\�\'7;�/n���k8Y�s��Z�;�:s���f��\�Z���\�T�\�fL�\�j6��\�\�\��:Ǆ�j�L�U����pH�q0�\�`��\"ӊY\�P��	H��\�f3�|��z�^�\�\���\�\��j���h�\�-,\�Zͯ\���\\�֭j՚\�׭QK��2X�-��t���҂�G\��\�U9[�:�մ\Z�\�\�\�Q�D}p�\�Rz�\�bzC%�c\�\�	L�l<Z�	t\�c�����\�ww\0\��CN�R\�=*�=:/\\J�,oV�\��6�h�\�)�ޔ%�Ⲷ*����GK�\r`���\�\�\�\�|Z\�\�E	�\���\�[�\�r9\�߷_�\�|�&\�ѹ\r+�P�\���ˏ\�A,\�`[NW\�n0UPn#�\�9�w\�S4\Z�Ca)9*�.\�\�l��\�Î\�d��r�\�)+�~\"R3N(\�F\�\�A�)W+�4-F�;�\�g\�\�If9)\'�e	�,G3@w,��\�P[\�\�z���Lx\��l��\�!��|>�Wަ��S�T�\�g0[M��\���Ü\�$���nX\���\�\�\�C�N�Pg\��\n\'\� {;{;�n�^�\�g<	\�Ϊ\�0�7�ֿU\�)Ѕ�\�ˬ�\�(@\� ���f\ny�\0�:m��$\�\�u��\��s\�\�I\�owz\�\��Mu�\0\��\�j\r\�\�$�\�FWe�?\�mf�4nMŭ�\�!U��S��?\��\�A����\�\�\�\�e)�c\�y1Z�,.\�\�\�u\n\�Ւr儨�C���g\�V�󽾿[��\�\�O�ؿm\�A\�އj}\�\�-8��ozP���K\�\��}٪֭���g#\�c\���|6�V�I���@g@\�w�7���\�\0骧�v\�tx�\�\�ㄙLL\�\�1Y�w��F\�\�\�\��\�tZ\�Pj\�)�rpPU�[V\�\� \�r|t�;�\��(g����W=6	\'x~\�\�p���.\�[�=\\U�Qv�\�\�VU�;\"E\�\n\�iAf\�rZ�\�\�\�\��M]B�\0T�	����W�:Bd\� <��0B��x~��\�\�\�=B[(\�*\�q�\�Б��Ȍt�`vb\�n?\\TӲ�\�/\�\�w*��\�B\�\�ww�e�\r�Up�`\�{hkR\�J��6�\������\�+�uU^��K�œ\r?㮝R\�#S\0?���і��\�tG-����\�؝h��^ى\��%�JoE�\�\�@\�fn���*�\'\�,\�7��r�(n�6\�a]1��vM��l�.\�\�hm8}�zƏ\�\�ذ��\n.\�\�f����\�Uo8�&\�a�\r��/\r�\�A�\��n�,�^/�\��M\�ݝ\�8ww|\�t/��\�3z��eV.\�ޠ\�K\�FQ�tw\�_xD\�T?\�h\�0\��\�i9dOiJ\�,+�0\\b\�.�xLzg\�h\�|�\�\�Hp�\�q`0\�\�,hQ\�8K�~\���D�\�\�\�\�\�Rq��\�d��\�T\�e�F\�0\�\����\��\�&\�ߵ\nSߧ\�W�=�e�h7P\�Jeҥ�\�-}��ǘ�2D)V%\\m������\�R\�3nc\�I�^�HL�kB:$�\�\��}�HO|\�\�n��\�\�Ѫ,\0\�/F˯\�\�\�\�O���b\� �\�9�\�ٜB����D��F���\�%Ϧ�!j��3��\�\0\�:)xrx$�}\�\�mv��\��\�t\�qQԨ\�Z\�G��j\n\��W\�a�\�\�-\�4�RJ�P�u�Y�\�\n�\�&\�\�@\�Qk\n��/\\\n�{c�HC��h�\"N\�\�\�Z0ǄD�֟G\�\n�ϊ�C\�\�l4��/�9�\��x	���\�\�\��l�^�\���\�A>ԕ޾w�!m�Dư�P\�\�\n\���<,�\�\�~f\rI�M\�\�l���2�00�`ݚ\�K���j୓\�y��\�\�\\��}VA�((�\�x��n\�5`5:+�7\�Q�G?\"\\\��\�Ȏd86\�D���,�\nE\�\"�̡\n�ÉG�Ư�*N��r%��\��������^�\�ڽ\Z��\�\�j�浃�Y\0�5(�%���/竕���\�L�\�*\�\'��\�/\�^=��(�aIa\�Ji�\�zs�Ȋ�s��\�v����\�@u\��r\�\�7\0\Z�\Z.�\�G�%Q$���Ϋ\�u>\�1!�\� \�c�\�q\�D�\��\� �\�~8�k`��/�IX\��xD��\�qAӢ3��\r\�,GY�T陥8�gV9��rI�\'\�\�M�;Ѥ\���\�\�U\�[�\��VU�\��Zy\'[�1�c-x�=w	E��Z������ٹ\�\�h>��k-��~�=�]\n�VR�\�\�\�;����T�=��;_\�I{\�ھ�a���m\�_9�XyC\"1�[[�ʜ� �Z\�r�>�%.�kƄs�p�\��\�\\��㍐ J�/�&\�6�=p\�~R��\�pH�9�в*b<��ȑ\n�~~i�$�}5\�%\�	�,����u\�JX&T�C��c�H\�\�\�JBp8\�\�\"KXZ\��\�\\�z\�T����u�aڲN�\�!�OH\��ܯ��g\�M��+�t�[\�W��Qg�\\1\���Z�F	g;\�[\�\�h|�O�vn�ִK�ӎӚ\�\0]0܄���\�Y����m\�G\�Q\�[�&(s\�\�g\��\'�i\��[&�ל^�\0�\�\�\�7�U�D��P\�\�B\�<�7)Ϫ2�7-!����\0\�G?\�/��c<\�:�,`D���\�����Z0�KB6\�\�\��i�\n�\�����%���\�\���(\�|��&�\n��u6&�[8��=�w`��e\�r_�3h�^:�+l\�[}:\�m�Il�b��\�L�gy\�ďf?	\�A�{?���j�L7+j�\\�p\� y=�n;�t \r3���4ľ����Y~�B�\�\�z��\�X��G�{\�\�\��\�\�\'|\�\�Q?�zs\n����Q�\�Gf���r\n\�\�א�(�@\�N(^ȅs��1�?ӽI޽z\�9\�7\�5v(L����s\ZztYM��Xn\'�\0\n��\�\�\�׋rׅ+o:�=�@�}Om{T�72}5Z/�^?�O\�`T�d^�~�\�/\�_o\�\��\�A\r�\�\����(>��[�N�~Q<&�?̗W�\���ͪ|�Z��/\�K��\�ݭƗY�(Ϯ�\�f\�O\����H�<\�\���vN�V\�K��ul\'&�\�a=��\�!��k�5^/��n\�g�C�\�\�&\0ߗ�|F\�	\�+�j}�\�<EZ\\������ZZ�GEt���w\����_\��\�\�+d\�c�\'{�\�\�m��� ��i&I��p\�=\�a��\�	��1	z�[�T�\n�Dq+8޿�c?\�Ǟ,��~��f�\�a?c\���3����Y�\Zg�g�cZ�����\�A\�%\�,�j\��\��g�9~\�\�?�\�s��_h\�\�־�\�.��89\�i��J)����}��$_1\�\�\�\�(�s\�3�\��\�~<�D�	\�\�\�|3��-��!\�	#\"԰/�L5\�z>E����Ev���\�O�\���~v\"Is�~��/MW۰�8���֘�)V�i�D�Wh\�\0���\\4�mh�)*m�\��Ds\�\�:�g:�\�!/\�\��b4�L�܃�\0V\0&SQj\�\�>\��!-&�\�\��\�\�PY\��k�/�L%��\�a[>揗�Iy\�?̙9\�l?���\��c\����\0�c�(��\���\�\�0��4׼fr�:!g�X\n|Z�l���X^R\�b~�V�UԸ\r{����j�\�8̯oPO\�7�U�+A\0�\�\�mo(�8\�tE<B�(6���3�\�Њ��/�9r�\\��\��Y	�}ām���Z�O�\�\�M볖\�z7!�^�#:_����@�2c�t\�\Z\�\�\�\�.e�jB�;4~]�`V�f�sKx��R\�b\��Ò\�ߓ\�\�\n�΋+\�\��t\�Q�\�\�v��u\�֔���LJi\����3\�ˋjB\n��\�a�\\\�̦�\�$:���<\�\�V,{��ϧ�}��N\��K;!YE��˻w0�w\�\����{gH\"\�׻w�_5�JH�V�J�:�\�$�9�&\'Ӊ\'R��t�\�%t\���-xppY�\Z`�\�\�K|@Z�\����\�Ǆ\��\�h\��$(o\�\\�u��b:��nkC\�k�}�\�Q}\�1Qx�\�.\�\�\�\�\��\�j8��S���Ӎ:�1�t=V��^\�]\�ЋsD)��U\�9�\���T��\�0	��e�\�+%\�\�\�R\�߳\�%�\�Iẓ�2���9ڎk)^Kn`\���k���&�f��/]&v�f�Vǜzҹ�%�d�2\�mb���U7{R\��\�-�0QnIp9˔S�k9��^x\rg����X,�?�N\�\�\�QVs�z�/J$�����Z�_LK\�\'ww�\�\�\�5\�/�����M�!g�\�0\�c1<�\�L9�\�N~ ��\�\Zy�]��.s\�v��#�t\�~\�\��}��1Ҷ\��\0���\�r�\�\�?\�\�|ι~��tJ�jB�WYM\0b5�W�n���j�*]jg\�ht�@FG&�\�\�J�C�h\�v,lm/J�`;R�8w^�[\�/R5��JtKT\�!s[\��8\�]\�-Y4 �\'��W�H͕r��}Z	�S8!�APp�Y���\�\�2���\�!\�Ȋ[�	\�Z��xG\�\��\�\�\�]�\�\�f�t=5\�ߗ¢��fm?�\�7�ῷv��?�\�	�N\�1\��1U;�\��dj�J�:`ɐw6\�ǩ�\����g\�mbg�K(n�\�\�+���\\\�u�ۉ?�::\�Vs=�\�Ϥ��K�;\�}J\�\�=\�<�\�.�mG�(/�\�\�\�B^��\�%���	\Z^	\�tO��\0�\�s�v:v���-��\�=*\�$TD����\�*|��\�����MQBn82z:�\�\�ā\�\�\�1t��*��<-����\�\��\�\�\�r>_pl\�\�\'\0�=�ٹ�\��\�M\�`\�K\��9\'4~�\�h�?�L�;\�Q�0\n�:�O�d��SpL	\'E\�|\�\�v���h�\�q\Z��Y;�{w�)\�݆Ilp�-\�6�. j�\�\�\���\���O��O\�s�V{\�{{y�K���\�\�#�<�\�\�?��Ӑ�}\�QOavO\�\�0�4KK\0=�jw���/�Nd���\�9:�\�T\��bM\�>\�@�C���8+\�ݕc\�_�\�~9���\�\�F��	\�\'~%�\�X�i=8h`Z-\�42�edۿ�_KJMGS\�s�\�\�]zp��r�\��\�ɧ%^$XMs\�\�\�\�\��\nA*,O}_��W%\�P\�{�0/\��ǔs�{�\�\�w�v\�\�\�z\�\��J�\�8@I2�\�����\�\�UԨ\�r�\�YGp��\�#�\�\'�\�\�=\�/92C��M�M�8���͜�\�p�re�FR�~FN�\�+2&2@zV��I��A�b\\\�\��7����9V\�\�\'���[�\�ŚH\"\'\�L\��\����7�.\�\�M����ٲl!m�5�+<\��\� o\�{	i��F&�Z�\�\�h�6C���eK8u\';�6��R�\'�1�\�J�q�e\��?�^ЫPx�\�O�\�p$o�\�d�Ϛh���e5\��\�L�^\�\n7׃-_5\�\�Z\�N!�\�\"\�T��צ\�S*V\"^��\�n�~,��\�\�{\�o�R���U�����gL\���Վ�C\�9\�{��`S� �\�c\�=Ih\�\��I�S\�\�EFc`�V4-n��J\�\�I��\��\�߃�\�օF�Z�e���\"a6\�9�NoHܿ\"�?��͸hNϨuZ��:<j\�j\����\�+4�=Ezu�Ra_Et\�\�쑻>\�\']J��c�E�Hr\�4m\nZ�<;Bk��\����o�^��~��h\�U\�6�cH���\�?��V�\��H\Z�Ņ�.\"j�ר�\r���J�\��M	_\�К*r�*���*4�o�k\�b�-\�=�EI\�\"xʪ䀽��h]~/`�E2m�\��o��\�\�;\�i	SW͍-�O!G��O��ZCS�7\�_5Q*�8��d\�TAsY��m��\�9r\�K�ƅ�w\�`1\��\�jF\�\�9�&��9lU�][�=q���%����������\����\�6#QZ8ߜ&Jd�\�qRJ�L�yV�L\�v^��X�f��Ɏ\�c\'a2��\�!S�y�\�\�-�Y�+Ҩ&v��\��\�j\�\�eo��X��V8\�-\�BdQ#ߖ6�}��\'Jo7�\�\'�\r\�\�\��כS˥��:_(�tB{�\�n���3v3\r;з�-\�\r�\�\'lKd⻖�-5HX\�<�\�\� \�gY��\"\�R�/����kH+\�wX\�^u�<\�@����-�l�cq�M�T-݌G��@-�ى\n�Y~.W�\�r\\Z}�!>�\�D\��-�+��\\�.�\�\�K�C\0�F\�\\���\rE�YV�&�\�\0k�>�fu�\�b\�n&\�u[A\�\�5H�T��`��B\�?����a\�IڕJ8m%���Ӽ\�\�\r\�X	��Й\�ɔ�RA�U1\�\�ȃ\�p�.��\�\�R�\ZhW�MA�m[��]�I�\�.���j�u\n+p\�~���\��\�\��@z�!�j���#�B7�}�ˡO_lIjϰ=���M\�\�D�^A\�gg\"V ���=�ԆX!�\�wCU�Z�\�z��-S�q\nl���\�m�r]ܹ\\$/({�\�Y9\�et9\�C\r�\0N\�\�	`\�\�Q���>�X\�\�\��b4;/c�<\�\�\Z�\�!z餌\"\�i\�\�\�4��ؒ�G��>p�HVc��͛\�9�ⶳ�5q\���F\�l�좚NdB܂\�%VIQ�Sش\�X(\�S�hk�2�`\'.\�&�bY�\�,\�|\��\�Lf�\r&2k�\�NU\�g \�VF\�e�����\�/gp�\0��\�|:\��}\�ԥq|+�1hЯ)1b\�ٲ\�s\�bQ\�u\���&�\�\�)HN\�\�\�e�~�@;��#��{W�<xG�й\�&޽\�&4�\�¨\rմ;]�c�r@չ��\�]~ĎP�\\�\0FeZ��\�\�\��$\�C\�Zs׏$�� |���o\���S�\�\0\�d�:X0�\�#K�gl>�\�B\�>m[\�{�x��k�	��~�ڵJ�#��\�Fz���]�k4\�-oN\Z\��w\�\�\��r!\�υu�ċ\"%���-�\��^�e�\�r\�\0� �ϦH�i\�7�=\�%�\�W\�{��n9�.�!$��.ڼ4\��%\�M�FT�@\�\��>%#�\'h@��at|ю\�n�\�\�Ç�g(�`\�s�\�0\�G\�F�%���U��Ku�B�#\�\�n\�ܽ��I�Aǲ}��L�3��њ���\�\�K�<�\���\��?���/��\�\�߳j=E�\��\�\�s�\�Uv\�\�%\��/��\�\��O�Z�\��\�)\�\�C�V��&9�\�����K�\�:�fo<kk�3n\����w߮\��R���gp_�Q\��W6tkg߯����{X�͛�\�R��Z�@�����\�\�g?����Ǹ�1�Hv��d��\�3�����V^�^0\�\�\�:�Z$/}>�\ZU3����\�l���i%(\��\�T$=I�:����Q\�l��	\�&g��:\Z�l�;}��X~\��<?�W\�\��]���*-�\�$�\���l��En��Ij\06A��Q�G6\�ޠ�[\�p;J[\�^\�;���Z�a�3e\�D��]P�\�&�{�uE��@VY�K2�Q\�\�uE�`ZR��W!�)�h0\�ƯЖ##�S2���ay7P���\�\rpL���ߏΦ\�`央\"z0\�z)j�A0\�M)_\0WIi\Z^Hn�\��\�|\�z�=}�#P�Բ�Y ?\�P*���ӏ�\�d�H\����P����/\�D~�+�ʂ\�g\�ŨZ�\��\�2\��#:�z7�a!\��<Q�~��\'R��v�	*�,\�a�{ɿn\�p�^�K橖�fb�g\���a�N;h�!�DGA\�������E3�\�\�\r	�m<\���\�K�|���\�g�Y\�\�\�~ϝ{ٍ7\�{Y,\�1؆W�\�\�\�\�?����\�\�\�_���7��{��\������el&\�E\�5\�݊\��%\�\�\�:v�\�\�Ν%|�\�hHˬe���k��\�\�l\�EEJ\��\�C5��G\�\�>t�k�\�n��c�\�$^�\��\�Q�&Q|bzr,\�aN��fR=ě\�Rh�Q�@%�i\�m0�ߖ��\'\�V||6\�G��.�s-ĐE4\�s�YJ� /\�\�+>O\�#\�\�j�(����\'�\�\�l\�\�\�\�C�\�\�\�Es\�\"\�z�\��|3Zz��������\�t1D׎�\�\�xͱ%$v\�\�z+�̓+]\���k!�ŷ=��G�\�\��~��B\�\�i{0\��Ξ\�b��}R�)\�+R�w\��\'\�\�V��F�c\�Ɯ\�H\".$\�7)*A����\��+�)�\�fǣ\��z\�p�\Z��xk�\�\�õ�d%Bw�=E�\�͎��A�\�Ԇb��F��C�U)_���ȅ�(K\�c��k��$\�YD{vv��l�i��ͣw����Z|�o	��}\�`K�\��E^��񇌕(�\�?��\�\���\�\�a	�I\��S�L�\�\�c���v��\�\�ª\�\�b\'\�V(\�C\�\�Z�M�߂��\�o��+�@P@�k�}7\�3k\�w��2�\��nfc�#�\�x�@�v}��M\r˩�FD5	\��u��\�\�G\��*����Y\�\�N\���|Fn\Z�mv0e���\�05oL�X\�\�\�\�L���35\�׫%��%23\n\�[@�J\��\�\�x�mT �֒�뛸!)JO���\�-F7�:��6\�\\SN\�8\n�\�%��aW��\�m)؉xK�oI潓(\�T\�ɔ�a֍d�J\r߭T\�\�\�\�\�\�Jb&(\�&6��2�X\�sR\�\�\��\�j\�q\����\\�`䞧�ga�J|A\�Fg�O\�}��\�e/ONB\�\��\r\�/\��\�w<�	oT\�뽿hَ9\�\��/�A,n\�8.�o7Q���\�DY�1:��o.����n���V��*ő	b�?\\�3��Jj8��o�Xߧ]k\�\�zp{ېXh)�<a�\�	�\�A\�uN�DBqku\�@yKkS�\�i\�N\'�\����rxx�\'���X�\��D8�P�PL��\�2\�Tz\�RE����p\�<�ـ\�\�m,�*\�^u\�\\\�J�\���SdEv@u�F\��\�\�\�.EG\��r\�O�x\�p�v6P����/\�\�w�r7c�X�/�\�I��KV\�G\�N��N�\ZE\�~a��\n���8�2\�!��J9�Ë50H�	���\���*\�Qz��֔ST�}\�R\�\'\�11�\�Fޓ`-��$���\�\�>K;�%\�08>\�*�\�!N.C\�5Q\�.���\��A�w�\��٫]\\\�7�d��D\�G\�	|	�3\�I?k \�\�ބX`�gBZM�U�Z�\�}]\�Z|����\�D[c)*tS�o\Z�a�{��扒\���Q�j$�\���H}7��ߦ^\�\���Ώ�;\\\\\�\�2�\��T�\�Q�\�T��\�Վ\�}�&\�_޿�=z�C]���\�h����\�nm;�7\�u\�fp\r�^{�8�.\�\�A�\�,��9\�4���*?�W\��䲪\�8\�sL��HY\�sf>޹����5���+ǳi�?�H��k�/�\�u\�\\\�\\�;\\��O\�o�\�m\�\�,9\�\�\��@�c�����xv��S&@�y㼎�\�\�=\n\'\�\��K>M,C�!\�\��]�1\\\0G�`\�[�\�r:��\�\�]���\��\�ʽ�/:r��J�o௄~�\�@~b\�\�TߦD\�N��\�.p\Z�\n���k\�+\"�\�1��e˙\r<�D	Hl�\�^�`�����M�u�~&h��\Z.	2XG!\�vz��`�\�X��4�)7�$0�I��H�\0|Mݍeȧa�Xȧ\��d\��}wwj��\�4_y\�}q�M_�����\����<�K\��2t�\���F!E8ʻ�(1\Z�3k�`\nl\�r��`R�ˇO������\�\�f��\0\�e*^�`ٹ%\�M�\�\�	\�\�\�Ɵ�\�\�&\���rۈ�\�\�n,�A>s[��\�\�\�C\�q���KJ�\\��`\�jҿJ�҈ww\�F��Qg�\�J� �D$U\�\�i\�M\�*�w�^UWث�\�����|N�hl��9\�Ξ3�#\�L�f}}�rB\�ü!ӓj�WP���]&gQ/��]�m�^�����̤\�O��֢�\"K<\'alb�Ym�(搤�@!V��\�GJ�6Ʋ�\�1�:-1�j��d�\�\Z�]q�!C1Z\��AR0�	@� �#5+t\�=�!s�E�32i\�\�\�n�*��o��.͎\�7\�:����\�ą�\nZ@)��|KC�B8{(k\\v\"#��\�\��\��߯+췖4\�~B�<_>�\�\"MtrK�\�4�ͬ{�aa\�\���d\��5\�֤\rqs\�߳�|M������8_\�\��-\�4\"#6(����τ�\�(]rT\�\�j�9;#A�\�\���\�E;���oW�>iö�\��\�v޾=E\�\�\�>c\�|�=���X���O`{S.\��aS�z�O>n���\n\�,\�:[�\�	�Qۃ�G5D,��5n0Tм��\�\�\�`������sz���R X�\�Cm\n���U\�q��\�)b���t��N�/\�EAy\�	i@=e�\�\��n.\�\�O\�˧��V�\Z\�:�����x�\�9ߞ>b;�tS(�����J\�_0\�xϜ�}�D��iڣh\�\�W$&\�\�\�n�\�k=��~��\rL]����t`�\�\�Kî\r�\�1\0�\��� p�\�\�\�\���(s�o\��E6�J�*�P�$��\�\���\�B|lX\"�,I\��C;�T\��$\nC\0c(�\�\�ǔ\0�mo�?\�\�&\��=���5YKtn�Z\n-(.\'�4S\�h��r1Σt�UI�\�]Dn]�\�\�\n�+iؓ#{n\�8\�\�ߏ���i\��!�~�h[\�\�\�~3ا{\�\�\�|VF-\�Sa&ʳ\'\�\�12g�ژ\�g\�\�~0eF\�9��� �8)Z���\�\\�\�\�\�\��;u.�\�(\���G�\��w�S�{$�����(X4PRd	\�V�\�Y^�zj\\�-�\Z\�\'@\��U\��\Z8�\�	{E\�a ,�N�ђ�\�&[�2|��?\�̫�Gr�`J�p\�\�\����\Zg\��2L�s%�>��hˑ�#H�\�\"\�g_wW\��Ⲽi}��>!eH�dqv\��\�\�\�\�O�h�D3d!�2j�VFK�\�$��	%\r\�)A\�rX���s\�\�V\�\�x^4G�\��֐Vo\�\�ͬ�;���/)_����D�XG_\��)��K\r������/�k��,\�Ȟ\�lƗ֏Q\�\��sW�\'\�ߎJ.\r�\�8�L�1_\�|zv1L�\��\�n?�@v�`\�|\n��\Z�\�^��^�����=\�m��W^8�O����o\�W\�*�t\�#n\Z_B���\����1y\�\�N���0�������r�\'����LA\�Ӵ�\�3\��D#\�i9\�4\��\0\�\�\�JF\�v�L.\�]U�\��-�?��\�%o�:\�V\�\�JVr�t�Z�\rr�����D�\�%Bp\�\�\�\�\�\����x�!�G�\�ޜ�B�0I!:�:�_\�ߜ�+�����.V*+ j\�ܼG�B����N�#y\�\���#��\�h\�{9g\�\�\�`ɥ@��z��Q[�\�i��9�\�\�\�թ9\�ͭ\��\�* &\�i�L�{���g��S�tչu\�N�\".�7\�G}N�\nVH\�\�ðo&I�6�k,\�f\�M�\�)Z(}4\�\�7c��rɇK�՛yR\�\�X�+-6�&5a~*\��i�\��\�\"bh�9i1\�*\��I�B��Ėqd%(I�E(\�p�*�\�\�u\�Ѥ�����E9�<�_�����A-ʁ���P�;t4z�_TpY���|?\�+\��;\�\�%�\�[\Z@B\�\�9VE}ӯ~~�d�~�݋go(b�і�MJ��\�\�\�\\�\�\�Y��ϝ�UjQ\�K�ꦐY\�\�&\�ີ�M���77�₮�\n�ut2�:͌C��boϼv9/�\"-\�+\n��\�!0�\����ِ\�\�]��e\�\0������:~Q��KF\r�%��-T!_\�H84\��q�\�I��\Z��\�\�\�Y�8���9O\�\� oDa�\\|g\�@\�n�\�\�\�܄m\�P\�Kƫ�\�0ͱfh\�\�-��%.lG\�OH\�y�:��>dEx(a��!��wuѢ�V2�,	\�F3<i�l$\�\�\Z\�k6ZSPs�O�5�W���o5H���\��q�\��>��|\�\�5\�\��\�Ϻ�B�$�uTO$\�\�3��\�\��.�\�	\�	�\�ko\r=\r<]3ǋi��P)��j2�Kh��\�t\�\�\n��!\�ZM+�o8��$ǆv�$\�\�\���z>GeA,\r�2���*C\0|D\�&߶�� ����\r��v�XZ2�\0Pdn}�\"�\�R(5h:\�\ZE�\�y.�\�����oW��7\�/��v���\�y\�ɩ\�BW� �\Zi����!�\�O\�֣�G\�y�?�Z2��	��}3_\�O(y\Z&>�\�sLDu����X�YU�\�z�Ù��eو�ЌG6�I�>�j�%�\r\0\�y�X$Ʃ\�@иhJᖝ�n�,�i�\�2\�\�@���k���-�\�E4n�Ū\��}\n�3��\�\�\"I:6\�\�\�L%���/��\�}S\�y\�O�)Đ�Á4�O\���[�mp��Gw�e���\���\�u�G`\rP]\"\�p\�V�­\�og�=��.[��A�G@\�QG`\�\�V\�o\��K!\��\�\��x(xρ\�$���9w(���M@�N\�ϫa�\�\�EfA��F-\�+	¢/{�\�t��Y\�\�S�p#,\ZI{\�f�Pꂛ��ڴ\�C��a�\�*+\��������O狧��\�O���O>z\�e�\�B&z?,\�\\�}|g\�z~_gU]!|�\�L\�ޯ\�ZJ9�[!�L��_���\�l|�\�U��\\\'������ܴ����)?�Nٌ�w��Z8_\��B\�}\����i��\���:�(�Ȧ�h���,�(-.E9\�	�\�M\�3ƨ�\�2[\��\�\��tXA>S	a��IsK\�ϡ\�g*�\"f\�=�������\���7ݢ�|1�XI�\�=\��t�\�d	\�\0Ez\�)��\"�9̡z�\�HǛ�k\�\�RO�O35h?$�\�\�z�\�Dm\��\rb�?\�\�Fv�\�7\�^��t\�[�-�\�U:�q\�\�\�Q\�\���,�\�i��;<&�Ӯ\�T\�1��I��LB\�\���Ǽ4�\�w���6/\�.Y��;\�s\�P\�.\�\��\�\��\�A\�a���R�f�Q�\�q�=n\�T����Nv\0i�&zwǿ��\�o�;:G��\�\���IVO4A���}�5�\�\�kf��Z\�p	�H�Z.���d\r��\�?�p3B�/PK��e	\"n��h\�\�V�\�r5�	�i�k=�җ\���\��a�|\�nk]\�f�4P\�\�\�t��\�\�O\�^v\�^����\�������\�\�\���=\�\��\�\�\�?B\�\�N?��x�y|x8\�ҟ3���\�\�1}���>~g\��{v�I�\�\�Xg2�\�\�foOa(�J�vM?Q�\�\�~�\�\�\�3�y�?����>�_�`\�[�	x\\\��\�N��5ő\0I�Y�y8V�2���Վ\�J\�\�[\n�\��k\�o�\�ڋ\�{mI`���C�z|\�Ī0\�\r��v������\��b�|\�\�u�\�x)<�}�\�[�g\�\�\�O�~�\'�WC>CC-\�0T/�\�@�\0���^�����������\�>��\'�~\�K�a<z�\�%~ځ�~�\�\�\�N\�K\�>�M|���*��\�\�_��Xm���\\ޤ\�6\���v\�{�@e�\�=��H�j�w�\�\�RKw\nZ<�֋�\�\�A� }\�\�1�Yf��H`�9)ǁH\�jCq^АžGo�\�)�\�}Ђ\�\�]\����´U��\r~��X��\�zfI{!��@\�Pl��\\\�\�㣓�;G2}1v4�����\�\�R䳰�V�M]�?g�ܿ\�(4\�ɽK\�\�\Zt\�:q�\�>Z�(�\���\�\0\�\�PK���\'���\�\��=e�^\\Fb\�\'b\�R����r]P�F�\�\�/\�8x�ZT%\�\�\�\�V!oqdW\��x�!\�E�i@��\\\\\�\�QGв��V8\�c\�FD��pRD�jz�\�3�\�\�v!\�w��e��\�\��� M�*\�\r�p9VuB�ij�&\�iC�1\�\�+;\�<^\'\�\"uCN_\�\�h\�<USt�8�1/(�6eqc\�^Qg\�L��\�\�I�֍h�\�yN��\�\�\�c�\���\��??���\�/J�\����\��\�\�\�\�d�\��s�	_|����?\�\\\��\�\�\�?\��_���p\�\�\��}s���>O�\��ވH\�	ˡT����J^%/�\��\0�\n��\�y}�\��@\\����Sz\�AC\�\�}\�wb4��;E�C\�\�5�%#\�o\�a���6�\�ϔ�G�� Eܷ\'c{H\�\�\��\�\��T�+Q�8�)e]�\�\'�>��SjS�Z\0�Ej\�����-}�\�ð��\�%Գ5�FD�\�vZ\�r4Ya>�\�zYp�<7�C��\�RQ\�\0&�\�#`��l�[�\�U#G*�\�0#�\���\���K�G<��\�P��[:Ӓ\�P\�\�~\�[�\�K\Z��\�\�#�5��0A!ɩy�\Z�\�:ij\�(��K\�JQ�\r\�?�;\�<[\Z|���\�\�|��\�P�~-9bA�5���G\\�$��\�\�OH�I@Ԏs��\��`\�a|��\r�/�\�\�\�t��\��v:\�\0\�\0}\�U�-.h�ɗVP~:?\��^q�\�\�I��\�\�ZB\�\�F\�\�\�qL\�\���1޽\�.�W�\�+��*qO�y<�\Z_bH\�5:\nC��P-�ƞ_�\�bX\�D\�\�*�\\���\�+G\�\�?]q�Y��\�v9�<�(\�}�\'ڦ\�gT$\�\���\�\�z�\�\�Y��5�Af9\�θ\�\�\�\'G\�H��\��~��kx6�aP_\��T\�\�H�Fd-G4p\�3Ē��\�\�Ҟ�\�\�\��\�\��΋+_shm�Ã&\�M\�ά\��wp38��m\�\�~W\�LX?�Wr�s&)\��H\0\"^zp�C[��\�\�_����\�\�8m%\�hW�`\�1��\0靟�\�<�\�\�U�t�*�e/�B\�OG\�\�IM\�\0U�5�\�\�P\�\�yۖ)�̐}��X\�\�\�{U\�|{=)up\�c{]��\��`���\�\�Tuu;�\�\\h=:\�kf|�h\�\�uG\�\�/n��e�\�FNcҝ�=�Qcv:�f�I\�Y�\�h�xu�\�2m���xe�?&s�\�LcX�&|u3[_�H�)\�\���0\��\�p��䡁\�Y�:=�\�2��.gg\�\0��� �\�fK�\�˫!G�U���I5\�f^�YF\�\�*5rN��a\�Ѵ\�α����[�	{!\'5;r�\�c���\�HMg�\�,Qf\��\�|%\�9�dNa=|U׋�b�x��[� \�\�\�N��\�\�N\�y@\�s�X��X	�Tg@G\�=\"g����c�p�\r�=\�\��\n�N��\0�\�C�B��IT���\�J;bυR+�s��N\"z�pq�{\�\�\0�\�k9ٚ浭\��\�/\�\�|�@�\�\r���9�\��&\�\�\�U]Dp��\�\�\�3b�G\�\"��Wլ�\�\\��t�I@��M0�\�\�^㢹GHZ)���\\��NA2\0/�Q��;��\�~0ƿny\�\�Ud�c\��v�<^����_K\�\�]\�\���s]N�@\�p\�\�I�\n\�P{\�y�pë�Z���R>��a�\�CYC�*\�z\�\��ӔI�azƵz\�O�o�k\�5�����vC�n|�*tC��G2\�|1����{Y�ê�\�ߴ��\�\\���d���\Z��Qka�i\�\�q�\��	��!d\��J�t��\�|\�FiT\��\�\�2��D2B�mN�\��_��A$JB�r��\�uช\r���\�\�q�\"[�x\�\\~\�˰8�\�\�am!�ؔl\�<�\'9O��ع�Q\�\Z���Rك�\�rr�D牲:o<���\nݩ�^�y\�j�|��ϭ�\0Y��`�\�\�X�p��ɿUDZˮ�0oE%ρt\Z�X\�\�\��l|�\�\�\�)��$?�|\�	\������ͻ\��ZY=ab�\�\Z��\����봇�31\\s\�ƚ�V\�×s�~\�\�EއE\����҉h�p�\r+�\�\�m�1�n?>\�\�\�{7\�	P�G�;�\�\���l\�辈k��tt	���&\r{�\�\�Ks�\�3�+X���*2��\���R9yn�H/��ӏ�_�y�\�×?|��o�&6I��\�\�r�\�\�)\n�3\�>\�plx�>\�\��\�h�.����\�\�\�)�H\�VV�\�r�3\0\�\�D�/2�\�g�%R�m���G\'\�\r9kL5��ls�\�v��\�}��C\�E�Pq�>�t)��>�\�f\�ޞ\��v���#�\�/��7A�Jb\�W�\�\�@\�\��TK],鉗hj\Zaxd�ྦྷ�\Z�3b�\�	��gEf\���Gv\�C\� n��8[�\�\�G����c\nI4�Sa���vq\�\�?�ec\��9�!\�TSH��\Z&\�Բ\�Ewr\�\�ݽ9D��L\�\'\�4��M\�\�es�8?P�{/\�Ey��\'\"\�WI7\�◜o��:ڸ\�\����lڶ\�c\�\�v�#�\�\���\�\r��0/�V�\�w�_�\�o�\�\�4\�$\�2�W��>\�K�Q\�\�Fpy�\'k��.��<(\�\�߰t��!��b�.0�U�B��Ѣ��0����Vޠ\�\nd#E�G�1� 2\��İ>s#!ˡR#\�&F\�\�\�\�R�;�%�i�\"�\�_3$ݙ��\�έ�4\��\0�\�\�lߍ\�Yg%�\�Wu�\�0��[�M�-91�5@N\� �\"\�\�e嗀�\0\�n�6a_Q轹w�!��T\�9z�\�\�wN`\'�%\�L\�^���\�\�h\��뵕��\�5�\�u��������\�\�2x����&\�6+\Z��{��%&;m<�Uat\�v��S�Zo����r}\���>��o�h���\�\�I-\�z���Y��ɼV>�G�ZF\��}v�\�b\�\�\'P\n$\�Tc���g�^\�/F\�\�b^�?�\�1\�2\n�K\�q�B�\�����yxz\�z�`��첂\�*s��j\�x,ڀC\�\�\�I�vӖn�ퟮ>@�O�|\\\�\�8��PI\'O��X6>ٝ����1\���\�-��ҡ�\�\r+,@Aƨ�;\��j�su~���H��#:��%n�\��܈\�J;�\"�I!é\�2��fy�\��F��|颅	\"j�P\�G��ȷ.�|���D#G��\Z��섺G�ˋIs�b>��\�?W��\�\�Z��-g�2A\�9��$|U�\��]\�#\n��iԛ0�v�K\\�j�{&Bw&\�\��\�j\�IQ\�y\�WD�g\��f��\\\�\�A:l��\�+v:ewF\�irN�cY6�R�\�\'�\��\�W`��\�vW\�p,aJo3C�Fu\07$w��@	��(v%\�\�xI|�6	�0}R>B���r��\�|�3��N�K)�Bۨ\\\n.\�h<�\\-���w���P�.0X\�\�ܯ��\�2\r�\�eb�K[�\�%\�\�\�I�`\�\ZJ��C1��\�\r)\�7$\�I\�/���@R\�x�0��\�g�oe4\�A&.;�	z�c3�M�\'BI�l=�p\�\�(u��ĕ�(l�&\r,r�N�\�EA\�(\�)\�\r\�vlrO:��vs)�.8��\�FޜF\�\�x\"�\�x�O�̄��\�\�\�\��#y��1��S\�\�@��MRB\�@bg�`ѵI&\�\Z\��nJQ�(j\�-��U�/z���׽p9��\��ua\�h��JWn9���\�\�\��\�FjE��\� Q��\�\�\�6ˊdr�fzA�p\�\��3\�tV\���6��n$؅!э4�\"\�9Z�<d1eWo�I\Z�����p��P��S�\���p���e\'��\�\�\�s����[\r�M5&�)B5񓳧Bѯj\\\�R�`�Y\���\�\'�N\"u�즨U�	\�\�\�\�\"�,1G���\�g\�\\w\� �8g����\�ٵ\�\�\�}K�c��\"\�g\\_l�7�&��\�v*�6v���9\�;M��A�,\�\r��k�6C	�@� �z@J\�1��6\�\�Epi)\rXY\�Xg͐�����iG���rajA\�&��\�����P\�?\�\�\�\�n{\�r��L_9\�OX)b\�B��ˬJ��N����9�2%�$m�ʘV\�K�\������\�޹]}��-�E��c6��\�\�_ԙ	 o�\�X\�t\�\'\"��V>\�}\0Ŧׁ\�\�D-\�٦2ʿ\�\�L#��{wWףӬ\�ދL\���\�\�ƴ\���)a蕇�\�\�F\�\�\����1�v鋲\�P�\'�\�9&\�O���,�3 v&�-�m�\����E�\���~�\�lZ�Iϊ�\Z\�E�9\�\�oM�\�W\�\�$��\�\�\�n�V8\�?�\��\��\�\�\�m���$n5�-ts\��\�f\\\�{��y�3�|���4?\�vݞ\�ݽ�/���\�>F\�4u�7@�\'1MKU\�[��\�\�o�v\�w�\�\�\��tп\�\���?\�!�~\0��o�ߞ�m�휈��\�kԒ�\�\�\�\�;�\�wg\�\�Xs/-�\�\�\0\�\�fe�\�痛��^��Rg�m�`>�\�fJy��z<\�Lʗ\�W\�\�\�:�Ie�0%s:\�\"ߣ��\��z9\�;\'�Q�w��\�\�\�\�5j\��\�\�|T\�|5_��gֳ\n�\�&\�\�\���҄ȇ��!\�0�d�$\�\"i�\�M5)�};��\��r\'��%�N\\�1\�ɱ��$`�t	\�k҂cߩ5�a�L�I6t\�\�\�\���\�#q\�\�X�\�2��~6\Z_$X\��&5���F\�D�r�\�����(m�h7\0�fK�A\�\�\�\�\���\�\�i�f��\�L^��\�G�����0�\�T��[����\n�n�d�(G�St��E�R}}\�#gB\�s�C�8\�qv\�v1zO�\�n�Ͱ��Y��	���\�f$&�I�\�f��\�Fṿ=)\�O[LQ\�\Z��z�\�|���\�\�&��D��w\�\�\�ݻ\'k�*�\�}`�AGn?~��Ľ�i��߽;q�2.sL�\'do�H\�\'\\\�2q\�2�\���2f.Ȩ>D�j\�+p�\�r��Vk�N\�\\�٩�(\',�Ku\��I\�b$G\'����\�o�π�B���ڿt�\�}���\�d�d\�\n#�Β�\�!uo{S\������*Z\�\�Bo\�ƪ뚾(\�\0\�\�����o8y��\�͏:\��Si�$\�p�\�H\�C�Ct\�\��*c�%�gv�\�y\�P\�\r:�$٦5вH���\�8(�$�8\�Гɴ�8�V\�?\�p�x��&\��Z&k�\�^*K�\��k\�JB\�\�8\Z��u\�^;��\�j�Hb\�N������fj�\�)	\�T��Խv.\\\�$ P��\����f@Z�u��k��7��G�e9^�\�a����<\�\�CJ\�\�\�Y�dʡ\r�u.�-Լ:ӣ	\'\�֯2\r;��c\�\�\ZfB��h�	P=t\�\�h9B:n}�R5\��ww\�i\��\�d�\�u\'��\�´|F\�\�6�G]��i\�C\�\��\�-/�v4\�n9��\0��%�\'�1G\����\�^{N���||\�1a\�Oh\�B3M\���\�he�\�\�f�:�\�r\�1�\�D�=��\�\�bd\�:\�\�k\�|��\�\��//����Of�v�]\�z\�\��E���\�%����*�\��b��ƙ\�m|��Ͱ\�s\\���d�\�C\�l\�\�h�唿�Ij\�H�-)���qd�w_R[:�\�ב ����QM��B\�h�@�a\�e�;y|b}���GoWpgWl1\�\Z��h\�$��\�\�\�Ǐ�\�K\�\�\��V�x|�\�q�	fu>���*�\�\�\�/\�M\�\�|.*q\�\'{\�\�\'�)�IdV4a�\�tD׈�\n6�ۏ\�\�P�[^\��E^V\�`\�}/d6Y���U%�th�\�|F�/J�����\�7Y��\�\�j���W\�1j\�_)��8\�r\�v���\�\nv�d\�ֽc [M+\��꧞�ҿ;p\�\�\���j4�\�A5��µ\�9\�\"\�2\�\0�\�\�\�N?/\�f���|����\��pYn�\�h������m4�,PNG��S9�`PK`Fz\�\�S^Db��\���~bYM�Z��m�婢bOϺ]\�<%\�Z\n��c:\�\�\�=\��� \�\����\�||_�M�bx�0/\�l\�y4�¬�s%Sn�:�yO��yp-#\�h�\�ܴn�\\��Z\�\�2D�\��2|&�x�R�o��ڰ{�W�[��,�\�p���E\�A��tb�E���\��{G���1g���m�3Rۂ\�\��k�{�\�(��˴\�/��j\�ܜ�\�\�\"�\n\�]I|\�y��!}a6�:ڡ�.\�V�Z�\�k�h�Ʊ\�]ӪB\�a\�i�3�G��=_V\��\��m�M\�A���seӿ�tX\�q\��>\\�\�Z�\"V���[S\\5F�\�e�\�N�\�F0f��\�*]y1Q���DЋ�YtB֧H+��zF�u���U\�Z�\�蘢驸sk�(r�H����c�\�Y�<ϔ�]vC\�w^\'�u�KER\�\�>N\".\�\�8��1�\�P�,p\�\���i�(;\�.F�.#s�\�9]���\�\r\Z\�R[�M�VR\�3;ru6j���j:!\�P3ܞ�4�\�C�z{o�	M\�\Z\Z��/Q\�B\�\�d�\rl�J�C�Nu1?vsS�B�~c���-Y>\�\n!\��T\�ߣ�7Qa�d���\�wc-�\r��4ˋI\�6�f6\��МQ\�\��_\�I%	u8\nYF�tIuٶb��\�\�|\rW\� G5ԑ�\�O�I֟\ZBnG�\Zo���� k\�t�&�.hU:Y�r��D$��J\r<��Z\���cˈ�\��,\�\�\��	\�R\�\��+\�\�>�Kb�otF�C B۾\�ɑ�I�!�:M{`\�C3ݿeu�\�t��ȶ[�(�JxF\r�&͜X��\�z�H�[\�\��i�(�*#ꄾX�\�ѶY��\��ҫgb\�\�͞#�v�|�Vز�\n�\��!~�3��o��o\��\�\�\�3\�Zs\�\�wVUY\�tZ�vmM\�{�>n\�jK�2���s���� �\�hn�E\�\�4)7�V7\�\Z\�\�˗k�ު�\�\�Q<*\n!���>�\�	ݯ�x\�\�\��P��v�\�j�@\n�́j�uRE/\r1\���[\�\��|\�\�6�;��L��\��Z&\��=\��i\�\�9e��R\�G9��\�\�x6B�����e�Ƣ�mu�j�|c��|�\����=@�\�\�\n�z���	�hO���\�J%8e\�W\�mM����x���rN\�\�L�\�j>��%�&�[\��L\�\�Ow��Ʀ\�ٱ~�i�P*g�k6b\�ّ??\r\�\�R�X\��}*�\rx~yG+o���A\�H���n%�,\�!WS]\��>M�;3h\�1g~\�Y\�x7|2�\�Ll�]m8\�1sP9\�\�\'�~6?;\�r3I[\�|��:���z�͈$�J�3\�ǝ;\�Tk����Efڪ�G�?\�1�kgL/�\�6\�0@\�\��zz�\�\0^\�\�o�\�z�\\�p�AV\��\�Q�����\\j��[G�B��\�\�voq?ׅ\�\�-�й\re�f\�#��u�g1��9��\�W�w\�Ym\�\r\�.b�?+\�[��i\�Sڑ�]�J\�q|A^\�X���Z?�>��|$�\�\�%j�\�\�\������ǿ�\�7؊Nz̓�訓�5/1\�\�=\�i�\��+(0-Z��N�^�$�W+ķ���X�H���^�W\�g*-�l�G���)\�vN�����}WѨ\�-َs�(�\n�\��\�ȃ\�d.�榓�Z���7)�P�I|��<xEUz�1P�����6s0\�Q\�W6\�A�A�\� ����\�сLg��\�tWm\�\�{N�4d\������[\'�>e�,�\�\�XT%&$�����\�\�\�\\�8j\�z\Z`��H\�e9|V֊\�\�G�Jŀ�\�zXP9\�\�|��r\\�C�#\�Vp̸��3KS+Ge��	M\�\�9E�\�+\�D�<:[os�i\�M(;co�@:W(�$�O>���\�o\�FqK~+Uѥ\�/�yl*���\��\�`��.Y�\�9�\�cP\n9%K(D�d���B�\�h�\�\�q�F�\�\�H|gE�4�c݌G5\�V���\�n\���o��\�wo�/gg(��\��\�\�o_��\�͋�g��_���ٷ_��/���~�?�k�}���/x\���=�rU\"\��V\�-X\Z7�\"ͯ\�~��JQ�DՌO_��\��[c\nw=�iI\Z|�9rq��߻>\�y\�\�\n2+\�UP\n<-L7K��\� ^\�|���\�P�]��A\�S7��0�=�\Z\�q\n�\��p����-\�V\�%\'Gv�Xv\"/~^��\�\0\�|�/\'��=�>`�J��ꃺ���e\\�\�\�&P�v\���!\�3\�;�\�ns8����(\�_�\�N>]R\�ܢ \���\�\�|��2�N{\�W\�\'�\��\�.�[6mQ$q\�\�e�\�\�r��*O�=AI.���	\�\�X1�l\�\�m�Ѝ-FĢ\�N\�?0�\�n��cvkT\�Qb\�S{EA7o�\�څQ�6\�\n;\�\�mˁ;-Ni�P\�3X�`I�>57�\� I\�\�\�\�>�\�%Q\���U�\�\�{м\��\�+��!-dX\�PbP2ۛ\�躧�؜��t\�\�vm�u\�c\�>\�V�\��\�y\�H\�@B��ˢ]\�\�G򁞠\�/\�K\�l�ow\�}[�vVշ��.\����\�\�)�Y�\'�\�\�U\"v�,v?8�o)�kͤ�f��%�-b\�\"��<))��9Ng�\�1!��\�\�:0J�\�^g8 OX\r�\�\�\�\Z\���e{\�\�m��8p�;�3����y��V9ɏ�\�Z�\�\�c\�\�\�yG�c��v\��̰\�3�;��\�&\r/~�}S����?�\�\�\';1\nXT@\�읗t�H\�닜s�NG-��\�B3i?\�UO��\��o�����i�O\�p\�\�\�1�&\�bQ\�\�\0\\�x\�\�~=7\�2\�\�A$y\�iC�\���E |\�\��A���\�◔�\�\�\���PH�&J \�ɵ\��Cy\�f�-i�a)U�T<\�yy0\'z\�U�\�5\Z\�ռ��T�0ï�.����\�+r�*��;P�\�7c#�\�\�`����F�h\�\�2�,D`MB\�O�/��+\�F\\�3uY͗p\�/X��\�@GK>\�.tn�#�\�\�=�9L�޷&0*���\�<\�˙$Da#^�S�r\�su\���|~�\'M\\6MҰ���Z5)گ��}\���9\�b�<�oD\�\�\�\��q\�;�H\�3C)b5��\�v(�\�d�E�@hu�\�X�:\�	��\�?�9;-/F\�+T\�-.��ٺ{:ZUc�\�e�}Ϫs�eWS�����\�Լ\�y��wt\�\�\�?\�fwidYQ?\�g{�b�!\�5�]`�`���s����+\��G)�%\���v�\�Դ9��Iq�E2C �h�?I\�;\�\�\�&.)�ņA(%ҌQ\nl��Z\�:Ĭ�\��ױ�Wv�\�\�L\�F��\�&kљn\�V[b>!ߘ�_��\�	 \'\rsw�H��\'�\0Eb(]�S:u� c��}�����\�\�i\�\�6R랉�;s\�\�TӰQcTy�fK�gX+�B⣗]i�\�<E�n+B�j\�b��\�A��A�S��iN��\rv�+\�X\����\�Q�޾T�a���|¾��d�f\�%\�\�h\�Z!���3?>7?K�.�3z�n\���|li�t�N\�9���=w\�}@\�)���O\�`q\�p�\�\�\�){\�G��\��ͬ��\0��иdi|\�\�\�o��6�eY��T}�H\�MC�\�fuoM\�>�l�\��Tj\�_h_q��}\�U\�:\�x���$\nHᕣ����\�=��O�*��y���kT��DtF\�\�XO���l�/�\�\�t4�\��Ô�\0-\�4O�\�\�b7\��\r��6\"N\�<��8X`I��1�|��]>y��ѿ�3���BX\n�\\G\�L`2�\�oa�\�G\�\�\��r=ެ�\�F\��\�\�WR��a\Z(�XE4\�N\�2�{�\�NX��\r���Y�;��\�\��\�r�vZ���G��:����:8B\r�#�K2]s�\�g���}�a�\�\�K\�\�G��{K\����`x߹Z�խ�5n�i�Tfh4�\�0�\�h\r_��D��5��@D�֩)cL \�:ao�p��7uK\�6b\�M�o���\�a���S�U$JCf�$w�<ܘ�\\���\�ϗ\�9�\���B\"����N(����B�R�s��CM�$eЁ��ӻ9�zл\����5h5���\�\����g\�md\�_\�\�\�0�UCd�KA\�4��U;�6�\�\�\����X�s��\��D�>bbkz%!�?ϻ����K�ذ�pF�\�cS/003~��}ևU�\��?�0\�K�~\�N�\�܂�5ZZ\�8�\0Œd\�fI\�ږwQD�7[\"ي\n\Z=��]��ƲS\�vP������\�\�Z�3V�\����\��~��\�\�`�23\���{��.Ѯ�\�\�>l\�[mF\�ґb�\�\�\�tEK��V�g\�~~ �d\'�\�\�\�Z\��1\�\�\�:�n5�\�@N��h��5^XXA�{\'\�.&��.�s\�`�0��O\�\�5gL�mT�\��\�\���|��\�5��6؉\�?\Z]wa���~oG|\�CPU��\�5\��zgp3~v1Z>CJ?b�%��\'\�7�\�\�j�̓c\�\r0\�{FQ3���د�J\�~��\�a�)�\�j�D��\�|\"TZ��iN\�\��b\�\0�ꯔ��L\�\�$\�X(ui��N�\�(\�s\�:x\�:2dic�\�&�\rō	M�WO4��,.F�\�j����\��v+\� \�]\\�\��A?��_L���\�\'K\�\�ξ\�r��;�¦1&����\'�\rdv\�4^��H1�B�I�\��y��\�zl\�T�\�\�\�v�\��HUFxQN��%�Ң.?3!X\�f\�\��)wf6��\�\�Ǘ4��\�~|��y\�q\�ɼ=j�7=Jaz\r��9EQ�䣪ֻ\ZΈ�_w�Z�5`�\�;�i_�0F\0\rP\�\rY��d����j�HJ=\��ݰQ\�m\n.D\���w��w\��\��\�x�\�\�q�)\�\�!\�q\�E�UA�c\��q\�\�\�~�\����3ஸ@ifYMy�}5 	��\nVB���\�G��^\0\nK\�%?!��{\�ĊZ��M\�sL�`�\�\�~�$�&y5<ݬn8�h`�\��C�٨8qS?\�\���\�\��`\�̃\��_�n\�\�}\�����}��<�]D�~\�͓/O�o��ƈ\���\���\�`\��W�r-�\r\�\�Ӎ��A:�t�\"�\�jdb{�\�\�\�\Z�M\�\�vG�\�\��\�&\�N\�smՉ>.\�4�\�p�\�\�@\r�F\�\���\�MI�N�.�m��f�\r��i�>\" �\�A��5�t\�&uA\�MEh��Sc\\\�\�r��~\���Vun�\�ߜE��ώy����u3\�.+�Z�%��\�v����\�\�\�.�\�z�\��r1_\�^h.��)\�\�ʉ?\\bu]w�5\�T��g\�F�\�M\�\�eyc��i�e���n���SL\�~�a�\�\�:\�S��\�}C7�Iy�]\��\��\�\�\�Ձ�*\�G�\�\�zUa\�X�lk�1~�\�\�\���]���Q5�\�[P�Y�����j���T\��\�\�+��\�\"\�\�\�W�\�\�\�\�e\�c<~\�1�s\�SB皙��\�\�Eٹ=\�[<ո(�:˼U��7\�\�\�BƷ����\�˿>��	^��/g���\�\��� ��F�;�鍙\n4\�Mh\�f\0�\�\�.p��U�\0GWah�\�Ŧ�\�Mdvz\�U5@#k�=Y�������F.\�\�\�1;\�\�NP\�\�,���\���(��\�?�R��wR\��F�\�-\�j��(�#�\�2t\�Gz��j\�N��XDB\�N�\�#g\�dݠ���\�P=#��pz5+�L*�ď �\�y\r�C��\�4��a\��G\�\'������01�nWF(s\�\�\0\r��\�^\�v2Y�\�,a #\�\�Ŀ�o���ѫKg�.m�t�.���a-̥k՝�\�Z��f9�)\�4cQ*[�3�VW+8��;��L�V\�֍�l�?\�%jfw\�\�Y\�Ϳ\�6�(�|M\�c+\�ν���iˠ�)J僷\�lz��F8\�d�9_�\\dNT�6����F\��z1§	�<Cq\�ƘV�\��j컈\�\�4b*t|x\�\��;.���q|\�\�\��00P~��<)7F��*@[�)1��\��,g�ow(c.�wPA���\�;L\��U\0��&\�	\�\�^���\�S\�\�A\�\�ލs���\�̏|t�;y��74�y�T�A�C�tM~�WP\�È�\�0\�\�O@[�\�\�W\�t\��\��p��Z�Ձѡ\�D9��	\��2��	�\n\��\�2%t\Z�$\��Y��;ͧ�>N��Q��]d@�\�q�fo\�Ѱ���!<	��\�5�2�I\�b\�}\�V0��lF7\�a\�E՞�S��\�C�+��燰���nkM��\�#���$\��`\Z��[�<5�8�]��\�Z�\�e/L\�٤3\�\�w<n\�)M�����t�\�g9C�w൛]��!)N-C���tFAd�k�\�%B`\� 	��\"\�O}�c�\���X\�K�!\'�\�p\�B�~h\\]C\��\0��>�\� ����~�σ{��\�>�!$C\�Ua|�\�Ԩ4�xع\�I�\�L+&6�N��9y؀\�\�\����҄�;\�J�호�.�\�15\�)�ɘ�螑\��(;!���b	\�\n�	������.|�㲧H���y��Η\�M��HJ|�/\Z?[\�Lh\�b�6ܭ\�X��d9�2��\�w�p�4ϭ�.���Rr�s��\�KAE\�~w���Ё�]N�\�f\�w�\��z~O�N\�9}}	|l�\�3^e(\�X\��\��H��[6�\��~߿lZ2\\6MM-�\�\�{�KJVlH��Swq\�H\0\��T�6R7�\�\��zʩ��/\��)b\�|\�:���n\� \�����r;��2Q�)��w��+���A]\�\�)f+ �).\�\��\�|$?�5��������ZC\r\�O|摢�ǆW\���?\��\�A�I�ߢ �S�b�>�V�\�|m��#\�$\�q\\\�\�\�2�/@i!,l��\��:�_VE�v����kR\�.gH\�5]�\�5zQ&\r\�K�w\�{�7�g͢tj;���\\�;R\\qE�D6���r\�\n�\�E뻾�֣\��\�g�\�\�\�\�]\0?z\�\�ǳ6LP�(�G�r(;\���i5\�8�X�|eo\�\�n\��w*>�\�%s@8\�6�B.ta����\�cR�[,�\�y\0>`T=8\�`m\�c)\��\�\�R��j\Z�\�KM�)gP�,�D\'\ZZ�K�\���e�\�A�\�\�do\�w��(/\�5]ѯ���,\r�\�\�8\�C�\�e�0|!�_\�e\nd�z֎DX\�p�\"� `\"\��!\�\�\������K\�Rh��\�ˋ\0PT\�\�\'?|\�j�D��O^�G9Z���k���o(#;\�0\n�\'��h�܂��\�,Pg�)���t�\�\�\�8p�=h�;=\Zx�\nf\�����PJ\�0\0dy$\�vN:5\�tW=c���\�s=��_��I:Y\��d�k�h��\�p�Կ\0hQd]\�R9\�\���\n\�\��D:u5Z�\�\�#a�(j\�W*���nc\\ˣj\\=��a�e�*�{ �\Z:%\�\�\�\�k �@����k�A��4\�\�\�esA\'�/=��Ʒ\\E��jțƹ��\�SC\��X��\��H\�)}欞\�r�:�7����5y�N�z\�\�v{�NzםG�;�\�!N��Mwֻ�d\��E\�\0\�\�\"�.\n?������\�\�|\��K\rB�\�c�\�y\�\��ʹG0���\���q#��yȮ�*I�G\�\�\�\���\�]��H���,�F��\�h�%*z��;��\�3��Tu��*�rz!\�\�\�	+5\�W\��	t\�\�N�n\�\�\�}]?��\\���QD\'+�\�ڰ��_0�7O�a\�i\�u\�]��Ɇ��/\���\�[���\��E�\�*[�UR�qǤ\��v\'����\�F�3\�u<:y\"\�`�3���\�}��\�	�*��Hk�\�]�\�C\�w�&\�\�\�7~*�2�P(`^\�xb\�\�L1\�ļ\�T}ó���\�T\�Z�<���\�P�n��q�*�?%�A�#ĸZ�{�\�(���LI�c�\�\�\�h�!�?M�\\��o\�¯7�z@�e\�v�c\�#R10F\�AA���)wwG���V�\��	C\�\�G�DR\�(T7��;�l�)ls��ؙ�Ţ r\�b\�v���A�\'��\"�|�\�*\�n\�\�\nbE\�\�Fg@e���r�cg!E���v=��\��𒫓\�,n�7g��\��S���n��\���\�I~I\�\�e\�G��-�\�xS\�\�\�w\��D\�5\�\�\�\�\�\��]�)�\���𷗛En\�\�R��\�[)A\n\�\�mc\�h�\�CPx?��Lφ\��\�*2Zm32%�A \�\���\��Itf(+/�5�5�j�\�;oa?\�\�z� �E\�����\�\�\�Y\�͘�\�n8�\��\�.(j�)�XݺR_��\�\�G\�\�\�VK��8\�\�0c9V)͎�Q<򰲯�7�\��\�\�\�}�\�/\�\�T4\�BX\�Tx\�hR��\�\�\�$\�}W5�\�Mŵ�z뮾*5s4�x\\\�:�\�\�N�1�g\'��g����J�Z\�5\�0�oI\�\r�\rM\�#օ%R�]\�P>Q\�Q�OBJ����-\�r�9YE�)�%Ɗ�Qn��̚�;\��\�~�]nޅ����:s�H\�.2)N6.�\"�d~�W]RH\�t���˿%����S܎�x��\�0,\�\��\� 5��l6��\�\�5\�o\�\'>Wp���\��\�Ȗ�MH\�MT\�z� `��\�k|)�`��_�7\��f�\��L�va6@\�m�h6�Y�0̈́��WǠ:�$\n\�*�)�bC?�\�lMt9�\�\�C\�\n\�\�F\�\�\�v�e\�,3��̔i­\'Y\��\�*�衚L|��0>�$�9rQ��j�M^T땚!�$,\0��\� I���=�_\"�Co^qHo)P����Xl�O�fw�����\�	\�\�\�9=BvnM���l5ˑml.�bSpׂk��y�k\�L`\�Y2Q��~�,��.������3\�\�|b\�\�v�\�\�Ftq�\�\�H\�\�\�\�\\�uqq�\�h�§\�7\�-���?\"6�{j\�wx�U�\n����$J��\�=b�a\r\�\r+\�1\��^\�k\"�\�����\�璴�h\�\0�#^�pt\�\�01\�k\"O�K\�<\�9X>)�\�x8�Nx���o]�\�+\"JfG�\'�\�B44\�g�sg\�ݔ�����P\�}S\�\��\��\�ޜ\�P�A�jAHA\�=y���\"�r����;S\"\"\���`ML;E:�!����ކ\�\�c]�\"�R\�>٤\�\�w<\�5�%����4^��r:Y���Z(8\��_N>q��\�\�>�\�\��s����!L��-��\�\�q;�Q�6�y��0���\��\�\�s�\0ϥ��\�\�\�\�Ó܍\�c�]g^�\�ք;dgnIwԷc\�~�#R��\�l��<:ɫ�~<>�~u%\�3���K��\0ծF\�\�^h\�\�OH��O���\�\�=��\�$_ĵK8b�I�>�\�\��o�Y���O�\�H��A\�M\���d\��pu�c5�0\�\'\�g<Pã�`\�6�Z��|_�\�\��xBI�JLnk�[�=��W�u�_R\�\�8\�{\�\�7\�P�ȍ�<1�XP\�\\�\�\�t��\Z?\�\�\r\�)\�\��)���<w���\�S�\�\�\�X\�9q?�\�ޏ�)��\�n�5��\�u��h}ѻ��N��� [�Z`�E6)W�\��\"�\�`\��I��Ft�\�8\'�S*|�\�C���@\�mʔ$\�_&,�I-&~\�\"x�MܑP\�D\�\�X\�Z�\�;)k_K`�I\�=�t�LEM\�]\�\�؋k$�Ҽ\�[�\r@e\�m\�9�j�^�|�\"��\�Tt���N\��{�9���\�^�e\��	1�N\�n�~\�4Sv�i�����0�]\�\�ϧ0��J5�Xu\�عu�t�\'�\�\�\0u�\�\�PO\�\n�l+�m��\n⪝��C\�{�#tj�\�H;q����SD�5DͶ\�6��>��\�(\n�L�E�}w\�泶2dX}:�\�\�\�\���r\Z|�hQ\�n����\�k\�Y]���lz��\�쐀;d��\�a\�_ϯ\�;e�\�\��:m9�Ԑ��\�)����\\Tw\�G)\\��K �\�_z���Rw\�l{�\Z=�w%�yRhV\�1�W%:ƅ���q�\nF\�Q��\�P\�\Zs�9f�S�G��P���\r2\�\�\��8̑+�F��<6\Z�\�.�e\Z�\�t�F��~|��\�tA%��छ\�A�V\�\�|�A�2\�\'_\�C\����!�:,�%�1��Ŵ�\��PX6�U\�)��լ\����R(�ܢ�\�\�\�]OK�\�OF\�px_�\�g�\�x�M[~#��A\�צ���\�\��p/�=fex� Rl�#�ft\\\�\�\r�g\�8Ԏ�Jq��N�uvwn5�D�\�W8@�%i\ZQ�*��\��<8��\�=G��kb�\�s;�+|)�\�DDz\�[�\�h�\�\�~�mc\�(�0�d%]�u\���T���Ռԭ]\�=�G��U8i�����\np��\\\�r\�D\�۾���E\��\�@p5v����\����#�^\�;݇�۬^\�E-T�Ov�w\�׈�o�\�@\�U�>;`�HY/;�\��lϹ^\�\�\�~�\��\�\�\�e\\\�	~��m�\�>\������H�>\��^�\05��.Y5y�\�hyٽ���Xy��ǦGI� յ�X�c�����:�o\nӛ�\"䜣�B�\�S>1y�W\"h�帐Sĸ��\n��sſ�7ϡ\�\���\�\��ͮs\�U�`���l��\�kt�:�5�q\�E��V	H�\�\�SyG\�l���ր\"Z\�w4\�����B\�K���rk��yM,�j�\�E�.\ZB�d\�n�|�\�\�=?�\rx\�h�\�r�+W=M�!����\�\�w�U�\\���I�qn,K�_<\�=�3�N���} ԗ\�)��ǒ�\�ma�-R�&sa�\�tH\r\"!\�b}�\�q�/+�Z�ӮW��J\�*s�~�(�l\�=/GKH\�\�\��{�\�\"�z�CދNȨ��\��1>\�\�Z�[\�\�\��\�.\�p\�B\������毵,z?\�,YQ��z;�\��Ǉ�a\�\'ێ��đ\�d4�6\�ݮ�R��p{\�zϤ\�݌�\�\��m��c8r|�t�ok!��@��O���\�X�U�Z�\�\nΦi��\�8\�1\�\�e�\"q.`[,�/-T��}T\\oD��֑B,�ˬI5\��\\;��/\�\\b>�,\��a�\�4�-��񦁎|G�J��\�\�\�%Љ����%��\�C��\����pM�z	�Κu��� �{�)����}Z�W���\�\�0\\�\Z��\�\��K\��{�c\���c�d��\�i\�:��dzؤp��s�oڧ\�,\�U�?犼͓K\�dA�+�Ʈ\��F��\��\�3J蚈\�C�ƹ�.O_SB\�\�qU?\�H<�kZ�x|��\���\\��n\�x\�%uۉ�\�/��^\�\�^<r7�J��/���\�����\�tD>\�\Zڒ�R�e\ZD\�\�݃�G�Gp�>�W2�\�\�s$�/%�/r�N&M��p,��\�O�\n<nV�Qt\�\\t\�?%\�\�>Trz\�Gs4�~|�hr\�n��\�L�i3�&j\�/���а\��_w�&\Z�����o\�m\��P���\�\�\�Yں!9/b斦\�\��Aw�\��\��z\�\�\�\�\�B\�#�� �\�\�NH�\�#�	\�{RUs\�\�)\�6�\�`Q}t�a\�\�	�b;\�@����\�x\�?\�\�\�Y\�\�y1�2=6z�.��P\��8\�Qam\�W��bD�\�-�\"��[͞�\�?\���\�ϊ?\�h�x��\�\Z\�k�WI�I\"�\��G\�L\�ΔM/��U\�S>Yf/��(\��\�0,6�H<SSA1\�\�WV\�\�\��}*4Id���\�hG}�\�	Q5<\�A\�V�s�&)\']��q3B�%\�o\���>Q\��~\�?-^\�\�/�\�O��˓�\�\�J�޼\"���w>\'�y�w�ӱ~\�gLZS\�N��o�]Fj\r\�uɒ\�KJB�K\Zv�5\�\�r��\�l�\'�h{�NS\'\��\�8µ\�\�iO�2\�l2w\�2�\�Ѯ��:�7���\���Qп��_\�m���\�܃\�in�\"�e�JA`#��\�PC?�2gk��T\"\�O�j\n.��\�&\�Q�.lf\�4\�f\�\�הHnGXQ\�\'Αx�;�\�\Zo%G~t/\�\��\� <׋\�ۙ\�q��={�(a���YpX\��Z��\�D���&��z\�\�X\�9Ÿ́4�@���\'�j	�\�&\�Z�k�\";_�0\�pF\�|t\���x\r=5\�.�\�\'�_a;#;�E\�L�\�t\"�r���\�k\0P�9`_qB���\�kA�l��N�\�D\�[\�,\n��_5\�h�4.\�U�yV�>�h^\�h�ղ��Q��#�+\��$\�5ď�7\�u-\�*uS�\�f�#�mzӍ��B5!\�\��\�~#\�\�M|�Z�홳>��\�\Z�k�`>/14�@��d�>{%��\�\�65�q�\�2\�<g\�c_��[\���\�\�b�c\�P*�m�U%pn���K$�n�\r�:���\�\�K\��<h��\���\�g�wCS\08�l\rYP\�,t1\�GN\�}�V\��x��\�U�)t\�>\�.C\�E\��hB�(a�\�\�A(`}y\�q��%ǵ*j!\�,\�G(�_M6�\�,GO��V���4X�\�l\�0���\�\�-\�\�\�\�\�@~��i;\Z���\�{���6fP������1����L��\"��\�HhX\�O\�\�\�\\\�FNt�\��\�n\"\�\��7�ݚ�w�e�Z���G?�\�4aF׃:��?M\�`ٌB\�6<�>``vO�\�&��\�\�i��\\�+\�ڕ`3	\�sӱT��4\�\�m�Ӌ.@�HpՖh�>w���F\�&?}��9�;�/\�w��\�q|:\�qzr��\�|I�k�O�kGO\��2\�\��+\ZJ\�\�\�O5մ�D��w��PχWd���\�h�\�-�\�\�G\�\�2\\c-��i�\� R�\�\�!^o�C\'\'S\n\�1r<8��k\�`�\�\�\��\ZM\�\�}\�\�Q�\�\�䖠��:t\�9\�J�\�Sb N���\r��\r�Ӹ�}n����o`1VP�\�P\�)4�N�ޑ\�$�,C]\��B�\�\nIg\�\�^m�\�<���6�\�jWȞք\n������&o� JS5���\�`�}\�ꮇ�\Z\'Pd��f6\�\�=S\��\�X\r\�o�g�{\�qծ�\��7�2\� &ڛ�8�y\�;��\�\�6[Aj\�˝����U��\�\"�0���։?n\�\�(n\�\�\��\'���\�f9mgQ�����u$a�\�7\�L��۞�\�\n=�\�,:8\��\�f\�\����������*�\�՝ ;O�5ei���\�\�g]�FL$E\�Z�[\�)\�\�[W\�\�\'n\�3�<�oqf\�\�zeq\�\"ʠ�\�څSH\�\�\��\0;5t\���ٻ\�\�\"\�\��\�,VY�q�-��wY�������\�0�\�<\��g���Ӭ����G�\�~�\�\0\��ς��n\�I�s�\�V�8��?�������(�>U�|0\�J\���>��:���$�\��\�h�.�1�Hн6\�\�D�Ge!��\�/(90m��tp��\�Z\�v\0�\�m\�g@\\�\�Q�sw\�n�>\r����?c��\�\�2\ZXQ\\U���m\�\�7R�Z��n��]WW�+\�}��\�\�\�%\�U\�,�\�1\��Be\��\�;9\�Ŝ�A�#��\�˞y��,(]��\��\�+��^x�՘6\�\�q\��Nh\�`�R�FD\�`]&\�K(�[\�W\�\�2\�\�F�\�z\��\�\�R.\�(�\�/�\�A\�k�f\�~:�n�R�\�qN��`Qâ��&֔B��Zk\�e\0a?\�M.AaW�=Ηp�u��I�;Y>ܪlb\�^��\��x�T�GC\�W!�B����f\���\��\����wx�;���H\�\\�IQa`��\��t4U6\�\����<)5v\�h\�6\�K(����&\\CŢ�t\�\����І���\�I���.2G�\����\�`o[�E9�CȢ&��.Ӱ�T�Q\�@�\0hgK�\�\�j6�&�:�\�`٩�w�״\�\�o@�Y߃A$f\�}�\��\'\Z	^2\\0�[�\����V�<ED��Y7E��`�l\"3⣩\�?��d\�j\�\�%ĉH�\��\�#)\��q��\n�/��?~q�\0��D�\�\�*�s\���\�F��E�>\�\"\��aWaz\�2����|L��2A��&f��q�\�h^��\�d\�\�-LhQTI��3\�^JZ�\�:��	\���7.�\�փ���D�^�kP!\�\�Vyj��\�$\�KOJ`\�-U7�A*c\�IneԪ\�ZD��8A\�b\�	j�Z<\��ݝ�\�`�I��z��nNJȂ`���\�s�;%pdr&�]\"?0*BX�\'���3��D\�\�\�\�F\�@���W�́\�k�\n\�\�\�MF9Ӧ���h�%N}�V92-J$\�pH\ZG�i?M��/\���8{_�xD�B|�\0�ٱN\�w�n�\�l\�55�\�1\r\�\�\�6\�\r�(v\�$��l\�c�3ξ�����{�\�\�m\� �LJ�|s��|߯s�\�ҋ\����E\��_\�\�\�T\n\�\�β\\]��^B\�\�;�;2wJ�o\�\�1a,�\��_�\�[\�٥J�HL\�Jz�o�:�een>\�a��\�<�\�-��)|�\�ce\�_a8\�̶i���aغo\�y������\�-\"8��u�&@�,3\���\�5w��\��\�?�t\�\�Q\�ࠅ\�L=�\�}G\n\"�|�ƨy�g��J�^��\�L\�f�)�d��\�c\0\�\�\�r~��^�Ckf��s߼\�\�	S\�\�\�?\�4ޟa%pf\�Rp�;R#�\Z���\rO�\r]`9?9F\�Y�?u\�C�\�K3\�7�Rݽ�N���\'6�W�^�f9Ut\�Kt�D�\�٩7\�a�$u;�L\'D\�\�f6�*]�)Ľ<+���\��C\�\��]I���.\Z����1ᑕ�33x֎�\n19\�\�;\�^�\�ԏT�t��\�\�c�\'�\�\�\Z>�-��\�\��h\0m��\�\�kQ;�\�W�6*\�D\�P�9�\�G\�4\Z�A\�\�]\�\��\�\�\\�(��{\�v��rB&R�\�\�弧7q\�c]]z�B8�\�U\�7�\�Ͱ\�\n��xJ\��M�wB�|\'��_q<��%S\ZlI��3X�n\�k��/����z�L<���\n\"�\�n�E\��C�5ȮeJ�z\��q\�\�i�+�\�\�?ZO-���v���<؅p�n��\�\�>�O\�\'T���_=CH\�\�=�=�7�\�8�{\���/o���{���{\�N\�,�\���	mDi\�]\�V�\�{{�?\r-�\�>����1FM�i\�b��؟E�����H\�\�{|�Tꑬs��Pd;�\n\�\�LD\n���\�\�P\n��\�A�U3�\�8\�γ:�ueg?@\�f��GU\�d�x�L\�u\�Q�\���\�\�R�T�N\�\�%\��&MC\�H��B8�뢽8�=�8\�=KI7�t�I7�C\�p	w�ͪx|Hje�\�O\'ם��\�\�\�\�M\�iѶ�>��\�=-�Z�\�1��*m·,�`ib�ɘN\Z��f]�\�	�]z7X;\�%��ĬmrpImÅJ\�t���hzA�\�\�Sj\'59\�`Y\�֭H�$\�2\�\�S/\�s��\�g]h\��\�te1L<g/��\�|q\�_\�l\��\�B��l�P\�v�\�Һ�\�\�\�Ɗ\�\�\�\�}��O<2�[��Џ�M�\�e�\�V\�\�\'M��n4��M�c\�U\r\�r�\n}�\�1I}cV\'�`�S�\��h+E\�V*d	)\�f�K&����x4���M,�SjEA*�.�U*��D�T\�9D?�Q�E\�\�h�\�\�:Q2r�]�����t�^ϯ�vP�ws�\�itA�\"\�.\�Șv�cgU\�lv*���\�\�l�Q\�#e�.�\�a�[.ӗ�����\��\�d�Dv��@�\�AIhɈ�\��=\�ݸ�\����I\�\0��ҚI�[\�\�3\�dm\�IM�3�`\'nռQ�ێmn�.��g\�\��\�%r\r,R\r\�.\�g�\��\�\"�\�\��$%��e�\�M�\�!�d5y\�\�XƝHs��c\�U��.�\��j�g3\�1b�\�@iJ5\�!���\�b�\�(\0\�I\�F\�6�&�\�f���\�Gs_\�z�\�,�[練=aʤ:l]8B�\�x>U\�	u�8�G�}>##|n0��\\\�g\�ҩv\�\�[M\�:�b;�9\�\�ms\�\rҴ\�\�E]S0*,\�MYN[\rv0L\�w���h�A\�\�bG\�kT�u\�=_	�I7�\�7#�\��?^c��~�h{8�K\�_\�4۬K\�N�vK\�\�\�2!����x^�|B��\�\�i�+(���^��(��ñ\"B\�r�.G��\�n��4���#Vws5�Đ��Һh\�\� r�	�\n�V\�vr\�z����\"�\�\�\�ǜ\�p\�x�-��&�9\n�\Z1\��\�����J8���w4|0��^c>�H[J\���BeՕ4\�Dbt\�l\��L,�\�\���8J)	\�)�Tgg(AǠtݳjZ��e)\��\��\�\�`�iA\�ʱ�iXL\�}�M�Uh?���\�:���\�XG&{Y�=\�W�F�w�\����C�\�e\�\�0C.\��\�8�]�\�R�<\�v�M`\�gh�Yl�CZL��x2�n\�L8��\�y)&�ӑ-`�:\�8;��\�l\�f\n\�z$����\�](\�p��?w~&�(x_p��!�_R���eɑ���6U`c��\�q����\�\�Z�Q\�!�	\�$��Y��8�*j�b�p\�\�Z�0�oơ3�L\����9	\'l\�cn\�b�#��`��Z\�8*�\����;Zb\�.\�,�4\�\�\�\�\�,\"���wϠw����\��\�f2\� 5A\�\�	\�]�(0~\�ab�[>\n��0\�vX�������D\�E�[?\�\�\��3p��kO�s��K\�ŅF\�1[��h�e45��\�4I\�I\�@�\�\�\�T�i\�\��ZhG��F>\�V<�=�]1\�&]���\���\�\�V�\rA\�=\�\�\�\�oZ�5BR�\�4\�|�_\���(_Aҟ\�\�t\�5\0�\�H郫\r�K�\�\��\�X�\�f��_0\'�M��K&\\���A#�@�\���/�\��pZ\�\�U{|��|B\Z�\��|R�7���\��-\��:W$\�1�\�0\�c\�lȞ\�̇\�a���\r\�p���\�\��uᯀ��VF���X1�8\�_\�)]\�ݼ�ioR\�\�`�|\��̱3��_�h\�#\�\�erk,��E%v�B=\�s\�	\�\�\�|:\��\�O?%Y\�\�{\��\�\0��_\0�Z��\�8�\��EȌ���/#����j����G�q�^/V_�U�52n��\�!-�2\\���u\�\�\�wY\�:9qS+�S+��;\�V:hS�IG�@\��\�!t�\��?\�x�\��j1\Z�~\��31\�պ��f� a�	�,�A�\��2k�~\�\���H�8�s/QgP���ʲ�\���\��ɤ�ep\�\����|F�\�Qԗ�yw���+G\�V�w\�\�\�h\��i\�n\�\�Z��\�\�𪂋\��\�9���m&\Z�:���,R�ɂ�\�\��\���e\�O��u�\�����,}\�\���ǚ�\�Z�Nڎ#�!�\��35b)h\�V8\��=�\�	\�n�ɫ�;\��\�\�l�q>]��Mt\�\�G�Y@3�\�JM\'۫x\�3\�\��\�g�\�@ǳ��\"T�\�7�\�fA�T�ŋ\�#\�f���;K\�\�Pm\�U#\��\�A�uL.(��<������\�\�(��b�L1�~\�ƚh/G�\�j�\�\�$\�N)\��F�+\�\���ʟ\�3Vh\�i\�.\��Y\�T\Z��{��@6DN\�z�LL\�v��\��M���jp$\��\�j�[�+\�m\�\�\r\��{\����K$8\�\�\�o�)	�%���\�$e��\n��/RאL\�8&_\�s\��\�\�;����m����\��=˽Z�Y��Z�\�\n��\�q�\�\0�\n�\�d\�\'�k�aFe������r\�\�ڡj�f\�\�v�\�\�\�{;*\��Ej\�g+%y@a�ᄵ\�}形����SR\�aV�f���o\�K�\\�#\0>�W��\�#v[�/�\�\�\�!��EO\r\�\�;,\�\�J��A��\�	�\��	�Z�7H_\��\n3�F�֮\�3�C�)��)�\� �\�G�\n\�c|P��&B\�\�<]\��c�U\�r���bm\�\�8o\�\�z\��\���xo�@�� [)�פ��fP+f\�(�\�l\�\"��\�*��_n%��V<��q\�+\�{q�P\�Ε_ȗ�0ґ�;���z�˴��Zq?!\�\�<��f�\�CD]@�y��%9���^��՝�#��|\�v�s��\�qV\�\�B���\�\�\�,\�3��&\0\�f�&x\��t\0\\�D<z�T�\�V�\�e��BFH=~�\�\�7\��F\�\�I�\rL.	)\� �:b\��s\�D\�t\�q�\��\�\�B���=@\�\�ݓ\�0P\�׸\�~��\�)�\��䮚�<�0\�!D\�\��\�\�.!�Ǉ��jM��-�JD�\�ֱϞ\Z����WZ\n�N�:\�>�m~��ι{\�\�=�VNh�\�&����O�>�\�\���5�\��r�g\�\�\n}eٰ\���Q�t�=\�����)ջ>�T�Ղ웃�Ȩ�?\�F��\�R�g֤f\���E@�o��\�)R\�7޻\�\�\�\�7�\�MP�&(y\�\�\�)Avݽ�/X����wH޴\�a�\�w\�-\�@pƶ@Y;���_߬K	s\�W�rb���\�v�\�I\�\���g�\�I�ק��υ\�ra��=��I\�ј�K�gi�\r�\�N�j�/6�K\r\�U�\�\�˕��\0�\�O��C{�TS&.\��U\�\�}�k����/l�-z<��v\�ᅜ\�\n\�Q[~�\�)�&�UJa\�\���\�L=\�c�L��<�\�ujNZ�Fv^Y\�J�QџK|��[�\�\�a\�f���K\�\�t�r���\'<��,�\�a0�\�\� 3#�\�\�\n��<�jԑh\�=�Ɨ�\�\�\�Y\�W�(E�R�$,�-�B\�\��\���\�z��bg�m,\��?�\�\�4�ky�Q�\�i�\�T_�\�\"y�ȲBҾ}\�C6S�O~\�j�ӤI\�4uT3�p>�\�\�9Z\�¡~��&i\�J�\�YΊ��\�\�y\�;]f\Z\�\\\\WtN\�z\�o�%\�\�q�F�p�J\�u:�VX�`DMD���a\�(�US�^\�5� +U\�6�+N\0w_��\��0~\�\�W�����_}\�A�I�\�\�\�[\�\�\�\�wl\�\�\�l\�_�9?�T��Q_!�q1\�	6yӘ�\��r9hϜ��=^e�~\�J!\�\�B����9#�Mwtn�\�\�\�#Ud\�l\�Rw-\�x\�\�H\�\�٘κ\�ud\r\�3\�A\���}�e�w\�(6Kr\�t�6�A\\.��A�&v\�Q\n���No�\���S�ʙ\��\�|u\�~a\�V\�g\�2�Hv\�\�\�R��?{ZP�\�\�\�U��\��\�)O\�&����[\�ٿ~�\�\���Ͽ\�?\��d\'ǫ،i� ��H�\r=�	\�\�28\���\�9\��j6A\�x\���\�k�\0\��\'�4r�U8�\�mo��J\�0&I\�\�.4�\�\�J�]\"��\�C\��3\�:\���2\�\�>UU-q!�wG\'v=��J<�$g��`�\�|6�\r\�R\�W�a�\�|Z�o4��Ka����\�\\�\�\�:�\"I�\�.�ˑ�C��\�Iu3\�a֨\�m\0\�+hN�\�Ш8�\��^�`�|���\�\�sҽ���N%�ŔKJ-Xx\�ԯ���W�ڲ�WT\��\�C5�\�}񡽊vF�ns\r�\���#1\�g��	\��\�-�\����p�\�\�mY��\�js��T��\�\�}\�ئ��;�x�m�\�NO\".q��~%��\�&\�\�,�C��AYl\�\�ߺ�vq���\\\����\�\�\�\"˱*\�\�\���P#`\�\�\�7|f�\�|�:\�߅\�̦�B܁u�+o�\��g\��\r\���vݫ\no*C\'\�D�jF\�Ex\��U���`)\�\�\��5�(޾/a���7:�l�F�\�䭊N{�pS�\nj\']^SE\"}/�G([#�AY�Ew\�u)V0O͂�\nl[{p��c8\�c\�p\�\�D\�\�\��|F3���\��3�q\rcU	w\�?-�}\�}G���.��GY>Ŀ\��/�\"+�FI�o[#P�)Ε\���˝�h$\\�+\�ˏ�\�F�b� �=H�\�E|�F࢈\�c�56\"!�A3�B���\�T��Z�5ȃď©\�h�-\�X��\�I\'���\0��jsHBM�g\\�An��^\�1c���*|1o�U)�I�g�\�\�]a\�\�\�b��^BGW��\�\�?�]L��C�sh�C��T\'�	\'\�՚�\�\�\ni6\ZT|欞I�\�`x��p$\�W�\r�\���!\�\�u�\�]�[�\�Y\���s;O\�(�߻�e�.\�Gw�\�x*\�\�t\�o �FS�\rD\�\�����P\�	\n\�o�\���k���o�\�6^�k6;1\��F{#\�(���i��\�<�Əw�:��x���\�(`��\�6\�+\�]�j\��_\�*0��j�?krF�\�g�LD\�	�S�Ϙ�\��9�	N��f\�Xv�\����\��$,\�\��5��T\�\�+[k71u�\�1\�\�/�Hrl`4\�Fr�F �\�U\�#æ�dfu/\�al9��>\��L҈�I+\�æ;�b~èј��nC	41�\�5h\�-@��\��0<W��\�\'L�\ruJ1�\�a_���֧OH�\�2&\�U�\��2\�oSٓh\�HV�e<�\�Wj\�61��\��n�j�j�ed\'Kn\�\Z\�\�~�]t����sz�}��+����t\�멯~��\�\n+\��E7w��#\�aݸ~\�\�=\�\�\�\�u\�n6��\�\��^��Y`\��M`���H�\�^���\\��פ��^\\/\�Y��\�\�\�\�\�Q�ٶ��\�a�B�7�o��w½^\�\�p�\\�nD\�\���QKQ9�f���6\�\�\�\��0?\��G��\�v��:ΙFv���\�1%�t;pƾf]��\�c<A}\�}�?\�\�Ub��G_B;�\�\����6j�\��Y\���\���Ł��?\�v�~��d\�\�o\�?��\�\rhb6���aʰ�+��r\�	�\�r\\��3C��{z�%|�}�/�yI\��\�\�\��vv\0�bt��V\�7� L��q�>\��\�-�� \�\�&^�Yu���u�p]ř\"#]��o�\��	�s�\�p�X��8\�?�`�x�\�\Z\�Rs��@[B�݈�\\S<X\�\�.9��B?��MD�F��p�^�M�\�\�\�L��\������p(/����X\�?���.�\�\�h\�\"r՚\�[7�M\�\�h�ƀ��Y�S~�1\�q�:��&p2�\����:�<��2�\��ɉ\�By\��c�`���*0OP�O\�D�?Ѕ�z�\�8\"�\�\�7�\ZBaN\'�e����\�\�g\�ycY\�r�W\�n\�3B������\\�e\�\�w\�K��h���|3�\�[_�3�\�A�\�\�\�u\��W6^�\�)\�\�v�m�\��]L�e���oN�̼\'a�r�E\�2B\�\�DN���\�\�\�\�r!|�	!\��\��g=�߬�	\�f�M\� �\�t�\�-\�\�r\�\�\�C^�\�S\�7|KS�T�C\�\�\��v��P�&G!�OLg\�C�{\�\�x%�p\��\�5�i�\��\�LAZM\"\�/m_XW&�?#��Eq\�d\��[\�?�\��\�gm^�\�Z<�V_\�iF\����\�3�CV�+b0$\�2�\\\�URI]�׀I9��\�z\�%Ե\��\��\�\�t\�\�a��\'\�Ŀ\�D�F\�Y�P#�v%��I}�\�L~�\0~����M��p�h\��>\0\�?��\"�\�_�`�,\�\�\0mE6�..Fm�Yd��\n\�JVo�|\�\�7hA\�\�4�{qsC\�%�J��<v��z\�\�Ҟ���EN\�\�9;\�\�m|�WǞ�;8]\�Xne$}�%��\'�\���dK<eu~AAE\�\�8ɵ\�|Yпww\�o\�\�\��8�d��u�\���/>�*�q�c�̌�c��>:;�S\�wl��\�\�\�	CZ\�O׏&�x\�A\�\�r\��ǟ�i+�\�+��\�K�\�\�u\\���<T�\�4�\�\�&��T?z\�\��\�7돃\��P���\�\�\n��5\���A\�)�\0\n��ۧ�\��s�(&��>\�\�Q\�v��i\�\����\�9\�`\�x�*�>WĆ\�\�ǟJ7;�c�\�?\Zy{�g,\��z�w�?]w�@\�\�O\'\�U\�S\�\�B��Pf�Z_7�\'\��G�{��cp���\���ŧkh�8��\'\�Ǎ�\�\�n\�>\�}!>]⏸�+\�\0\�q\�q\0\�_>3@\Z\��>��\�ϵڶv\���\�s|��\�R*ث꼚\�\�DNvL)\�_9J�˕��@@/ٸ[\�1~��#\��|c��=\�a�A�\�\�\�\�\�i\���\�P?p����C\�d\r]\�#�\�\�\�bY�V��C:qݍvRM��\��+�9̦�{u1\�L�J����\�\�-��vz!��4��\�k�V\n�$&9ܙ�,�\��\�L���\���WQn>�\\>\�W��D&[��3}V�K6#\��&��/�\�M>q�\�l/)\�K2����\�|��\�I^s�qXoÛ\�j��in�՚�\��1L\�w�v�	\��t>\�\�󳳕\�\�,�l-�\���Ik`|\���a�\�M�K~҅v�Վ\�\�h��\�\���WW�O��Z��\�\��*��X\����\�շ\�2���<PN��}�t��T�\�\Z������\�Y�)z��\�J�+�����w�vqj@W���\�!���\��7C�d�ez\�\�gnT�Q(\�\�\��\�|\�ueCK\��-\�;�\�~\r;2\Z��Z�\�\Z{��8�\��\�4���-g�B�\0��\�6�ə\��;�&�ď0\�\�\�k>���\�{��z�\Z�[r	\�5C\���Ai�\�)L�w����rS\r\�p\�4�Li�H\'�\�.�\�9��qϼhgx\�*g\�s\r�;�\��VL\�[�\�{$R#�\�%	ˑD�ϊc�6\�5�#��\\��\�\�Cp�J ��io. \�\�8���,> B\��\�{<�_�ic\\Y<\�\�Oђ�\n�\�>�Ce\�*\�\�Q-�(�l\�\�Le�\�pR\�\�0�O\�4��\�Vv\�N\�\�\�.r�\�R�\��7l\�5.�{\�#\�G�z\�\�lm�\�;h�ݻT\�\�\�\�K<m�\��\�\�;\�Ut�zA�B����\�7mR1A\�,\�+�\��\'\�*>xI�\Z\�\�V�R��\�lR\\m�+HP�4�Όg8�Pܤ�Cu��~�#�/\�\�F5��h\��\�⏁ٸ�\��\�#���s�-�Âj\��R8�t�mO�B\�:\�WR��aG;��t]-\���JPu[��lD	�d�$K���\�\�M\"\�5,��4u�O䎊:����ј�<��O-iP|�cZ\�\�\�o���\�\�v�+\�\�\���y\�\����n7���S #~ɥ\�񎡑x2��\�1�V?\�i;�a�cm\�4\�P\�\��С��s\"�\�-;\�z�Z�]a\�\��)\�=n�\�0�:�OȣP��\�ڲQe\r���6�7au4�� ~<\�`�\�\�\�Iϗ]�L�p�(h\�]?-�\�5�?��yCkIO�6\�\0�\�\�s\�\r�\�`�<\�\�\Z���^L\r-di\0�5�D^��\�t/&c���Yp��\���eW��@����5GNt\�xd+�ՙ�W.��\�W{b\�e\�E5<�[u쟰�����\�>˅ı=,|~mo�\�Ōij��g\�q\�~d�ޟ�\�A\�\�B\�#�Ԯ�b��{�x��t��ŰhtB�����\�e\�\Zz��\�\�\�]㨄t�\�V\�L��\�g<-hzE;Ӣ��G��rl�\�R2U�\�\�\�1ĭ\�\��\�\�\�Ԋ��D3�E\�\�\�]�\'1ݗ�U\ngIh�\�^�*V5(��0/�[덑])B��[\�V\r\�D1\�cF\�<�5({���\�f/Շ�5\�u_\�\��\�\�٥֜��r0�����k\�\�\n�\�^$Q�&Y����\�Q\�Tg�{L&K<����G\�(D��\�ih�hf\�{5ڃ\0�\�\�I_AS�;r��=�D�\�|Z\�>�r��䤪���v� P\�γ\�5F,\�7�㠰�~\�\�^�\Z�B\�Z\�z��ȉI���|H\�	H���E�\�\�ý\��\�h��[�\�\����V\�Dj��E��_�d�c\�\�@���#�w\�pE#f\�2�2J\��SPR\�E�\�\�~\�G!i0NޟM6\�&�\�`o���u�c%W\�zW\Z�[�8}+m�cz��\�g�%\n`d@�Q����\���3W	����dv\�`z�eh�}A\�SJ�\�\�ޭU�(\�q�ki\�\�\�Q��S-O\�\\כ!�\�Q�\�9���~ʬX�&\��?�� &;%`�L5/��x�7\�wZi��bK��\�ꅥm~�Y\�\\�,�1��2��@K\r�U$� \�*��f\�`uq�J�Bv3�pV�Ae��\�3t�\\�\�;\'t�9n�l\�xI-»�jr\"�\�T�0��aǇYV�k\�\�\rb	�}4\�\�TWf\�\��\�t4V1�ώ�\��E\�\�\�\�\�M\�V\�\'HS�7\Z��+fԂՆ�EW��m�ᮟ�����\�:-D��ǂD�E\�e�Y��\�=\�Ƃ2{�\�7�_,\0\�2�to�\��m���P(\�(��Q\�#�\"\��=a�&�<4(oZ\�\�B܀\��\���\�r\�\�Gk�\�\�E\�t彍t}�a=R\��J��d].5�1�k\�l]=��M���28tk�\�\��Y�\�+&N�x\ra��r�\�3w�g97\�7�	ԝ\��溓�n\�V�B\�\�^�l�<\�\�?\�VFvi�WL�+/\�ʾiԍ\�\�5-L�V��y\�\�A\�2�F�9�k�\�l?s\nxƠ�Ce(v�e�����G��o\���\�\�|�\"\�\�]\�\�\�t�O�B�\�Ҧ}���^c�\�jnʳ>�����+w��;$ҺOg0>{O\�}{���+ԍ\�\���X�-�\��_�j%�3�}\�\r\�ƃ/?攂\�\�b�K::5\�L��\�;\�w�\�H�N����x�\�\�Zժ5��[\\�_�- v����E�\�N\�\�mܶ\�� \�Ǚ�����<Ld���<����\�L3�{�3l\"|\���\�\rY�\�J噸g\�}{e!\�<.\�\�\�9��&2�zOA�\"\�\0����]�ޢ��8:\��J\��q*\�\�i�aU v\�(�K\�$����mD�溟,k\�W�F}D_��?�\rAG�7��~\�֊\�07�6�\�FD�m[�k\0\�G�4����.�š7\�\�qF�\�;}.{4\Z�<=K��0>$��\�\�C\0\Z\�+�\Z/\�}��\�\��tR[n��\�\�d!���\�J�T�\�.��㷏r�^t0X��\�dRz՗\�E\�aX�eu\�%�%\�9)2\�\�\�\0\rP�BkM\�\�,�\0\�5<C]�ay=.V�*��BȮ\�NG#�#\�/��ɐ��M?9��ǣ\��\�;6(��\��\�|\�GL���m��\�`��W&�\�+\�{FT��\�Yc 3Kz���\�	]om�;qs\�\����\�S\��\n\���A6pA�x\�CMl-�\�{j��m�����-\n�/_�\�)~ZVW\��c�4�x�5X�g���k+T�\�(>ح\�C\�0���\�e��0ZH\�?{��\�W�D�\"����<��\�{�I:�\�Y�(1\�\��h+���MDk�c|�� ��sQ�\�J\�C\�\�p+��$�\�*vg\�I_G��fwT��\�b�9T\�;�����\�;��_�\���������3�dP�\�+oN\�\�\�5\�\��x�ng�\�=�\�-X���\�0W�\n����[��5/�\�ϼ	?;�E\�\��eҹ٦\"_�x\�\�\�\�\�<ة\�Ϊ;\��$\�ѹE�~6\��9��\"4JW�t\��AA\"���\�\"�o���\�r<�� ��:�=jt\�\�r\0��@ϥ\���\�F8���g�\�b�}�K5���r[`��@�{]�\"nSMR\��i\�\�\�7\0�ch\�F-\�Q\��1�{F}\�4\�\�Ũ?�A(\�2q�\�\�ؗ�z	�R��m�\�\�O:X8\�[@��\�8Ɨ\�\"�����\�R�\�\r\�\� h�\�D\�\�o\�T[V\�VZ%{�}Yެ����f�����o�i^\�ċO;K�JŤ#��K\��\�I���m0!\�ػc\�䤈`\0Z%D\�>\��\0g���ߙX�9��\�\�\�\�\�\�\�o:�!.\�n 2�$:�3.vR�3����j,@�&u%qe(\�\�,��z�6�����,�ſ}(�\���?�8HNuvC�:\�W?jv)�k/A\�I\�\�M[�6FaĪ��<:�Q�En�k\��H}6\�%\�%�`\�(\�\��x2c Ǘ\'\�{I���n�\�\Z�F�l�F\�\�ڤh\�z��:爍A�J�Ms��\��ě+�\� ܛ\0\�\\x�\�h\�{~wwȠ֮4̧&�vv���־2r� =\�LI\�B�r\�ܺ��Ͼ.�Җ�	�\���\�b<Z��\�#A\�.e�fo�u�U�)\�(���\\z����&F�\�/1��б\�\��r\��I\�%\\R�Ϙ�<�S#z��ϗgڀ;\�\�\��e\�E]^�\�\�\�*�K�1��J��\�\�\�S\����K\rO	#`��^ϗ\�T\\��Ԭ\'샨����8�\ZTA7���8:phˮ��̯ڝO\�m%��l\'R�#:7G_U\�;3LMl�<\�\�5$M�1�J�}Y�\�w[y���\�pR�\��/\��\�f�Y.$\�_4�T�\�\�j��\��\�*�c�913\�\�\�D�hV\����Yk�\�%>���̿�8\"\�.�S_\�\�-O�N�hȣua�\�\�a�\�5M�\�݈PO��+HRߊ0\�Kڵ�4\"/HM�01+\�; ��s���y\r�\�\�r�\"\�O\�t\�(��<�F�y\�\"\'�\Zː\�\�;\�w�\�X2:����\�\�\�|3��\�߽�pGma�6H8WY!Kbs\�C�eD�\�:7Hyh�\�!���^Ǻ�v\�\�\��\�\�mO�\�\�g\�#�@��\�4e\�\�\��+�\�\�-����3\�A�!s�5wW\��*�\�\�BǇt&\�yj�G��1I-�g\0��`zӕԠ�\r&9.z\��؏�ቩC�f\�\�\���\�!W\�7�C��\�% \�\�$��\�OhsO��~SǼ����&�\�҉к�^�m����Ҍ�C~<\�7Ѽ�mOB\�\�r��\�\�ټְ_�]I�|��\�E0�r)\r���m {^<�h~5�\rU�;0�\�\\\�!�ьXx�=S\�Vْ�f��(\�Ώ�OC�\�h�#F\'\�U#\�\���Q�^�����(�\�\�\�v�Mw\�H�R��\�\'\�\�#�v\'�\�8�&�ӑ��Et�Q\��u$	]P�`ĦX\�+ꝋ\��\�\�\\�m��}��5�\�\�?!I��\�f.�\�|�\\3���8���شp�H�\�\�:=i�\�	J¥�/����S+.���\�\�DT��1]����:�$v�w�G\�\�0h�\�\�-/X\��h�I�/��`,���\�p0X[7�>6�f�?z\�c\�\�{\�x&�\n�\�l�\n�\�I�0M\rbf��\�\���h\��u\�\�|�\�U[1���\�}f�Y\�*\�4�B\�H����W[�\�n�x\��Tu�\�Yԩ\�=���o\���񠹖s�\�X$��Z\�P\"$���LF�z7\�V7�qw1��\�\�#;=\n`^ay)�\����:�\�{_d^\�BO��%\�%��\�>�Ŵ+�ߣ�)\�Sr\���\�5\�$mkz\Z\�\�jnL\�g?+���\�C��J_\�;\�k����\��\�6��\�u�׎\�6�\�x�\\\�oBM��+�8\�a�ڒ�\�co%\Z;�s�Yo[�W�d�\Z/oĺ]\�4w*���\�S�\�2d�ΏD8�]|�\�r��r�\����Ʊx\�5�\�N�Y�Hoʱ\�\�2De\"uur��\�=��q�Y/\\�ĕ\��<\�q���\�\��\�O$�[JX\'\�\�\�\r\�\�\�{j9Q����7*bR`��4Z\�Ͼ�-p9M�\��Q�\rF7,\�K/�o\�:]�t\�5�ШP��|1���Ʈ%*\�+=\���y}Ѥ���3 �跓ؠ��\�IQi	��Dq�(i��v�\��o�Ə!؃>���\�b�͸\�$F;�i�\�Cο�b1ڑ4�\�t\�.\�3L�$	\�Ĉ��hk\�\�\"��t������5�\�,	SPYe\�]ȴ\r\�����?}\Z\Z\�5\Z\�W�QQ\�qd�dY�#һ`M1�qwGjωȬ@:\\p�\"�m	U1\�\��OTN�/K�\�+\�أR%z]��\�l\�ؔ�Rڿ\�\�\�K\��4\�\�\��T\�\�+l�=��U��j���v�\rĺZēl�\�=��HJmC�秫9E뎶9D�7��WB��g\0��M/{�\� \�\��\�b���-\"�\�3�Z����40\�\�@��Tq�]��\�\\�o�]�Nis\�O\�\�\�&+�.\�%\�\0�՗Ew�\0i4i\�\\�\�\0=\�&e��\�\�)3��\�?�\�㬺�\�\0j��\�pU.ߗ�SH|���wAѓpJ\�\�Il�\�/K �\�O\�L��8���S���.��\n��\�\�F\�d\�q�P\�\�,{\�\\��\�\�*\�j�qykbV�wOdn�\�d\�\�\�{�\�\0mI��\�+�kB��Awn*\r\��\�j�Nf�ɉ��4����y�\���I\0P\�^\��-�ϻ;\r�f�����\'ҳ!�\\p���\��\�ۃ;d5��2	�#}�z�\�y�^\�.�Hm�nI*D6$@\Z�\�Vٯ���\�*\\�쓺[\"\�m\�e\�\�IK�\�9ف�u��u3��	�� ������ɑ��7�\��\�IH:M��b��)�\�{��^�Wx\�p�=JH+\�z\�\��\"J_\�/\�\�~�@�j}\��0Z\�\�\�I�ͩԓ���o�\�G�t\��\���:�\�\��RPK\�\�;溋~�\�sf��Ϊ羙�Q\�\�Q�DU���h\�\�sn��9\�3f|��*wZ\�E�2�3OCG_\�07��Sd4\�K\�c#7�� \�1��r�\�\�3j�Xu�\��8w��]��ki02WI\�\�xtul\�\�\�|6mƴ�O8��E�\'D�W6�w\Zo\"YÞ��\���\�ZW|Q\ru\\)-\�8�[�\�rҪs� gbU&wD\�1V�/�H���j\�g��X�2��~�4��8\�44-k\�\�\�\�\�0\�[e\�إ\�̜\�\�kř�|/V\�	-�ms\�\�\�Vy�Z\\g\�5�?ԓ�L��\�\�R��u�\�fΛ$�=�\"\"\�6�\r�-�e}H|:d\0��\�r��1jp��y��\��v�>���h�\ZA�MR�Z\�\�\�V N~S}f���\�W���EE|Ge.\�1\��i|\�\�G�\�*�zR�A\Z(Q� \�6�\\S-t4{�x\�^A\r6K\�\�v�\�p\"\�9�{��L�G��\�8E\�\�ߜW8����\�\�I&%מ<\�]w\'G�\�N$R�\�X�\�\��3�#��agD\�\�\�\n\�}̻�\rԠ;1��\�HC20�I��\"M	BC�I�G\�[���W+D$86掖�W\r\�\\P��XPvq\�8��*\�w[bp�E\�\�\\M\�����6\����#@X�ka\0Ҽ	s\��m[7��]\�\�\�t���\�\�\rK\'L*\Z�\�[E��Ϊ�Ιh�Y)J3�=	��ҫa\�,q&\�\�uG5\����$����b:V/� �\'GX\�\��\�q��\�\�sUlG� !\��n>m�\�\�EC\�{�n\�§�\�\0���=��?:<\�]\�ڙ�u]/\�5\�j�\�t[\�\�\�\��.�\�k�,Z�\'��\�E8S\�@}\�u\��\�撮�|԰2\�E�X�q4y�Y���iCq\'�5�Y[\��l�`h\Z�����!k�\�<=��4y蜄ԧ��O�W#�\�Aۉ%3�Ɲ�\�\�b:Z�͗(ě�\�?����T���{\�рa�\�ڲ�o\�\�\�\�K|^9F[5bt\Z\�p \Z5�\�[�C�\���\�Q\�Ռ;TD�\'Qn�n��>\�O9BZb_IW~�y\�i��!\'vb�\�\�\�J�\0)N���\n�%Z�Ğ%�[��\�4\�\�.6�+��$\�w\�\�QM�4\�k3q\�j^;�:?_�������\�\�[\',\�\�JX��|�=y\�|\�N1;N卭y��\��N@PL�P.�bz{�\��e�e�/�\�{i2(�Dڽ�4\�x|\�\�Sk��l\n�\�e�\n���\�\�8l&�њ\�O��\�Jp��\�޴T� \�L��DEͅR\�\�\�k\�k\�[!��\\S�\�\'E{�	�\�F!��\�0�wwam�!3\�\�&�\�E�!�H����49ߗ=twg�7_\Z,5m���#��4\�>\�q�\�vS\�x@�GV\�m���^d�;+�B\r\"F���\�ѱ��F&{���~1�Z}?�f2՝[���U�͖\�r\�f�B�\�x>\�\\\�v׍�FT�)\�,�C$alk%DD\�Z\��\�r\�I�C��=ԼBң\"T���\��\�7(+}�ܧ���IWt\���\�˨�\�$\�f��\�)�Ԗ\�\�\�.^�2XN\�\���&��~\�Q�CZ�\"0�\�hڭ$�{(�X��\�E\�\��7\�|�pQu�\�\�t�Q\��\�Z��媦\�iċ�\�N�m�34r��\�\�\Z\n�<�G>�K\�~7H\����|*͢_2�,{\�c\�\�?\��������\�K�b����F\�S/�<\��\������)�I�\�E!�\�=���	��\�s��=\�\�xq�Ӣ\�C\�\�`�h��Cܶ�ʹR��\��ӳ��NN�\�Q�t�\�w1�\��\"\�-.\��`צ �7l�.�\�Ҏ\�G3��e\�S��o^>o[\�b@\�6~\�dB�����\�\��n��]*�v�.\�\�\n5ku*�m�\�;�\�p�\�>\0\'Dv�1��(M���\�\\��χ)M:N�k�E�\�.��\�\��ߎ�w\�\�#V\���\Z�\���\�-�M��Ρ	*�e�\�?\�B6\�Q��� $�z�Cd\r�E�=S�����w\�f8\�\�G�\����X�+�\�-Ǌ��w��v�\�\���P�\�G���\�\�\�_0\�\�7�F�\0\��\�o�X�?ѹu\�:����\�\��\0M\�\�6��\��F���\�\�,I`�,\�0#;;�\�g��l,k>��\�c\�x\�T�35k)��\��y��G�\��\'`I]Ь9zOg�D�����\�0DY��Qč�uQ�}�I�\\{0Xq\�K��ƿA�|1�M��q8\�d��\�\\�^\�\'e���\���\��\�-\���l��	�]��0^�\�\�~�ڜ^U��\�-��\�d2D�\0F�������\�Lm\�gu��z\�mS��T�c�Et\rpZ���xV�\�n�\�j�6R�\�\�\�\�\���Eh[0&at^T�{Bp\�~�\�7n���\�\�IJ&�yy\�Ͷ\�\�%Y�\"�\�8�Խ\��Yȵ)�s+��\05��+b��5\\3\�W\�\�\���K�R�����7�)p�\�Y\��Ǝ	�F���_\�̗W�v��)�4�\�t)�a��\�\�\�r[�,��\n:\�Ȗ=�\�3�\�\��\�\�O��B�ᦧ�w���N�t�z�+\�\�ʳ�5\�\�U���ƂyX\'�\�~\�\�k+�d�\�P�b\�a�S,��\���\\W\�לx��]�7�ʶJJ��\�+2F\�\�,=kF.s�\�p��\�\�+N\��,Z�2~\�z~�0DF�&��<_��KtW��Ǚ(���\�\�\�\�D\�	\�`Ga@NnH.4��x��S\�\'R�\��m�{�0u�5�R�_;B\�4���r�I\�\�;�\�\�\�S�\'��e �k\�\�[��HҧszL�E`�㮧�8�J���4ˢ#�\�&AUZ���e��}\��v�\�\�T�5?},Fn�V@A�\�z>\�Q\����\��2^�nj�a��\�F�\�p\�iʡ\�\�φ���M�~��\'spzQ\�\�^L�@,\�7l!�[>�A����\��ᇤ|\�eORR7�����\�[W��F�D�M%b\�e\�;S��ho-�y#�\�}6\0��\"%�}=�Oˑjh�h\�1\0^>�A�)@\�\�j]�0��\�0��k �;\0h�ZM\�<���\�\�<�\�!=���>�\�\�~`�3}I\�\�\�\�\�\�J�v1]��Ϗ\�\�71��n./\Z�\r��T-�&����bs\�j��xsQ�\�ʞ���i\�\�`\ZO~Gs�\�\��\��\�Hc\�\�.���\�\�o�\�\�qJ<`\�t\�L�Hn=p\�mݻ�L\�w,\�?aZj\�N<lZlw\�k\�L��N\�PԢ\�5at\rݭ�\�G\�AP��B�`׃Q~ʝ�f\�\�\�@\�u[]\�شS\�(\�Oo�z=Y^?q)\�#�o߉qx�2\�\r;״b+q5�^�\0�牷A�@(199�\�m�$�\�\�nϦ;�\��cE�\�k\\�\�\�)�~Jx\�L��u���\�xJA�\�\��7TѪW㡤^VS\��L�\�*�P}�\�\�\�\�\�|ێ�\�\��\�n�IVpW�\n�LQ�]\�9G�\n�G�Q-�\�{\�\���\�F�\�C\�\�\�\�4�\�M\�\�*ߊ̖�\�!? ���\�~Pw\�\�q�\�,-\Z+�-k`��\�2\�1�_Ƚ�i\�\���\�Ŵ\Z��\�|Ë�\�\�z�@��Z0-�\�[\�,ʽ\"S��=�+\��ؽ�Ny1=YM��|��y��\\J/��j�ߦEc7R\�?�\�\\\�\��\�tzQ0��M̼|\�U�~c(\nI\'v���\�dJ��\�\��e6NTO�Ĭ͔�?0a�<�r�\�\�R�c.\�h�\�|\�d����*��}\�ŉ�ݫB-=\�\�#U^s]����q>�\�@\�)f�\�!ߡ]��55|N{IKk<a>`�aĿ\�2G\�v,s�\���̿|����T\0�o���Q�e����\�\"e\'�\'M�.r6x�\�+b<�]\�\�@:�5hA4\�H�3X\�c\�wW\�E9�=�֘x\�\�\rs\��\n����\�2�:z�9\n�7F\�p\�k�&,��oD:B\�D�\�U\�\������\�\�͗\�{a[wz!\�1wݬɑ\�.\��~[\���[�6\�;G\�8]<�1��H�;�\�!��f^��:=\�e\�7m�yL2\�ż\�\�V�A�/�u$�\�\�\�ijf۠IF]\�\�}b�\�P�쬚Aqҡ\�m?S�,�6\�LUq\�\�]?M\�r!�F\"�3�\�^.X���\�n\�i�;����EL�c�N��_H,=��hc\�/\�3�3N�x7\�y�Q�\�\��B�2�\�4`�dšy#\�\rBݓ���\r��u��+�:\�I�\�P\�)�!ڀ˒�(�.�\�\'\�16H\�W�����\nc)�\�&\�@\�\�Ծ{_�\�Ѕ\�=�ܳ�d\r-!\ZW�D���k��\�0�1��Q�B;\�ߜ�\nN$m�/��>z�R��HRAԼ\�i���#ԙ�f�\�\�\�X�iA ���K\�~u2��\�\�86WigiCn\�GoQK�[7H���{\0\�\�#�%�˞���<\�!�\�\�M\\#U�KR}3��٢2y�\�o���\Z4\�]�2c|��b��Y\�\�\�b��*�Q�\\�(����Z�o\� 3\�%��(�֧\�c�\�\��Q�YT�\�P��\�D\�z\�\�Hh�#K��� �O|�x�\�@5\�j\��،���@\�\�\"�eB\�\��\�F���,\�ޝF��u\�\�\�\�\�?�4JYQ�\��ojW�\�\�r�\��`�k�D�и��U�K\Zz�ҽhY6	L\n+C�!�%�\�\��V�\\Pro\���b�(\'j\�\�%\�\�N�M��5sߗ\�5����󊊍d\'�\�_y�\�&8�Ր�d\r*L\��\�\�\����:g74�^ן\�C�4ƀG\�A\�8\�e0�\�v�m0��nV�1gy���&}${��>ɜ�ׁ0��\�9����\�r�\�˂��\�\�k|q�\�@�>�\���2Պ�U�\�:Q��e�	̂s<q��o�GP\n��\�#v��(\�(�\����\�Mb�I�\�\�2o�+�\�2]�����7v\Z\r^O\��e:���\�\�]�,�{������)\�#K*0dt\�����\��S(��Ȱ\�9\���x9[O{�\�ܸ�G|Y�\�\��\�\�G\�z�A�?��~\���\�ߔ\�\�\�팀f9�q\�\�\�\�r\�3�\�4\n\��u5�%`�]\�nƗ݋�z�j@i~�R��6��Є�@\��\"\���R~���\�\�����\�K�$��\�܃v\\8\�>��\�\���\Zy\'���Iwu2Ḑ�kR\�\�g\�\�\��\�\���Y��Ǒ�䬕\�\�T~2�\�7;ފ���\0',1478565071,NULL),(11,'.AusDto0VXGf','phabricator:celerity:/res/defaultX/phabricator/1fe2510c/rsrc/js/core/Fav-_OyDN346AtZK','raw','JX.install(\'Favicon\',{statics:{_favicon:null,setFavicon:function(favicon){var\nself=JX.Favicon;self._favicon=favicon;self._update();},_update:function(){var\nself=JX.Favicon;var\ncur_favicon=JX.$(\'favicon\');if(self._favicon===null){self._favicon=cur_favicon.href;}var\nnew_favicon=JX.$N(\'link\',{href:self._favicon,id:\'favicon\',rel:\'shortcut icon\',});JX.DOM.replace(cur_favicon,new_favicon);}}});',1478565099,NULL),(12,'wAoGIn_B9hBe','phabricator:celerity:/res/defaultX/phabricator/634399e9/differential.pkg-pNxV2IffL7Gs','deflate','\�}ks\�H�\���\�̬@�@Z����#\�\�\�1�t۾�=;\Z�\"@	�вF\��|\�\0I��n#\�D\�#�*++3+++\��?�yٴIQ��w\�\�e�/���_\�\�\��2}QW��\"��*�$\r��E�\����-\�\�ͫr\�&�Uֆ�r��\�o�\�͛*\�TF�\�^\�\������\�曬h2Io�\��(���m3;\�<�!�\�K\�K\�\�K\�}\�\�E�:�\�\�Κ�\�d�P\�U�G0�6_4���y�^���\�R3��\�\�u]\�\�eZ\�Mq\�?\�\rt\'ʛwI\�fO���n�\�ˬ�iسr]�T~�٪��Gy�L.��\0R�&k_�$\�\�eU\n��F��s\�\��W�\�U_G�p�^�I����\�\niV�	\�)נ\�\�q||xHY\�Ko��su���\�\�JЗ4�\�0�o,@G1�������>\�0�08l��\�GC�0~ �ک�~~�\�&�8\��#��\�u^�\�CZ!f\�:��Ms�\Z@s>����\Z��\r5y)\�\�\�\�\�\�7������X�\�&�\�t	�E(�t3��\Z�=ـi~�q�\Z>d\�X/\�pΩ֌�\'vA�c8�>��k\�rV��~d��\'!ΰ�	\�B�*p\���)\�\�]\�p�&5�������\�8\�\�Ȓ\�\�\��m;�7ux\�\�q��7M\"��Kr���8�B[ɇ:)�eVOS/�\�l\�\���Z\�sMkv\'�\�o\�@DpKX;M��OTd��\�1V\���x�\�\�R\�\�ʫ�\Z���{MV��d����S��<?\�\�j\�R	\r�b\�\�W�\�J+\'\nh=q~��\��py��L���ѧ[��v\�C*q�a\��Z\�ʮQ�B\�P\�}[\'���U�\�S�B�W\�\�EoE�BOE��ފ���z\Z=\��*�V�v)VS_\������\�\n\�\�,|�ÅS����:\��.��\"a�}�S\\m���uB�\Z\�\0ӟ�\n`jA8m+�`^^�yA��J�G\Z0y�\�6&\�_\�^Aw�SAH��R�\�i$\��+�j�\�g\�Y\0\�N\��U\�AF��k�\�\�*[Ĕ\r\�z\�\�\��0gZ&��i�3\�I/��\�Nl^�%�U�:Ӛ\� B\�NL\r��P\�\�\r\�2��\�%2CY0{\r\�%&������c�\�\�\�t�>T��\�P�M���o{\r*�u\Z���\�\�uy�A%\�t�\�\�C\�$�\��;]@K/\�zT\�\��uBw{\0@*�y4+|vE�\���a<\�A�_7\�@��c���!�`\�\nnM\�.\�ukY̅>��[\�F��:�\�\�%urk5� ���Ϫ�Y\�\�X��jMʴ\�걳lE\�\�\�skt\�#���*�\�\�\�?x\�9�\0�\�\�XMv9\��g�GL\nRD�/)S̞Q#!\�\�yv���~�\��\�C��\��+\�\�W\�(�\���\�\�\0\�\�u����5\�Q��&�\�C����f\�!ȸ�\�?��5!\ZR��y�I�\�)b��*�V�>�\��2\�?[�:XL�TD\���D\�E(��\'�@���k\�,���lHVP\�Ư�[���\rr�\��\�\�\�p�3�F(x�ͮ��\�\�(tE~F\�j\�\�Ǌ\n����:\�5\\aEi1\�hE�SEWF\�\�S\��b\���Β�c���*-�]&�}u\�k�\��z��IgHT\'\�Ͼ�N^\\�\�^�y�\�\Zp��:�Q\"�o2���\\>\ZTD6��j���$�Z(\�H\r��p�e\�v��Y\�ݍ2>\rlpT�`9I�3Y\��S\�O9qbZ�\�o5\�w��1�	�}\�W��fd\r�zpT�\�+��EPG��S�\�ʴa\�\�ȹ,�K�\\�\��{6�\�њחeʭ��Bz#Ii\��\��V\���ʒ\�E\"3+�\�,�=��\0s�Ny\\v?e@@�O\�D���\�-���\�O�5��zJ�n[��p�\\e��T_¯ZO�ʪG�i����mh\n��֑�V��(\\\��;\�\�)h���VpI@F�݈\��9���A\�u{\�HTěV%jЍ�AY�;\�.5�U\�6�e���ឫ\�\�\�<\�WS\�p{l�7\�9�cZT	\���\�\�+��D`+iv\�F�\�\�R�i�\�u\��\"�\�V�[}\�5A\�\�U�M�x�?x�՟�z\�e�p��N%XlTmOll\" �UV�y\�\�pAF�P��\�\�\"i\ZN3Z;/���\�B\����:Yej\��dK\��yF;\�ux\�6Jޫ��UOU�gn,er*diT)u8c���Z\�;\�\�>\�Yf���I�ΡuX�\�\�H��\�.�(����\�fպ\���%Z��z�Oqw�\�\��K=\�\��ʂ�\�\�y��\"\��}\�\���;���BlR0\�\�\"}o\�2d3ç\�\�[{L�i�;\��%��\���Aj��\����\�r�6��@\�O�m�+m�\�G\�0,\�\��2ma\�\�D\�t�\�0:KΪ\�*\�5�\r�2/3�C��4��D{�5b��)\�\�9�Ñs�J�\�\�7��b�>���U��ޯ22�.[\�\�\�꼪��~�9&����l�\�]\�\�4�]gc\�\�E�f���b�\�D\"7+\�\�9�ID\�1\�\�;�S[��o��,F\�\�\�\��Ac��jm\�A�K׊\�f9>���ϳ�c���	��Ҏ�-_f\�\�\�%I�/9���,�,\��4Y\����\�0\�e~Żۄ�Ȝ$\�SN�\�WU�\�`\�|I\��v��8n���Ü�HikL�OR�SEշ\�X���;\�C9�m\�Wfj\r�e�)�q%Rl�d���7׆�jP�`u\�\\+у\�DE)q0�\��HH��@\��@&~\��\�\�\�\�BO�*�\�\�O���\�F��(G�LXא�}>���>?��tg��\�_[�J#\�AHh&����b�E���\�M�=\�c!\r�\�&�o\�C\��6NHD�\��S���cL$��Q[�\r�=�8EG�/�u\�Hz\�#�ժ\�\�;�\�D(�\�J���Ҍ�(X\\\'\�U\�ub\nѥ\�����\��C\�sڹ�Ζ�>�*��w�߷\�0\�r*��� \�8돓\�:\�ߓ\�\"P9�)g�\�&�&�\�c�c\�p�\�\ZfX4V\�Z\��j@AB\�NV�9Cw�\��v�$잗\��\�c�T��\�\�������� ��A��\�S\�,\�\�����\�3#�B\�)\0.1X;�\�q~UV5l\�6�M\�k\�h!�R\�\�ꣳ�c\�- ���fw\�}�<\�*w�4o�&�\�\�dl�Q�g\�<�\�A�%By��?\�\n�kx~3sےr\�(ҋEVM|v>�����R��Q�FE\�r\�\�\�uUY_�\��\0����ۣ�_�������\'\�\���C\��y~��݃g\"Y]���\�gZ�\�Z\�n��Nz�NF��iU^\0��\�\�Ԕ>f�Qd˖�\0K�\�\�͛�l9\�o�Y�6P\Z\�2�����p��?s�Zq\\�V�\�,,G�;��\��\�*��Gn/\��\�\�V:GiWz-�X\�\�\�4Y��ӝ�w�KSm��yl-���;I�\��\�\��w�\�C��\�U�s�\�s�|�\�\�/(��\�*m+�(h�؎q1-�/\���\�\��\�\�q!�j;��\�\�d�\�q\����=Cߋ-�؛�\�\�h���\�ή\0�\�uU@�D\�l�\��V*\��W!Fe\'\���KD�\�{��\�oI\�\�M7	\�����\�9tsz�/�%i\�\�\�\��qGW5�@#=Ri��\�\�L��*-uu�\�l;R�8\���[(0%�J\�\�R\rrP��9\�\�?���!�\�̌X�b�L\��@�\�պ�\�\n}\�\n:�9D�&F\�\�Yz�m��%{\�uJ [\�R�$�l�ٍ5��Hf�e\�SRC�\�\�\�H�\�\�=`D#\�C簓\�(\Z2\�I[{\�\nR``�qS�\�U��B���3�\�âwg�-R\�%I,�\r��\�6���X$8=\�\�/P��@��l,����߼y[��\�b\��]����§˼nZb�1�+[D���k\��\�FS�\Z{m?>*9�lXF�����7woH��\�kqJCP`j�\"�\�<�ޒ\"�\�N����}s�\�~�z�����Q�\�=�u��\�*\�-�=�<�]\"���>\Z�Ug�t��Q ��\�k\�u�H`�.�O��\�M\�۩�!�\0DAr5�r\��\���\��\�E\�y|t0\�z�\�q�\��q���h\�Z;L\�}�]e0(\�Tp�n\�ٶ}�l�M�\�0^\�Jƕ\�\�&~\�A�Ȗ��\'��\��P1\�K��AC\����\�G\r9��\�\�\"%m\�k=�su�1y����^�@_E*\���Z/�\�ݲ����!%k���\�b�\�\�\�uf��ı��\�z�KA\r¨/=�\�2S1\����^��eugu\�?�ܟ��d�\����x\�\�O\�(\�\�t\�X\�u��:yQ6Yͻ�\�@�mZjV\�\�j���b*$*�s��0B�\�I��܀SP�\�S5I�\��9)�9Ъ$\��o�*��!�]	��c�\�sX��I\r}\�T▟}��s�^�55Q\�3�\\N -b\�\�e\�\�\�HCA��\'�cnM\�\�&t	䂌B[9�Sn<��L�X���M�Ǫ,o4O`%\�k\�\�\�\�\�К\�[}\�w\�\���ŋkv\�3��ZE�u���\�\���\�\�}Kw���r��\"=\'\"w|6���1V��kT�\�\�Ug^\' D��b,�#��ԁK\�\�`״7���\�ܳ�ucY�,��F\�w쀥�c::\�s� JE}<�Y\�3g\Z\� n\�L(`.\���\�b\�D3�K\�	�\�C\�;)jw�̞�A�\�Qq\�IZ$M�:���\�Z\\��{\�;�ks[tU+X$\�*\�l)\�v	r�W\�lqsY}Q��Dc>D^�\�\��\�1��W�����\�T�UÈT?\�x�a/)Ā%_bU\\�\��Lf;56J�x��A0�\� r�ި}�5�\��\�d�m\�\�ݐ7�\�8P��\�}kܾ�\�m����C�K6Ea�ģ�I��m\�D�e�sD�c�\��:t\Z�y\�\��̒vh�\�:�\��`��\�4�Fah\�\�E\�M0;�����L���\�dv�	�\�1;\�.){s��ju��\'�\'�i(؄�\�Ɂ�\�v�Jw��υ{\�O;��\�ՐƠ\��\��\n�s���%k�Ð\Z�\�@ug\�\�\�x\�m\�H��d4\�\nB̊��1\�?p\�\�1ѻ�\��t�~ST-�|�G�w��l��\���\�fە����\n�J\�c\�^���T�\�\�<||\�\�\�?g�gmu\0����3e�Mb%:�tĜ��gC�{2\�U�8\"����#Ra|��\n�BM 0Z2�\�9uOz\0\��Ü}���Q�63��<\�tn}$�)b\��\�ڗ�iW��*�-��1	\�c��\�,�\�mwI��a��W\�G\�C�\�V\�s�1\�U\�T�S�?�R�>S*���͕��z�O\�n\�a��T\�tVj�\Z�\�\�U\0)\"�hA!^����\�_c\'Mm=�]U�uAɝ�َ}[��l5\�Ջ�\�(KZ�St\Z2u\�$��Mb�]֞�ۊo~B��L~̗y��K�\��\��G\�jQ9\��mO�\�f;lu|�\�;$��cUҶ�J��j84�\�w�5\�nH\�3\�(�`�jWC��㫇\�/\�\��)L!�Mn�z�\�5\�\�\�H��H�����q�\�r#�\�N8\'��{\�9��\�\�T�LY\�\�vu]�D\�R\�\0�/H�p\�\�iLM=c*�ܺ\�v�\�:�XT�1Xų=M\"\�\�UU���#l��\�$\�ʎ<\\�S�\�6lm\�E-�\�Jn%\�We�\'�4̔\��S�8�.O\�\��\�\�\�de\�\�\�\��\�\�\�!K��߇��\���\�,�\�0ď\Z⛪\��\�n�o�/�d^\�\��	�;-~e���?P\�<�f\0\�n�&�����Pl	�`Ψ�\����\�\�Q\ZNq\��\�T\�\�Ȁ\0%#%��zDSa��\�&:�>\�4ԞI?�cϞ�(;�\�.���:I�\�[ioY�8\'(]�2x*���\�M�\�X�&#;e׀D\�κr�,O�A!\�+}�I�\�uzr~�CN��\�,m.��\�%���.rw\�ɛ�Ӽ\�0^���(\�\���1\n�?���\�c��rPL�EMr�*��ȏ`�s\�ْ	\�Sqs&\�;X,0��KG\�Xןd�.���j\�H\'�Jm@\n\�\���\�Ƒ\�UM�\��ͧ8��6��~g\�(\�l4���Ie�\Z|\�:r\��\�~�k\�\�N?U�N�fפV�\�Y5+2Ռ{a9d�sχj\���D���w-[��Ad\��\�$ܮ4��A�\�9:	w�f��B7Y\�L�\�d�r�L\�\�\�6_\��\�r�0\�V��ժ�ݜ���9��-�f޹��Ζ\�\��r����S\r�N����\"~\n\�\�L��%�\�D�\�L��T3\�b�\�%�I\��O\�\��s\�\�s	��?\�=U����\�\�[\nzL�sg�\�ݭ�U��Mn-\�=��~_(֭\�\�.�(�b\�`j�6-\�c�uZ\�n¢�\�̈́���>�\�e�hvԛ\�bMJȽJ\�b\�K��u\�Tu<9q7\�\���ʋ\�\�q\�5Yi\�\�2��ź�\�^l,�o�q�CՖQ�M�\�bS\0H\�\�*�\�i�w܋~(�\�M3\�a��.x\�\��;�\�.	@|v\�S��t\�{mZ~��3\��\�\�\�.�\�\�\��\Zu5%&C�%�<�O\\��R���!�Q��Zp�,D�a�\�&(\��`�\��E�6@��s�bk�\���`x0x�X`�W�\�:eb�?��\��f�Ѳt�\�\�!���@:\��P0aiH񡑺1�z[\0�?��v�\�\�\�y2GP�{J=��Ňł$�sh\�ޅ�]�\��I�8���*Ri��qkAr��\�\�\�\�\\�)W�$az\�ƃ�B�F�t\�V<}�\�q���1+���\�\�D(nt\�\�~�\�:\�,i9��3��RJ,d,\�\�68^\'\��[Z㖓g��\�\�c��)X\�J���N�VXZF\�\�]��\�)�K\�ڈ=<T\�b\�\�4\�g\�>й\�\�\�\��L�\�\���χ��I�ޖ~\�\�iC�\�!��׼\�\0u]���\�#VTַ\�j��\�ņ�v�\0\n���&�Q�vJZ\�^��\��F�ɕ\�\��lM��]�\�,�\�Þ2�%벽�E\�\�xn\��\�\��`\�\�oP��\�\���M\�\�fT\�\�F\'�d\�?�]g>\�\�[�GyY\�ư|B> ��\�Dy]\�\�bݎ�O 	_��\�j�\��Hn\��)^�J��H�?�\�\r�\ZH&\0\�%6wc5�\�__\�\�d�6_�C��O`\�be\�6�\�7Χ�;yrå?X^U#up�\�a�=��X����tb�5�h�^�\�و��Q�\��\�fgx�����\�;���\�\�5t\��%�zR��/+h�z�S<J�fU$��q�H#\�A8\�K�k�\�\�\�AW\�5��)\�e���\�oХ7>b��\n9MT+㺇��/V��Ꜳ��\�нYt	{n�an��\�ص�M	�5�V\�m�^\�����-�a��G\'䤃���\�\�JP�S�\���ڗ&q\�\�i\�w\�9�]ȗ�\�{\�ӿp�\�/\n݉���Q���\Zݒ�\�3j7\�	����kX��\rMf\'��<�����m�}OS��\��}#E[A�Ū�\�ջ�z\�+\�g�\��m�B�\�j;5�c��vd?�`MiW\�\�#+��	4�!�\���_\�\���\�dn\�><���|3��\�˃>\��\�_2Z�\�jnB\�l\�]\�\�\�\�\�̤�8n&u\n\�\�G,�\\}D/;2EغrHa[��Sʧk��Dآ# �1�!���m~�\�\���C;9��j\�v<�5C{��qp\�kZ�c�*p\�0�		��.(�\�\��\�1��\�d;��M�ccX�)4��vb���Y:\����J�T%��\�?gx\�4�\�ʯqiĂ5�k\�\�	\�g5\Z�k\�1��K6\�P�g�Ȇ\�e\0;�\�Gd�{�0���m��Ƶ\��-�\��\�>�|��JN�\�\�ƪ�7\�<(0��tt=�)\�\r1�7Ur�^#ե�P��-[��m�(\�\�H\�	\�f\�zv8:�A\�y\�A��:#s��\�\�vD\"r�P\�d�c\�A�4W1���\�\ZB�\\%V�2D�4\�s;�L\�=�~��\�\����Ԍ�\�E�\�\�\0\�\\��\�½�#�i�V!��\�B�9�I�n/��+\���/�\��6>X$%_����\�b\'�\�M\�\�\�\�jMHs�{ݠ+\�ۻ�ǽ�\�\�0�Vf`hU\�\�+�#�;x��u�gD\�@6���s\�6\'�6\�66u\�\�-�.\�<�\�\�\"_H4�7\�m\��\�\\]��	�\�ֈ>�\�M�\�(8\n�\�$SV�\�^��7�-�}\�R��˫肋�3�\�(�A[f\�\�Vǲm\\\\Iy�u\�\�\�|la@\�a*�U_�k~t��̚/�c0-�\�ǵG\�mR��:����\��\�m0e�>\�3\����Z|\�l�x\n#�M�]�iO	\�Vx.�W�&|�lz\\�Rf΅\�\�\�x\n\"`\�U\�\Z\�\��Ew��p�\�uDM\�\�\�fqe|@n\�y�vև�\�\�_(\�\�ʪ5�\�_��X�\0��\�N܄2�x\� (;:Ξl\�f�?q\�\�\���\�\0�\��r.h%\"::*�Z��`tWX�3G��$\�C�\�ˢX\�VS2\�3��̆��MO�)�Y�>��j�\�\�m�-������p��Y\�V�t��e�cc���f\�7��tE䈀9#1\�=7\�\"\�dZ5䦻\�=d_\�f�w�W�o�ߪ�\�\Z\�o��\��=kYn�\�t�i�&��\�\�\��E�\�{�\0ϻ\���\�wJ�X\�0�oe\�\\7��\�]¹\�j�pzؑ��\"\�\�(�\�`��u��\Z��v?Y\�U\�\�jo\�\�Q�0E\�5΀:�o\"X�9Y\�\�P\�S�׆<s\0	�A��8#��]\�o\�}L��_\'����C���0[c���Y�4�q��B�\0g}y��\�\�ԛU ��F.&(���.�3����_�m�Va\�+W�C���	T\r*�\Z|%鹼D�V@���f�Q f\�\��*u\�\��\�\�)�\'�wR\�_E\n؀c�\�:\�\�F\�\�g\�\�q\�\�xB�KM:n��g���U\�V���|\�٬z\n\��,|j6bm:<<T.�Zw //5��\�7\�#��̺�&\�!\�\�\�I\'\0����p�s��3;I��vCq0��_�	�d#\���X�_\�\�ύ�\�tK0ˤK\�����R\��N\�\�)J\�㣤\�e�\�h>3Ƥ�\��O��#\Zk�����;\�0CT�\�k�\�:_\��\\$���;\�\0ߚ��[��\�?[��JHϼ\�v��\�m�&8o��֊&z���D\�J\�.�#	�wBmG\��\Z���𖡃�a�\\�9�y�+x�\�\�&�|�\��S�sg�iX��*䇍�0D�M�ȉ��L��>v:�\�\�-=.1��&T\���A#�\�\�qt�;\�\�}�\"hH\\*���M��YU����\�ۘC\�_*\�\�s:OY��Z�\�m�cf?(Թ>���\�\r\�ޠ�6W\rX7��^��5,_����%��\�)\���V\� Q-\�۟�K{LP�\'�|XA��\�^����uY�\�\�=Nh#\�#�ۿ�@mn\�7��1�S\���%(l\�tK	\�-�\�WO\"tY\�\���\�\�?\�\� x[Tz@#f�)Ŋ�,\���\�\�䙧�\n��jH�\�\�\��\��\0�\���߂\�{����/�n�)�\��4�zG��~\�\�䫷�n8=������\�b�\����\�NoC\�Ľk��\�Wg\�-3�˱�@9\�bU/�\�a�L��Fy��È\�C\�4+|D�FL���~���&q����\��%B\�5�\�δ�V�\�2�E�%\�\�\� É\�\r\"�;�$�R�0oC��Vhf�9�=Ͼ�\n� Qu�[�\�\�v��MwTn�ں���=:\�\�\�Cd\�q�W\�W���6m\�ԋ\�2�\r�0cvZ�O���F\�5�Q���@B�\��\�%9���RLu)!�{XΉ۫}�?)&i\�\�cfk��}/�\�A+F�0\�3ok�Ѳ#\'�mlHB�6ݫ\�]��v/�[CyJ��\�0s\�\�\�nl�@\�\�H?.\�\�\�˴_+q#�a�>6�^6\�Z*on\��~��q\�5D�)/%\�S\�\�hJ腻s|jی/\�\�^�dC�u=���py\�]ֳ�jF\�����94�V�ƚy�\�\�o�\���KW��Gxޤ�Hw\�I�UG\"\�K�$⩬|a\�#\�\�M$�+\�R�{\�	t}�N\�DW��G$\�P얣��ո�n,�5�\�ώ�si��FA�uِ����|�x$\�69\�G@O���!�\�H>\�x�6��\"\\E\r�ǊiƩV��v\�\�\�7���\ZN���\�\�=��ڄ�\0�\�\�Ǻ����o=uw�\�aͯ3�\'�\�`Y\�/�g\��8�\�Y��\r޶ҿv\Z��Y��$\�J\�ߗ\�5\�	|ω\�\�l�U炫�\�ahA\r/Unʻ]���U�J؋Ϗ;��(qؖ:�+O\�|c\��\�l\�~�#�$��^\�\�\�\\K{���U\�,@�\���(Ik�|沯פ\�\�\�?�\�1�d�vБ�^	9\�3\�^B�0Ț\�)wH�0 [U��\�6��QW�gXԇ{1ҵ�|\�U��/�/,���9J�d�$\�s�\�sn�G\�X���#�嫑���gt{�on�0�R��\�u\�)\�S�oXF�\n/T�i�P�w��L�Z��V��jmR\���I�)�\���\�\�澏�͎C�\�m�Q\�y�r�\�QĹd�}�m�}\��n�z|`�hGT1\\?��5<���l�r)\�m\rc��I\�v>\�\� \�eM�;\�K�m\�ˀ��\r�\�\�\�O?mM���j2+�5��\�׶T\�ޡQ�\n\�>��AԦz\�SZ5~j<lSr&?2�Z\r\�\��^3Ӊ��<\�<{���\�\�,��ú�&��\�\0�v�LV����\�\�\�\�\Zo�qC:�ǁ�bG�\�\\U����\�\�\�$O�Wy\�m��NG��N�/B\�F��~qY*����,�)~��\�(s,\����\�މ�;\�\�{��\�\\�K�\��Q��I$�\0\����ީ��-��ю~S�s��\��Z�I+�4��(#�|u\�r֫\�C�`�h��@��P̍\�5��\��s\�L|_\�ٯ��O6�\�Y��\�^!\�\�ᆊ\�P\��uÝm\r�m\��aO\�h�&\"h\�D�3 ?}��>}�y\n\�O;\�\�\�g�>�F�\�x�q��� \�Hm�{:\�\�\��	\�\�u\�4i|�\\	\�\�ggP�(\�4���\�C/xp�Ͳ$�3\�H��?q2\n+D��V�$�ԋ��\�N=O؞ua�\�\�\�YY7�˄���\����*\�U��~|�����=v	�5Œ\�	}�r��_8\��\�);�\Z�S\�AtD\'Mx\�T�i�\�}\�\���)f�B \��Č\�i�D\"�\�\�+e|(\�\�9�>��_�\\[\� ��[\�Y\�\�Pd�4Ӹ�#c4��\�+\��VV�C�\r�+\���v��\���m�P3O\�\�P�P\�\�\�~\�S/�W/\�\�Mv\nӡ+��g\�\����[{�\�\\\�a�\�\�S\�(�\'\�\�\�e���P�qh`\�\�\�/���T�7-�L��6�\\�\�)T��+V_7�k\�t)6���86�K��\�[$;N\�+\�CKB�g�\"6�\'��\�jX�@(yu&!T���l�a��j�Ho2f9֙��P7��\�i���W\�?�N\�\��q\�խ�gMV\�Y�L\�{sO��[�M]\r���\�(U\�qc�нQ\�`\���\�6\�\�\�C�v�\Z=o�G�n^\�T\�فT]s�\Z\��TW�=�\�\"\�[�I\r\�b�\�\�\�J\�\�\�n��<G\� \'T\��u9����~�\�k T�\�\"/�[�B&�R���Ω|�ޡ\�R��t\�u��V׹�5;\Zb\�2@S�\�\rQ\"�ou\�{5�B\�׊syX\�\n���\���\�ǥf\�\���j<��0/�q�8�ww;q)\�\�_��\��\�_�V\�G8�rˊ�T��\�\'��|�2wiG\�>��\�I����G5\\=\\$\�-t�;\�ԏe�\�3|:W?)Յ�k�<��2>mqy?��e�z�\��c��Ll\�b\�Trp��\�J\�\�\��:0Ă�G�?Я�.��-�=\�ȩE�\�	Y3��r\�m�Q�\Z�rBG����\�\��\�:��0�K����|\��@�Օ�`����^��\�K��z�b�\�P��\�d\�a\�L\�;�A�K�l\"h\�e\���#��AA�)]$\�d�v����s\������d���;CZ\��\�\�*;H��y�\�8�Z\�/(\n��_Oa�\�4\�z��\�\�����j\�P�P�\�Sc\r�\�\�<\�O�bg�&\"ИR�_,�\"�\�k;��ݢ:aV\0���V�zc�U\�w����\�Xh�\��Ȃ�8\�*�ِ\���}%3+Z�f\�V�|@\�\�}�\�S+Px\�Nb8�ˀ�\�6n\��.;�]��R6G\��\�C\�N�)M\�_�m>S�\�7�\�\'w�\�^��N��P^�WS<\�bCËBI\�H\�\�5�D\�/�1\��GÊ�{\�\�\�\��J��Gϼ�Ku\"��os\�\n\'wxt�\���˅0���c�\�\\\�\�\��G7>�|tn�ま4\�˦�\�rZN�k�D�\�)5d��L&\�\�{�2nu�`��O0���\0m\�c\�V?UwY}�4�\0E\�\�S�94\�>�\�2�~)\Z)Mo��E?<��ox\����wL@�0vΜ�3��b�\�n\�m+z\�X���0\�\r\�跾�>x�|\�\�\�\��m�3jP\�h\�xrssN1Է\��5l�\�\Z{go�.99Ԟ/ƻ����w\�/��-\��^F~�\�\�ް��ӣvB\��;?x:R��]�\�-^ݦ�7�R�4�\�\���/\��\�%=�\�!\r��O\�褫��8G\�\�?۝j\�J�\��~��Bn� �\�uŻ\'��\�pzK\�\�\�[ \��35�E��Ӈ�\�L�|p�/M�^vw���������\�է\�^���$�\�f\�*Yd�c��YmCĨ���7\�ru^}J�ڃs���L\��\�\���{����P�@>6\�\\}C�n�\�f\���\�Ou�L=b��,���\�,�H\�E���Ծ7\�+@��\�\�!:Y��9\�֜�}BTB��N=��h\�`\�iL)��Z\���\�q|J,\�b\�?�~�2��g)\'�����e0\�}\�u@W(3`#\�	\�N\�q����\�>�i���r�\�>�j͐�T�n\�N{\�t�\����\��^\�S��l\"/J�;\�\�	�\�Pk�\'�\�\�ƱC���!wLu\�/2c8\n���R\�\��[\��z���\�`@�Q\�M��\�\�mC��Q�\�\�\�q��!\�^3\�l�B1��\�uy{OC�r\�\�\�}r\�/\�\��\�0\�\r\�\n[\�@e7$�\�`\"?)�)|\��\n�b1�\�:3\���\�§7�>׏�]�c\�1ۜ�\�yz�\\ݚC|\�\�c�\��\�j�5%n\�}��(\0tr\".(\�\�޽\�%�1�^\����\�)\"~�U_���BXW����\�b�\�{c*C��¡ª} Ұ-R/\"�G��\�:k���\\EѮ՚\�\�b�\�%\��\�i6���W��<�ni-��<��Z?\�o[Ť@��\��+\�x��f��ε�\�r��> �����~\�\�\Z��>��\�1ɂC�h\'k���VA�=\�\�t�w�~\�^U��ΒM}��ਗ਼Z\�\��\�kXW�6�I�)o\\|�ר��v{\0y\r�\�L�Jy\�\'�\�Éz� �\\�\r�U�\�c1�|�SbO7\�1Ŧ�\�.ҋUE�\�\�_��\�\�\��P�W��k�\�lJ�[�\��I����\�}�?�\'G\�-���\0\n�\�҇\���1�	M�K|Eܔ8R��އ\�qMhvz?���\r\�\�\�U�f\�V�n\��۾r�y\Z�\rl\�\�\�@�\�?�sh���¦\�c\�q].A\�\�@�\�ߪ\��z�\�x@W\�\�\�|4(֭[�־\r\\j:\�N\'�|\�}\�2>m��\�A\�H\�ͽw\��X!\�Kd\�#O�l6�J�\�\�)\\{\n�]d�o�-0*\�\�\�-k�g\�\�G\�>�\�\�;dD��\�D\�:g7\�Ƭ�_� 9���J.\��I����\�\�_v��\rϻ[�����I\�U�m(WM�i��heV`[¯ګ<\r��\np\�/\�\�gt\�\�\�Z���2[EB\�\�#�\��e�}L�\�G@��\� ~\Z\��\0',1478565099,NULL),(13,'EtZaGHxuW_1a','phabricator:celerity:/res/defaultX/phabricator/5e2634b9/rsrc/js/applicat-XT0uT4Ci5.1Z','raw','JX.behavior(\'aphlict-status\',function(config){var\npht=JX.phtize(config.pht);function\nupdate(){var\nclient=JX.Aphlict.getInstance();if(!client){return;}var\nnode;try{node=JX.$(config.nodeID);}catch(ignored){return;}var\nstatus=client.getStatus();var\nicon=config.icon[status];var\nstatus_node=JX.$N(\'span\',{className:\'connection-status-text aphlict-connection-status-\'+status},pht(status));var\nicon_node=new\nJX.PHUIXIconView().setIcon(icon[\'icon\']).setColor(icon[\'color\']).getNode();var\ncontent=[icon_node,\' \',status_node];JX.DOM.setContent(node,content);}JX.Aphlict.listen(\'didChangeStatus\',update);update();});',1478565100,NULL);
/*!40000 ALTER TABLE `cache_general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_markupcache`
--

DROP TABLE IF EXISTS `cache_markupcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_markupcache` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `cacheData` longblob NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cacheKey` (`cacheKey`),
  KEY `dateCreated` (`dateCreated`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_markupcache`
--

LOCK TABLES `cache_markupcache` WRITE;
/*!40000 ALTER TABLE `cache_markupcache` DISABLE KEYS */;
INSERT INTO `cache_markupcache` VALUES (1,'DeGQA004TtV7:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:304:\"<p>Welcome to Phabricator, here are some links to get you started:</p>\n\n<ul class=\"remarkup-list\">\n<li class=\"remarkup-list-item\">1Z</li>\n<li class=\"remarkup-list-item\">2Z</li>\n<li class=\"remarkup-list-item\">3Z</li>\n<li class=\"remarkup-list-item\">4Z</li>\n<li class=\"remarkup-list-item\">5Z</li>\n</ul>\";}s:7:\"storage\";a:5:{s:3:\"1Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:82:\"<a href=\"/config/\" class=\"remarkup-link\" target=\"_blank\">Configure Phabricator</a>\";}s:3:\"2Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:78:\"<a href=\"/guides/\" class=\"remarkup-link\" target=\"_blank\">Quick Start Guide</a>\";}s:3:\"3Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:83:\"<a href=\"/diffusion/\" class=\"remarkup-link\" target=\"_blank\">Create a Repository</a>\";}s:3:\"4Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:86:\"<a href=\"/people/invite/send/\" class=\"remarkup-link\" target=\"_blank\">Invite People</a>\";}s:3:\"5Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:122:\"<a href=\"https://twitter.com/phabricator/\" class=\"remarkup-link\" target=\"_blank\" rel=\"noreferrer\">Follow us on Twitter</a>\";}}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565098,1478565098),(2,'fo7P4TuH2YZr:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:240:\"<div class=\"remarkup-note\"><span class=\"remarkup-note-word\">NOTE:</span> Any user who can browse to this install&#039;s login page will be able to register a Phabricator account. To restrict who can register an account, configure 1Z.</div>\";}s:7:\"storage\";a:1:{s:3:\"1Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:103:\"<a href=\"/config/edit/auth.email-domains/\" class=\"remarkup-link\" target=\"_blank\">auth.email-domains</a>\";}}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565129,1478565129),(3,'rVwVLSGl.Y9K:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:240:\"<div class=\"remarkup-warning\">Examine the table below for information on how password hashes will be stored in the database.</div>\n\n\n\n<div class=\"remarkup-note\">You can select a minimum password length by setting 1Z in configuration.</div>\";}s:7:\"storage\";a:1:{s:3:\"1Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:68:\"<tt class=\"remarkup-monospaced\">account.minimum-password-length</tt>\";}}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565129,1478565129),(4,'nHato9WQIj8d:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:3488:\"<p>When a user types their LDAP username and password into Phabricator, Phabricator can either bind to LDAP with those credentials directly (which is simpler, but not as powerful) or bind to LDAP with anonymous credentials, then search for record matching the supplied credentials (which is more complicated, but more powerful).</p>\n\n<p>For many installs, direct binding is sufficient. However, you may want to search first if:</p>\n\n<ul class=\"remarkup-list\">\n<li class=\"remarkup-list-item\">You want users to be able to login with either their username     or their email address.</li>\n<li class=\"remarkup-list-item\">The login/username is not part of the distinguished name in     your LDAP records.</li>\n<li class=\"remarkup-list-item\">You want to restrict logins to a subset of users (like only     those in certain departments).</li>\n<li class=\"remarkup-list-item\">Your LDAP server is configured in some other way that prevents     direct binding from working correctly.</li>\n</ul>\n\n<p><strong>To bind directly</strong>, enter the LDAP attribute corresponding to the login name into the <strong>Search Attributes</strong> box below. Often, this is something like 1Z or 2Z. This is the simplest configuration, but will only work if the username is part of the distinguished name, and won&#039;t let you apply complex restrictions to logins.</p>\n\n<div class=\"remarkup-code-block\" data-code-lang=\"text\" data-sigil=\"remarkup-code-block\"><div class=\"remarkup-code-header\">Simple Direct Binding</div><pre class=\"remarkup-code\">sn</pre></div>\n\n<p><strong>To search first</strong>, provide an anonymous username and password below (or check the <strong>Always Search</strong> checkbox), then enter one or more search queries into this field, one per line. After binding, these queries will be used to identify the record associated with the login name the user typed.</p>\n\n<p>Searches will be tried in order until a matching record is found. Each query can be a simple attribute name (like 3Z or 4Z), which will search for a matching record, or it can be a complex query that uses the string 5Z to represent the login name.</p>\n\n<p>A common simple configuration is just an attribute name, like 6Z, which will work the same way direct binding works:</p>\n\n<div class=\"remarkup-code-block\" data-code-lang=\"text\" data-sigil=\"remarkup-code-block\"><div class=\"remarkup-code-header\">Simple Example</div><pre class=\"remarkup-code\">sn</pre></div>\n\n<p>A slightly more complex configuration might let the user login with either their login name or email address:</p>\n\n<div class=\"remarkup-code-block\" data-code-lang=\"text\" data-sigil=\"remarkup-code-block\"><div class=\"remarkup-code-header\">Match Several Attributes</div><pre class=\"remarkup-code\">mail\nsn</pre></div>\n\n<p>If your LDAP directory is more complex, or you want to perform sophisticated filtering, you can use more complex queries. Depending on your directory structure, this example might allow users to login with either their email address or username, but only if they&#039;re in specific departments:</p>\n\n<div class=\"remarkup-code-block\" data-code-lang=\"text\" data-sigil=\"remarkup-code-block\"><div class=\"remarkup-code-header\">Complex Example</div><pre class=\"remarkup-code\">(&amp;(mail=${login})(|(departmentNumber=1)(departmentNumber=2)))\n(&amp;(sn=${login})(|(departmentNumber=1)(departmentNumber=2)))</pre></div>\n\n<p>All of the attribute names used here are just examples: your LDAP server may use different attribute names.</p>\";}s:7:\"storage\";a:6:{s:3:\"1Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:39:\"<tt class=\"remarkup-monospaced\">sn</tt>\";}s:3:\"2Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:40:\"<tt class=\"remarkup-monospaced\">uid</tt>\";}s:3:\"3Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:39:\"<tt class=\"remarkup-monospaced\">sn</tt>\";}s:3:\"4Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:41:\"<tt class=\"remarkup-monospaced\">mail</tt>\";}s:3:\"5Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:45:\"<tt class=\"remarkup-monospaced\">${login}</tt>\";}s:3:\"6Z\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:39:\"<tt class=\"remarkup-monospaced\">sn</tt>\";}}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565151,1478565151),(5,'.4EOg8Xw88WD:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:189:\"<p>To search for an LDAP record before authenticating, either check the <strong>Always Search</strong> checkbox or enter an anonymous username and password to use to perform the search.</p>\";}s:7:\"storage\";a:0:{}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565151,1478565151),(6,'2SwALNO6Yg70:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:315:\"<p>Optionally, specify a username attribute to use to prefill usernames when registering a new account. This is purely cosmetic and does not affect the login process, but you can configure it to make sure users get the same default username as their LDAP username, so usernames remain consistent across systems.</p>\";}s:7:\"storage\";a:0:{}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565151,1478565151),(7,'H_JFmx5tn1ir:oneoff@16@7VwBk7PApozc','a:3:{s:6:\"output\";O:14:\"PhutilSafeHTML\":1:{s:23:\"\0PhutilSafeHTML\0content\";s:253:\"<p>Optionally, specify one or more comma-separated attributes to use to prefill the &quot;Real Name&quot; field when registering a new account. This is purely cosmetic and does not affect the login process, but can make registration a little easier.</p>\";}s:7:\"storage\";a:0:{}s:8:\"metadata\";a:0:{}}','{\"host\":\"ca96c5a35ee9\"}',1478565151,1478565151);
/*!40000 ALTER TABLE `cache_markupcache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_calendar`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_calendar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_calendar`;

--
-- Table structure for table `calendar_event`
--

DROP TABLE IF EXISTS `calendar_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `hostPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `isCancelled` tinyint(1) NOT NULL,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `isAllDay` tinyint(1) NOT NULL,
  `icon` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `isRecurring` tinyint(1) NOT NULL,
  `instanceOfEventPHID` varbinary(64) DEFAULT NULL,
  `sequenceIndex` int(10) unsigned DEFAULT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `isStub` tinyint(1) NOT NULL,
  `utcInitialEpoch` int(10) unsigned NOT NULL,
  `utcUntilEpoch` int(10) unsigned DEFAULT NULL,
  `utcInstanceEpoch` int(10) unsigned DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `importAuthorPHID` varbinary(64) DEFAULT NULL,
  `importSourcePHID` varbinary(64) DEFAULT NULL,
  `importUIDIndex` binary(12) DEFAULT NULL,
  `importUID` longtext COLLATE utf8mb4_bin,
  `seriesParentPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_instance` (`instanceOfEventPHID`,`sequenceIndex`),
  UNIQUE KEY `key_rdate` (`instanceOfEventPHID`,`utcInstanceEpoch`),
  KEY `key_space` (`spacePHID`),
  KEY `key_epoch` (`utcInitialEpoch`,`utcUntilEpoch`),
  KEY `key_series` (`seriesParentPHID`,`utcInitialEpoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_event`
--

LOCK TABLES `calendar_event` WRITE;
/*!40000 ALTER TABLE `calendar_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_eventinvitee`
--

DROP TABLE IF EXISTS `calendar_eventinvitee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_eventinvitee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventPHID` varbinary(64) NOT NULL,
  `inviteePHID` varbinary(64) NOT NULL,
  `inviterPHID` varbinary(64) NOT NULL,
  `status` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `availability` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_event` (`eventPHID`,`inviteePHID`),
  KEY `key_invitee` (`inviteePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_eventinvitee`
--

LOCK TABLES `calendar_eventinvitee` WRITE;
/*!40000 ALTER TABLE `calendar_eventinvitee` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_eventinvitee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_eventtransaction`
--

DROP TABLE IF EXISTS `calendar_eventtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_eventtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_eventtransaction`
--

LOCK TABLES `calendar_eventtransaction` WRITE;
/*!40000 ALTER TABLE `calendar_eventtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_eventtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_eventtransaction_comment`
--

DROP TABLE IF EXISTS `calendar_eventtransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_eventtransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_eventtransaction_comment`
--

LOCK TABLES `calendar_eventtransaction_comment` WRITE;
/*!40000 ALTER TABLE `calendar_eventtransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_eventtransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_export`
--

DROP TABLE IF EXISTS `calendar_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_export` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `policyMode` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `queryKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `secretKey` binary(20) NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_secret` (`secretKey`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_author` (`authorPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_export`
--

LOCK TABLES `calendar_export` WRITE;
/*!40000 ALTER TABLE `calendar_export` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_exporttransaction`
--

DROP TABLE IF EXISTS `calendar_exporttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_exporttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_exporttransaction`
--

LOCK TABLES `calendar_exporttransaction` WRITE;
/*!40000 ALTER TABLE `calendar_exporttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_exporttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_externalinvitee`
--

DROP TABLE IF EXISTS `calendar_externalinvitee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_externalinvitee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `nameIndex` binary(12) NOT NULL,
  `uri` longtext COLLATE utf8mb4_bin NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `sourcePHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_name` (`nameIndex`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_externalinvitee`
--

LOCK TABLES `calendar_externalinvitee` WRITE;
/*!40000 ALTER TABLE `calendar_externalinvitee` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_externalinvitee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_import`
--

DROP TABLE IF EXISTS `calendar_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_import` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `engineType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `triggerPHID` varbinary(64) DEFAULT NULL,
  `triggerFrequency` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_author` (`authorPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_import`
--

LOCK TABLES `calendar_import` WRITE;
/*!40000 ALTER TABLE `calendar_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_importlog`
--

DROP TABLE IF EXISTS `calendar_importlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_importlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `importPHID` varbinary(64) NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_import` (`importPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_importlog`
--

LOCK TABLES `calendar_importlog` WRITE;
/*!40000 ALTER TABLE `calendar_importlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_importlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_importtransaction`
--

DROP TABLE IF EXISTS `calendar_importtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_importtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_importtransaction`
--

LOCK TABLES `calendar_importtransaction` WRITE;
/*!40000 ALTER TABLE `calendar_importtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_importtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_notification`
--

DROP TABLE IF EXISTS `calendar_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventPHID` varbinary(64) NOT NULL,
  `utcInitialEpoch` int(10) unsigned NOT NULL,
  `targetPHID` varbinary(64) NOT NULL,
  `didNotifyEpoch` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_notify` (`eventPHID`,`utcInitialEpoch`,`targetPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_notification`
--

LOCK TABLES `calendar_notification` WRITE;
/*!40000 ALTER TABLE `calendar_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_chatlog`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_chatlog` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_chatlog`;

--
-- Table structure for table `chatlog_channel`
--

DROP TABLE IF EXISTS `chatlog_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatlog_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serviceName` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `serviceType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `channelName` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_channel` (`channelName`,`serviceType`,`serviceName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chatlog_channel`
--

LOCK TABLES `chatlog_channel` WRITE;
/*!40000 ALTER TABLE `chatlog_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `chatlog_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chatlog_event`
--

DROP TABLE IF EXISTS `chatlog_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatlog_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epoch` int(10) unsigned NOT NULL,
  `author` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `message` longtext COLLATE utf8mb4_bin NOT NULL,
  `loggedByPHID` varbinary(64) NOT NULL,
  `channelID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chatlog_event`
--

LOCK TABLES `chatlog_event` WRITE;
/*!40000 ALTER TABLE `chatlog_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `chatlog_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_conduit`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_conduit` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_conduit`;

--
-- Table structure for table `conduit_certificatetoken`
--

DROP TABLE IF EXISTS `conduit_certificatetoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduit_certificatetoken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userPHID` (`userPHID`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conduit_certificatetoken`
--

LOCK TABLES `conduit_certificatetoken` WRITE;
/*!40000 ALTER TABLE `conduit_certificatetoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `conduit_certificatetoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conduit_methodcalllog`
--

DROP TABLE IF EXISTS `conduit_methodcalllog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduit_methodcalllog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connectionID` bigint(20) unsigned DEFAULT NULL,
  `method` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `error` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `duration` bigint(20) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `callerPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_method` (`method`),
  KEY `key_callermethod` (`callerPHID`,`method`),
  KEY `key_date` (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conduit_methodcalllog`
--

LOCK TABLES `conduit_methodcalllog` WRITE;
/*!40000 ALTER TABLE `conduit_methodcalllog` DISABLE KEYS */;
/*!40000 ALTER TABLE `conduit_methodcalllog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conduit_token`
--

DROP TABLE IF EXISTS `conduit_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduit_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `tokenType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `expires` int(10) unsigned DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_token` (`token`),
  KEY `key_object` (`objectPHID`,`tokenType`),
  KEY `key_expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conduit_token`
--

LOCK TABLES `conduit_token` WRITE;
/*!40000 ALTER TABLE `conduit_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `conduit_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_config`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_config` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_config`;

--
-- Table structure for table `config_entry`
--

DROP TABLE IF EXISTS `config_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `namespace` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `configKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `value` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_name` (`namespace`,`configKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_entry`
--

LOCK TABLES `config_entry` WRITE;
/*!40000 ALTER TABLE `config_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_transaction`
--

DROP TABLE IF EXISTS `config_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_transaction`
--

LOCK TABLES `config_transaction` WRITE;
/*!40000 ALTER TABLE `config_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_conpherence`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_conpherence` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_conpherence`;

--
-- Table structure for table `conpherence_index`
--

DROP TABLE IF EXISTS `conpherence_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conpherence_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `threadPHID` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) NOT NULL,
  `previousTransactionPHID` varbinary(64) DEFAULT NULL,
  `corpus` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_transaction` (`transactionPHID`),
  UNIQUE KEY `key_previous` (`previousTransactionPHID`),
  KEY `key_thread` (`threadPHID`),
  FULLTEXT KEY `key_corpus` (`corpus`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conpherence_index`
--

LOCK TABLES `conpherence_index` WRITE;
/*!40000 ALTER TABLE `conpherence_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `conpherence_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conpherence_participant`
--

DROP TABLE IF EXISTS `conpherence_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conpherence_participant` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participantPHID` varbinary(64) NOT NULL,
  `conpherencePHID` varbinary(64) NOT NULL,
  `participationStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `dateTouched` int(10) unsigned NOT NULL,
  `behindTransactionPHID` varbinary(64) NOT NULL,
  `seenMessageCount` bigint(20) unsigned NOT NULL,
  `settings` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `conpherencePHID` (`conpherencePHID`,`participantPHID`),
  KEY `unreadCount` (`participantPHID`,`participationStatus`),
  KEY `participationIndex` (`participantPHID`,`dateTouched`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conpherence_participant`
--

LOCK TABLES `conpherence_participant` WRITE;
/*!40000 ALTER TABLE `conpherence_participant` DISABLE KEYS */;
/*!40000 ALTER TABLE `conpherence_participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conpherence_thread`
--

DROP TABLE IF EXISTS `conpherence_thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conpherence_thread` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `messageCount` bigint(20) unsigned NOT NULL,
  `recentParticipantPHIDs` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `joinPolicy` varbinary(64) NOT NULL,
  `mailKey` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conpherence_thread`
--

LOCK TABLES `conpherence_thread` WRITE;
/*!40000 ALTER TABLE `conpherence_thread` DISABLE KEYS */;
/*!40000 ALTER TABLE `conpherence_thread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conpherence_threadtitle_ngrams`
--

DROP TABLE IF EXISTS `conpherence_threadtitle_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conpherence_threadtitle_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conpherence_threadtitle_ngrams`
--

LOCK TABLES `conpherence_threadtitle_ngrams` WRITE;
/*!40000 ALTER TABLE `conpherence_threadtitle_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `conpherence_threadtitle_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conpherence_transaction`
--

DROP TABLE IF EXISTS `conpherence_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conpherence_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conpherence_transaction`
--

LOCK TABLES `conpherence_transaction` WRITE;
/*!40000 ALTER TABLE `conpherence_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `conpherence_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conpherence_transaction_comment`
--

DROP TABLE IF EXISTS `conpherence_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conpherence_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `conpherencePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`),
  UNIQUE KEY `key_draft` (`authorPHID`,`conpherencePHID`,`transactionPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conpherence_transaction_comment`
--

LOCK TABLES `conpherence_transaction_comment` WRITE;
/*!40000 ALTER TABLE `conpherence_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `conpherence_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_countdown`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_countdown` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_countdown`;

--
-- Table structure for table `countdown`
--

DROP TABLE IF EXISTS `countdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countdown` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_epoch` (`epoch`),
  KEY `key_author` (`authorPHID`,`epoch`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countdown`
--

LOCK TABLES `countdown` WRITE;
/*!40000 ALTER TABLE `countdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `countdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countdown_transaction`
--

DROP TABLE IF EXISTS `countdown_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countdown_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countdown_transaction`
--

LOCK TABLES `countdown_transaction` WRITE;
/*!40000 ALTER TABLE `countdown_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `countdown_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countdown_transaction_comment`
--

DROP TABLE IF EXISTS `countdown_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countdown_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countdown_transaction_comment`
--

LOCK TABLES `countdown_transaction_comment` WRITE;
/*!40000 ALTER TABLE `countdown_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `countdown_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_daemon`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_daemon` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_daemon`;

--
-- Table structure for table `daemon_log`
--

DROP TABLE IF EXISTS `daemon_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daemon_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `daemon` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `host` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `argv` longtext COLLATE utf8mb4_bin NOT NULL,
  `explicitArgv` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `status` varchar(8) COLLATE utf8mb4_bin NOT NULL,
  `runningAsUser` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `daemonID` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_daemonID` (`daemonID`),
  KEY `status` (`status`),
  KEY `dateCreated` (`dateCreated`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daemon_log`
--

LOCK TABLES `daemon_log` WRITE;
/*!40000 ALTER TABLE `daemon_log` DISABLE KEYS */;
INSERT INTO `daemon_log` VALUES (1,'PhabricatorRepositoryPullLocalDaemon','ca96c5a35ee9',43,'[]','[]',1478565045,1478565165,'run','phd','43:fbzamuuqh'),(2,'PhabricatorTriggerDaemon','ca96c5a35ee9',43,'[]','[]',1478565045,1478565165,'run','phd','43:46d4aiyo7'),(3,'PhabricatorTaskmasterDaemon','ca96c5a35ee9',43,'[]','[]',1478565045,1478565165,'run','phd','43:yf77kd6t5');
/*!40000 ALTER TABLE `daemon_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daemon_logevent`
--

DROP TABLE IF EXISTS `daemon_logevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daemon_logevent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logID` int(10) unsigned NOT NULL,
  `logType` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `message` longtext COLLATE utf8mb4_bin NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logID` (`logID`,`epoch`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daemon_logevent`
--

LOCK TABLES `daemon_logevent` WRITE;
/*!40000 ALTER TABLE `daemon_logevent` DISABLE KEYS */;
INSERT INTO `daemon_logevent` VALUES (1,1,'INIT','Starting process.',1478565045),(2,2,'INIT','Starting process.',1478565045),(3,3,'INIT','Starting process.',1478565045);
/*!40000 ALTER TABLE `daemon_logevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_dashboard`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_dashboard` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_dashboard`;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `layoutConfig` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_install`
--

DROP TABLE IF EXISTS `dashboard_install`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_install` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `installerPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `applicationClass` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `dashboardPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`applicationClass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_install`
--

LOCK TABLES `dashboard_install` WRITE;
/*!40000 ALTER TABLE `dashboard_install` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_install` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_panel`
--

DROP TABLE IF EXISTS `dashboard_panel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_panel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `panelType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `isArchived` tinyint(1) NOT NULL DEFAULT '0',
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_panel`
--

LOCK TABLES `dashboard_panel` WRITE;
/*!40000 ALTER TABLE `dashboard_panel` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_panel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_paneltransaction`
--

DROP TABLE IF EXISTS `dashboard_paneltransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_paneltransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_paneltransaction`
--

LOCK TABLES `dashboard_paneltransaction` WRITE;
/*!40000 ALTER TABLE `dashboard_paneltransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_paneltransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_transaction`
--

DROP TABLE IF EXISTS `dashboard_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_transaction`
--

LOCK TABLES `dashboard_transaction` WRITE;
/*!40000 ALTER TABLE `dashboard_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_differential`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_differential` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_differential`;

--
-- Table structure for table `differential_affectedpath`
--

DROP TABLE IF EXISTS `differential_affectedpath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_affectedpath` (
  `repositoryID` int(10) unsigned NOT NULL,
  `pathID` int(10) unsigned NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `revisionID` int(10) unsigned NOT NULL,
  KEY `repositoryID` (`repositoryID`,`pathID`,`epoch`),
  KEY `revisionID` (`revisionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_affectedpath`
--

LOCK TABLES `differential_affectedpath` WRITE;
/*!40000 ALTER TABLE `differential_affectedpath` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_affectedpath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_changeset`
--

DROP TABLE IF EXISTS `differential_changeset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_changeset` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `diffID` int(10) unsigned NOT NULL,
  `oldFile` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `awayPaths` longtext COLLATE utf8mb4_bin,
  `changeType` int(10) unsigned NOT NULL,
  `fileType` int(10) unsigned NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin,
  `oldProperties` longtext COLLATE utf8mb4_bin,
  `newProperties` longtext COLLATE utf8mb4_bin,
  `addLines` int(10) unsigned NOT NULL,
  `delLines` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `diffID` (`diffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_changeset`
--

LOCK TABLES `differential_changeset` WRITE;
/*!40000 ALTER TABLE `differential_changeset` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_changeset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_changeset_parse_cache`
--

DROP TABLE IF EXISTS `differential_changeset_parse_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_changeset_parse_cache` (
  `id` int(10) unsigned NOT NULL,
  `cache` longblob NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dateCreated` (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_changeset_parse_cache`
--

LOCK TABLES `differential_changeset_parse_cache` WRITE;
/*!40000 ALTER TABLE `differential_changeset_parse_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_changeset_parse_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_commit`
--

DROP TABLE IF EXISTS `differential_commit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_commit` (
  `revisionID` int(10) unsigned NOT NULL,
  `commitPHID` varbinary(64) NOT NULL,
  PRIMARY KEY (`revisionID`,`commitPHID`),
  UNIQUE KEY `commitPHID` (`commitPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_commit`
--

LOCK TABLES `differential_commit` WRITE;
/*!40000 ALTER TABLE `differential_commit` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_commit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_customfieldnumericindex`
--

DROP TABLE IF EXISTS `differential_customfieldnumericindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_customfieldnumericindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`),
  KEY `key_find` (`indexKey`,`indexValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_customfieldnumericindex`
--

LOCK TABLES `differential_customfieldnumericindex` WRITE;
/*!40000 ALTER TABLE `differential_customfieldnumericindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_customfieldnumericindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_customfieldstorage`
--

DROP TABLE IF EXISTS `differential_customfieldstorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_customfieldstorage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `fieldIndex` binary(12) NOT NULL,
  `fieldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`fieldIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_customfieldstorage`
--

LOCK TABLES `differential_customfieldstorage` WRITE;
/*!40000 ALTER TABLE `differential_customfieldstorage` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_customfieldstorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_customfieldstringindex`
--

DROP TABLE IF EXISTS `differential_customfieldstringindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_customfieldstringindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`(64)),
  KEY `key_find` (`indexKey`,`indexValue`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_customfieldstringindex`
--

LOCK TABLES `differential_customfieldstringindex` WRITE;
/*!40000 ALTER TABLE `differential_customfieldstringindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_customfieldstringindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_diff`
--

DROP TABLE IF EXISTS `differential_diff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_diff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `revisionID` int(10) unsigned DEFAULT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `repositoryPHID` varbinary(64) DEFAULT NULL,
  `sourceMachine` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sourcePath` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sourceControlSystem` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `sourceControlBaseRevision` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sourceControlPath` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lintStatus` int(10) unsigned NOT NULL,
  `unitStatus` int(10) unsigned NOT NULL,
  `lineCount` int(10) unsigned NOT NULL,
  `branch` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bookmark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `creationMethod` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `repositoryUUID` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `revisionID` (`revisionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_diff`
--

LOCK TABLES `differential_diff` WRITE;
/*!40000 ALTER TABLE `differential_diff` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_diff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_diffproperty`
--

DROP TABLE IF EXISTS `differential_diffproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_diffproperty` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `diffID` int(10) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `diffID` (`diffID`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_diffproperty`
--

LOCK TABLES `differential_diffproperty` WRITE;
/*!40000 ALTER TABLE `differential_diffproperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_diffproperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_difftransaction`
--

DROP TABLE IF EXISTS `differential_difftransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_difftransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_difftransaction`
--

LOCK TABLES `differential_difftransaction` WRITE;
/*!40000 ALTER TABLE `differential_difftransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_difftransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_draft`
--

DROP TABLE IF EXISTS `differential_draft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_draft` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `draftKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_unique` (`objectPHID`,`authorPHID`,`draftKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_draft`
--

LOCK TABLES `differential_draft` WRITE;
/*!40000 ALTER TABLE `differential_draft` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_draft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_hiddencomment`
--

DROP TABLE IF EXISTS `differential_hiddencomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_hiddencomment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `commentID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_user` (`userPHID`,`commentID`),
  KEY `key_comment` (`commentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_hiddencomment`
--

LOCK TABLES `differential_hiddencomment` WRITE;
/*!40000 ALTER TABLE `differential_hiddencomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_hiddencomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_hunk`
--

DROP TABLE IF EXISTS `differential_hunk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_hunk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `changesetID` int(10) unsigned NOT NULL,
  `changes` longtext COLLATE utf8mb4_bin,
  `oldOffset` int(10) unsigned NOT NULL,
  `oldLen` int(10) unsigned NOT NULL,
  `newOffset` int(10) unsigned NOT NULL,
  `newLen` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `changesetID` (`changesetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_hunk`
--

LOCK TABLES `differential_hunk` WRITE;
/*!40000 ALTER TABLE `differential_hunk` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_hunk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_hunk_modern`
--

DROP TABLE IF EXISTS `differential_hunk_modern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_hunk_modern` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `changesetID` int(10) unsigned NOT NULL,
  `oldOffset` int(10) unsigned NOT NULL,
  `oldLen` int(10) unsigned NOT NULL,
  `newOffset` int(10) unsigned NOT NULL,
  `newLen` int(10) unsigned NOT NULL,
  `dataType` binary(4) NOT NULL,
  `dataEncoding` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL,
  `dataFormat` binary(4) NOT NULL,
  `data` longblob NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_changeset` (`changesetID`),
  KEY `key_created` (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_hunk_modern`
--

LOCK TABLES `differential_hunk_modern` WRITE;
/*!40000 ALTER TABLE `differential_hunk_modern` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_hunk_modern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_revision`
--

DROP TABLE IF EXISTS `differential_revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_revision` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `originalTitle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `summary` longtext COLLATE utf8mb4_bin NOT NULL,
  `testPlan` longtext COLLATE utf8mb4_bin NOT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `lastReviewerPHID` varbinary(64) DEFAULT NULL,
  `lineCount` int(10) unsigned DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `attached` longtext COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(40) NOT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  KEY `authorPHID` (`authorPHID`,`status`),
  KEY `repositoryPHID` (`repositoryPHID`),
  KEY `key_status` (`status`,`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_revision`
--

LOCK TABLES `differential_revision` WRITE;
/*!40000 ALTER TABLE `differential_revision` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_revisionhash`
--

DROP TABLE IF EXISTS `differential_revisionhash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_revisionhash` (
  `revisionID` int(10) unsigned NOT NULL,
  `type` binary(4) NOT NULL,
  `hash` binary(40) NOT NULL,
  KEY `type` (`type`,`hash`),
  KEY `revisionID` (`revisionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_revisionhash`
--

LOCK TABLES `differential_revisionhash` WRITE;
/*!40000 ALTER TABLE `differential_revisionhash` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_revisionhash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_transaction`
--

DROP TABLE IF EXISTS `differential_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_transaction`
--

LOCK TABLES `differential_transaction` WRITE;
/*!40000 ALTER TABLE `differential_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `differential_transaction_comment`
--

DROP TABLE IF EXISTS `differential_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `differential_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `revisionPHID` varbinary(64) DEFAULT NULL,
  `changesetID` int(10) unsigned DEFAULT NULL,
  `isNewFile` tinyint(1) NOT NULL,
  `lineNumber` int(10) unsigned NOT NULL,
  `lineLength` int(10) unsigned NOT NULL,
  `fixedState` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `hasReplies` tinyint(1) NOT NULL,
  `replyToCommentPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`),
  KEY `key_changeset` (`changesetID`),
  KEY `key_draft` (`authorPHID`,`transactionPHID`),
  KEY `key_revision` (`revisionPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `differential_transaction_comment`
--

LOCK TABLES `differential_transaction_comment` WRITE;
/*!40000 ALTER TABLE `differential_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `differential_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_diviner`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_diviner` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_diviner`;

--
-- Table structure for table `diviner_liveatom`
--

DROP TABLE IF EXISTS `diviner_liveatom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diviner_liveatom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `symbolPHID` varbinary(64) NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `atomData` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `symbolPHID` (`symbolPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diviner_liveatom`
--

LOCK TABLES `diviner_liveatom` WRITE;
/*!40000 ALTER TABLE `diviner_liveatom` DISABLE KEYS */;
/*!40000 ALTER TABLE `diviner_liveatom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diviner_livebook`
--

DROP TABLE IF EXISTS `diviner_livebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diviner_livebook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `repositoryPHID` varbinary(64) DEFAULT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `configurationData` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diviner_livebook`
--

LOCK TABLES `diviner_livebook` WRITE;
/*!40000 ALTER TABLE `diviner_livebook` DISABLE KEYS */;
/*!40000 ALTER TABLE `diviner_livebook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diviner_livebooktransaction`
--

DROP TABLE IF EXISTS `diviner_livebooktransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diviner_livebooktransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diviner_livebooktransaction`
--

LOCK TABLES `diviner_livebooktransaction` WRITE;
/*!40000 ALTER TABLE `diviner_livebooktransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `diviner_livebooktransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diviner_livesymbol`
--

DROP TABLE IF EXISTS `diviner_livesymbol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diviner_livesymbol` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `bookPHID` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `atomIndex` int(10) unsigned NOT NULL,
  `identityHash` binary(12) NOT NULL,
  `graphHash` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `title` longtext COLLATE utf8mb4_bin,
  `titleSlugHash` binary(12) DEFAULT NULL,
  `groupName` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_bin,
  `isDocumentable` tinyint(1) NOT NULL,
  `nodeHash` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identityHash` (`identityHash`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `graphHash` (`graphHash`),
  UNIQUE KEY `nodeHash` (`nodeHash`),
  KEY `key_slug` (`titleSlugHash`),
  KEY `bookPHID` (`bookPHID`,`type`,`name`(64),`context`(64),`atomIndex`),
  KEY `name` (`name`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diviner_livesymbol`
--

LOCK TABLES `diviner_livesymbol` WRITE;
/*!40000 ALTER TABLE `diviner_livesymbol` DISABLE KEYS */;
/*!40000 ALTER TABLE `diviner_livesymbol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_doorkeeper`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_doorkeeper` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_doorkeeper`;

--
-- Table structure for table `doorkeeper_externalobject`
--

DROP TABLE IF EXISTS `doorkeeper_externalobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doorkeeper_externalobject` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `objectKey` binary(12) NOT NULL,
  `applicationType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `applicationDomain` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `objectType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `objectID` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `objectURI` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `importerPHID` varbinary(64) DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_object` (`objectKey`),
  KEY `key_full` (`applicationType`,`applicationDomain`,`objectType`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doorkeeper_externalobject`
--

LOCK TABLES `doorkeeper_externalobject` WRITE;
/*!40000 ALTER TABLE `doorkeeper_externalobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `doorkeeper_externalobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_draft`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_draft` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_draft`;

--
-- Table structure for table `draft`
--

DROP TABLE IF EXISTS `draft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `draft` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authorPHID` varbinary(64) NOT NULL,
  `draftKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `draft` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authorPHID` (`authorPHID`,`draftKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `draft`
--

LOCK TABLES `draft` WRITE;
/*!40000 ALTER TABLE `draft` DISABLE KEYS */;
/*!40000 ALTER TABLE `draft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `draft_versioneddraft`
--

DROP TABLE IF EXISTS `draft_versioneddraft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `draft_versioneddraft` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_object` (`objectPHID`,`authorPHID`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `draft_versioneddraft`
--

LOCK TABLES `draft_versioneddraft` WRITE;
/*!40000 ALTER TABLE `draft_versioneddraft` DISABLE KEYS */;
/*!40000 ALTER TABLE `draft_versioneddraft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_drydock`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_drydock` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_drydock`;

--
-- Table structure for table `drydock_authorization`
--

DROP TABLE IF EXISTS `drydock_authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_authorization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `blueprintPHID` varbinary(64) NOT NULL,
  `blueprintAuthorizationState` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `objectAuthorizationState` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_unique` (`objectPHID`,`blueprintPHID`),
  KEY `key_blueprint` (`blueprintPHID`,`blueprintAuthorizationState`),
  KEY `key_object` (`objectPHID`,`objectAuthorizationState`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_authorization`
--

LOCK TABLES `drydock_authorization` WRITE;
/*!40000 ALTER TABLE `drydock_authorization` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_blueprint`
--

DROP TABLE IF EXISTS `drydock_blueprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_blueprint` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `className` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `blueprintName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_blueprint`
--

LOCK TABLES `drydock_blueprint` WRITE;
/*!40000 ALTER TABLE `drydock_blueprint` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_blueprint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_blueprintname_ngrams`
--

DROP TABLE IF EXISTS `drydock_blueprintname_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_blueprintname_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_blueprintname_ngrams`
--

LOCK TABLES `drydock_blueprintname_ngrams` WRITE;
/*!40000 ALTER TABLE `drydock_blueprintname_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_blueprintname_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_blueprinttransaction`
--

DROP TABLE IF EXISTS `drydock_blueprinttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_blueprinttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_blueprinttransaction`
--

LOCK TABLES `drydock_blueprinttransaction` WRITE;
/*!40000 ALTER TABLE `drydock_blueprinttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_blueprinttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_command`
--

DROP TABLE IF EXISTS `drydock_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_command` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authorPHID` varbinary(64) NOT NULL,
  `targetPHID` varbinary(64) NOT NULL,
  `command` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `isConsumed` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_target` (`targetPHID`,`isConsumed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_command`
--

LOCK TABLES `drydock_command` WRITE;
/*!40000 ALTER TABLE `drydock_command` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_command` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_lease`
--

DROP TABLE IF EXISTS `drydock_lease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_lease` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `until` int(10) unsigned DEFAULT NULL,
  `ownerPHID` varbinary(64) DEFAULT NULL,
  `attributes` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `resourceType` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `resourcePHID` varbinary(64) DEFAULT NULL,
  `authorizingPHID` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_resource` (`resourcePHID`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_lease`
--

LOCK TABLES `drydock_lease` WRITE;
/*!40000 ALTER TABLE `drydock_lease` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_lease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_log`
--

DROP TABLE IF EXISTS `drydock_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epoch` int(10) unsigned NOT NULL,
  `blueprintPHID` varbinary(64) DEFAULT NULL,
  `resourcePHID` varbinary(64) DEFAULT NULL,
  `leasePHID` varbinary(64) DEFAULT NULL,
  `type` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `epoch` (`epoch`),
  KEY `key_blueprint` (`blueprintPHID`,`type`),
  KEY `key_resource` (`resourcePHID`,`type`),
  KEY `key_lease` (`leasePHID`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_log`
--

LOCK TABLES `drydock_log` WRITE;
/*!40000 ALTER TABLE `drydock_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_repositoryoperation`
--

DROP TABLE IF EXISTS `drydock_repositoryoperation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_repositoryoperation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `repositoryTarget` longblob NOT NULL,
  `operationType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `operationState` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isDismissed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`),
  KEY `key_repository` (`repositoryPHID`,`operationState`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_repositoryoperation`
--

LOCK TABLES `drydock_repositoryoperation` WRITE;
/*!40000 ALTER TABLE `drydock_repositoryoperation` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_repositoryoperation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_resource`
--

DROP TABLE IF EXISTS `drydock_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `ownerPHID` varbinary(64) DEFAULT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `attributes` longtext COLLATE utf8mb4_bin NOT NULL,
  `capabilities` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `blueprintPHID` varbinary(64) NOT NULL,
  `until` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_type` (`type`,`status`),
  KEY `key_blueprint` (`blueprintPHID`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_resource`
--

LOCK TABLES `drydock_resource` WRITE;
/*!40000 ALTER TABLE `drydock_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drydock_slotlock`
--

DROP TABLE IF EXISTS `drydock_slotlock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drydock_slotlock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ownerPHID` varbinary(64) NOT NULL,
  `lockIndex` binary(12) NOT NULL,
  `lockKey` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_lock` (`lockIndex`),
  KEY `key_owner` (`ownerPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drydock_slotlock`
--

LOCK TABLES `drydock_slotlock` WRITE;
/*!40000 ALTER TABLE `drydock_slotlock` DISABLE KEYS */;
/*!40000 ALTER TABLE `drydock_slotlock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_fact`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_fact` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_fact`;

--
-- Table structure for table `fact_aggregate`
--

DROP TABLE IF EXISTS `fact_aggregate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fact_aggregate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `factType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `valueX` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `factType` (`factType`,`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fact_aggregate`
--

LOCK TABLES `fact_aggregate` WRITE;
/*!40000 ALTER TABLE `fact_aggregate` DISABLE KEYS */;
/*!40000 ALTER TABLE `fact_aggregate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fact_cursor`
--

DROP TABLE IF EXISTS `fact_cursor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fact_cursor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `position` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fact_cursor`
--

LOCK TABLES `fact_cursor` WRITE;
/*!40000 ALTER TABLE `fact_cursor` DISABLE KEYS */;
/*!40000 ALTER TABLE `fact_cursor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fact_raw`
--

DROP TABLE IF EXISTS `fact_raw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fact_raw` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `factType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `objectA` varbinary(64) NOT NULL,
  `valueX` bigint(20) NOT NULL,
  `valueY` bigint(20) NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objectPHID` (`objectPHID`),
  KEY `factType` (`factType`,`epoch`),
  KEY `factType_2` (`factType`,`objectA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fact_raw`
--

LOCK TABLES `fact_raw` WRITE;
/*!40000 ALTER TABLE `fact_raw` DISABLE KEYS */;
/*!40000 ALTER TABLE `fact_raw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_feed`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_feed` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_feed`;

--
-- Table structure for table `feed_storydata`
--

DROP TABLE IF EXISTS `feed_storydata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_storydata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `chronologicalKey` bigint(20) unsigned NOT NULL,
  `storyType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `storyData` longtext COLLATE utf8mb4_bin NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chronologicalKey` (`chronologicalKey`),
  UNIQUE KEY `phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feed_storydata`
--

LOCK TABLES `feed_storydata` WRITE;
/*!40000 ALTER TABLE `feed_storydata` DISABLE KEYS */;
/*!40000 ALTER TABLE `feed_storydata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feed_storynotification`
--

DROP TABLE IF EXISTS `feed_storynotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_storynotification` (
  `userPHID` varbinary(64) NOT NULL,
  `primaryObjectPHID` varbinary(64) NOT NULL,
  `chronologicalKey` bigint(20) unsigned NOT NULL,
  `hasViewed` tinyint(1) NOT NULL,
  UNIQUE KEY `userPHID` (`userPHID`,`chronologicalKey`),
  KEY `userPHID_2` (`userPHID`,`hasViewed`,`primaryObjectPHID`),
  KEY `key_object` (`primaryObjectPHID`),
  KEY `key_chronological` (`chronologicalKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feed_storynotification`
--

LOCK TABLES `feed_storynotification` WRITE;
/*!40000 ALTER TABLE `feed_storynotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `feed_storynotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feed_storyreference`
--

DROP TABLE IF EXISTS `feed_storyreference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_storyreference` (
  `objectPHID` varbinary(64) NOT NULL,
  `chronologicalKey` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `objectPHID` (`objectPHID`,`chronologicalKey`),
  KEY `chronologicalKey` (`chronologicalKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feed_storyreference`
--

LOCK TABLES `feed_storyreference` WRITE;
/*!40000 ALTER TABLE `feed_storyreference` DISABLE KEYS */;
/*!40000 ALTER TABLE `feed_storyreference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_file`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_file` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_file`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `mimeType` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `byteSize` bigint(20) unsigned NOT NULL,
  `storageEngine` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `storageFormat` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `storageHandle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `secretKey` binary(20) DEFAULT NULL,
  `contentHash` binary(40) DEFAULT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `ttl` int(10) unsigned DEFAULT NULL,
  `isExplicitUpload` tinyint(1) DEFAULT '1',
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `isPartial` tinyint(1) NOT NULL DEFAULT '0',
  `builtinKey` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `key_builtin` (`builtinKey`),
  KEY `authorPHID` (`authorPHID`),
  KEY `contentHash` (`contentHash`),
  KEY `key_ttl` (`ttl`),
  KEY `key_dateCreated` (`dateCreated`),
  KEY `key_partial` (`authorPHID`,`isPartial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_chunk`
--

DROP TABLE IF EXISTS `file_chunk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_chunk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chunkHandle` binary(12) NOT NULL,
  `byteStart` bigint(20) unsigned NOT NULL,
  `byteEnd` bigint(20) unsigned NOT NULL,
  `dataFilePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_file` (`chunkHandle`,`byteStart`,`byteEnd`),
  KEY `key_data` (`dataFilePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_chunk`
--

LOCK TABLES `file_chunk` WRITE;
/*!40000 ALTER TABLE `file_chunk` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_chunk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_externalrequest`
--

DROP TABLE IF EXISTS `file_externalrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_externalrequest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filePHID` varbinary(64) DEFAULT NULL,
  `ttl` int(10) unsigned NOT NULL,
  `uri` longtext COLLATE utf8mb4_bin NOT NULL,
  `uriIndex` binary(12) NOT NULL,
  `isSuccessful` tinyint(1) NOT NULL,
  `responseMessage` longtext COLLATE utf8mb4_bin,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_uriindex` (`uriIndex`),
  KEY `key_ttl` (`ttl`),
  KEY `key_file` (`filePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_externalrequest`
--

LOCK TABLES `file_externalrequest` WRITE;
/*!40000 ALTER TABLE `file_externalrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_externalrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_imagemacro`
--

DROP TABLE IF EXISTS `file_imagemacro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_imagemacro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `filePHID` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  `audioPHID` varbinary(64) DEFAULT NULL,
  `audioBehavior` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `name` (`name`),
  KEY `key_disabled` (`isDisabled`),
  KEY `key_dateCreated` (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_imagemacro`
--

LOCK TABLES `file_imagemacro` WRITE;
/*!40000 ALTER TABLE `file_imagemacro` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_imagemacro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_storageblob`
--

DROP TABLE IF EXISTS `file_storageblob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_storageblob` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longblob NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_storageblob`
--

LOCK TABLES `file_storageblob` WRITE;
/*!40000 ALTER TABLE `file_storageblob` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_storageblob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_transaction`
--

DROP TABLE IF EXISTS `file_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_transaction`
--

LOCK TABLES `file_transaction` WRITE;
/*!40000 ALTER TABLE `file_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_transaction_comment`
--

DROP TABLE IF EXISTS `file_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_transaction_comment`
--

LOCK TABLES `file_transaction_comment` WRITE;
/*!40000 ALTER TABLE `file_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_transformedfile`
--

DROP TABLE IF EXISTS `file_transformedfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_transformedfile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `originalPHID` varbinary(64) NOT NULL,
  `transform` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `transformedPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `originalPHID` (`originalPHID`,`transform`),
  KEY `transformedPHID` (`transformedPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_transformedfile`
--

LOCK TABLES `file_transformedfile` WRITE;
/*!40000 ALTER TABLE `file_transformedfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_transformedfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `macro_transaction`
--

DROP TABLE IF EXISTS `macro_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `macro_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `macro_transaction`
--

LOCK TABLES `macro_transaction` WRITE;
/*!40000 ALTER TABLE `macro_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `macro_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `macro_transaction_comment`
--

DROP TABLE IF EXISTS `macro_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `macro_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `macro_transaction_comment`
--

LOCK TABLES `macro_transaction_comment` WRITE;
/*!40000 ALTER TABLE `macro_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `macro_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_flag`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_flag` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_flag`;

--
-- Table structure for table `flag`
--

DROP TABLE IF EXISTS `flag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ownerPHID` varbinary(64) NOT NULL,
  `type` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `reasonPHID` varbinary(64) NOT NULL,
  `color` int(10) unsigned NOT NULL,
  `note` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ownerPHID` (`ownerPHID`,`type`,`objectPHID`),
  KEY `objectPHID` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flag`
--

LOCK TABLES `flag` WRITE;
/*!40000 ALTER TABLE `flag` DISABLE KEYS */;
/*!40000 ALTER TABLE `flag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_fund`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_fund` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_fund`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fund_backer`
--

DROP TABLE IF EXISTS `fund_backer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_backer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `initiativePHID` varbinary(64) NOT NULL,
  `backerPHID` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `amountAsCurrency` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_initiative` (`initiativePHID`),
  KEY `key_backer` (`backerPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_backer`
--

LOCK TABLES `fund_backer` WRITE;
/*!40000 ALTER TABLE `fund_backer` DISABLE KEYS */;
/*!40000 ALTER TABLE `fund_backer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fund_backertransaction`
--

DROP TABLE IF EXISTS `fund_backertransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_backertransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_backertransaction`
--

LOCK TABLES `fund_backertransaction` WRITE;
/*!40000 ALTER TABLE `fund_backertransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `fund_backertransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fund_initiative`
--

DROP TABLE IF EXISTS `fund_initiative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_initiative` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ownerPHID` varbinary(64) NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `merchantPHID` varbinary(64) DEFAULT NULL,
  `risks` longtext COLLATE utf8mb4_bin NOT NULL,
  `totalAsCurrency` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_status` (`status`),
  KEY `key_owner` (`ownerPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_initiative`
--

LOCK TABLES `fund_initiative` WRITE;
/*!40000 ALTER TABLE `fund_initiative` DISABLE KEYS */;
/*!40000 ALTER TABLE `fund_initiative` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fund_initiativetransaction`
--

DROP TABLE IF EXISTS `fund_initiativetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_initiativetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_initiativetransaction`
--

LOCK TABLES `fund_initiativetransaction` WRITE;
/*!40000 ALTER TABLE `fund_initiativetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `fund_initiativetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fund_initiativetransaction_comment`
--

DROP TABLE IF EXISTS `fund_initiativetransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_initiativetransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_initiativetransaction_comment`
--

LOCK TABLES `fund_initiativetransaction_comment` WRITE;
/*!40000 ALTER TABLE `fund_initiativetransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `fund_initiativetransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_harbormaster`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_harbormaster` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_harbormaster`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_build`
--

DROP TABLE IF EXISTS `harbormaster_build`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_build` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `buildablePHID` varbinary(64) NOT NULL,
  `buildPlanPHID` varbinary(64) NOT NULL,
  `buildStatus` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `buildGeneration` int(10) unsigned NOT NULL DEFAULT '0',
  `planAutoKey` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `buildParameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `initiatorPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_planautokey` (`buildablePHID`,`planAutoKey`),
  KEY `key_buildable` (`buildablePHID`),
  KEY `key_plan` (`buildPlanPHID`),
  KEY `key_status` (`buildStatus`),
  KEY `key_initiator` (`initiatorPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_build`
--

LOCK TABLES `harbormaster_build` WRITE;
/*!40000 ALTER TABLE `harbormaster_build` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_build` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildable`
--

DROP TABLE IF EXISTS `harbormaster_buildable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `buildablePHID` varbinary(64) NOT NULL,
  `containerPHID` varbinary(64) DEFAULT NULL,
  `buildableStatus` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isManualBuildable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_buildable` (`buildablePHID`),
  KEY `key_container` (`containerPHID`),
  KEY `key_manual` (`isManualBuildable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildable`
--

LOCK TABLES `harbormaster_buildable` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildable` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildabletransaction`
--

DROP TABLE IF EXISTS `harbormaster_buildabletransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildabletransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildabletransaction`
--

LOCK TABLES `harbormaster_buildabletransaction` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildabletransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildabletransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildartifact`
--

DROP TABLE IF EXISTS `harbormaster_buildartifact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildartifact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `artifactType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `artifactIndex` binary(12) NOT NULL,
  `artifactKey` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `artifactData` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `buildTargetPHID` varbinary(64) NOT NULL,
  `isReleased` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_artifact` (`artifactType`,`artifactIndex`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_garbagecollect` (`artifactType`,`dateCreated`),
  KEY `key_target` (`buildTargetPHID`,`artifactType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildartifact`
--

LOCK TABLES `harbormaster_buildartifact` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildartifact` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildartifact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildcommand`
--

DROP TABLE IF EXISTS `harbormaster_buildcommand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildcommand` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authorPHID` varbinary(64) NOT NULL,
  `targetPHID` varbinary(64) NOT NULL,
  `command` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_target` (`targetPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildcommand`
--

LOCK TABLES `harbormaster_buildcommand` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildcommand` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildcommand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildlintmessage`
--

DROP TABLE IF EXISTS `harbormaster_buildlintmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildlintmessage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `buildTargetPHID` varbinary(64) NOT NULL,
  `path` longtext COLLATE utf8mb4_bin NOT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `characterOffset` int(10) unsigned DEFAULT NULL,
  `code` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `severity` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_target` (`buildTargetPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildlintmessage`
--

LOCK TABLES `harbormaster_buildlintmessage` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildlintmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildlintmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildlog`
--

DROP TABLE IF EXISTS `harbormaster_buildlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `logSource` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `logType` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `duration` int(10) unsigned DEFAULT NULL,
  `live` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `buildTargetPHID` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_buildtarget` (`buildTargetPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildlog`
--

LOCK TABLES `harbormaster_buildlog` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildlogchunk`
--

DROP TABLE IF EXISTS `harbormaster_buildlogchunk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildlogchunk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logID` int(10) unsigned NOT NULL,
  `encoding` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `chunk` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_log` (`logID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildlogchunk`
--

LOCK TABLES `harbormaster_buildlogchunk` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildlogchunk` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildlogchunk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildmessage`
--

DROP TABLE IF EXISTS `harbormaster_buildmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildmessage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authorPHID` varbinary(64) NOT NULL,
  `buildTargetPHID` varbinary(64) NOT NULL,
  `type` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `isConsumed` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_buildtarget` (`buildTargetPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildmessage`
--

LOCK TABLES `harbormaster_buildmessage` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildplan`
--

DROP TABLE IF EXISTS `harbormaster_buildplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildplan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `planStatus` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `planAutoKey` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_planautokey` (`planAutoKey`),
  KEY `key_status` (`planStatus`),
  KEY `key_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildplan`
--

LOCK TABLES `harbormaster_buildplan` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildplan` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildplanname_ngrams`
--

DROP TABLE IF EXISTS `harbormaster_buildplanname_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildplanname_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildplanname_ngrams`
--

LOCK TABLES `harbormaster_buildplanname_ngrams` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildplanname_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildplanname_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildplantransaction`
--

DROP TABLE IF EXISTS `harbormaster_buildplantransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildplantransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildplantransaction`
--

LOCK TABLES `harbormaster_buildplantransaction` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildplantransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildplantransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildstep`
--

DROP TABLE IF EXISTS `harbormaster_buildstep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildstep` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `buildPlanPHID` varbinary(64) NOT NULL,
  `className` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `stepAutoKey` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_stepautokey` (`buildPlanPHID`,`stepAutoKey`),
  KEY `key_plan` (`buildPlanPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildstep`
--

LOCK TABLES `harbormaster_buildstep` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildstep` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildstep` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildsteptransaction`
--

DROP TABLE IF EXISTS `harbormaster_buildsteptransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildsteptransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildsteptransaction`
--

LOCK TABLES `harbormaster_buildsteptransaction` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildsteptransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildsteptransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildtarget`
--

DROP TABLE IF EXISTS `harbormaster_buildtarget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildtarget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `buildPHID` varbinary(64) NOT NULL,
  `buildStepPHID` varbinary(64) NOT NULL,
  `className` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `variables` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `targetStatus` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateStarted` int(10) unsigned DEFAULT NULL,
  `dateCompleted` int(10) unsigned DEFAULT NULL,
  `buildGeneration` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_build` (`buildPHID`,`buildStepPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildtarget`
--

LOCK TABLES `harbormaster_buildtarget` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildtarget` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildtarget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildtransaction`
--

DROP TABLE IF EXISTS `harbormaster_buildtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildtransaction`
--

LOCK TABLES `harbormaster_buildtransaction` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_buildunitmessage`
--

DROP TABLE IF EXISTS `harbormaster_buildunitmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_buildunitmessage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `buildTargetPHID` varbinary(64) NOT NULL,
  `engine` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `namespace` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `result` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `duration` double DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_target` (`buildTargetPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_buildunitmessage`
--

LOCK TABLES `harbormaster_buildunitmessage` WRITE;
/*!40000 ALTER TABLE `harbormaster_buildunitmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_buildunitmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_object`
--

DROP TABLE IF EXISTS `harbormaster_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_object` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_object`
--

LOCK TABLES `harbormaster_object` WRITE;
/*!40000 ALTER TABLE `harbormaster_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harbormaster_scratchtable`
--

DROP TABLE IF EXISTS `harbormaster_scratchtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harbormaster_scratchtable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `bigData` longtext COLLATE utf8mb4_bin,
  `nonmutableData` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harbormaster_scratchtable`
--

LOCK TABLES `harbormaster_scratchtable` WRITE;
/*!40000 ALTER TABLE `harbormaster_scratchtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `harbormaster_scratchtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lisk_counter`
--

DROP TABLE IF EXISTS `lisk_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lisk_counter` (
  `counterName` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `counterValue` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`counterName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lisk_counter`
--

LOCK TABLES `lisk_counter` WRITE;
/*!40000 ALTER TABLE `lisk_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `lisk_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_herald`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_herald` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_herald`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_action`
--

DROP TABLE IF EXISTS `herald_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ruleID` int(10) unsigned NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ruleID` (`ruleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_action`
--

LOCK TABLES `herald_action` WRITE;
/*!40000 ALTER TABLE `herald_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_condition`
--

DROP TABLE IF EXISTS `herald_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_condition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ruleID` int(10) unsigned NOT NULL,
  `fieldName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `fieldCondition` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `value` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ruleID` (`ruleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_condition`
--

LOCK TABLES `herald_condition` WRITE;
/*!40000 ALTER TABLE `herald_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_rule`
--

DROP TABLE IF EXISTS `herald_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `contentType` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `mustMatchAll` tinyint(1) NOT NULL,
  `configVersion` int(10) unsigned NOT NULL DEFAULT '1',
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `repetitionPolicy` int(10) unsigned DEFAULT NULL,
  `ruleType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `isDisabled` int(10) unsigned NOT NULL DEFAULT '0',
  `triggerObjectPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_trigger` (`triggerObjectPHID`),
  KEY `key_author` (`authorPHID`),
  KEY `key_ruletype` (`ruleType`),
  KEY `key_name` (`name`(128))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_rule`
--

LOCK TABLES `herald_rule` WRITE;
/*!40000 ALTER TABLE `herald_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_ruleapplied`
--

DROP TABLE IF EXISTS `herald_ruleapplied`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_ruleapplied` (
  `ruleID` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  PRIMARY KEY (`ruleID`,`phid`),
  KEY `phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_ruleapplied`
--

LOCK TABLES `herald_ruleapplied` WRITE;
/*!40000 ALTER TABLE `herald_ruleapplied` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_ruleapplied` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_ruletransaction`
--

DROP TABLE IF EXISTS `herald_ruletransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_ruletransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_ruletransaction`
--

LOCK TABLES `herald_ruletransaction` WRITE;
/*!40000 ALTER TABLE `herald_ruletransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_ruletransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_ruletransaction_comment`
--

DROP TABLE IF EXISTS `herald_ruletransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_ruletransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_ruletransaction_comment`
--

LOCK TABLES `herald_ruletransaction_comment` WRITE;
/*!40000 ALTER TABLE `herald_ruletransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_ruletransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_savedheader`
--

DROP TABLE IF EXISTS `herald_savedheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_savedheader` (
  `phid` varbinary(64) NOT NULL,
  `header` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_savedheader`
--

LOCK TABLES `herald_savedheader` WRITE;
/*!40000 ALTER TABLE `herald_savedheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_savedheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herald_transcript`
--

DROP TABLE IF EXISTS `herald_transcript`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herald_transcript` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  `host` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `duration` double NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `dryRun` tinyint(1) NOT NULL,
  `objectTranscript` longblob NOT NULL,
  `ruleTranscripts` longblob NOT NULL,
  `conditionTranscripts` longblob NOT NULL,
  `applyTranscripts` longblob NOT NULL,
  `garbageCollected` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  KEY `objectPHID` (`objectPHID`),
  KEY `garbageCollected` (`garbageCollected`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herald_transcript`
--

LOCK TABLES `herald_transcript` WRITE;
/*!40000 ALTER TABLE `herald_transcript` DISABLE KEYS */;
/*!40000 ALTER TABLE `herald_transcript` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_legalpad`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_legalpad` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_legalpad`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legalpad_document`
--

DROP TABLE IF EXISTS `legalpad_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legalpad_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `contributorCount` int(10) unsigned NOT NULL DEFAULT '0',
  `recentContributorPHIDs` longtext COLLATE utf8mb4_bin NOT NULL,
  `creatorPHID` varbinary(64) NOT NULL,
  `versions` int(10) unsigned NOT NULL DEFAULT '0',
  `documentBodyPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `signatureType` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `preamble` longtext COLLATE utf8mb4_bin NOT NULL,
  `requireSignature` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_creator` (`creatorPHID`,`dateModified`),
  KEY `key_required` (`requireSignature`,`dateModified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legalpad_document`
--

LOCK TABLES `legalpad_document` WRITE;
/*!40000 ALTER TABLE `legalpad_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `legalpad_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legalpad_documentbody`
--

DROP TABLE IF EXISTS `legalpad_documentbody`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legalpad_documentbody` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `creatorPHID` varbinary(64) NOT NULL,
  `documentPHID` varbinary(64) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `text` longtext COLLATE utf8mb4_bin,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_document` (`documentPHID`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legalpad_documentbody`
--

LOCK TABLES `legalpad_documentbody` WRITE;
/*!40000 ALTER TABLE `legalpad_documentbody` DISABLE KEYS */;
/*!40000 ALTER TABLE `legalpad_documentbody` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legalpad_documentsignature`
--

DROP TABLE IF EXISTS `legalpad_documentsignature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legalpad_documentsignature` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentPHID` varbinary(64) NOT NULL,
  `documentVersion` int(10) unsigned NOT NULL DEFAULT '0',
  `signatureType` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `signerPHID` varbinary(64) DEFAULT NULL,
  `signerName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `signerEmail` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `signatureData` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `secretKey` binary(20) NOT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `isExemption` tinyint(1) NOT NULL DEFAULT '0',
  `exemptionPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_signer` (`signerPHID`,`dateModified`),
  KEY `secretKey` (`secretKey`),
  KEY `key_document` (`documentPHID`,`signerPHID`,`documentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legalpad_documentsignature`
--

LOCK TABLES `legalpad_documentsignature` WRITE;
/*!40000 ALTER TABLE `legalpad_documentsignature` DISABLE KEYS */;
/*!40000 ALTER TABLE `legalpad_documentsignature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legalpad_transaction`
--

DROP TABLE IF EXISTS `legalpad_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legalpad_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legalpad_transaction`
--

LOCK TABLES `legalpad_transaction` WRITE;
/*!40000 ALTER TABLE `legalpad_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `legalpad_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legalpad_transaction_comment`
--

DROP TABLE IF EXISTS `legalpad_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legalpad_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `documentID` int(10) unsigned DEFAULT NULL,
  `lineNumber` int(10) unsigned NOT NULL,
  `lineLength` int(10) unsigned NOT NULL,
  `fixedState` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `hasReplies` tinyint(1) NOT NULL,
  `replyToCommentPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`),
  UNIQUE KEY `key_draft` (`authorPHID`,`documentID`,`transactionPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legalpad_transaction_comment`
--

LOCK TABLES `legalpad_transaction_comment` WRITE;
/*!40000 ALTER TABLE `legalpad_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `legalpad_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_maniphest`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_maniphest` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_maniphest`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_customfieldnumericindex`
--

DROP TABLE IF EXISTS `maniphest_customfieldnumericindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_customfieldnumericindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`),
  KEY `key_find` (`indexKey`,`indexValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_customfieldnumericindex`
--

LOCK TABLES `maniphest_customfieldnumericindex` WRITE;
/*!40000 ALTER TABLE `maniphest_customfieldnumericindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_customfieldnumericindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_customfieldstorage`
--

DROP TABLE IF EXISTS `maniphest_customfieldstorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_customfieldstorage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `fieldIndex` binary(12) NOT NULL,
  `fieldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`fieldIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_customfieldstorage`
--

LOCK TABLES `maniphest_customfieldstorage` WRITE;
/*!40000 ALTER TABLE `maniphest_customfieldstorage` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_customfieldstorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_customfieldstringindex`
--

DROP TABLE IF EXISTS `maniphest_customfieldstringindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_customfieldstringindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`(64)),
  KEY `key_find` (`indexKey`,`indexValue`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_customfieldstringindex`
--

LOCK TABLES `maniphest_customfieldstringindex` WRITE;
/*!40000 ALTER TABLE `maniphest_customfieldstringindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_customfieldstringindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_nameindex`
--

DROP TABLE IF EXISTS `maniphest_nameindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_nameindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `indexedObjectPHID` varbinary(64) NOT NULL,
  `indexedObjectName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`indexedObjectPHID`),
  KEY `key_name` (`indexedObjectName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_nameindex`
--

LOCK TABLES `maniphest_nameindex` WRITE;
/*!40000 ALTER TABLE `maniphest_nameindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_nameindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_task`
--

DROP TABLE IF EXISTS `maniphest_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `ownerPHID` varbinary(64) DEFAULT NULL,
  `status` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `originalTitle` longtext COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `ownerOrdering` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `originalEmailSource` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `subpriority` double NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `points` double DEFAULT NULL,
  `bridgedObjectPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `key_bridgedobject` (`bridgedObjectPHID`),
  KEY `priority` (`priority`,`status`),
  KEY `status` (`status`),
  KEY `ownerPHID` (`ownerPHID`,`status`),
  KEY `authorPHID` (`authorPHID`,`status`),
  KEY `ownerOrdering` (`ownerOrdering`),
  KEY `priority_2` (`priority`,`subpriority`),
  KEY `key_dateCreated` (`dateCreated`),
  KEY `key_dateModified` (`dateModified`),
  KEY `key_title` (`title`(64)),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_task`
--

LOCK TABLES `maniphest_task` WRITE;
/*!40000 ALTER TABLE `maniphest_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_transaction`
--

DROP TABLE IF EXISTS `maniphest_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_transaction`
--

LOCK TABLES `maniphest_transaction` WRITE;
/*!40000 ALTER TABLE `maniphest_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maniphest_transaction_comment`
--

DROP TABLE IF EXISTS `maniphest_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maniphest_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maniphest_transaction_comment`
--

LOCK TABLES `maniphest_transaction_comment` WRITE;
/*!40000 ALTER TABLE `maniphest_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `maniphest_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_meta_data`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_meta_data` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_meta_data`;

--
-- Table structure for table `patch_status`
--

DROP TABLE IF EXISTS `patch_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patch_status` (
  `patch` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `applied` int(10) unsigned NOT NULL,
  `duration` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`patch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patch_status`
--

LOCK TABLES `patch_status` WRITE;
/*!40000 ALTER TABLE `patch_status` DISABLE KEYS */;
INSERT INTO `patch_status` VALUES ('phabricator:000.project.sql',1466802850,NULL),('phabricator:0000.legacy.sql',1466802850,NULL),('phabricator:001.maniphest_projects.sql',1466802850,NULL),('phabricator:002.oauth.sql',1466802850,NULL),('phabricator:003.more_oauth.sql',1466802851,NULL),('phabricator:004.daemonrepos.sql',1466802851,NULL),('phabricator:005.workers.sql',1466802851,NULL),('phabricator:006.repository.sql',1466802851,NULL),('phabricator:007.daemonlog.sql',1466802851,NULL),('phabricator:008.repoopt.sql',1466802851,NULL),('phabricator:009.repo_summary.sql',1466802851,NULL),('phabricator:010.herald.sql',1466802851,NULL),('phabricator:011.badcommit.sql',1466802851,NULL),('phabricator:012.dropphidtype.sql',1466802851,NULL),('phabricator:013.commitdetail.sql',1466802851,NULL),('phabricator:014.shortcuts.sql',1466802851,NULL),('phabricator:015.preferences.sql',1466802851,NULL),('phabricator:016.userrealnameindex.sql',1466802851,NULL),('phabricator:017.sessionkeys.sql',1466802851,NULL),('phabricator:018.owners.sql',1466802851,NULL),('phabricator:019.arcprojects.sql',1466802851,NULL),('phabricator:020.pathcapital.sql',1466802851,NULL),('phabricator:021.xhpastview.sql',1466802851,NULL),('phabricator:022.differentialcommit.sql',1466802851,NULL),('phabricator:023.dxkeys.sql',1466802851,NULL),('phabricator:024.mlistkeys.sql',1466802851,NULL),('phabricator:025.commentopt.sql',1466802851,NULL),('phabricator:026.diffpropkey.sql',1466802851,NULL),('phabricator:027.metamtakeys.sql',1466802851,NULL),('phabricator:028.systemagent.sql',1466802851,NULL),('phabricator:029.cursors.sql',1466802851,NULL),('phabricator:030.imagemacro.sql',1466802851,NULL),('phabricator:031.workerrace.sql',1466802851,NULL),('phabricator:032.viewtime.sql',1466802851,NULL),('phabricator:033.privtest.sql',1466802851,NULL),('phabricator:034.savedheader.sql',1466802851,NULL),('phabricator:035.proxyimage.sql',1466802851,NULL),('phabricator:036.mailkey.sql',1466802851,NULL),('phabricator:037.setuptest.sql',1466802851,NULL),('phabricator:038.admin.sql',1466802851,NULL),('phabricator:039.userlog.sql',1466802851,NULL),('phabricator:040.transform.sql',1466802851,NULL),('phabricator:041.heraldrepetition.sql',1466802851,NULL),('phabricator:042.commentmetadata.sql',1466802852,NULL),('phabricator:043.pastebin.sql',1466802852,NULL),('phabricator:044.countdown.sql',1466802852,NULL),('phabricator:045.timezone.sql',1466802852,NULL),('phabricator:046.conduittoken.sql',1466802852,NULL),('phabricator:047.projectstatus.sql',1466802852,NULL),('phabricator:048.relationshipkeys.sql',1466802852,NULL),('phabricator:049.projectowner.sql',1466802852,NULL),('phabricator:050.taskdenormal.sql',1466802852,NULL),('phabricator:051.projectfilter.sql',1466802852,NULL),('phabricator:052.pastelanguage.sql',1466802852,NULL),('phabricator:053.feed.sql',1466802852,NULL),('phabricator:054.subscribers.sql',1466802852,NULL),('phabricator:055.add_author_to_files.sql',1466802852,NULL),('phabricator:056.slowvote.sql',1466802852,NULL),('phabricator:057.parsecache.sql',1466802852,NULL),('phabricator:058.missingkeys.sql',1466802852,NULL),('phabricator:059.engines.php',1466802852,NULL),('phabricator:060.phriction.sql',1466802852,NULL),('phabricator:061.phrictioncontent.sql',1466802852,NULL),('phabricator:062.phrictionmenu.sql',1466802852,NULL),('phabricator:063.pasteforks.sql',1466802852,NULL),('phabricator:064.subprojects.sql',1466802852,NULL),('phabricator:065.sshkeys.sql',1466802852,NULL),('phabricator:066.phrictioncontent.sql',1466802852,NULL),('phabricator:067.preferences.sql',1466802852,NULL),('phabricator:068.maniphestauxiliarystorage.sql',1466802852,NULL),('phabricator:069.heraldxscript.sql',1466802852,NULL),('phabricator:070.differentialaux.sql',1466802852,NULL),('phabricator:071.contentsource.sql',1466802852,NULL),('phabricator:072.blamerevert.sql',1466802852,NULL),('phabricator:073.reposymbols.sql',1466802852,NULL),('phabricator:074.affectedpath.sql',1466802852,NULL),('phabricator:075.revisionhash.sql',1466802852,NULL),('phabricator:076.indexedlanguages.sql',1466802852,NULL),('phabricator:077.originalemail.sql',1466802852,NULL),('phabricator:078.nametoken.sql',1466802852,NULL),('phabricator:079.nametokenindex.php',1466802852,NULL),('phabricator:080.filekeys.sql',1466802852,NULL),('phabricator:081.filekeys.php',1466802852,NULL),('phabricator:082.xactionkey.sql',1466802852,NULL),('phabricator:083.dxviewtime.sql',1466802852,NULL),('phabricator:084.pasteauthorkey.sql',1466802852,NULL),('phabricator:085.packagecommitrelationship.sql',1466802852,NULL),('phabricator:086.formeraffil.sql',1466802852,NULL),('phabricator:087.phrictiondelete.sql',1466802853,NULL),('phabricator:088.audit.sql',1466802853,NULL),('phabricator:089.projectwiki.sql',1466802853,NULL),('phabricator:090.forceuniqueprojectnames.php',1466802853,NULL),('phabricator:091.uniqueslugkey.sql',1466802853,NULL),('phabricator:092.dropgithubnotification.sql',1466802853,NULL),('phabricator:093.gitremotes.php',1466802853,NULL),('phabricator:094.phrictioncolumn.sql',1466802853,NULL),('phabricator:095.directory.sql',1466802853,NULL),('phabricator:096.filename.sql',1466802853,NULL),('phabricator:097.heraldruletypes.sql',1466802853,NULL),('phabricator:098.heraldruletypemigration.php',1466802853,NULL),('phabricator:099.drydock.sql',1466802853,NULL),('phabricator:100.projectxaction.sql',1466802853,NULL),('phabricator:101.heraldruleapplied.sql',1466802853,NULL),('phabricator:102.heraldcleanup.php',1466802853,NULL),('phabricator:103.heraldedithistory.sql',1466802853,NULL),('phabricator:104.searchkey.sql',1466802853,NULL),('phabricator:105.mimetype.sql',1466802853,NULL),('phabricator:106.chatlog.sql',1466802853,NULL),('phabricator:107.oauthserver.sql',1466802853,NULL),('phabricator:108.oauthscope.sql',1466802853,NULL),('phabricator:109.oauthclientphidkey.sql',1466802853,NULL),('phabricator:110.commitaudit.sql',1466802853,NULL),('phabricator:111.commitauditmigration.php',1466802853,NULL),('phabricator:112.oauthaccesscoderedirecturi.sql',1466802853,NULL),('phabricator:113.lastreviewer.sql',1466802853,NULL),('phabricator:114.auditrequest.sql',1466802853,NULL),('phabricator:115.prepareutf8.sql',1466802853,NULL),('phabricator:116.utf8-backup-first-expect-wait.sql',1466802855,NULL),('phabricator:117.repositorydescription.php',1466802855,NULL),('phabricator:118.auditinline.sql',1466802855,NULL),('phabricator:119.filehash.sql',1466802855,NULL),('phabricator:120.noop.sql',1466802855,NULL),('phabricator:121.drydocklog.sql',1466802855,NULL),('phabricator:122.flag.sql',1466802855,NULL),('phabricator:123.heraldrulelog.sql',1466802855,NULL),('phabricator:124.subpriority.sql',1466802855,NULL),('phabricator:125.ipv6.sql',1466802855,NULL),('phabricator:126.edges.sql',1466802855,NULL),('phabricator:127.userkeybody.sql',1466802855,NULL),('phabricator:128.phabricatorcom.sql',1466802855,NULL),('phabricator:129.savedquery.sql',1466802855,NULL),('phabricator:130.denormalrevisionquery.sql',1466802855,NULL),('phabricator:131.migraterevisionquery.php',1466802855,NULL),('phabricator:132.phame.sql',1466802855,NULL),('phabricator:133.imagemacro.sql',1466802855,NULL),('phabricator:134.emptysearch.sql',1466802855,NULL),('phabricator:135.datecommitted.sql',1466802855,NULL),('phabricator:136.sex.sql',1466802855,NULL),('phabricator:137.auditmetadata.sql',1466802855,NULL),('phabricator:138.notification.sql',1466802855,NULL),('phabricator:20121209.pholioxactions.sql',1466802856,NULL),('phabricator:20121209.xmacroadd.sql',1466802856,NULL),('phabricator:20121209.xmacromigrate.php',1466802856,NULL),('phabricator:20121209.xmacromigratekey.sql',1466802856,NULL),('phabricator:20121220.generalcache.sql',1466802856,NULL),('phabricator:20121226.config.sql',1466802856,NULL),('phabricator:20130101.confxaction.sql',1466802856,NULL),('phabricator:20130102.metamtareceivedmailmessageidhash.sql',1466802856,NULL),('phabricator:20130103.filemetadata.sql',1466802856,NULL),('phabricator:20130111.conpherence.sql',1466802856,NULL),('phabricator:20130127.altheraldtranscript.sql',1466802856,NULL),('phabricator:20130131.conpherencepics.sql',1466802857,NULL),('phabricator:20130201.revisionunsubscribed.php',1466802856,NULL),('phabricator:20130201.revisionunsubscribed.sql',1466802857,NULL),('phabricator:20130214.chatlogchannel.sql',1466802857,NULL),('phabricator:20130214.chatlogchannelid.sql',1466802857,NULL),('phabricator:20130214.token.sql',1466802857,NULL),('phabricator:20130215.phabricatorfileaddttl.sql',1466802857,NULL),('phabricator:20130217.cachettl.sql',1466802857,NULL),('phabricator:20130218.longdaemon.sql',1466802857,NULL),('phabricator:20130218.updatechannelid.php',1466802857,NULL),('phabricator:20130219.commitsummary.sql',1466802857,NULL),('phabricator:20130219.commitsummarymig.php',1466802857,NULL),('phabricator:20130222.dropchannel.sql',1466802857,NULL),('phabricator:20130226.commitkey.sql',1466802857,NULL),('phabricator:20130304.lintauthor.sql',1466802857,NULL),('phabricator:20130310.xactionmeta.sql',1466802857,NULL),('phabricator:20130317.phrictionedge.sql',1466802857,NULL),('phabricator:20130319.conpherence.sql',1466802857,NULL),('phabricator:20130319.phabricatorfileexplicitupload.sql',1466802857,NULL),('phabricator:20130320.phlux.sql',1466802857,NULL),('phabricator:20130321.token.sql',1466802857,NULL),('phabricator:20130322.phortune.sql',1466802857,NULL),('phabricator:20130323.phortunepayment.sql',1466802857,NULL),('phabricator:20130324.phortuneproduct.sql',1466802857,NULL),('phabricator:20130330.phrequent.sql',1466802857,NULL),('phabricator:20130403.conpherencecache.sql',1466802857,NULL),('phabricator:20130403.conpherencecachemig.php',1466802857,NULL),('phabricator:20130409.commitdrev.php',1466802857,NULL),('phabricator:20130417.externalaccount.sql',1466802857,NULL),('phabricator:20130423.conpherenceindices.sql',1466802857,NULL),('phabricator:20130423.phortunepaymentrevised.sql',1466802857,NULL),('phabricator:20130423.updateexternalaccount.sql',1466802857,NULL),('phabricator:20130426.search_savedquery.sql',1466802857,NULL),('phabricator:20130502.countdownrevamp1.sql',1466802857,NULL),('phabricator:20130502.countdownrevamp2.php',1466802857,NULL),('phabricator:20130502.countdownrevamp3.sql',1466802857,NULL),('phabricator:20130507.releephrqmailkey.sql',1466802857,NULL),('phabricator:20130507.releephrqmailkeypop.php',1466802857,NULL),('phabricator:20130507.releephrqsimplifycols.sql',1466802857,NULL),('phabricator:20130508.releephtransactions.sql',1466802857,NULL),('phabricator:20130508.releephtransactionsmig.php',1466802857,NULL),('phabricator:20130508.search_namedquery.sql',1466802857,NULL),('phabricator:20130513.receviedmailstatus.sql',1466802857,NULL),('phabricator:20130519.diviner.sql',1466802857,NULL),('phabricator:20130521.dropconphimages.sql',1466802857,NULL),('phabricator:20130523.maniphest_owners.sql',1466802857,NULL),('phabricator:20130524.repoxactions.sql',1466802857,NULL),('phabricator:20130529.macroauthor.sql',1466802857,NULL),('phabricator:20130529.macroauthormig.php',1466802857,NULL),('phabricator:20130530.macrodatekey.sql',1466802857,NULL),('phabricator:20130530.pastekeys.sql',1466802857,NULL),('phabricator:20130530.sessionhash.php',1466802857,NULL),('phabricator:20130531.filekeys.sql',1466802857,NULL),('phabricator:20130602.morediviner.sql',1466802858,NULL),('phabricator:20130602.namedqueries.sql',1466802858,NULL),('phabricator:20130606.userxactions.sql',1466802858,NULL),('phabricator:20130607.xaccount.sql',1466802858,NULL),('phabricator:20130611.migrateoauth.php',1466802858,NULL),('phabricator:20130611.nukeldap.php',1466802858,NULL),('phabricator:20130613.authdb.sql',1466802858,NULL),('phabricator:20130619.authconf.php',1466802858,NULL),('phabricator:20130620.diffxactions.sql',1466802858,NULL),('phabricator:20130621.diffcommentphid.sql',1466802858,NULL),('phabricator:20130621.diffcommentphidmig.php',1466802858,NULL),('phabricator:20130621.diffcommentunphid.sql',1466802858,NULL),('phabricator:20130622.doorkeeper.sql',1466802858,NULL),('phabricator:20130628.legalpadv0.sql',1466802858,NULL),('phabricator:20130701.conduitlog.sql',1466802858,NULL),('phabricator:20130703.legalpaddocdenorm.php',1466802858,NULL),('phabricator:20130703.legalpaddocdenorm.sql',1466802858,NULL),('phabricator:20130709.droptimeline.sql',1466802858,NULL),('phabricator:20130709.legalpadsignature.sql',1466802858,NULL),('phabricator:20130711.pholioimageobsolete.php',1466802858,NULL),('phabricator:20130711.pholioimageobsolete.sql',1466802858,NULL),('phabricator:20130711.pholioimageobsolete2.sql',1466802858,NULL),('phabricator:20130711.trimrealnames.php',1466802858,NULL),('phabricator:20130714.votexactions.sql',1466802858,NULL),('phabricator:20130715.votecomments.php',1466802858,NULL),('phabricator:20130715.voteedges.sql',1466802858,NULL),('phabricator:20130716.archivememberlessprojects.php',1466802858,NULL),('phabricator:20130722.pholioreplace.sql',1466802858,NULL),('phabricator:20130723.taskstarttime.sql',1466802858,NULL),('phabricator:20130726.ponderxactions.sql',1466802858,NULL),('phabricator:20130727.ponderquestionstatus.sql',1466802858,NULL),('phabricator:20130728.ponderunique.php',1466802858,NULL),('phabricator:20130728.ponderuniquekey.sql',1466802858,NULL),('phabricator:20130728.ponderxcomment.php',1466802858,NULL),('phabricator:20130731.releephcutpointidentifier.sql',1466802858,NULL),('phabricator:20130731.releephproject.sql',1466802858,NULL),('phabricator:20130731.releephrepoid.sql',1466802858,NULL),('phabricator:20130801.pastexactions.php',1466802858,NULL),('phabricator:20130801.pastexactions.sql',1466802858,NULL),('phabricator:20130802.heraldphid.sql',1466802858,NULL),('phabricator:20130802.heraldphids.php',1466802858,NULL),('phabricator:20130802.heraldphidukey.sql',1466802858,NULL),('phabricator:20130802.heraldxactions.sql',1466802858,NULL),('phabricator:20130805.pasteedges.sql',1466802858,NULL),('phabricator:20130805.pastemailkey.sql',1466802858,NULL),('phabricator:20130805.pastemailkeypop.php',1466802858,NULL),('phabricator:20130814.usercustom.sql',1466802858,NULL),('phabricator:20130820.file-mailkey-populate.php',1466802859,NULL),('phabricator:20130820.filemailkey.sql',1466802859,NULL),('phabricator:20130820.filexactions.sql',1466802859,NULL),('phabricator:20130820.releephxactions.sql',1466802858,NULL),('phabricator:20130826.divinernode.sql',1466802859,NULL),('phabricator:20130912.maniphest.1.touch.sql',1466802859,NULL),('phabricator:20130912.maniphest.2.created.sql',1466802859,NULL),('phabricator:20130912.maniphest.3.nameindex.sql',1466802859,NULL),('phabricator:20130912.maniphest.4.fillindex.php',1466802859,NULL),('phabricator:20130913.maniphest.1.migratesearch.php',1466802859,NULL),('phabricator:20130914.usercustom.sql',1466802859,NULL),('phabricator:20130915.maniphestcustom.sql',1466802859,NULL),('phabricator:20130915.maniphestmigrate.php',1466802859,NULL),('phabricator:20130915.maniphestqdrop.sql',1466802859,NULL),('phabricator:20130919.mfieldconf.php',1466802859,NULL),('phabricator:20130920.repokeyspolicy.sql',1466802859,NULL),('phabricator:20130921.mtransactions.sql',1466802859,NULL),('phabricator:20130921.xmigratemaniphest.php',1466802859,NULL),('phabricator:20130923.mrename.sql',1466802859,NULL),('phabricator:20130924.mdraftkey.sql',1466802859,NULL),('phabricator:20130925.mpolicy.sql',1466802859,NULL),('phabricator:20130925.xpolicy.sql',1466802859,NULL),('phabricator:20130926.dcustom.sql',1466802859,NULL),('phabricator:20130926.dinkeys.sql',1466802859,NULL),('phabricator:20130926.dinline.php',1466802859,NULL),('phabricator:20130927.audiomacro.sql',1466802859,NULL),('phabricator:20130929.filepolicy.sql',1466802859,NULL),('phabricator:20131004.dxedgekey.sql',1466802859,NULL),('phabricator:20131004.dxreviewers.php',1466802859,NULL),('phabricator:20131006.hdisable.sql',1466802859,NULL),('phabricator:20131010.pstorage.sql',1466802859,NULL),('phabricator:20131015.cpolicy.sql',1466802859,NULL),('phabricator:20131020.col1.sql',1466802859,NULL),('phabricator:20131020.harbormaster.sql',1466802859,NULL),('phabricator:20131020.pcustom.sql',1466802859,NULL),('phabricator:20131020.pxaction.sql',1466802859,NULL),('phabricator:20131020.pxactionmig.php',1466802859,NULL),('phabricator:20131025.repopush.sql',1466802859,NULL),('phabricator:20131026.commitstatus.sql',1466802859,NULL),('phabricator:20131030.repostatusmessage.sql',1466802859,NULL),('phabricator:20131031.vcspassword.sql',1466802859,NULL),('phabricator:20131105.buildstep.sql',1466802859,NULL),('phabricator:20131106.diffphid.1.col.sql',1466802859,NULL),('phabricator:20131106.diffphid.2.mig.php',1466802859,NULL),('phabricator:20131106.diffphid.3.key.sql',1466802859,NULL),('phabricator:20131106.nuance-v0.sql',1466802859,NULL),('phabricator:20131107.buildlog.sql',1466802859,NULL),('phabricator:20131112.userverified.1.col.sql',1466802859,NULL),('phabricator:20131112.userverified.2.mig.php',1466802859,NULL),('phabricator:20131118.ownerorder.php',1466802859,NULL),('phabricator:20131119.passphrase.sql',1466802859,NULL),('phabricator:20131120.nuancesourcetype.sql',1466802860,NULL),('phabricator:20131121.passphraseedge.sql',1466802860,NULL),('phabricator:20131121.repocredentials.1.col.sql',1466802860,NULL),('phabricator:20131121.repocredentials.2.mig.php',1466802860,NULL),('phabricator:20131122.repomirror.sql',1466802860,NULL),('phabricator:20131123.drydockblueprintpolicy.sql',1466802860,NULL),('phabricator:20131129.drydockresourceblueprint.sql',1466802860,NULL),('phabricator:20131204.pushlog.sql',1466802860,NULL),('phabricator:20131205.buildsteporder.sql',1466802860,NULL),('phabricator:20131205.buildstepordermig.php',1466802860,NULL),('phabricator:20131205.buildtargets.sql',1466802860,NULL),('phabricator:20131206.phragment.sql',1466802860,NULL),('phabricator:20131206.phragmentnull.sql',1466802860,NULL),('phabricator:20131208.phragmentsnapshot.sql',1466802860,NULL),('phabricator:20131211.phragmentedges.sql',1466802860,NULL),('phabricator:20131217.pushlogphid.1.col.sql',1466802860,NULL),('phabricator:20131217.pushlogphid.2.mig.php',1466802860,NULL),('phabricator:20131217.pushlogphid.3.key.sql',1466802860,NULL),('phabricator:20131219.pxdrop.sql',1466802860,NULL),('phabricator:20131224.harbormanual.sql',1466802860,NULL),('phabricator:20131227.heraldobject.sql',1466802860,NULL),('phabricator:20131231.dropshortcut.sql',1466802860,NULL),('phabricator:20131302.maniphestvalue.sql',1466802857,NULL),('phabricator:20140104.harbormastercmd.sql',1466802860,NULL),('phabricator:20140106.macromailkey.1.sql',1466802860,NULL),('phabricator:20140106.macromailkey.2.php',1466802860,NULL),('phabricator:20140108.ddbpname.1.sql',1466802860,NULL),('phabricator:20140108.ddbpname.2.php',1466802860,NULL),('phabricator:20140109.ddxactions.sql',1466802860,NULL),('phabricator:20140109.projectcolumnsdates.sql',1466802860,NULL),('phabricator:20140113.legalpadsig.1.sql',1466802860,NULL),('phabricator:20140113.legalpadsig.2.php',1466802860,NULL),('phabricator:20140115.auth.1.id.sql',1466802860,NULL),('phabricator:20140115.auth.2.expires.sql',1466802860,NULL),('phabricator:20140115.auth.3.unlimit.php',1466802860,NULL),('phabricator:20140115.legalpadsigkey.sql',1466802860,NULL),('phabricator:20140116.reporefcursor.sql',1466802860,NULL),('phabricator:20140126.diff.1.parentrevisionid.sql',1466802860,NULL),('phabricator:20140126.diff.2.repositoryphid.sql',1466802860,NULL),('phabricator:20140130.dash.1.board.sql',1466802860,NULL),('phabricator:20140130.dash.2.panel.sql',1466802860,NULL),('phabricator:20140130.dash.3.boardxaction.sql',1466802860,NULL),('phabricator:20140130.dash.4.panelxaction.sql',1466802860,NULL),('phabricator:20140130.mail.1.retry.sql',1466802860,NULL),('phabricator:20140130.mail.2.next.sql',1466802860,NULL),('phabricator:20140201.gc.1.mailsent.sql',1466802860,NULL),('phabricator:20140201.gc.2.mailreceived.sql',1466802860,NULL),('phabricator:20140205.cal.1.rename.sql',1466802860,NULL),('phabricator:20140205.cal.2.phid-col.sql',1466802860,NULL),('phabricator:20140205.cal.3.phid-mig.php',1466802860,NULL),('phabricator:20140205.cal.4.phid-key.sql',1466802860,NULL),('phabricator:20140210.herald.rule-condition-mig.php',1466802860,NULL),('phabricator:20140210.projcfield.1.blurb.php',1466802860,NULL),('phabricator:20140210.projcfield.2.piccol.sql',1466802860,NULL),('phabricator:20140210.projcfield.3.picmig.sql',1466802860,NULL),('phabricator:20140210.projcfield.4.memmig.sql',1466802860,NULL),('phabricator:20140210.projcfield.5.dropprofile.sql',1466802860,NULL),('phabricator:20140211.dx.1.nullablechangesetid.sql',1466802860,NULL),('phabricator:20140211.dx.2.migcommenttext.php',1466802860,NULL),('phabricator:20140211.dx.3.migsubscriptions.sql',1466802860,NULL),('phabricator:20140211.dx.999.drop.relationships.sql',1466802860,NULL),('phabricator:20140212.dx.1.armageddon.php',1466802860,NULL),('phabricator:20140214.clean.1.legacycommentid.sql',1466802860,NULL),('phabricator:20140214.clean.2.dropcomment.sql',1466802860,NULL),('phabricator:20140214.clean.3.dropinline.sql',1466802860,NULL),('phabricator:20140218.differentialdraft.sql',1466802860,NULL),('phabricator:20140218.passwords.1.extend.sql',1466802860,NULL),('phabricator:20140218.passwords.2.prefix.sql',1466802860,NULL),('phabricator:20140218.passwords.3.vcsextend.sql',1466802860,NULL),('phabricator:20140218.passwords.4.vcs.php',1466802860,NULL),('phabricator:20140223.bigutf8scratch.sql',1466802860,NULL),('phabricator:20140224.dxclean.1.datecommitted.sql',1466802860,NULL),('phabricator:20140226.dxcustom.1.fielddata.php',1466802860,NULL),('phabricator:20140226.dxcustom.99.drop.sql',1466802860,NULL),('phabricator:20140228.dxcomment.1.sql',1466802860,NULL),('phabricator:20140305.diviner.1.slugcol.sql',1466802861,NULL),('phabricator:20140305.diviner.2.slugkey.sql',1466802861,NULL),('phabricator:20140311.mdroplegacy.sql',1466802861,NULL),('phabricator:20140314.projectcolumn.1.statuscol.sql',1466802861,NULL),('phabricator:20140314.projectcolumn.2.statuskey.sql',1466802861,NULL),('phabricator:20140317.mupdatedkey.sql',1466802861,NULL),('phabricator:20140321.harbor.1.bxaction.sql',1466802861,NULL),('phabricator:20140321.mstatus.1.col.sql',1466802861,NULL),('phabricator:20140321.mstatus.2.mig.php',1466802861,NULL),('phabricator:20140323.harbor.1.renames.php',1466802861,NULL),('phabricator:20140323.harbor.2.message.sql',1466802861,NULL),('phabricator:20140325.push.1.event.sql',1466802861,NULL),('phabricator:20140325.push.2.eventphid.sql',1466802861,NULL),('phabricator:20140325.push.3.groups.php',1466802861,NULL),('phabricator:20140325.push.4.prune.sql',1466802861,NULL),('phabricator:20140326.project.1.colxaction.sql',1466802861,NULL),('phabricator:20140328.releeph.1.productxaction.sql',1466802861,NULL),('phabricator:20140330.flagtext.sql',1466802861,NULL),('phabricator:20140402.actionlog.sql',1466802861,NULL),('phabricator:20140410.accountsecret.1.sql',1466802861,NULL),('phabricator:20140410.accountsecret.2.php',1466802861,NULL),('phabricator:20140416.harbor.1.sql',1466802861,NULL),('phabricator:20140420.rel.1.objectphid.sql',1466802861,NULL),('phabricator:20140420.rel.2.objectmig.php',1466802861,NULL),('phabricator:20140421.slowvotecolumnsisclosed.sql',1466802861,NULL),('phabricator:20140423.session.1.hisec.sql',1466802861,NULL),('phabricator:20140427.mfactor.1.sql',1466802861,NULL),('phabricator:20140430.auth.1.partial.sql',1466802861,NULL),('phabricator:20140430.dash.1.paneltype.sql',1466802861,NULL),('phabricator:20140430.dash.2.edge.sql',1466802861,NULL),('phabricator:20140501.passphraselockcredential.sql',1466802861,NULL),('phabricator:20140501.remove.1.dlog.sql',1466802861,NULL),('phabricator:20140507.smstable.sql',1466802861,NULL),('phabricator:20140509.coverage.1.sql',1466802861,NULL),('phabricator:20140509.dashboardlayoutconfig.sql',1466802861,NULL),('phabricator:20140512.dparents.1.sql',1466802861,NULL),('phabricator:20140514.harbormasterbuildabletransaction.sql',1466802861,NULL),('phabricator:20140514.pholiomockclose.sql',1466802861,NULL),('phabricator:20140515.trust-emails.sql',1466802861,NULL),('phabricator:20140517.dxbinarycache.sql',1466802861,NULL),('phabricator:20140518.dxmorebinarycache.sql',1466802861,NULL),('phabricator:20140519.dashboardinstall.sql',1466802861,NULL),('phabricator:20140520.authtemptoken.sql',1466802861,NULL),('phabricator:20140521.projectslug.1.create.sql',1466802861,NULL),('phabricator:20140521.projectslug.2.mig.php',1466802861,NULL),('phabricator:20140522.projecticon.sql',1466802861,NULL),('phabricator:20140524.auth.mfa.cache.sql',1466802861,NULL),('phabricator:20140525.hunkmodern.sql',1466802861,NULL),('phabricator:20140615.pholioedit.1.sql',1466802861,NULL),('phabricator:20140615.pholioedit.2.sql',1466802861,NULL),('phabricator:20140617.daemon.explicit-argv.sql',1466802861,NULL),('phabricator:20140617.daemonlog.sql',1466802861,NULL),('phabricator:20140624.projcolor.1.sql',1466802861,NULL),('phabricator:20140624.projcolor.2.sql',1466802861,NULL),('phabricator:20140629.dasharchive.1.sql',1466802861,NULL),('phabricator:20140629.legalsig.1.sql',1466802861,NULL),('phabricator:20140629.legalsig.2.php',1466802861,NULL),('phabricator:20140701.legalexemption.1.sql',1466802861,NULL),('phabricator:20140701.legalexemption.2.sql',1466802861,NULL),('phabricator:20140703.legalcorp.1.sql',1466802861,NULL),('phabricator:20140703.legalcorp.2.sql',1466802861,NULL),('phabricator:20140703.legalcorp.3.sql',1466802861,NULL),('phabricator:20140703.legalcorp.4.sql',1466802861,NULL),('phabricator:20140703.legalcorp.5.sql',1466802861,NULL),('phabricator:20140704.harbormasterstep.1.sql',1466802861,NULL),('phabricator:20140704.harbormasterstep.2.sql',1466802861,NULL),('phabricator:20140704.legalpreamble.1.sql',1466802861,NULL),('phabricator:20140706.harbormasterdepend.1.php',1466802861,NULL),('phabricator:20140706.pedge.1.sql',1466802861,NULL),('phabricator:20140711.pnames.1.sql',1466802861,NULL),('phabricator:20140711.pnames.2.php',1466802861,NULL),('phabricator:20140711.workerpriority.sql',1466802862,NULL),('phabricator:20140712.projcoluniq.sql',1466802862,NULL),('phabricator:20140721.phortune.1.cart.sql',1466802862,NULL),('phabricator:20140721.phortune.2.purchase.sql',1466802862,NULL),('phabricator:20140721.phortune.3.charge.sql',1466802862,NULL),('phabricator:20140721.phortune.4.cartstatus.sql',1466802862,NULL),('phabricator:20140721.phortune.5.cstatusdefault.sql',1466802862,NULL),('phabricator:20140721.phortune.6.onetimecharge.sql',1466802862,NULL),('phabricator:20140721.phortune.7.nullmethod.sql',1466802862,NULL),('phabricator:20140722.appname.php',1466802862,NULL),('phabricator:20140722.audit.1.xactions.sql',1466802862,NULL),('phabricator:20140722.audit.2.comments.sql',1466802862,NULL),('phabricator:20140722.audit.3.miginlines.php',1466802862,NULL),('phabricator:20140722.audit.4.migtext.php',1466802862,NULL),('phabricator:20140722.renameauth.php',1466802862,NULL),('phabricator:20140723.apprenamexaction.sql',1466802862,NULL),('phabricator:20140725.audit.1.migxactions.php',1466802862,NULL),('phabricator:20140731.audit.1.subscribers.php',1466802862,NULL),('phabricator:20140731.cancdn.php',1466802862,NULL),('phabricator:20140731.harbormasterstepdesc.sql',1466802862,NULL),('phabricator:20140805.boardcol.1.sql',1466802862,NULL),('phabricator:20140805.boardcol.2.php',1466802862,NULL),('phabricator:20140807.harbormastertargettime.sql',1466802862,NULL),('phabricator:20140808.boardprop.1.sql',1466802862,NULL),('phabricator:20140808.boardprop.2.sql',1466802862,NULL),('phabricator:20140808.boardprop.3.php',1466802862,NULL),('phabricator:20140811.blob.1.sql',1466802862,NULL),('phabricator:20140811.blob.2.sql',1466802862,NULL),('phabricator:20140812.projkey.1.sql',1466802862,NULL),('phabricator:20140812.projkey.2.sql',1466802862,NULL),('phabricator:20140814.passphrasecredentialconduit.sql',1466802862,NULL),('phabricator:20140815.cancdncase.php',1466802862,NULL),('phabricator:20140818.harbormasterindex.1.sql',1466802862,NULL),('phabricator:20140821.harbormasterbuildgen.1.sql',1466802862,NULL),('phabricator:20140822.daemonenvhash.sql',1466802862,NULL),('phabricator:20140902.almanacdevice.1.sql',1466802862,NULL),('phabricator:20140904.macroattach.php',1466802862,NULL),('phabricator:20140911.fund.1.initiative.sql',1466802862,NULL),('phabricator:20140911.fund.2.xaction.sql',1466802862,NULL),('phabricator:20140911.fund.3.edge.sql',1466802862,NULL),('phabricator:20140911.fund.4.backer.sql',1466802862,NULL),('phabricator:20140911.fund.5.backxaction.sql',1466802862,NULL),('phabricator:20140914.betaproto.php',1466802862,NULL),('phabricator:20140917.project.canlock.sql',1466802862,NULL),('phabricator:20140918.schema.1.dropaudit.sql',1466802862,NULL),('phabricator:20140918.schema.2.dropauditinline.sql',1466802862,NULL),('phabricator:20140918.schema.3.wipecache.sql',1466802862,NULL),('phabricator:20140918.schema.4.cachetype.sql',1466802862,NULL),('phabricator:20140918.schema.5.slowvote.sql',1466802862,NULL),('phabricator:20140919.schema.01.calstatus.sql',1466802862,NULL),('phabricator:20140919.schema.02.calname.sql',1466802862,NULL),('phabricator:20140919.schema.03.dropaux.sql',1466802862,NULL),('phabricator:20140919.schema.04.droptaskproj.sql',1466802862,NULL),('phabricator:20140926.schema.01.droprelev.sql',1466802862,NULL),('phabricator:20140926.schema.02.droprelreqev.sql',1466802862,NULL),('phabricator:20140926.schema.03.dropldapinfo.sql',1466802862,NULL),('phabricator:20140926.schema.04.dropoauthinfo.sql',1466802862,NULL),('phabricator:20140926.schema.05.dropprojaffil.sql',1466802862,NULL),('phabricator:20140926.schema.06.dropsubproject.sql',1466802862,NULL),('phabricator:20140926.schema.07.droppondcom.sql',1466802862,NULL),('phabricator:20140927.schema.01.dropsearchq.sql',1466802862,NULL),('phabricator:20140927.schema.02.pholio1.sql',1466802862,NULL),('phabricator:20140927.schema.03.pholio2.sql',1466802862,NULL),('phabricator:20140927.schema.04.pholio3.sql',1466802862,NULL),('phabricator:20140927.schema.05.phragment1.sql',1466802862,NULL),('phabricator:20140927.schema.06.releeph1.sql',1466802862,NULL),('phabricator:20141001.schema.01.version.sql',1466802862,NULL),('phabricator:20141001.schema.02.taskmail.sql',1466802862,NULL),('phabricator:20141002.schema.01.liskcounter.sql',1466802862,NULL),('phabricator:20141002.schema.02.draftnull.sql',1466802862,NULL),('phabricator:20141004.currency.01.sql',1466802862,NULL),('phabricator:20141004.currency.02.sql',1466802862,NULL),('phabricator:20141004.currency.03.sql',1466802862,NULL),('phabricator:20141004.currency.04.sql',1466802862,NULL),('phabricator:20141004.currency.05.sql',1466802862,NULL),('phabricator:20141004.currency.06.sql',1466802862,NULL),('phabricator:20141004.harborliskcounter.sql',1466802862,NULL),('phabricator:20141005.phortuneproduct.sql',1466802862,NULL),('phabricator:20141006.phortunecart.sql',1466802862,NULL),('phabricator:20141006.phortunemerchant.sql',1466802863,NULL),('phabricator:20141006.phortunemerchantx.sql',1466802863,NULL),('phabricator:20141007.fundmerchant.sql',1466802863,NULL),('phabricator:20141007.fundrisks.sql',1466802863,NULL),('phabricator:20141007.fundtotal.sql',1466802863,NULL),('phabricator:20141007.phortunecartmerchant.sql',1466802863,NULL),('phabricator:20141007.phortunecharge.sql',1466802863,NULL),('phabricator:20141007.phortunepayment.sql',1466802863,NULL),('phabricator:20141007.phortuneprovider.sql',1466802863,NULL),('phabricator:20141007.phortuneproviderx.sql',1466802863,NULL),('phabricator:20141008.phortunemerchdesc.sql',1466802863,NULL),('phabricator:20141008.phortuneprovdis.sql',1466802863,NULL),('phabricator:20141008.phortunerefund.sql',1466802863,NULL),('phabricator:20141010.fundmailkey.sql',1466802863,NULL),('phabricator:20141011.phortunemerchedit.sql',1466802863,NULL),('phabricator:20141012.phortunecartxaction.sql',1466802863,NULL),('phabricator:20141013.phortunecartkey.sql',1466802863,NULL),('phabricator:20141016.almanac.device.sql',1466802863,NULL),('phabricator:20141016.almanac.dxaction.sql',1466802863,NULL),('phabricator:20141016.almanac.interface.sql',1466802863,NULL),('phabricator:20141016.almanac.network.sql',1466802863,NULL),('phabricator:20141016.almanac.nxaction.sql',1466802863,NULL),('phabricator:20141016.almanac.service.sql',1466802863,NULL),('phabricator:20141016.almanac.sxaction.sql',1466802863,NULL),('phabricator:20141017.almanac.binding.sql',1466802863,NULL),('phabricator:20141017.almanac.bxaction.sql',1466802863,NULL),('phabricator:20141025.phriction.1.xaction.sql',1466802863,NULL),('phabricator:20141025.phriction.2.xaction.sql',1466802863,NULL),('phabricator:20141025.phriction.mailkey.sql',1466802863,NULL),('phabricator:20141103.almanac.1.delprop.sql',1466802863,NULL),('phabricator:20141103.almanac.2.addprop.sql',1466802863,NULL),('phabricator:20141104.almanac.3.edge.sql',1466802863,NULL),('phabricator:20141105.ssh.1.rename.sql',1466802863,NULL),('phabricator:20141106.dropold.sql',1466802863,NULL),('phabricator:20141106.uniqdrafts.php',1466802863,NULL),('phabricator:20141107.phriction.policy.1.sql',1466802863,NULL),('phabricator:20141107.phriction.policy.2.php',1466802863,NULL),('phabricator:20141107.phriction.popkeys.php',1466802863,NULL),('phabricator:20141107.ssh.1.colname.sql',1466802863,NULL),('phabricator:20141107.ssh.2.keyhash.sql',1466802863,NULL),('phabricator:20141107.ssh.3.keyindex.sql',1466802863,NULL),('phabricator:20141107.ssh.4.keymig.php',1466802863,NULL),('phabricator:20141107.ssh.5.indexnull.sql',1466802863,NULL),('phabricator:20141107.ssh.6.indexkey.sql',1466802863,NULL),('phabricator:20141107.ssh.7.colnull.sql',1466802863,NULL),('phabricator:20141113.auditdupes.php',1466802863,NULL),('phabricator:20141118.diffxaction.sql',1466802863,NULL),('phabricator:20141119.commitpedge.sql',1466802863,NULL),('phabricator:20141119.differential.diff.policy.sql',1466802863,NULL),('phabricator:20141119.sshtrust.sql',1466802863,NULL),('phabricator:20141123.taskpriority.1.sql',1466802863,NULL),('phabricator:20141123.taskpriority.2.sql',1466802863,NULL),('phabricator:20141210.maniphestsubscribersmig.1.sql',1466802863,NULL),('phabricator:20141210.maniphestsubscribersmig.2.sql',1466802863,NULL),('phabricator:20141210.reposervice.sql',1466802863,NULL),('phabricator:20141212.conduittoken.sql',1466802863,NULL),('phabricator:20141215.almanacservicetype.sql',1466802863,NULL),('phabricator:20141217.almanacdevicelock.sql',1466802864,NULL),('phabricator:20141217.almanaclock.sql',1466802864,NULL),('phabricator:20141218.maniphestcctxn.php',1466802864,NULL),('phabricator:20141222.maniphestprojtxn.php',1466802864,NULL),('phabricator:20141223.daemonloguser.sql',1466802864,NULL),('phabricator:20141223.daemonobjectphid.sql',1466802864,NULL),('phabricator:20141230.pasteeditpolicycolumn.sql',1466802864,NULL),('phabricator:20141230.pasteeditpolicyexisting.sql',1466802864,NULL),('phabricator:20150102.policyname.php',1466802864,NULL),('phabricator:20150102.tasksubscriber.sql',1466802864,NULL),('phabricator:20150105.conpsearch.sql',1466802864,NULL),('phabricator:20150114.oauthserver.client.policy.sql',1466802864,NULL),('phabricator:20150115.applicationemails.sql',1466802864,NULL),('phabricator:20150115.trigger.1.sql',1466802864,NULL),('phabricator:20150115.trigger.2.sql',1466802864,NULL),('phabricator:20150116.maniphestapplicationemails.php',1466802864,NULL),('phabricator:20150120.maniphestdefaultauthor.php',1466802864,NULL),('phabricator:20150124.subs.1.sql',1466802864,NULL),('phabricator:20150129.pastefileapplicationemails.php',1466802864,NULL),('phabricator:20150130.phortune.1.subphid.sql',1466802864,NULL),('phabricator:20150130.phortune.2.subkey.sql',1466802864,NULL),('phabricator:20150131.phortune.1.defaultpayment.sql',1466802864,NULL),('phabricator:20150205.authprovider.autologin.sql',1466802864,NULL),('phabricator:20150205.daemonenv.sql',1466802864,NULL),('phabricator:20150209.invite.sql',1466802864,NULL),('phabricator:20150209.oauthclient.trust.sql',1466802864,NULL),('phabricator:20150210.invitephid.sql',1466802864,NULL),('phabricator:20150212.legalpad.session.1.sql',1466802864,NULL),('phabricator:20150212.legalpad.session.2.sql',1466802864,NULL),('phabricator:20150219.scratch.nonmutable.sql',1466802864,NULL),('phabricator:20150223.daemon.1.id.sql',1466802864,NULL),('phabricator:20150223.daemon.2.idlegacy.sql',1466802864,NULL),('phabricator:20150223.daemon.3.idkey.sql',1466802864,NULL),('phabricator:20150312.filechunk.1.sql',1466802864,NULL),('phabricator:20150312.filechunk.2.sql',1466802864,NULL),('phabricator:20150312.filechunk.3.sql',1466802864,NULL),('phabricator:20150317.conpherence.isroom.1.sql',1466802864,NULL),('phabricator:20150317.conpherence.isroom.2.sql',1466802864,NULL),('phabricator:20150317.conpherence.policy.sql',1466802864,NULL),('phabricator:20150410.nukeruleedit.sql',1466802864,NULL),('phabricator:20150420.invoice.1.sql',1466802864,NULL),('phabricator:20150420.invoice.2.sql',1466802864,NULL),('phabricator:20150425.isclosed.sql',1466802864,NULL),('phabricator:20150427.calendar.1.edge.sql',1466802864,NULL),('phabricator:20150427.calendar.1.xaction.sql',1466802864,NULL),('phabricator:20150427.calendar.2.xaction.sql',1466802864,NULL),('phabricator:20150428.calendar.1.iscancelled.sql',1466802864,NULL),('phabricator:20150428.calendar.1.name.sql',1466802864,NULL),('phabricator:20150429.calendar.1.invitee.sql',1466802864,NULL),('phabricator:20150430.calendar.1.policies.sql',1466802864,NULL),('phabricator:20150430.multimeter.1.sql',1466802864,NULL),('phabricator:20150430.multimeter.2.host.sql',1466802864,NULL),('phabricator:20150430.multimeter.3.viewer.sql',1466802864,NULL),('phabricator:20150430.multimeter.4.context.sql',1466802864,NULL),('phabricator:20150430.multimeter.5.label.sql',1466802864,NULL),('phabricator:20150501.calendar.1.reply.sql',1466802864,NULL),('phabricator:20150501.calendar.2.reply.php',1466802864,NULL),('phabricator:20150501.conpherencepics.sql',1466802865,NULL),('phabricator:20150503.repositorysymbols.1.sql',1466802865,NULL),('phabricator:20150503.repositorysymbols.2.php',1466802865,NULL),('phabricator:20150503.repositorysymbols.3.sql',1466802865,NULL),('phabricator:20150504.symbolsproject.1.php',1466802865,NULL),('phabricator:20150504.symbolsproject.2.sql',1466802865,NULL),('phabricator:20150506.calendarunnamedevents.1.php',1466802865,NULL),('phabricator:20150507.calendar.1.isallday.sql',1466802865,NULL),('phabricator:20150513.user.cache.1.sql',1466802865,NULL),('phabricator:20150514.calendar.status.sql',1466802865,NULL),('phabricator:20150514.phame.blog.xaction.sql',1466802865,NULL),('phabricator:20150514.user.cache.2.sql',1466802865,NULL),('phabricator:20150515.phame.post.xaction.sql',1466802865,NULL),('phabricator:20150515.project.mailkey.1.sql',1466802865,NULL),('phabricator:20150515.project.mailkey.2.php',1466802865,NULL),('phabricator:20150519.calendar.calendaricon.sql',1466802865,NULL),('phabricator:20150521.releephrepository.sql',1466802865,NULL),('phabricator:20150525.diff.hidden.1.sql',1466802865,NULL),('phabricator:20150526.owners.mailkey.1.sql',1466802865,NULL),('phabricator:20150526.owners.mailkey.2.php',1466802865,NULL),('phabricator:20150526.owners.xaction.sql',1466802865,NULL),('phabricator:20150527.calendar.recurringevents.sql',1466802865,NULL),('phabricator:20150601.spaces.1.namespace.sql',1466802865,NULL),('phabricator:20150601.spaces.2.xaction.sql',1466802865,NULL),('phabricator:20150602.mlist.1.sql',1466802865,NULL),('phabricator:20150602.mlist.2.php',1466802865,NULL),('phabricator:20150604.spaces.1.sql',1466802865,NULL),('phabricator:20150605.diviner.edges.sql',1466802865,NULL),('phabricator:20150605.diviner.editPolicy.sql',1466802865,NULL),('phabricator:20150605.diviner.xaction.sql',1466802865,NULL),('phabricator:20150606.mlist.1.php',1466802865,NULL),('phabricator:20150609.inline.sql',1466802865,NULL),('phabricator:20150609.spaces.1.pholio.sql',1466802865,NULL),('phabricator:20150609.spaces.2.maniphest.sql',1466802865,NULL),('phabricator:20150610.spaces.1.desc.sql',1466802865,NULL),('phabricator:20150610.spaces.2.edge.sql',1466802865,NULL),('phabricator:20150610.spaces.3.archive.sql',1466802865,NULL),('phabricator:20150611.spaces.1.mailxaction.sql',1466802865,NULL),('phabricator:20150611.spaces.2.appmail.sql',1466802865,NULL),('phabricator:20150616.divinerrepository.sql',1466802865,NULL),('phabricator:20150617.harbor.1.lint.sql',1466802865,NULL),('phabricator:20150617.harbor.2.unit.sql',1466802865,NULL),('phabricator:20150618.harbor.1.planauto.sql',1466802866,NULL),('phabricator:20150618.harbor.2.stepauto.sql',1466802866,NULL),('phabricator:20150618.harbor.3.buildauto.sql',1466802866,NULL),('phabricator:20150619.conpherencerooms.1.sql',1466802866,NULL),('phabricator:20150619.conpherencerooms.2.sql',1466802866,NULL),('phabricator:20150619.conpherencerooms.3.sql',1466802866,NULL),('phabricator:20150621.phrase.1.sql',1466802866,NULL),('phabricator:20150621.phrase.2.sql',1466802866,NULL),('phabricator:20150622.bulk.1.job.sql',1466802866,NULL),('phabricator:20150622.bulk.2.task.sql',1466802866,NULL),('phabricator:20150622.bulk.3.xaction.sql',1466802866,NULL),('phabricator:20150622.bulk.4.edge.sql',1466802866,NULL),('phabricator:20150622.metamta.1.phid-col.sql',1466802866,NULL),('phabricator:20150622.metamta.2.phid-mig.php',1466802866,NULL),('phabricator:20150622.metamta.3.phid-key.sql',1466802866,NULL),('phabricator:20150622.metamta.4.actor-phid-col.sql',1466802866,NULL),('phabricator:20150622.metamta.5.actor-phid-mig.php',1466802866,NULL),('phabricator:20150622.metamta.6.actor-phid-key.sql',1466802866,NULL),('phabricator:20150624.spaces.1.repo.sql',1466802866,NULL),('phabricator:20150626.spaces.1.calendar.sql',1466802866,NULL),('phabricator:20150630.herald.1.sql',1466802866,NULL),('phabricator:20150630.herald.2.sql',1466802866,NULL),('phabricator:20150701.herald.1.sql',1466802866,NULL),('phabricator:20150701.herald.2.sql',1466802866,NULL),('phabricator:20150702.spaces.1.slowvote.sql',1466802866,NULL),('phabricator:20150706.herald.1.sql',1466802866,NULL),('phabricator:20150707.herald.1.sql',1466802866,NULL),('phabricator:20150708.arcanistproject.sql',1466802866,NULL),('phabricator:20150708.herald.1.sql',1466802866,NULL),('phabricator:20150708.herald.2.sql',1466802866,NULL),('phabricator:20150708.herald.3.sql',1466802866,NULL),('phabricator:20150712.badges.1.sql',1466802866,NULL),('phabricator:20150714.spaces.countdown.1.sql',1466802866,NULL),('phabricator:20150717.herald.1.sql',1466802866,NULL),('phabricator:20150719.countdown.1.sql',1466802866,NULL),('phabricator:20150719.countdown.2.sql',1466802866,NULL),('phabricator:20150719.countdown.3.sql',1466802866,NULL),('phabricator:20150721.phurl.1.url.sql',1466802866,NULL),('phabricator:20150721.phurl.2.xaction.sql',1466802866,NULL),('phabricator:20150721.phurl.3.xactioncomment.sql',1466802866,NULL),('phabricator:20150721.phurl.4.url.sql',1466802866,NULL),('phabricator:20150721.phurl.5.edge.sql',1466802866,NULL),('phabricator:20150721.phurl.6.alias.sql',1466802866,NULL),('phabricator:20150721.phurl.7.authorphid.sql',1466802866,NULL),('phabricator:20150722.dashboard.1.sql',1466802866,NULL),('phabricator:20150722.dashboard.2.sql',1466802866,NULL),('phabricator:20150723.countdown.1.sql',1466802866,NULL),('phabricator:20150724.badges.comments.1.sql',1466802866,NULL),('phabricator:20150724.countdown.comments.1.sql',1466802866,NULL),('phabricator:20150725.badges.mailkey.1.sql',1466802866,NULL),('phabricator:20150725.badges.mailkey.2.php',1466802866,NULL),('phabricator:20150725.badges.viewpolicy.3.sql',1466802866,NULL),('phabricator:20150725.countdown.mailkey.1.sql',1466802866,NULL),('phabricator:20150725.countdown.mailkey.2.php',1466802866,NULL),('phabricator:20150725.slowvote.mailkey.1.sql',1466802866,NULL),('phabricator:20150725.slowvote.mailkey.2.php',1466802866,NULL),('phabricator:20150727.heraldaction.1.sql',1466802866,NULL),('phabricator:20150730.herald.1.sql',1466802866,NULL),('phabricator:20150730.herald.2.sql',1466802866,NULL),('phabricator:20150730.herald.3.sql',1466802866,NULL),('phabricator:20150730.herald.4.sql',1466802866,NULL),('phabricator:20150730.herald.5.sql',1466802866,NULL),('phabricator:20150730.herald.6.sql',1466802866,NULL),('phabricator:20150730.herald.7.sql',1466802866,NULL),('phabricator:20150803.herald.1.sql',1466802866,NULL),('phabricator:20150803.herald.2.sql',1466802866,NULL),('phabricator:20150804.ponder.answer.mailkey.1.sql',1466802866,NULL),('phabricator:20150804.ponder.answer.mailkey.2.php',1466802866,NULL),('phabricator:20150804.ponder.question.1.sql',1466802867,NULL),('phabricator:20150804.ponder.question.2.sql',1466802867,NULL),('phabricator:20150804.ponder.question.3.sql',1466802867,NULL),('phabricator:20150804.ponder.spaces.4.sql',1466802867,NULL),('phabricator:20150805.paste.status.1.sql',1466802867,NULL),('phabricator:20150805.paste.status.2.sql',1466802867,NULL),('phabricator:20150806.ponder.answer.1.sql',1466802867,NULL),('phabricator:20150806.ponder.editpolicy.2.sql',1466802867,NULL),('phabricator:20150806.ponder.status.1.sql',1466802867,NULL),('phabricator:20150806.ponder.status.2.sql',1466802867,NULL),('phabricator:20150806.ponder.status.3.sql',1466802867,NULL),('phabricator:20150808.ponder.vote.1.sql',1466802867,NULL),('phabricator:20150808.ponder.vote.2.sql',1466802867,NULL),('phabricator:20150812.ponder.answer.1.sql',1466802867,NULL),('phabricator:20150812.ponder.answer.2.sql',1466802867,NULL),('phabricator:20150814.harbormater.artifact.phid.sql',1466802867,NULL),('phabricator:20150815.owners.status.1.sql',1466802867,NULL),('phabricator:20150815.owners.status.2.sql',1466802867,NULL),('phabricator:20150823.nuance.queue.1.sql',1466802867,NULL),('phabricator:20150823.nuance.queue.2.sql',1466802867,NULL),('phabricator:20150823.nuance.queue.3.sql',1466802867,NULL),('phabricator:20150823.nuance.queue.4.sql',1466802867,NULL),('phabricator:20150828.ponder.wiki.1.sql',1466802867,NULL),('phabricator:20150829.ponder.dupe.1.sql',1466802867,NULL),('phabricator:20150904.herald.1.sql',1466802867,NULL),('phabricator:20150906.mailinglist.sql',1466802867,NULL),('phabricator:20150910.owners.custom.1.sql',1466802867,NULL),('phabricator:20150916.drydock.slotlocks.1.sql',1466802867,NULL),('phabricator:20150922.drydock.commands.1.sql',1466802867,NULL),('phabricator:20150923.drydock.resourceid.1.sql',1466802867,NULL),('phabricator:20150923.drydock.resourceid.2.sql',1466802867,NULL),('phabricator:20150923.drydock.resourceid.3.sql',1466802867,NULL),('phabricator:20150923.drydock.taskid.1.sql',1466802867,NULL),('phabricator:20150924.drydock.disable.1.sql',1466802867,NULL),('phabricator:20150924.drydock.status.1.sql',1466802867,NULL),('phabricator:20150928.drydock.rexpire.1.sql',1466802867,NULL),('phabricator:20150930.drydock.log.1.sql',1466802867,NULL),('phabricator:20151001.drydock.rname.1.sql',1466802867,NULL),('phabricator:20151002.dashboard.status.1.sql',1466802867,NULL),('phabricator:20151002.harbormaster.bparam.1.sql',1466802867,NULL),('phabricator:20151009.drydock.auth.1.sql',1466802867,NULL),('phabricator:20151010.drydock.auth.2.sql',1466802867,NULL),('phabricator:20151013.drydock.op.1.sql',1466802867,NULL),('phabricator:20151023.harborpolicy.1.sql',1466802867,NULL),('phabricator:20151023.harborpolicy.2.php',1466802867,NULL),('phabricator:20151023.patchduration.sql',1466802867,16373),('phabricator:20151030.harbormaster.initiator.sql',1466802867,21600),('phabricator:20151106.editengine.1.table.sql',1466802867,9430),('phabricator:20151106.editengine.2.xactions.sql',1466802867,7174),('phabricator:20151106.phame.post.mailkey.1.sql',1466802867,19922),('phabricator:20151106.phame.post.mailkey.2.php',1466802867,1343),('phabricator:20151107.phame.blog.mailkey.1.sql',1466802867,17107),('phabricator:20151107.phame.blog.mailkey.2.php',1466802867,1049),('phabricator:20151108.phame.blog.joinpolicy.sql',1466802867,16781),('phabricator:20151108.xhpast.stderr.sql',1466802867,23962),('phabricator:20151109.phame.post.comments.1.sql',1466802867,8796),('phabricator:20151109.repository.coverage.1.sql',1466802867,1058),('phabricator:20151109.xhpast.db.1.sql',1466802867,1587),('phabricator:20151109.xhpast.db.2.sql',1466802867,561),('phabricator:20151110.daemonenvhash.sql',1466802867,36237),('phabricator:20151111.phame.blog.archive.1.sql',1466802867,16500),('phabricator:20151111.phame.blog.archive.2.sql',1466802867,479),('phabricator:20151112.herald.edge.sql',1466802867,14091),('phabricator:20151116.owners.edge.sql',1466802867,11769),('phabricator:20151128.phame.blog.picture.1.sql',1466802867,15526),('phabricator:20151130.phurl.mailkey.1.sql',1466802868,10082),('phabricator:20151130.phurl.mailkey.2.php',1466802868,1190),('phabricator:20151202.versioneddraft.1.sql',1466802868,8290),('phabricator:20151207.editengine.1.sql',1466802868,76502),('phabricator:20151210.land.1.refphid.sql',1466802868,15998),('phabricator:20151210.land.2.refphid.php',1466802868,751),('phabricator:20151215.phame.1.autotitle.sql',1466802868,20074),('phabricator:20151218.key.1.keyphid.sql',1466802868,15772),('phabricator:20151218.key.2.keyphid.php',1466802868,454),('phabricator:20151219.proj.01.prislug.sql',1466802868,22082),('phabricator:20151219.proj.02.prislugkey.sql',1466802868,15591),('phabricator:20151219.proj.03.copyslug.sql',1466802868,581),('phabricator:20151219.proj.04.dropslugkey.sql',1466802868,8692),('phabricator:20151219.proj.05.dropslug.sql',1466802868,21494),('phabricator:20151219.proj.06.defaultpolicy.php',1466802868,1250),('phabricator:20151219.proj.07.viewnull.sql',1466802868,14942),('phabricator:20151219.proj.08.editnull.sql',1466802868,11831),('phabricator:20151219.proj.09.joinnull.sql',1466802868,10583),('phabricator:20151219.proj.10.subcolumns.sql',1466802868,201986),('phabricator:20151219.proj.11.subprojectphids.sql',1466802868,23604),('phabricator:20151221.search.1.version.sql',1466802868,9540),('phabricator:20151221.search.2.ownersngrams.sql',1466802868,7522),('phabricator:20151221.search.3.reindex.php',1466802868,415),('phabricator:20151223.proj.01.paths.sql',1466802868,22569),('phabricator:20151223.proj.02.depths.sql',1466802868,25408),('phabricator:20151223.proj.03.pathkey.sql',1466802868,13193),('phabricator:20151223.proj.04.keycol.sql',1466802868,27276),('phabricator:20151223.proj.05.updatekeys.php',1466802868,451),('phabricator:20151223.proj.06.uniq.sql',1466802868,11754),('phabricator:20151226.reop.1.sql',1466802868,19139),('phabricator:20151227.proj.01.materialize.sql',1466802868,535),('phabricator:20151231.proj.01.icon.php',1466802868,1991),('phabricator:20160102.badges.award.sql',1466802868,10113),('phabricator:20160110.repo.01.slug.sql',1466802868,32438),('phabricator:20160110.repo.02.slug.php',1466802868,459),('phabricator:20160111.repo.01.slugx.sql',1466802868,627),('phabricator:20160112.repo.01.uri.sql',1466802868,8500),('phabricator:20160112.repo.02.uri.index.php',1466802868,64),('phabricator:20160113.propanel.1.storage.sql',1466802868,6858),('phabricator:20160113.propanel.2.xaction.sql',1466802868,7710),('phabricator:20160119.project.1.silence.sql',1466802868,579),('phabricator:20160122.project.1.boarddefault.php',1466802868,904),('phabricator:20160124.people.1.icon.sql',1466802868,12728),('phabricator:20160124.people.2.icondefault.sql',1466802868,477),('phabricator:20160128.repo.1.pull.sql',1466802868,9886),('phabricator:20160201.revision.properties.1.sql',1478565040,51199),('phabricator:20160201.revision.properties.2.sql',1478565040,1754),('phabricator:20160202.board.1.proxy.sql',1466802868,17041),('phabricator:20160202.ipv6.1.sql',1466802868,22960),('phabricator:20160202.ipv6.2.php',1466802868,1991),('phabricator:20160206.cover.1.sql',1466802868,29137),('phabricator:20160208.task.1.sql',1466802868,32546),('phabricator:20160208.task.2.sql',1466802868,33818),('phabricator:20160208.task.3.sql',1466802868,34881),('phabricator:20160212.proj.1.sql',1466802868,28365),('phabricator:20160212.proj.2.sql',1466802868,504),('phabricator:20160215.owners.policy.1.sql',1466802868,18780),('phabricator:20160215.owners.policy.2.sql',1466802868,17029),('phabricator:20160215.owners.policy.3.sql',1466802868,432),('phabricator:20160215.owners.policy.4.sql',1466802868,361),('phabricator:20160218.callsigns.1.sql',1466802869,12331),('phabricator:20160221.almanac.1.devicen.sql',1466802869,9432),('phabricator:20160221.almanac.2.devicei.php',1466802869,1470),('phabricator:20160221.almanac.3.servicen.sql',1466802869,7845),('phabricator:20160221.almanac.4.servicei.php',1466802869,916),('phabricator:20160221.almanac.5.networkn.sql',1466802869,8044),('phabricator:20160221.almanac.6.networki.php',1466802869,903),('phabricator:20160221.almanac.7.namespacen.sql',1466802869,7463),('phabricator:20160221.almanac.8.namespace.sql',1466802869,7462),('phabricator:20160221.almanac.9.namespacex.sql',1466802869,7400),('phabricator:20160222.almanac.1.properties.php',1466802869,1750),('phabricator:20160223.almanac.1.bound.sql',1466802869,16093),('phabricator:20160223.almanac.2.lockbind.sql',1466802869,447),('phabricator:20160223.almanac.3.devicelock.sql',1466802869,19320),('phabricator:20160223.almanac.4.servicelock.sql',1466802869,23933),('phabricator:20160223.paste.fileedges.php',1466802869,654),('phabricator:20160225.almanac.1.disablebinding.sql',1466802869,24011),('phabricator:20160225.almanac.2.stype.sql',1466802869,7243),('phabricator:20160225.almanac.3.stype.php',1466802869,464),('phabricator:20160227.harbormaster.1.plann.sql',1466802869,7447),('phabricator:20160227.harbormaster.2.plani.php',1466802869,367),('phabricator:20160303.drydock.1.bluen.sql',1466802869,6561),('phabricator:20160303.drydock.2.bluei.php',1466802869,323),('phabricator:20160303.drydock.3.edge.sql',1466802869,12513),('phabricator:20160308.nuance.01.disabled.sql',1466802869,14286),('phabricator:20160308.nuance.02.cursordata.sql',1466802869,14221),('phabricator:20160308.nuance.03.sourcen.sql',1466802869,7121),('phabricator:20160308.nuance.04.sourcei.php',1466802869,1214),('phabricator:20160308.nuance.05.sourcename.sql',1466802869,10244),('phabricator:20160308.nuance.06.label.sql',1466802869,18965),('phabricator:20160308.nuance.07.itemtype.sql',1466802869,25794),('phabricator:20160308.nuance.08.itemkey.sql',1466802869,21958),('phabricator:20160308.nuance.09.itemcontainer.sql',1466802869,22612),('phabricator:20160308.nuance.10.itemkeyu.sql',1466802869,552),('phabricator:20160308.nuance.11.requestor.sql',1466802869,14556),('phabricator:20160308.nuance.12.queue.sql',1466802869,19884),('phabricator:20160316.lfs.01.token.resource.sql',1466802869,13284),('phabricator:20160316.lfs.02.token.user.sql',1466802869,15555),('phabricator:20160316.lfs.03.token.properties.sql',1466802869,16563),('phabricator:20160316.lfs.04.token.default.sql',1466802869,581),('phabricator:20160317.lfs.01.ref.sql',1466802869,8120),('phabricator:20160321.nuance.01.taskbridge.sql',1466802869,28702),('phabricator:20160322.nuance.01.itemcommand.sql',1466802869,11727),('phabricator:20160323.badgemigrate.sql',1466802869,873),('phabricator:20160329.nuance.01.requestor.sql',1466802869,1313),('phabricator:20160329.nuance.02.requestorsource.sql',1466802869,1704),('phabricator:20160329.nuance.03.requestorxaction.sql',1466802869,1686),('phabricator:20160329.nuance.04.requestorcomment.sql',1466802869,1374),('phabricator:20160330.badges.migratequality.sql',1466802869,9959),('phabricator:20160330.badges.qualityxaction.mig.sql',1466802869,2022),('phabricator:20160331.fund.comments.1.sql',1466802869,6337),('phabricator:20160404.oauth.1.xaction.sql',1466802869,6577),('phabricator:20160405.oauth.2.disable.sql',1466802869,15800),('phabricator:20160406.badges.ngrams.php',1466802869,678),('phabricator:20160406.badges.ngrams.sql',1466802869,8117),('phabricator:20160406.columns.1.php',1466802869,543),('phabricator:20160411.repo.1.version.sql',1466802869,7060),('phabricator:20160418.repouri.1.sql',1466802869,6372),('phabricator:20160418.repouri.2.sql',1466802869,13856),('phabricator:20160418.repoversion.1.sql',1466802869,15751),('phabricator:20160419.pushlog.1.sql',1466802869,25118),('phabricator:20160424.locks.1.sql',1466802869,15525),('phabricator:20160426.searchedge.sql',1466802869,15875),('phabricator:20160428.repo.1.urixaction.sql',1466802869,7234),('phabricator:20160503.repo.01.lpath.sql',1466802869,23546),('phabricator:20160503.repo.02.lpathkey.sql',1466802869,12838),('phabricator:20160503.repo.03.lpathmigrate.php',1466802869,473),('phabricator:20160503.repo.04.mirrormigrate.php',1466802869,540),('phabricator:20160503.repo.05.urimigrate.php',1466802869,355),('phabricator:20160510.repo.01.uriindex.php',1466802869,4261),('phabricator:20160513.owners.01.autoreview.sql',1466802869,17537),('phabricator:20160513.owners.02.autoreviewnone.sql',1466802869,514),('phabricator:20160516.owners.01.dominion.sql',1466802869,16006),('phabricator:20160516.owners.02.dominionstrong.sql',1466802869,563),('phabricator:20160517.oauth.01.edge.sql',1466802869,13998),('phabricator:20160518.ssh.01.activecol.sql',1466802869,15647),('phabricator:20160518.ssh.02.activeval.sql',1466802869,485),('phabricator:20160518.ssh.03.activekey.sql',1466802869,10420),('phabricator:20160519.ssh.01.xaction.sql',1466802869,9024),('phabricator:20160531.pref.01.xaction.sql',1466802869,7837),('phabricator:20160531.pref.02.datecreatecol.sql',1466802869,11848),('phabricator:20160531.pref.03.datemodcol.sql',1466802869,13814),('phabricator:20160531.pref.04.datecreateval.sql',1466802869,429),('phabricator:20160531.pref.05.datemodval.sql',1466802869,305),('phabricator:20160531.pref.06.phidcol.sql',1466802869,12896),('phabricator:20160531.pref.07.phidval.php',1466802869,679),('phabricator:20160601.user.01.cache.sql',1466802869,9294),('phabricator:20160601.user.02.copyprefs.php',1466802869,1552),('phabricator:20160601.user.03.removetime.sql',1466802869,18764),('phabricator:20160601.user.04.removetranslation.sql',1466802869,20404),('phabricator:20160601.user.05.removesex.sql',1466802869,23968),('phabricator:20160603.user.01.removedcenabled.sql',1466802869,25907),('phabricator:20160603.user.02.removedctab.sql',1466802869,20792),('phabricator:20160603.user.03.removedcvisible.sql',1466802869,22140),('phabricator:20160604.user.01.stringmailprefs.php',1466802869,683),('phabricator:20160604.user.02.removeimagecache.sql',1466802870,22776),('phabricator:20160605.user.01.prefnulluser.sql',1466802870,12931),('phabricator:20160605.user.02.prefbuiltin.sql',1466802870,13598),('phabricator:20160605.user.03.builtinunique.sql',1466802870,12068),('phabricator:20160616.phame.blog.header.1.sql',1466802870,21493),('phabricator:20160616.repo.01.oldref.sql',1466802870,9001),('phabricator:20160617.harbormaster.01.arelease.sql',1466802870,16927),('phabricator:20160618.phame.blog.subtitle.sql',1466802870,28832),('phabricator:20160620.phame.blog.parentdomain.2.sql',1466802870,32443),('phabricator:20160620.phame.blog.parentsite.1.sql',1466802870,35279),('phabricator:20160623.phame.blog.fulldomain.1.sql',1466802870,37279),('phabricator:20160623.phame.blog.fulldomain.2.sql',1466802870,500),('phabricator:20160623.phame.blog.fulldomain.3.sql',1466802870,589),('phabricator:20160706.phame.blog.parentdomain.2.sql',1478565040,55659),('phabricator:20160706.phame.blog.parentsite.1.sql',1478565041,55407),('phabricator:20160707.calendar.01.stub.sql',1478565041,59938),('phabricator:20160711.files.01.builtin.sql',1478565041,43674),('phabricator:20160711.files.02.builtinkey.sql',1478565041,17105),('phabricator:20160713.event.01.host.sql',1478565041,10118),('phabricator:20160715.event.01.alldayfrom.sql',1478565041,63596),('phabricator:20160715.event.02.alldayto.sql',1478565041,52740),('phabricator:20160715.event.03.allday.php',1478565041,468),('phabricator:20160720.calendar.invitetxn.php',1478565041,10546),('phabricator:20160721.pack.01.pub.sql',1478565041,30421),('phabricator:20160721.pack.02.pubxaction.sql',1478565041,38632),('phabricator:20160721.pack.03.edge.sql',1478565041,48446),('phabricator:20160721.pack.04.pkg.sql',1478565041,25192),('phabricator:20160721.pack.05.pkgxaction.sql',1478565041,29195),('phabricator:20160721.pack.06.version.sql',1478565041,30514),('phabricator:20160721.pack.07.versionxaction.sql',1478565041,24040),('phabricator:20160722.pack.01.pubngrams.sql',1478565041,31996),('phabricator:20160722.pack.02.pkgngrams.sql',1478565041,57210),('phabricator:20160722.pack.03.versionngrams.sql',1478565041,27240),('phabricator:20160810.commit.01.summarylength.sql',1478565041,11119),('phabricator:20160824.connectionlog.sql',1478565041,20786),('phabricator:20160824.repohint.01.hint.sql',1478565041,22413),('phabricator:20160824.repohint.02.movebad.php',1478565041,4120),('phabricator:20160824.repohint.03.nukebad.sql',1478565041,20874),('phabricator:20160825.ponder.sql',1478565041,3404),('phabricator:20160829.pastebin.01.language.sql',1478565041,58681),('phabricator:20160829.pastebin.02.language.sql',1478565041,3605),('phabricator:20160913.conpherence.topic.1.sql',1478565042,56640),('phabricator:20160919.repo.messagecount.sql',1478565042,42783),('phabricator:20160919.repo.messagedefault.sql',1478565042,7865),('phabricator:20160921.fileexternalrequest.sql',1478565042,58442),('phabricator:20160927.phurl.ngrams.php',1478565042,5241),('phabricator:20160927.phurl.ngrams.sql',1478565042,28976),('phabricator:20160928.repo.messagecount.sql',1478565042,2011),('phabricator:20160928.tokentoken.sql',1478565042,27773),('phabricator:20161003.cal.01.utcepoch.sql',1478565042,171273),('phabricator:20161003.cal.02.parameters.sql',1478565042,58714),('phabricator:20161004.cal.01.noepoch.php',1478565042,20754),('phabricator:20161005.cal.01.rrules.php',1478565042,2103),('phabricator:20161005.cal.02.export.sql',1478565042,29619),('phabricator:20161005.cal.03.exportxaction.sql',1478565042,26952),('phabricator:20161005.conpherence.image.1.sql',1478565042,49672),('phabricator:20161005.conpherence.image.2.php',1478565042,4731),('phabricator:20161011.conpherence.ngrams.php',1478565042,1467),('phabricator:20161011.conpherence.ngrams.sql',1478565042,34454),('phabricator:20161012.cal.01.import.sql',1478565042,18260),('phabricator:20161012.cal.02.importxaction.sql',1478565042,19050),('phabricator:20161012.cal.03.eventimport.sql',1478565042,167029),('phabricator:20161013.cal.01.importlog.sql',1478565043,19669),('phabricator:20161016.conpherence.imagephids.sql',1478565043,30422),('phabricator:20161025.phortune.contact.1.sql',1478565043,32583),('phabricator:20161025.phortune.merchant.image.1.sql',1478565043,34536),('phabricator:20161026.calendar.01.importtriggers.sql',1478565043,84566),('phabricator:20161027.calendar.01.externalinvitee.sql',1478565043,28223),('phabricator:20161029.phortune.invoice.1.sql',1478565043,106477),('phabricator:20161031.calendar.01.seriesparent.sql',1478565043,69966),('phabricator:20161031.calendar.02.notifylog.sql',1478565043,19863),('phabricator:20161101.calendar.01.noholiday.sql',1478565043,25532),('phabricator:20161101.calendar.02.removecolumns.sql',1478565043,236361),('phabricator:20161104.calendar.01.availability.sql',1478565043,36049),('phabricator:20161104.calendar.02.availdefault.sql',1478565043,1405),('phabricator:daemonstatus.sql',1466802856,NULL),('phabricator:daemonstatuskey.sql',1466802856,NULL),('phabricator:daemontaskarchive.sql',1466802856,NULL),('phabricator:db.almanac',1466802850,NULL),('phabricator:db.audit',1466802850,NULL),('phabricator:db.auth',1466802850,NULL),('phabricator:db.badges',1466802850,NULL),('phabricator:db.cache',1466802850,NULL),('phabricator:db.calendar',1466802850,NULL),('phabricator:db.chatlog',1466802850,NULL),('phabricator:db.conduit',1466802850,NULL),('phabricator:db.config',1466802850,NULL),('phabricator:db.conpherence',1466802850,NULL),('phabricator:db.countdown',1466802850,NULL),('phabricator:db.daemon',1466802850,NULL),('phabricator:db.dashboard',1466802850,NULL),('phabricator:db.differential',1466802850,NULL),('phabricator:db.diviner',1466802850,NULL),('phabricator:db.doorkeeper',1466802850,NULL),('phabricator:db.draft',1466802850,NULL),('phabricator:db.drydock',1466802850,NULL),('phabricator:db.fact',1466802850,NULL),('phabricator:db.feed',1466802850,NULL),('phabricator:db.file',1466802850,NULL),('phabricator:db.flag',1466802850,NULL),('phabricator:db.fund',1466802850,NULL),('phabricator:db.harbormaster',1466802850,NULL),('phabricator:db.herald',1466802850,NULL),('phabricator:db.legalpad',1466802850,NULL),('phabricator:db.maniphest',1466802850,NULL),('phabricator:db.meta_data',1466802850,NULL),('phabricator:db.metamta',1466802850,NULL),('phabricator:db.multimeter',1466802850,NULL),('phabricator:db.nuance',1466802850,NULL),('phabricator:db.oauth_server',1466802850,NULL),('phabricator:db.owners',1466802850,NULL),('phabricator:db.packages',1478565040,1590),('phabricator:db.passphrase',1466802850,NULL),('phabricator:db.pastebin',1466802850,NULL),('phabricator:db.phame',1466802850,NULL),('phabricator:db.phlux',1466802850,NULL),('phabricator:db.pholio',1466802850,NULL),('phabricator:db.phortune',1466802850,NULL),('phabricator:db.phragment',1466802850,NULL),('phabricator:db.phrequent',1466802850,NULL),('phabricator:db.phriction',1466802850,NULL),('phabricator:db.phurl',1466802850,NULL),('phabricator:db.policy',1466802850,NULL),('phabricator:db.ponder',1466802850,NULL),('phabricator:db.project',1466802850,NULL),('phabricator:db.releeph',1466802850,NULL),('phabricator:db.repository',1466802850,NULL),('phabricator:db.search',1466802850,NULL),('phabricator:db.slowvote',1466802850,NULL),('phabricator:db.spaces',1466802850,NULL),('phabricator:db.system',1466802850,NULL),('phabricator:db.timeline',1466802850,NULL),('phabricator:db.token',1466802850,NULL),('phabricator:db.user',1466802850,NULL),('phabricator:db.worker',1466802850,NULL),('phabricator:db.xhpast',1466802850,NULL),('phabricator:db.xhpastview',1466802850,NULL),('phabricator:db.xhprof',1466802850,NULL),('phabricator:differentialbookmarks.sql',1466802856,NULL),('phabricator:draft-metadata.sql',1466802856,NULL),('phabricator:dropfileproxyimage.sql',1466802856,NULL),('phabricator:drydockresoucetype.sql',1466802856,NULL),('phabricator:drydocktaskid.sql',1466802856,NULL),('phabricator:edgetype.sql',1466802856,NULL),('phabricator:emailtable.sql',1466802855,NULL),('phabricator:emailtableport.sql',1466802855,NULL),('phabricator:emailtableremove.sql',1466802855,NULL),('phabricator:fact-raw.sql',1466802856,NULL),('phabricator:harbormasterobject.sql',1466802856,NULL),('phabricator:holidays.sql',1466802855,NULL),('phabricator:ldapinfo.sql',1466802855,NULL),('phabricator:legalpad-mailkey-populate.php',1466802858,NULL),('phabricator:legalpad-mailkey.sql',1466802858,NULL),('phabricator:liskcounters-task.sql',1466802856,NULL),('phabricator:liskcounters.php',1466802856,NULL),('phabricator:liskcounters.sql',1466802856,NULL),('phabricator:maniphestxcache.sql',1466802856,NULL),('phabricator:markupcache.sql',1466802856,NULL),('phabricator:migrate-differential-dependencies.php',1466802856,NULL),('phabricator:migrate-maniphest-dependencies.php',1466802856,NULL),('phabricator:migrate-maniphest-revisions.php',1466802856,NULL),('phabricator:migrate-project-edges.php',1466802856,NULL),('phabricator:owners-exclude.sql',1466802856,NULL),('phabricator:pastepolicy.sql',1466802856,NULL),('phabricator:phameblog.sql',1466802856,NULL),('phabricator:phamedomain.sql',1466802856,NULL),('phabricator:phameoneblog.sql',1466802856,NULL),('phabricator:phamepolicy.sql',1466802856,NULL),('phabricator:phiddrop.sql',1466802855,NULL),('phabricator:pholio.sql',1466802856,NULL),('phabricator:policy-project.sql',1466802856,NULL),('phabricator:ponder-comments.sql',1466802856,NULL),('phabricator:ponder-mailkey-populate.php',1466802856,NULL),('phabricator:ponder-mailkey.sql',1466802856,NULL),('phabricator:ponder.sql',1466802856,NULL),('phabricator:releeph.sql',1466802857,NULL),('phabricator:repository-lint.sql',1466802856,NULL),('phabricator:statustxt.sql',1466802856,NULL),('phabricator:symbolcontexts.sql',1466802856,NULL),('phabricator:testdatabase.sql',1466802855,NULL),('phabricator:threadtopic.sql',1466802855,NULL),('phabricator:userstatus.sql',1466802855,NULL),('phabricator:usertranslation.sql',1466802855,NULL),('phabricator:xhprof.sql',1466802856,NULL);
/*!40000 ALTER TABLE `patch_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_metamta`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_metamta` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_metamta`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metamta_applicationemail`
--

DROP TABLE IF EXISTS `metamta_applicationemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metamta_applicationemail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `applicationPHID` varbinary(64) NOT NULL,
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `configData` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_address` (`address`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_application` (`applicationPHID`),
  KEY `key_space` (`spacePHID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metamta_applicationemail`
--

LOCK TABLES `metamta_applicationemail` WRITE;
/*!40000 ALTER TABLE `metamta_applicationemail` DISABLE KEYS */;
/*!40000 ALTER TABLE `metamta_applicationemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metamta_applicationemailtransaction`
--

DROP TABLE IF EXISTS `metamta_applicationemailtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metamta_applicationemailtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metamta_applicationemailtransaction`
--

LOCK TABLES `metamta_applicationemailtransaction` WRITE;
/*!40000 ALTER TABLE `metamta_applicationemailtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `metamta_applicationemailtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metamta_mail`
--

DROP TABLE IF EXISTS `metamta_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metamta_mail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `actorPHID` varbinary(64) DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `message` longtext COLLATE utf8mb4_bin,
  `relatedPHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `relatedPHID` (`relatedPHID`),
  KEY `key_created` (`dateCreated`),
  KEY `key_actorPHID` (`actorPHID`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metamta_mail`
--

LOCK TABLES `metamta_mail` WRITE;
/*!40000 ALTER TABLE `metamta_mail` DISABLE KEYS */;
/*!40000 ALTER TABLE `metamta_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metamta_receivedmail`
--

DROP TABLE IF EXISTS `metamta_receivedmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metamta_receivedmail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `headers` longtext COLLATE utf8mb4_bin NOT NULL,
  `bodies` longtext COLLATE utf8mb4_bin NOT NULL,
  `attachments` longtext COLLATE utf8mb4_bin NOT NULL,
  `relatedPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_bin,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `messageIDHash` binary(12) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relatedPHID` (`relatedPHID`),
  KEY `authorPHID` (`authorPHID`),
  KEY `key_messageIDHash` (`messageIDHash`),
  KEY `key_created` (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metamta_receivedmail`
--

LOCK TABLES `metamta_receivedmail` WRITE;
/*!40000 ALTER TABLE `metamta_receivedmail` DISABLE KEYS */;
/*!40000 ALTER TABLE `metamta_receivedmail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms`
--

DROP TABLE IF EXISTS `sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `providerShortName` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `providerSMSID` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `toNumber` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `fromNumber` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_bin NOT NULL,
  `sendStatus` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_provider` (`providerSMSID`,`providerShortName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms`
--

LOCK TABLES `sms` WRITE;
/*!40000 ALTER TABLE `sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_multimeter`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_multimeter` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_multimeter`;

--
-- Table structure for table `multimeter_context`
--

DROP TABLE IF EXISTS `multimeter_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimeter_context` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `nameHash` binary(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_hash` (`nameHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimeter_context`
--

LOCK TABLES `multimeter_context` WRITE;
/*!40000 ALTER TABLE `multimeter_context` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimeter_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimeter_event`
--

DROP TABLE IF EXISTS `multimeter_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimeter_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventType` int(10) unsigned NOT NULL,
  `eventLabelID` int(10) unsigned NOT NULL,
  `resourceCost` bigint(20) NOT NULL,
  `sampleRate` int(10) unsigned NOT NULL,
  `eventContextID` int(10) unsigned NOT NULL,
  `eventHostID` int(10) unsigned NOT NULL,
  `eventViewerID` int(10) unsigned NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `requestKey` binary(12) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_request` (`requestKey`),
  KEY `key_type` (`eventType`,`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimeter_event`
--

LOCK TABLES `multimeter_event` WRITE;
/*!40000 ALTER TABLE `multimeter_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimeter_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimeter_host`
--

DROP TABLE IF EXISTS `multimeter_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimeter_host` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `nameHash` binary(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_hash` (`nameHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimeter_host`
--

LOCK TABLES `multimeter_host` WRITE;
/*!40000 ALTER TABLE `multimeter_host` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimeter_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimeter_label`
--

DROP TABLE IF EXISTS `multimeter_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimeter_label` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `nameHash` binary(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_hash` (`nameHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimeter_label`
--

LOCK TABLES `multimeter_label` WRITE;
/*!40000 ALTER TABLE `multimeter_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimeter_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimeter_viewer`
--

DROP TABLE IF EXISTS `multimeter_viewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimeter_viewer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `nameHash` binary(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_hash` (`nameHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimeter_viewer`
--

LOCK TABLES `multimeter_viewer` WRITE;
/*!40000 ALTER TABLE `multimeter_viewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimeter_viewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_nuance`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_nuance` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_nuance`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_importcursordata`
--

DROP TABLE IF EXISTS `nuance_importcursordata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_importcursordata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `sourcePHID` varbinary(64) NOT NULL,
  `cursorKey` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `cursorType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_source` (`sourcePHID`,`cursorKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_importcursordata`
--

LOCK TABLES `nuance_importcursordata` WRITE;
/*!40000 ALTER TABLE `nuance_importcursordata` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_importcursordata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_item`
--

DROP TABLE IF EXISTS `nuance_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `ownerPHID` varbinary(64) DEFAULT NULL,
  `requestorPHID` varbinary(64) DEFAULT NULL,
  `sourcePHID` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `queuePHID` varbinary(64) DEFAULT NULL,
  `itemType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `itemKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `itemContainerKey` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_item` (`sourcePHID`,`itemKey`),
  KEY `key_source` (`sourcePHID`,`status`),
  KEY `key_owner` (`ownerPHID`,`status`),
  KEY `key_requestor` (`requestorPHID`,`status`),
  KEY `key_queue` (`queuePHID`,`status`),
  KEY `key_container` (`sourcePHID`,`itemContainerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_item`
--

LOCK TABLES `nuance_item` WRITE;
/*!40000 ALTER TABLE `nuance_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_itemcommand`
--

DROP TABLE IF EXISTS `nuance_itemcommand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_itemcommand` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `command` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_item` (`itemPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_itemcommand`
--

LOCK TABLES `nuance_itemcommand` WRITE;
/*!40000 ALTER TABLE `nuance_itemcommand` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_itemcommand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_itemtransaction`
--

DROP TABLE IF EXISTS `nuance_itemtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_itemtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_itemtransaction`
--

LOCK TABLES `nuance_itemtransaction` WRITE;
/*!40000 ALTER TABLE `nuance_itemtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_itemtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_itemtransaction_comment`
--

DROP TABLE IF EXISTS `nuance_itemtransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_itemtransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_itemtransaction_comment`
--

LOCK TABLES `nuance_itemtransaction_comment` WRITE;
/*!40000 ALTER TABLE `nuance_itemtransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_itemtransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_queue`
--

DROP TABLE IF EXISTS `nuance_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_queue`
--

LOCK TABLES `nuance_queue` WRITE;
/*!40000 ALTER TABLE `nuance_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_queuetransaction`
--

DROP TABLE IF EXISTS `nuance_queuetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_queuetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_queuetransaction`
--

LOCK TABLES `nuance_queuetransaction` WRITE;
/*!40000 ALTER TABLE `nuance_queuetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_queuetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_queuetransaction_comment`
--

DROP TABLE IF EXISTS `nuance_queuetransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_queuetransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_queuetransaction_comment`
--

LOCK TABLES `nuance_queuetransaction_comment` WRITE;
/*!40000 ALTER TABLE `nuance_queuetransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_queuetransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_source`
--

DROP TABLE IF EXISTS `nuance_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `defaultQueuePHID` varbinary(64) NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_type` (`type`,`dateModified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_source`
--

LOCK TABLES `nuance_source` WRITE;
/*!40000 ALTER TABLE `nuance_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_sourcename_ngrams`
--

DROP TABLE IF EXISTS `nuance_sourcename_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_sourcename_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_sourcename_ngrams`
--

LOCK TABLES `nuance_sourcename_ngrams` WRITE;
/*!40000 ALTER TABLE `nuance_sourcename_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_sourcename_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_sourcetransaction`
--

DROP TABLE IF EXISTS `nuance_sourcetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_sourcetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_sourcetransaction`
--

LOCK TABLES `nuance_sourcetransaction` WRITE;
/*!40000 ALTER TABLE `nuance_sourcetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_sourcetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nuance_sourcetransaction_comment`
--

DROP TABLE IF EXISTS `nuance_sourcetransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nuance_sourcetransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nuance_sourcetransaction_comment`
--

LOCK TABLES `nuance_sourcetransaction_comment` WRITE;
/*!40000 ALTER TABLE `nuance_sourcetransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `nuance_sourcetransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_oauth_server`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_oauth_server` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_oauth_server`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_oauthclientauthorization`
--

DROP TABLE IF EXISTS `oauth_server_oauthclientauthorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_oauthclientauthorization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `userPHID` varbinary(64) NOT NULL,
  `clientPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `scope` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `userPHID` (`userPHID`,`clientPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_oauthclientauthorization`
--

LOCK TABLES `oauth_server_oauthclientauthorization` WRITE;
/*!40000 ALTER TABLE `oauth_server_oauthclientauthorization` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_oauthclientauthorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_oauthserveraccesstoken`
--

DROP TABLE IF EXISTS `oauth_server_oauthserveraccesstoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_oauthserveraccesstoken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `userPHID` varbinary(64) NOT NULL,
  `clientPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_oauthserveraccesstoken`
--

LOCK TABLES `oauth_server_oauthserveraccesstoken` WRITE;
/*!40000 ALTER TABLE `oauth_server_oauthserveraccesstoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_oauthserveraccesstoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_oauthserverauthorizationcode`
--

DROP TABLE IF EXISTS `oauth_server_oauthserverauthorizationcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_oauthserverauthorizationcode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `clientPHID` varbinary(64) NOT NULL,
  `clientSecret` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `userPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `redirectURI` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_oauthserverauthorizationcode`
--

LOCK TABLES `oauth_server_oauthserverauthorizationcode` WRITE;
/*!40000 ALTER TABLE `oauth_server_oauthserverauthorizationcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_oauthserverauthorizationcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_oauthserverclient`
--

DROP TABLE IF EXISTS `oauth_server_oauthserverclient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_oauthserverclient` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `secret` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `redirectURI` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `creatorPHID` varbinary(64) NOT NULL,
  `isTrusted` tinyint(1) NOT NULL DEFAULT '0',
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `creatorPHID` (`creatorPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_oauthserverclient`
--

LOCK TABLES `oauth_server_oauthserverclient` WRITE;
/*!40000 ALTER TABLE `oauth_server_oauthserverclient` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_oauthserverclient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_transaction`
--

DROP TABLE IF EXISTS `oauth_server_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_transaction`
--

LOCK TABLES `oauth_server_transaction` WRITE;
/*!40000 ALTER TABLE `oauth_server_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_owners`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_owners` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_owners`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_customfieldnumericindex`
--

DROP TABLE IF EXISTS `owners_customfieldnumericindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_customfieldnumericindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`),
  KEY `key_find` (`indexKey`,`indexValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_customfieldnumericindex`
--

LOCK TABLES `owners_customfieldnumericindex` WRITE;
/*!40000 ALTER TABLE `owners_customfieldnumericindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_customfieldnumericindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_customfieldstorage`
--

DROP TABLE IF EXISTS `owners_customfieldstorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_customfieldstorage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `fieldIndex` binary(12) NOT NULL,
  `fieldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`fieldIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_customfieldstorage`
--

LOCK TABLES `owners_customfieldstorage` WRITE;
/*!40000 ALTER TABLE `owners_customfieldstorage` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_customfieldstorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_customfieldstringindex`
--

DROP TABLE IF EXISTS `owners_customfieldstringindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_customfieldstringindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`(64)),
  KEY `key_find` (`indexKey`,`indexValue`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_customfieldstringindex`
--

LOCK TABLES `owners_customfieldstringindex` WRITE;
/*!40000 ALTER TABLE `owners_customfieldstringindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_customfieldstringindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_name_ngrams`
--

DROP TABLE IF EXISTS `owners_name_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_name_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_name_ngrams`
--

LOCK TABLES `owners_name_ngrams` WRITE;
/*!40000 ALTER TABLE `owners_name_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_name_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_owner`
--

DROP TABLE IF EXISTS `owners_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_owner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packageID` int(10) unsigned NOT NULL,
  `userPHID` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `packageID` (`packageID`,`userPHID`),
  KEY `userPHID` (`userPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_owner`
--

LOCK TABLES `owners_owner` WRITE;
/*!40000 ALTER TABLE `owners_owner` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_owner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_package`
--

DROP TABLE IF EXISTS `owners_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_package` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `originalName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `primaryOwnerPHID` varbinary(64) DEFAULT NULL,
  `auditingEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `mailKey` binary(20) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `autoReview` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dominion` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_package`
--

LOCK TABLES `owners_package` WRITE;
/*!40000 ALTER TABLE `owners_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_packagetransaction`
--

DROP TABLE IF EXISTS `owners_packagetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_packagetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_packagetransaction`
--

LOCK TABLES `owners_packagetransaction` WRITE;
/*!40000 ALTER TABLE `owners_packagetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_packagetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners_path`
--

DROP TABLE IF EXISTS `owners_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owners_path` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packageID` int(10) unsigned NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `excluded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `packageID` (`packageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners_path`
--

LOCK TABLES `owners_path` WRITE;
/*!40000 ALTER TABLE `owners_path` DISABLE KEYS */;
/*!40000 ALTER TABLE `owners_path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_packages`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_packages` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_packages`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_package`
--

DROP TABLE IF EXISTS `packages_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_package` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisherPHID` varbinary(64) NOT NULL,
  `packageKey` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_package` (`publisherPHID`,`packageKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_package`
--

LOCK TABLES `packages_package` WRITE;
/*!40000 ALTER TABLE `packages_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_packagename_ngrams`
--

DROP TABLE IF EXISTS `packages_packagename_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_packagename_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_packagename_ngrams`
--

LOCK TABLES `packages_packagename_ngrams` WRITE;
/*!40000 ALTER TABLE `packages_packagename_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_packagename_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_packagetransaction`
--

DROP TABLE IF EXISTS `packages_packagetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_packagetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_packagetransaction`
--

LOCK TABLES `packages_packagetransaction` WRITE;
/*!40000 ALTER TABLE `packages_packagetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_packagetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_publisher`
--

DROP TABLE IF EXISTS `packages_publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_publisher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisherKey` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_publisher` (`publisherKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_publisher`
--

LOCK TABLES `packages_publisher` WRITE;
/*!40000 ALTER TABLE `packages_publisher` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_publishername_ngrams`
--

DROP TABLE IF EXISTS `packages_publishername_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_publishername_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_publishername_ngrams`
--

LOCK TABLES `packages_publishername_ngrams` WRITE;
/*!40000 ALTER TABLE `packages_publishername_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_publishername_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_publishertransaction`
--

DROP TABLE IF EXISTS `packages_publishertransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_publishertransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_publishertransaction`
--

LOCK TABLES `packages_publishertransaction` WRITE;
/*!40000 ALTER TABLE `packages_publishertransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_publishertransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_version`
--

DROP TABLE IF EXISTS `packages_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `packagePHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_package` (`packagePHID`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_version`
--

LOCK TABLES `packages_version` WRITE;
/*!40000 ALTER TABLE `packages_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_versionname_ngrams`
--

DROP TABLE IF EXISTS `packages_versionname_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_versionname_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_versionname_ngrams`
--

LOCK TABLES `packages_versionname_ngrams` WRITE;
/*!40000 ALTER TABLE `packages_versionname_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_versionname_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_versiontransaction`
--

DROP TABLE IF EXISTS `packages_versiontransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_versiontransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_versiontransaction`
--

LOCK TABLES `packages_versiontransaction` WRITE;
/*!40000 ALTER TABLE `packages_versiontransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_versiontransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_passphrase`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_passphrase` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_passphrase`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passphrase_credential`
--

DROP TABLE IF EXISTS `passphrase_credential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passphrase_credential` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `credentialType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `providesType` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `secretID` int(10) unsigned DEFAULT NULL,
  `isDestroyed` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isLocked` tinyint(1) NOT NULL,
  `allowConduit` tinyint(1) NOT NULL DEFAULT '0',
  `authorPHID` varbinary(64) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_secret` (`secretID`),
  KEY `key_type` (`credentialType`),
  KEY `key_provides` (`providesType`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passphrase_credential`
--

LOCK TABLES `passphrase_credential` WRITE;
/*!40000 ALTER TABLE `passphrase_credential` DISABLE KEYS */;
/*!40000 ALTER TABLE `passphrase_credential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passphrase_credentialtransaction`
--

DROP TABLE IF EXISTS `passphrase_credentialtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passphrase_credentialtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passphrase_credentialtransaction`
--

LOCK TABLES `passphrase_credentialtransaction` WRITE;
/*!40000 ALTER TABLE `passphrase_credentialtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `passphrase_credentialtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passphrase_secret`
--

DROP TABLE IF EXISTS `passphrase_secret`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passphrase_secret` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `secretData` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passphrase_secret`
--

LOCK TABLES `passphrase_secret` WRITE;
/*!40000 ALTER TABLE `passphrase_secret` DISABLE KEYS */;
/*!40000 ALTER TABLE `passphrase_secret` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_pastebin`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_pastebin` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_pastebin`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pastebin_paste`
--

DROP TABLE IF EXISTS `pastebin_paste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastebin_paste` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `filePHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `language` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `parentPHID` varbinary(64) DEFAULT NULL,
  `viewPolicy` varbinary(64) DEFAULT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `parentPHID` (`parentPHID`),
  KEY `authorPHID` (`authorPHID`),
  KEY `key_dateCreated` (`dateCreated`),
  KEY `key_language` (`language`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pastebin_paste`
--

LOCK TABLES `pastebin_paste` WRITE;
/*!40000 ALTER TABLE `pastebin_paste` DISABLE KEYS */;
/*!40000 ALTER TABLE `pastebin_paste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pastebin_pastetransaction`
--

DROP TABLE IF EXISTS `pastebin_pastetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastebin_pastetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pastebin_pastetransaction`
--

LOCK TABLES `pastebin_pastetransaction` WRITE;
/*!40000 ALTER TABLE `pastebin_pastetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `pastebin_pastetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pastebin_pastetransaction_comment`
--

DROP TABLE IF EXISTS `pastebin_pastetransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastebin_pastetransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `lineNumber` int(10) unsigned DEFAULT NULL,
  `lineLength` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pastebin_pastetransaction_comment`
--

LOCK TABLES `pastebin_pastetransaction_comment` WRITE;
/*!40000 ALTER TABLE `pastebin_pastetransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `pastebin_pastetransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phame`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phame` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phame`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phame_blog`
--

DROP TABLE IF EXISTS `phame_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phame_blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `domain` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `configData` longtext COLLATE utf8mb4_bin NOT NULL,
  `creatorPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `viewPolicy` varbinary(64) DEFAULT NULL,
  `editPolicy` varbinary(64) DEFAULT NULL,
  `mailKey` binary(20) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  `headerImagePHID` varbinary(64) DEFAULT NULL,
  `subtitle` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `parentDomain` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `parentSite` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `domainFullURI` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phame_blog`
--

LOCK TABLES `phame_blog` WRITE;
/*!40000 ALTER TABLE `phame_blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `phame_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phame_blogtransaction`
--

DROP TABLE IF EXISTS `phame_blogtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phame_blogtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phame_blogtransaction`
--

LOCK TABLES `phame_blogtransaction` WRITE;
/*!40000 ALTER TABLE `phame_blogtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phame_blogtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phame_post`
--

DROP TABLE IF EXISTS `phame_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phame_post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `bloggerPHID` varbinary(64) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phameTitle` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_bin,
  `visibility` int(10) unsigned NOT NULL DEFAULT '0',
  `configData` longtext COLLATE utf8mb4_bin,
  `datePublished` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `blogPHID` varbinary(64) DEFAULT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  KEY `bloggerPosts` (`bloggerPHID`,`visibility`,`datePublished`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phame_post`
--

LOCK TABLES `phame_post` WRITE;
/*!40000 ALTER TABLE `phame_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `phame_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phame_posttransaction`
--

DROP TABLE IF EXISTS `phame_posttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phame_posttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phame_posttransaction`
--

LOCK TABLES `phame_posttransaction` WRITE;
/*!40000 ALTER TABLE `phame_posttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phame_posttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phame_posttransaction_comment`
--

DROP TABLE IF EXISTS `phame_posttransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phame_posttransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phame_posttransaction_comment`
--

LOCK TABLES `phame_posttransaction_comment` WRITE;
/*!40000 ALTER TABLE `phame_posttransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `phame_posttransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phlux`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phlux` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phlux`;

--
-- Table structure for table `phlux_transaction`
--

DROP TABLE IF EXISTS `phlux_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phlux_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phlux_transaction`
--

LOCK TABLES `phlux_transaction` WRITE;
/*!40000 ALTER TABLE `phlux_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phlux_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phlux_variable`
--

DROP TABLE IF EXISTS `phlux_variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phlux_variable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `variableKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `variableValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_key` (`variableKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phlux_variable`
--

LOCK TABLES `phlux_variable` WRITE;
/*!40000 ALTER TABLE `phlux_variable` DISABLE KEYS */;
/*!40000 ALTER TABLE `phlux_variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_pholio`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_pholio` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_pholio`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pholio_image`
--

DROP TABLE IF EXISTS `pholio_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pholio_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `mockID` int(10) unsigned DEFAULT NULL,
  `filePHID` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isObsolete` tinyint(1) NOT NULL DEFAULT '0',
  `replacesImagePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyPHID` (`phid`),
  KEY `mockID` (`mockID`,`isObsolete`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pholio_image`
--

LOCK TABLES `pholio_image` WRITE;
/*!40000 ALTER TABLE `pholio_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `pholio_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pholio_mock`
--

DROP TABLE IF EXISTS `pholio_mock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pholio_mock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `originalName` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `coverPHID` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `status` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  KEY `authorPHID` (`authorPHID`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pholio_mock`
--

LOCK TABLES `pholio_mock` WRITE;
/*!40000 ALTER TABLE `pholio_mock` DISABLE KEYS */;
/*!40000 ALTER TABLE `pholio_mock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pholio_transaction`
--

DROP TABLE IF EXISTS `pholio_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pholio_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pholio_transaction`
--

LOCK TABLES `pholio_transaction` WRITE;
/*!40000 ALTER TABLE `pholio_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `pholio_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pholio_transaction_comment`
--

DROP TABLE IF EXISTS `pholio_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pholio_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `imageID` int(10) unsigned DEFAULT NULL,
  `x` int(10) unsigned DEFAULT NULL,
  `y` int(10) unsigned DEFAULT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`),
  UNIQUE KEY `key_draft` (`authorPHID`,`imageID`,`transactionPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pholio_transaction_comment`
--

LOCK TABLES `pholio_transaction_comment` WRITE;
/*!40000 ALTER TABLE `pholio_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `pholio_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phortune`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phortune` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phortune`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_account`
--

DROP TABLE IF EXISTS `phortune_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_account`
--

LOCK TABLES `phortune_account` WRITE;
/*!40000 ALTER TABLE `phortune_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_accounttransaction`
--

DROP TABLE IF EXISTS `phortune_accounttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_accounttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_accounttransaction`
--

LOCK TABLES `phortune_accounttransaction` WRITE;
/*!40000 ALTER TABLE `phortune_accounttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_accounttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_cart`
--

DROP TABLE IF EXISTS `phortune_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `accountPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `cartClass` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `merchantPHID` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `subscriptionPHID` varbinary(64) DEFAULT NULL,
  `isInvoice` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_account` (`accountPHID`),
  KEY `key_merchant` (`merchantPHID`),
  KEY `key_subscription` (`subscriptionPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_cart`
--

LOCK TABLES `phortune_cart` WRITE;
/*!40000 ALTER TABLE `phortune_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_carttransaction`
--

DROP TABLE IF EXISTS `phortune_carttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_carttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_carttransaction`
--

LOCK TABLES `phortune_carttransaction` WRITE;
/*!40000 ALTER TABLE `phortune_carttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_carttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_charge`
--

DROP TABLE IF EXISTS `phortune_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_charge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `accountPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `cartPHID` varbinary(64) NOT NULL,
  `paymentMethodPHID` varbinary(64) DEFAULT NULL,
  `amountAsCurrency` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `merchantPHID` varbinary(64) NOT NULL,
  `providerPHID` varbinary(64) NOT NULL,
  `amountRefundedAsCurrency` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `refundingPHID` varbinary(64) DEFAULT NULL,
  `refundedChargePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_cart` (`cartPHID`),
  KEY `key_account` (`accountPHID`),
  KEY `key_merchant` (`merchantPHID`),
  KEY `key_provider` (`providerPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_charge`
--

LOCK TABLES `phortune_charge` WRITE;
/*!40000 ALTER TABLE `phortune_charge` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_charge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_merchant`
--

DROP TABLE IF EXISTS `phortune_merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_merchant` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `contactInfo` longtext COLLATE utf8mb4_bin NOT NULL,
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  `invoiceEmail` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `invoiceFooter` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_merchant`
--

LOCK TABLES `phortune_merchant` WRITE;
/*!40000 ALTER TABLE `phortune_merchant` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_merchanttransaction`
--

DROP TABLE IF EXISTS `phortune_merchanttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_merchanttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_merchanttransaction`
--

LOCK TABLES `phortune_merchanttransaction` WRITE;
/*!40000 ALTER TABLE `phortune_merchanttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_merchanttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_paymentmethod`
--

DROP TABLE IF EXISTS `phortune_paymentmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_paymentmethod` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `accountPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `brand` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `expires` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `lastFourDigits` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `merchantPHID` varbinary(64) NOT NULL,
  `providerPHID` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_account` (`accountPHID`,`status`),
  KEY `key_merchant` (`merchantPHID`,`accountPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_paymentmethod`
--

LOCK TABLES `phortune_paymentmethod` WRITE;
/*!40000 ALTER TABLE `phortune_paymentmethod` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_paymentmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_paymentproviderconfig`
--

DROP TABLE IF EXISTS `phortune_paymentproviderconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_paymentproviderconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `merchantPHID` varbinary(64) NOT NULL,
  `providerClassKey` binary(12) NOT NULL,
  `providerClass` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_merchant` (`merchantPHID`,`providerClassKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_paymentproviderconfig`
--

LOCK TABLES `phortune_paymentproviderconfig` WRITE;
/*!40000 ALTER TABLE `phortune_paymentproviderconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_paymentproviderconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_paymentproviderconfigtransaction`
--

DROP TABLE IF EXISTS `phortune_paymentproviderconfigtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_paymentproviderconfigtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_paymentproviderconfigtransaction`
--

LOCK TABLES `phortune_paymentproviderconfigtransaction` WRITE;
/*!40000 ALTER TABLE `phortune_paymentproviderconfigtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_paymentproviderconfigtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_product`
--

DROP TABLE IF EXISTS `phortune_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `productClassKey` binary(12) NOT NULL,
  `productClass` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `productRefKey` binary(12) NOT NULL,
  `productRef` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_product` (`productClassKey`,`productRefKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_product`
--

LOCK TABLES `phortune_product` WRITE;
/*!40000 ALTER TABLE `phortune_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_purchase`
--

DROP TABLE IF EXISTS `phortune_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_purchase` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `productPHID` varbinary(64) NOT NULL,
  `accountPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `cartPHID` varbinary(64) DEFAULT NULL,
  `basePriceAsCurrency` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_cart` (`cartPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_purchase`
--

LOCK TABLES `phortune_purchase` WRITE;
/*!40000 ALTER TABLE `phortune_purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phortune_subscription`
--

DROP TABLE IF EXISTS `phortune_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phortune_subscription` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `accountPHID` varbinary(64) NOT NULL,
  `merchantPHID` varbinary(64) NOT NULL,
  `triggerPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `subscriptionClassKey` binary(12) NOT NULL,
  `subscriptionClass` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `subscriptionRefKey` binary(12) NOT NULL,
  `subscriptionRef` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `defaultPaymentMethodPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_subscription` (`subscriptionClassKey`,`subscriptionRefKey`),
  KEY `key_account` (`accountPHID`),
  KEY `key_merchant` (`merchantPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phortune_subscription`
--

LOCK TABLES `phortune_subscription` WRITE;
/*!40000 ALTER TABLE `phortune_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `phortune_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phragment`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phragment` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phragment`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phragment_fragment`
--

DROP TABLE IF EXISTS `phragment_fragment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phragment_fragment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `path` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `depth` int(10) unsigned NOT NULL,
  `latestVersionPHID` varbinary(64) DEFAULT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phragment_fragment`
--

LOCK TABLES `phragment_fragment` WRITE;
/*!40000 ALTER TABLE `phragment_fragment` DISABLE KEYS */;
/*!40000 ALTER TABLE `phragment_fragment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phragment_fragmentversion`
--

DROP TABLE IF EXISTS `phragment_fragmentversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phragment_fragmentversion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `fragmentPHID` varbinary(64) NOT NULL,
  `filePHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_version` (`fragmentPHID`,`sequence`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phragment_fragmentversion`
--

LOCK TABLES `phragment_fragmentversion` WRITE;
/*!40000 ALTER TABLE `phragment_fragmentversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `phragment_fragmentversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phragment_snapshot`
--

DROP TABLE IF EXISTS `phragment_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phragment_snapshot` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `primaryFragmentPHID` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_name` (`primaryFragmentPHID`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phragment_snapshot`
--

LOCK TABLES `phragment_snapshot` WRITE;
/*!40000 ALTER TABLE `phragment_snapshot` DISABLE KEYS */;
/*!40000 ALTER TABLE `phragment_snapshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phragment_snapshotchild`
--

DROP TABLE IF EXISTS `phragment_snapshotchild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phragment_snapshotchild` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `snapshotPHID` varbinary(64) NOT NULL,
  `fragmentPHID` varbinary(64) NOT NULL,
  `fragmentVersionPHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_child` (`snapshotPHID`,`fragmentPHID`,`fragmentVersionPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phragment_snapshotchild`
--

LOCK TABLES `phragment_snapshotchild` WRITE;
/*!40000 ALTER TABLE `phragment_snapshotchild` DISABLE KEYS */;
/*!40000 ALTER TABLE `phragment_snapshotchild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phrequent`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phrequent` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phrequent`;

--
-- Table structure for table `phrequent_usertime`
--

DROP TABLE IF EXISTS `phrequent_usertime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phrequent_usertime` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_bin,
  `dateStarted` int(10) unsigned NOT NULL,
  `dateEnded` int(10) unsigned DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phrequent_usertime`
--

LOCK TABLES `phrequent_usertime` WRITE;
/*!40000 ALTER TABLE `phrequent_usertime` DISABLE KEYS */;
/*!40000 ALTER TABLE `phrequent_usertime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phriction`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phriction` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phriction`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phriction_content`
--

DROP TABLE IF EXISTS `phriction_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phriction_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentID` int(10) unsigned NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_bin,
  `changeType` int(10) unsigned NOT NULL DEFAULT '0',
  `changeRef` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `documentID` (`documentID`,`version`),
  KEY `authorPHID` (`authorPHID`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phriction_content`
--

LOCK TABLES `phriction_content` WRITE;
/*!40000 ALTER TABLE `phriction_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `phriction_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phriction_document`
--

DROP TABLE IF EXISTS `phriction_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phriction_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `slug` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `depth` int(10) unsigned NOT NULL,
  `contentID` int(10) unsigned DEFAULT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `depth` (`depth`,`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phriction_document`
--

LOCK TABLES `phriction_document` WRITE;
/*!40000 ALTER TABLE `phriction_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `phriction_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phriction_transaction`
--

DROP TABLE IF EXISTS `phriction_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phriction_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phriction_transaction`
--

LOCK TABLES `phriction_transaction` WRITE;
/*!40000 ALTER TABLE `phriction_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phriction_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phriction_transaction_comment`
--

DROP TABLE IF EXISTS `phriction_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phriction_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phriction_transaction_comment`
--

LOCK TABLES `phriction_transaction_comment` WRITE;
/*!40000 ALTER TABLE `phriction_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `phriction_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_phurl`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_phurl` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_phurl`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phurl_phurlname_ngrams`
--

DROP TABLE IF EXISTS `phurl_phurlname_ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phurl_phurlname_ngrams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectID` int(10) unsigned NOT NULL,
  `ngram` char(3) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_object` (`objectID`),
  KEY `key_ngram` (`ngram`,`objectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phurl_phurlname_ngrams`
--

LOCK TABLES `phurl_phurlname_ngrams` WRITE;
/*!40000 ALTER TABLE `phurl_phurlname_ngrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `phurl_phurlname_ngrams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phurl_url`
--

DROP TABLE IF EXISTS `phurl_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phurl_url` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `longURL` longtext COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `alias` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_instance` (`alias`),
  KEY `key_author` (`authorPHID`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phurl_url`
--

LOCK TABLES `phurl_url` WRITE;
/*!40000 ALTER TABLE `phurl_url` DISABLE KEYS */;
/*!40000 ALTER TABLE `phurl_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phurl_urltransaction`
--

DROP TABLE IF EXISTS `phurl_urltransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phurl_urltransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phurl_urltransaction`
--

LOCK TABLES `phurl_urltransaction` WRITE;
/*!40000 ALTER TABLE `phurl_urltransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `phurl_urltransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phurl_urltransaction_comment`
--

DROP TABLE IF EXISTS `phurl_urltransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phurl_urltransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phurl_urltransaction_comment`
--

LOCK TABLES `phurl_urltransaction_comment` WRITE;
/*!40000 ALTER TABLE `phurl_urltransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `phurl_urltransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_policy`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_policy` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_policy`;

--
-- Table structure for table `policy`
--

DROP TABLE IF EXISTS `policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `rules` longtext COLLATE utf8mb4_bin NOT NULL,
  `defaultAction` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy`
--

LOCK TABLES `policy` WRITE;
/*!40000 ALTER TABLE `policy` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_ponder`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_ponder` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_ponder`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponder_answer`
--

DROP TABLE IF EXISTS `ponder_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponder_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionID` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `voteCount` int(10) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `key_oneanswerperquestion` (`questionID`,`authorPHID`),
  KEY `questionID` (`questionID`),
  KEY `authorPHID` (`authorPHID`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponder_answer`
--

LOCK TABLES `ponder_answer` WRITE;
/*!40000 ALTER TABLE `ponder_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponder_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponder_answertransaction`
--

DROP TABLE IF EXISTS `ponder_answertransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponder_answertransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponder_answertransaction`
--

LOCK TABLES `ponder_answertransaction` WRITE;
/*!40000 ALTER TABLE `ponder_answertransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponder_answertransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponder_answertransaction_comment`
--

DROP TABLE IF EXISTS `ponder_answertransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponder_answertransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponder_answertransaction_comment`
--

LOCK TABLES `ponder_answertransaction_comment` WRITE;
/*!40000 ALTER TABLE `ponder_answertransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponder_answertransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponder_question`
--

DROP TABLE IF EXISTS `ponder_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponder_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin,
  `answerCount` int(10) unsigned NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `answerWiki` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  KEY `authorPHID` (`authorPHID`),
  KEY `status` (`status`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponder_question`
--

LOCK TABLES `ponder_question` WRITE;
/*!40000 ALTER TABLE `ponder_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponder_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponder_questiontransaction`
--

DROP TABLE IF EXISTS `ponder_questiontransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponder_questiontransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponder_questiontransaction`
--

LOCK TABLES `ponder_questiontransaction` WRITE;
/*!40000 ALTER TABLE `ponder_questiontransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponder_questiontransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponder_questiontransaction_comment`
--

DROP TABLE IF EXISTS `ponder_questiontransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponder_questiontransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponder_questiontransaction_comment`
--

LOCK TABLES `ponder_questiontransaction_comment` WRITE;
/*!40000 ALTER TABLE `ponder_questiontransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponder_questiontransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_project`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_project` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_project`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `joinPolicy` varbinary(64) NOT NULL,
  `isMembershipLocked` tinyint(1) NOT NULL DEFAULT '0',
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  `icon` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `color` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `primarySlug` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `parentProjectPHID` varbinary(64) DEFAULT NULL,
  `hasWorkboard` tinyint(1) NOT NULL,
  `hasMilestones` tinyint(1) NOT NULL,
  `hasSubprojects` tinyint(1) NOT NULL,
  `milestoneNumber` int(10) unsigned DEFAULT NULL,
  `projectPath` varbinary(64) NOT NULL,
  `projectDepth` int(10) unsigned NOT NULL,
  `projectPathKey` binary(4) NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_pathkey` (`projectPathKey`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_primaryslug` (`primarySlug`),
  UNIQUE KEY `key_milestone` (`parentProjectPHID`,`milestoneNumber`),
  KEY `key_icon` (`icon`),
  KEY `key_color` (`color`),
  KEY `key_path` (`projectPath`,`projectDepth`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_column`
--

DROP TABLE IF EXISTS `project_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_column` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `projectPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `proxyPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_proxy` (`projectPHID`,`proxyPHID`),
  KEY `key_status` (`projectPHID`,`status`,`sequence`),
  KEY `key_sequence` (`projectPHID`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_column`
--

LOCK TABLES `project_column` WRITE;
/*!40000 ALTER TABLE `project_column` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_columnposition`
--

DROP TABLE IF EXISTS `project_columnposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_columnposition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `boardPHID` varbinary(64) NOT NULL,
  `columnPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `boardPHID` (`boardPHID`,`columnPHID`,`objectPHID`),
  KEY `objectPHID` (`objectPHID`,`boardPHID`),
  KEY `boardPHID_2` (`boardPHID`,`columnPHID`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_columnposition`
--

LOCK TABLES `project_columnposition` WRITE;
/*!40000 ALTER TABLE `project_columnposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_columnposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_columntransaction`
--

DROP TABLE IF EXISTS `project_columntransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_columntransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_columntransaction`
--

LOCK TABLES `project_columntransaction` WRITE;
/*!40000 ALTER TABLE `project_columntransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_columntransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_customfieldnumericindex`
--

DROP TABLE IF EXISTS `project_customfieldnumericindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_customfieldnumericindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`),
  KEY `key_find` (`indexKey`,`indexValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_customfieldnumericindex`
--

LOCK TABLES `project_customfieldnumericindex` WRITE;
/*!40000 ALTER TABLE `project_customfieldnumericindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_customfieldnumericindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_customfieldstorage`
--

DROP TABLE IF EXISTS `project_customfieldstorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_customfieldstorage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `fieldIndex` binary(12) NOT NULL,
  `fieldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`fieldIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_customfieldstorage`
--

LOCK TABLES `project_customfieldstorage` WRITE;
/*!40000 ALTER TABLE `project_customfieldstorage` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_customfieldstorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_customfieldstringindex`
--

DROP TABLE IF EXISTS `project_customfieldstringindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_customfieldstringindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`(64)),
  KEY `key_find` (`indexKey`,`indexValue`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_customfieldstringindex`
--

LOCK TABLES `project_customfieldstringindex` WRITE;
/*!40000 ALTER TABLE `project_customfieldstringindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_customfieldstringindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_datasourcetoken`
--

DROP TABLE IF EXISTS `project_datasourcetoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_datasourcetoken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(10) unsigned NOT NULL,
  `token` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`,`projectID`),
  KEY `projectID` (`projectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_datasourcetoken`
--

LOCK TABLES `project_datasourcetoken` WRITE;
/*!40000 ALTER TABLE `project_datasourcetoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_datasourcetoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_slug`
--

DROP TABLE IF EXISTS `project_slug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_slug` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectPHID` varbinary(64) NOT NULL,
  `slug` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_slug` (`slug`),
  KEY `key_projectPHID` (`projectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_slug`
--

LOCK TABLES `project_slug` WRITE;
/*!40000 ALTER TABLE `project_slug` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_slug` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_transaction`
--

DROP TABLE IF EXISTS `project_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_transaction`
--

LOCK TABLES `project_transaction` WRITE;
/*!40000 ALTER TABLE `project_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_releeph`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_releeph` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_releeph`;

--
-- Table structure for table `releeph_branch`
--

DROP TABLE IF EXISTS `releeph_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `basename` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `releephProjectID` int(10) unsigned NOT NULL,
  `createdByUserPHID` varbinary(64) NOT NULL,
  `cutPointCommitPHID` varbinary(64) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `symbolicName` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `releephProjectID_2` (`releephProjectID`,`basename`),
  UNIQUE KEY `releephProjectID_name` (`releephProjectID`,`name`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `releephProjectID` (`releephProjectID`,`symbolicName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_branch`
--

LOCK TABLES `releeph_branch` WRITE;
/*!40000 ALTER TABLE `releeph_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releeph_branchtransaction`
--

DROP TABLE IF EXISTS `releeph_branchtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_branchtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_branchtransaction`
--

LOCK TABLES `releeph_branchtransaction` WRITE;
/*!40000 ALTER TABLE `releeph_branchtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_branchtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releeph_producttransaction`
--

DROP TABLE IF EXISTS `releeph_producttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_producttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_producttransaction`
--

LOCK TABLES `releeph_producttransaction` WRITE;
/*!40000 ALTER TABLE `releeph_producttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_producttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releeph_project`
--

DROP TABLE IF EXISTS `releeph_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `trunkBranch` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `createdByUserPHID` varbinary(64) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projectName` (`name`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_project`
--

LOCK TABLES `releeph_project` WRITE;
/*!40000 ALTER TABLE `releeph_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releeph_request`
--

DROP TABLE IF EXISTS `releeph_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `branchID` int(10) unsigned NOT NULL,
  `requestUserPHID` varbinary(64) NOT NULL,
  `requestCommitPHID` varbinary(64) DEFAULT NULL,
  `commitIdentifier` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `commitPHID` varbinary(64) DEFAULT NULL,
  `pickStatus` int(10) unsigned DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `userIntents` longtext COLLATE utf8mb4_bin,
  `inBranch` tinyint(1) NOT NULL DEFAULT '0',
  `mailKey` binary(20) NOT NULL,
  `requestedObjectPHID` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `requestIdentifierBranch` (`requestCommitPHID`,`branchID`),
  KEY `branchID` (`branchID`),
  KEY `key_requestedObject` (`requestedObjectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_request`
--

LOCK TABLES `releeph_request` WRITE;
/*!40000 ALTER TABLE `releeph_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releeph_requesttransaction`
--

DROP TABLE IF EXISTS `releeph_requesttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_requesttransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_requesttransaction`
--

LOCK TABLES `releeph_requesttransaction` WRITE;
/*!40000 ALTER TABLE `releeph_requesttransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_requesttransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releeph_requesttransaction_comment`
--

DROP TABLE IF EXISTS `releeph_requesttransaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releeph_requesttransaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releeph_requesttransaction_comment`
--

LOCK TABLES `releeph_requesttransaction_comment` WRITE;
/*!40000 ALTER TABLE `releeph_requesttransaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `releeph_requesttransaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_repository`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_repository` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_repository`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository`
--

DROP TABLE IF EXISTS `repository`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `callsign` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `versionControlSystem` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `uuid` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `pushPolicy` varbinary(64) NOT NULL,
  `credentialPHID` varbinary(64) DEFAULT NULL,
  `almanacServicePHID` varbinary(64) DEFAULT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `repositorySlug` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `localPath` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `callsign` (`callsign`),
  UNIQUE KEY `key_slug` (`repositorySlug`),
  UNIQUE KEY `key_local` (`localPath`),
  KEY `key_vcs` (`versionControlSystem`),
  KEY `key_name` (`name`(128)),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository`
--

LOCK TABLES `repository` WRITE;
/*!40000 ALTER TABLE `repository` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_auditrequest`
--

DROP TABLE IF EXISTS `repository_auditrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_auditrequest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auditorPHID` varbinary(64) NOT NULL,
  `commitPHID` varbinary(64) NOT NULL,
  `auditStatus` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `auditReasons` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_unique` (`commitPHID`,`auditorPHID`),
  KEY `commitPHID` (`commitPHID`),
  KEY `auditorPHID` (`auditorPHID`,`auditStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_auditrequest`
--

LOCK TABLES `repository_auditrequest` WRITE;
/*!40000 ALTER TABLE `repository_auditrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_auditrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_branch`
--

DROP TABLE IF EXISTS `repository_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryID` int(10) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `lintCommit` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `repositoryID` (`repositoryID`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_branch`
--

LOCK TABLES `repository_branch` WRITE;
/*!40000 ALTER TABLE `repository_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_commit`
--

DROP TABLE IF EXISTS `repository_commit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_commit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryID` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `commitIdentifier` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `mailKey` binary(20) NOT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `auditStatus` int(10) unsigned NOT NULL,
  `summary` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `importStatus` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  UNIQUE KEY `key_commit_identity` (`commitIdentifier`,`repositoryID`),
  KEY `repositoryID_2` (`repositoryID`,`epoch`),
  KEY `authorPHID` (`authorPHID`,`auditStatus`,`epoch`),
  KEY `repositoryID` (`repositoryID`,`importStatus`),
  KEY `key_epoch` (`epoch`),
  KEY `key_author` (`authorPHID`,`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_commit`
--

LOCK TABLES `repository_commit` WRITE;
/*!40000 ALTER TABLE `repository_commit` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_commit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_commitdata`
--

DROP TABLE IF EXISTS `repository_commitdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_commitdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `commitID` int(10) unsigned NOT NULL,
  `authorName` longtext COLLATE utf8mb4_bin NOT NULL,
  `commitMessage` longtext COLLATE utf8mb4_bin NOT NULL,
  `commitDetails` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `commitID` (`commitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_commitdata`
--

LOCK TABLES `repository_commitdata` WRITE;
/*!40000 ALTER TABLE `repository_commitdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_commitdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_commithint`
--

DROP TABLE IF EXISTS `repository_commithint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_commithint` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryPHID` varbinary(64) NOT NULL,
  `oldCommitIdentifier` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `newCommitIdentifier` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `hintType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_old` (`repositoryPHID`,`oldCommitIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_commithint`
--

LOCK TABLES `repository_commithint` WRITE;
/*!40000 ALTER TABLE `repository_commithint` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_commithint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_coverage`
--

DROP TABLE IF EXISTS `repository_coverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_coverage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branchID` int(10) unsigned NOT NULL,
  `commitID` int(10) unsigned NOT NULL,
  `pathID` int(10) unsigned NOT NULL,
  `coverage` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_path` (`branchID`,`pathID`,`commitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_coverage`
--

LOCK TABLES `repository_coverage` WRITE;
/*!40000 ALTER TABLE `repository_coverage` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_coverage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_filesystem`
--

DROP TABLE IF EXISTS `repository_filesystem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_filesystem` (
  `repositoryID` int(10) unsigned NOT NULL,
  `parentID` int(10) unsigned NOT NULL,
  `svnCommit` int(10) unsigned NOT NULL,
  `pathID` int(10) unsigned NOT NULL,
  `existed` tinyint(1) NOT NULL,
  `fileType` int(10) unsigned NOT NULL,
  PRIMARY KEY (`repositoryID`,`parentID`,`pathID`,`svnCommit`),
  KEY `repositoryID` (`repositoryID`,`svnCommit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_filesystem`
--

LOCK TABLES `repository_filesystem` WRITE;
/*!40000 ALTER TABLE `repository_filesystem` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_filesystem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_gitlfsref`
--

DROP TABLE IF EXISTS `repository_gitlfsref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_gitlfsref` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryPHID` varbinary(64) NOT NULL,
  `objectHash` binary(64) NOT NULL,
  `byteSize` bigint(20) unsigned NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `filePHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_hash` (`repositoryPHID`,`objectHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_gitlfsref`
--

LOCK TABLES `repository_gitlfsref` WRITE;
/*!40000 ALTER TABLE `repository_gitlfsref` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_gitlfsref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_lintmessage`
--

DROP TABLE IF EXISTS `repository_lintmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_lintmessage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branchID` int(10) unsigned NOT NULL,
  `path` longtext COLLATE utf8mb4_bin NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `code` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `severity` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `branchID` (`branchID`,`path`(64)),
  KEY `branchID_2` (`branchID`,`code`,`path`(64)),
  KEY `key_author` (`authorPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_lintmessage`
--

LOCK TABLES `repository_lintmessage` WRITE;
/*!40000 ALTER TABLE `repository_lintmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_lintmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_mirror`
--

DROP TABLE IF EXISTS `repository_mirror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_mirror` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `remoteURI` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `credentialPHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_repository` (`repositoryPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_mirror`
--

LOCK TABLES `repository_mirror` WRITE;
/*!40000 ALTER TABLE `repository_mirror` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_mirror` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_oldref`
--

DROP TABLE IF EXISTS `repository_oldref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_oldref` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryPHID` varbinary(64) NOT NULL,
  `commitIdentifier` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_repository` (`repositoryPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_oldref`
--

LOCK TABLES `repository_oldref` WRITE;
/*!40000 ALTER TABLE `repository_oldref` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_oldref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_parents`
--

DROP TABLE IF EXISTS `repository_parents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_parents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `childCommitID` int(10) unsigned NOT NULL,
  `parentCommitID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_child` (`childCommitID`,`parentCommitID`),
  KEY `key_parent` (`parentCommitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_parents`
--

LOCK TABLES `repository_parents` WRITE;
/*!40000 ALTER TABLE `repository_parents` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_parents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_path`
--

DROP TABLE IF EXISTS `repository_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_path` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` longtext COLLATE utf8mb4_bin NOT NULL,
  `pathHash` binary(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pathHash` (`pathHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_path`
--

LOCK TABLES `repository_path` WRITE;
/*!40000 ALTER TABLE `repository_path` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_pathchange`
--

DROP TABLE IF EXISTS `repository_pathchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_pathchange` (
  `repositoryID` int(10) unsigned NOT NULL,
  `pathID` int(10) unsigned NOT NULL,
  `commitID` int(10) unsigned NOT NULL,
  `targetPathID` int(10) unsigned DEFAULT NULL,
  `targetCommitID` int(10) unsigned DEFAULT NULL,
  `changeType` int(10) unsigned NOT NULL,
  `fileType` int(10) unsigned NOT NULL,
  `isDirect` tinyint(1) NOT NULL,
  `commitSequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`commitID`,`pathID`),
  KEY `repositoryID` (`repositoryID`,`pathID`,`commitSequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_pathchange`
--

LOCK TABLES `repository_pathchange` WRITE;
/*!40000 ALTER TABLE `repository_pathchange` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_pathchange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_pullevent`
--

DROP TABLE IF EXISTS `repository_pullevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_pullevent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) DEFAULT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `pullerPHID` varbinary(64) DEFAULT NULL,
  `remoteAddress` varbinary(64) DEFAULT NULL,
  `remoteProtocol` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `resultType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `resultCode` int(10) unsigned NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_repository` (`repositoryPHID`),
  KEY `key_epoch` (`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_pullevent`
--

LOCK TABLES `repository_pullevent` WRITE;
/*!40000 ALTER TABLE `repository_pullevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_pullevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_pushevent`
--

DROP TABLE IF EXISTS `repository_pushevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_pushevent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `pusherPHID` varbinary(64) NOT NULL,
  `remoteAddress` varbinary(64) DEFAULT NULL,
  `remoteProtocol` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `rejectCode` int(10) unsigned NOT NULL,
  `rejectDetails` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_repository` (`repositoryPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_pushevent`
--

LOCK TABLES `repository_pushevent` WRITE;
/*!40000 ALTER TABLE `repository_pushevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_pushevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_pushlog`
--

DROP TABLE IF EXISTS `repository_pushlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_pushlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `pushEventPHID` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `pusherPHID` varbinary(64) NOT NULL,
  `refType` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `refNameHash` binary(12) DEFAULT NULL,
  `refNameRaw` longblob,
  `refNameEncoding` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL,
  `refOld` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `refNew` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `mergeBase` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `changeFlags` int(10) unsigned NOT NULL,
  `devicePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_repository` (`repositoryPHID`),
  KEY `key_ref` (`repositoryPHID`,`refNew`),
  KEY `key_pusher` (`pusherPHID`),
  KEY `key_name` (`repositoryPHID`,`refNameHash`),
  KEY `key_event` (`pushEventPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_pushlog`
--

LOCK TABLES `repository_pushlog` WRITE;
/*!40000 ALTER TABLE `repository_pushlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_pushlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_refcursor`
--

DROP TABLE IF EXISTS `repository_refcursor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_refcursor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `refType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `refNameHash` binary(12) NOT NULL,
  `refNameRaw` longblob NOT NULL,
  `refNameEncoding` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL,
  `commitIdentifier` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `isClosed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_cursor` (`repositoryPHID`,`refType`,`refNameHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_refcursor`
--

LOCK TABLES `repository_refcursor` WRITE;
/*!40000 ALTER TABLE `repository_refcursor` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_refcursor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_statusmessage`
--

DROP TABLE IF EXISTS `repository_statusmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_statusmessage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryID` int(10) unsigned NOT NULL,
  `statusType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `statusCode` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  `messageCount` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `repositoryID` (`repositoryID`,`statusType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_statusmessage`
--

LOCK TABLES `repository_statusmessage` WRITE;
/*!40000 ALTER TABLE `repository_statusmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_statusmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_summary`
--

DROP TABLE IF EXISTS `repository_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_summary` (
  `repositoryID` int(10) unsigned NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `lastCommitID` int(10) unsigned NOT NULL,
  `epoch` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`repositoryID`),
  KEY `key_epoch` (`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_summary`
--

LOCK TABLES `repository_summary` WRITE;
/*!40000 ALTER TABLE `repository_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_symbol`
--

DROP TABLE IF EXISTS `repository_symbol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_symbol` (
  `repositoryPHID` varbinary(64) NOT NULL,
  `symbolContext` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `symbolName` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `symbolType` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `symbolLanguage` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `pathID` int(10) unsigned NOT NULL,
  `lineNumber` int(10) unsigned NOT NULL,
  KEY `symbolName` (`symbolName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_symbol`
--

LOCK TABLES `repository_symbol` WRITE;
/*!40000 ALTER TABLE `repository_symbol` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_symbol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_transaction`
--

DROP TABLE IF EXISTS `repository_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_transaction`
--

LOCK TABLES `repository_transaction` WRITE;
/*!40000 ALTER TABLE `repository_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_uri`
--

DROP TABLE IF EXISTS `repository_uri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_uri` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `repositoryPHID` varbinary(64) NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `builtinProtocol` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `builtinIdentifier` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ioType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `displayType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `credentialPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_builtin` (`repositoryPHID`,`builtinProtocol`,`builtinIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_uri`
--

LOCK TABLES `repository_uri` WRITE;
/*!40000 ALTER TABLE `repository_uri` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_uri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_uriindex`
--

DROP TABLE IF EXISTS `repository_uriindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_uriindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryPHID` varbinary(64) NOT NULL,
  `repositoryURI` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_repository` (`repositoryPHID`),
  KEY `key_uri` (`repositoryURI`(128))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_uriindex`
--

LOCK TABLES `repository_uriindex` WRITE;
/*!40000 ALTER TABLE `repository_uriindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_uriindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_uritransaction`
--

DROP TABLE IF EXISTS `repository_uritransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_uritransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_uritransaction`
--

LOCK TABLES `repository_uritransaction` WRITE;
/*!40000 ALTER TABLE `repository_uritransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_uritransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_vcspassword`
--

DROP TABLE IF EXISTS `repository_vcspassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_vcspassword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `passwordHash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`userPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_vcspassword`
--

LOCK TABLES `repository_vcspassword` WRITE;
/*!40000 ALTER TABLE `repository_vcspassword` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_vcspassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_workingcopyversion`
--

DROP TABLE IF EXISTS `repository_workingcopyversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_workingcopyversion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repositoryPHID` varbinary(64) NOT NULL,
  `devicePHID` varbinary(64) NOT NULL,
  `repositoryVersion` int(10) unsigned NOT NULL,
  `isWriting` tinyint(1) NOT NULL,
  `writeProperties` longtext COLLATE utf8mb4_bin,
  `lockOwner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_workingcopy` (`repositoryPHID`,`devicePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_workingcopyversion`
--

LOCK TABLES `repository_workingcopyversion` WRITE;
/*!40000 ALTER TABLE `repository_workingcopyversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_workingcopyversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_search`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_search` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_search`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_document`
--

DROP TABLE IF EXISTS `search_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_document` (
  `phid` varbinary(64) NOT NULL,
  `documentType` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `documentTitle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `documentCreated` int(10) unsigned NOT NULL,
  `documentModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`phid`),
  KEY `documentCreated` (`documentCreated`),
  KEY `key_type` (`documentType`,`documentCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_document`
--

LOCK TABLES `search_document` WRITE;
/*!40000 ALTER TABLE `search_document` DISABLE KEYS */;
INSERT INTO `search_document` VALUES ('PHID-USER-q5wjhmvxv4zslcvjko2a','USER','superadmin (Superadmin)',1478565097,1478565097);
/*!40000 ALTER TABLE `search_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_documentfield`
--

DROP TABLE IF EXISTS `search_documentfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_documentfield` (
  `phid` varbinary(64) NOT NULL,
  `phidType` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `field` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `auxPHID` varbinary(64) DEFAULT NULL,
  `corpus` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  KEY `phid` (`phid`),
  FULLTEXT KEY `corpus` (`corpus`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_documentfield`
--

LOCK TABLES `search_documentfield` WRITE;
/*!40000 ALTER TABLE `search_documentfield` DISABLE KEYS */;
INSERT INTO `search_documentfield` VALUES ('PHID-USER-q5wjhmvxv4zslcvjko2a','USER','titl',NULL,'superadmin (Superadmin)');
/*!40000 ALTER TABLE `search_documentfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_documentrelationship`
--

DROP TABLE IF EXISTS `search_documentrelationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_documentrelationship` (
  `phid` varbinary(64) NOT NULL,
  `relatedPHID` varbinary(64) NOT NULL,
  `relation` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `relatedType` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `relatedTime` int(10) unsigned NOT NULL,
  KEY `phid` (`phid`),
  KEY `relatedPHID` (`relatedPHID`,`relation`),
  KEY `relation` (`relation`,`relatedPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_documentrelationship`
--

LOCK TABLES `search_documentrelationship` WRITE;
/*!40000 ALTER TABLE `search_documentrelationship` DISABLE KEYS */;
INSERT INTO `search_documentrelationship` VALUES ('PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-USER-q5wjhmvxv4zslcvjko2a','open','USER',1478565097);
/*!40000 ALTER TABLE `search_documentrelationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_editengineconfiguration`
--

DROP TABLE IF EXISTS `search_editengineconfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_editengineconfiguration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `engineKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `builtinKey` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDisabled` tinyint(1) NOT NULL DEFAULT '0',
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isEdit` tinyint(1) NOT NULL,
  `createOrder` int(10) unsigned NOT NULL,
  `editOrder` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_engine` (`engineKey`,`builtinKey`),
  KEY `key_default` (`engineKey`,`isDefault`,`isDisabled`),
  KEY `key_edit` (`engineKey`,`isEdit`,`isDisabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_editengineconfiguration`
--

LOCK TABLES `search_editengineconfiguration` WRITE;
/*!40000 ALTER TABLE `search_editengineconfiguration` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_editengineconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_editengineconfigurationtransaction`
--

DROP TABLE IF EXISTS `search_editengineconfigurationtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_editengineconfigurationtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_editengineconfigurationtransaction`
--

LOCK TABLES `search_editengineconfigurationtransaction` WRITE;
/*!40000 ALTER TABLE `search_editengineconfigurationtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_editengineconfigurationtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_indexversion`
--

DROP TABLE IF EXISTS `search_indexversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_indexversion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `extensionKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `version` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_object` (`objectPHID`,`extensionKey`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_indexversion`
--

LOCK TABLES `search_indexversion` WRITE;
/*!40000 ALTER TABLE `search_indexversion` DISABLE KEYS */;
INSERT INTO `search_indexversion` VALUES (1,'PHID-USER-q5wjhmvxv4zslcvjko2a','fulltext','none:none');
/*!40000 ALTER TABLE `search_indexversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_namedquery`
--

DROP TABLE IF EXISTS `search_namedquery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_namedquery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `engineClassName` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `queryName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `queryKey` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `isBuiltin` tinyint(1) NOT NULL DEFAULT '0',
  `isDisabled` tinyint(1) NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_userquery` (`userPHID`,`engineClassName`,`queryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_namedquery`
--

LOCK TABLES `search_namedquery` WRITE;
/*!40000 ALTER TABLE `search_namedquery` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_namedquery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_profilepanelconfiguration`
--

DROP TABLE IF EXISTS `search_profilepanelconfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_profilepanelconfiguration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `profilePHID` varbinary(64) NOT NULL,
  `panelKey` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `builtinKey` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `panelOrder` int(10) unsigned DEFAULT NULL,
  `visibility` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `panelProperties` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_profile` (`profilePHID`,`panelOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_profilepanelconfiguration`
--

LOCK TABLES `search_profilepanelconfiguration` WRITE;
/*!40000 ALTER TABLE `search_profilepanelconfiguration` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_profilepanelconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_profilepanelconfigurationtransaction`
--

DROP TABLE IF EXISTS `search_profilepanelconfigurationtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_profilepanelconfigurationtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_profilepanelconfigurationtransaction`
--

LOCK TABLES `search_profilepanelconfigurationtransaction` WRITE;
/*!40000 ALTER TABLE `search_profilepanelconfigurationtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_profilepanelconfigurationtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_savedquery`
--

DROP TABLE IF EXISTS `search_savedquery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_savedquery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `engineClassName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `queryKey` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_queryKey` (`queryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_savedquery`
--

LOCK TABLES `search_savedquery` WRITE;
/*!40000 ALTER TABLE `search_savedquery` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_savedquery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_slowvote`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_slowvote` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_slowvote`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slowvote_choice`
--

DROP TABLE IF EXISTS `slowvote_choice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slowvote_choice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pollID` int(10) unsigned NOT NULL,
  `optionID` int(10) unsigned NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pollID` (`pollID`),
  KEY `authorPHID` (`authorPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slowvote_choice`
--

LOCK TABLES `slowvote_choice` WRITE;
/*!40000 ALTER TABLE `slowvote_choice` DISABLE KEYS */;
/*!40000 ALTER TABLE `slowvote_choice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slowvote_option`
--

DROP TABLE IF EXISTS `slowvote_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slowvote_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pollID` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pollID` (`pollID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slowvote_option`
--

LOCK TABLES `slowvote_option` WRITE;
/*!40000 ALTER TABLE `slowvote_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `slowvote_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slowvote_poll`
--

DROP TABLE IF EXISTS `slowvote_poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slowvote_poll` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `responseVisibility` int(10) unsigned NOT NULL,
  `shuffle` int(10) unsigned NOT NULL,
  `method` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `isClosed` tinyint(1) NOT NULL,
  `spacePHID` varbinary(64) DEFAULT NULL,
  `mailKey` binary(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phid` (`phid`),
  KEY `key_space` (`spacePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slowvote_poll`
--

LOCK TABLES `slowvote_poll` WRITE;
/*!40000 ALTER TABLE `slowvote_poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `slowvote_poll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slowvote_transaction`
--

DROP TABLE IF EXISTS `slowvote_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slowvote_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slowvote_transaction`
--

LOCK TABLES `slowvote_transaction` WRITE;
/*!40000 ALTER TABLE `slowvote_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `slowvote_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slowvote_transaction_comment`
--

DROP TABLE IF EXISTS `slowvote_transaction_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slowvote_transaction_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `transactionPHID` varbinary(64) DEFAULT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_version` (`transactionPHID`,`commentVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slowvote_transaction_comment`
--

LOCK TABLES `slowvote_transaction_comment` WRITE;
/*!40000 ALTER TABLE `slowvote_transaction_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `slowvote_transaction_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_spaces`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_spaces` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_spaces`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spaces_namespace`
--

DROP TABLE IF EXISTS `spaces_namespace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spaces_namespace` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `namespaceName` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `isDefaultNamespace` tinyint(1) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `isArchived` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_default` (`isDefaultNamespace`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spaces_namespace`
--

LOCK TABLES `spaces_namespace` WRITE;
/*!40000 ALTER TABLE `spaces_namespace` DISABLE KEYS */;
/*!40000 ALTER TABLE `spaces_namespace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spaces_namespacetransaction`
--

DROP TABLE IF EXISTS `spaces_namespacetransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spaces_namespacetransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spaces_namespacetransaction`
--

LOCK TABLES `spaces_namespacetransaction` WRITE;
/*!40000 ALTER TABLE `spaces_namespacetransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `spaces_namespacetransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_system`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_system` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_system`;

--
-- Table structure for table `system_actionlog`
--

DROP TABLE IF EXISTS `system_actionlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_actionlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actorHash` binary(12) NOT NULL,
  `actorIdentity` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `action` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `score` double NOT NULL,
  `epoch` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_epoch` (`epoch`),
  KEY `key_action` (`actorHash`,`action`,`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_actionlog`
--

LOCK TABLES `system_actionlog` WRITE;
/*!40000 ALTER TABLE `system_actionlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_actionlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_destructionlog`
--

DROP TABLE IF EXISTS `system_destructionlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_destructionlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectClass` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `rootLogID` int(10) unsigned DEFAULT NULL,
  `objectPHID` varbinary(64) DEFAULT NULL,
  `objectMonogram` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `epoch` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_epoch` (`epoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_destructionlog`
--

LOCK TABLES `system_destructionlog` WRITE;
/*!40000 ALTER TABLE `system_destructionlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_destructionlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_token`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_token` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_token`;

--
-- Table structure for table `token_count`
--

DROP TABLE IF EXISTS `token_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_count` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `tokenCount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_objectPHID` (`objectPHID`),
  KEY `key_count` (`tokenCount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_count`
--

LOCK TABLES `token_count` WRITE;
/*!40000 ALTER TABLE `token_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `token_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token_given`
--

DROP TABLE IF EXISTS `token_given`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_given` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `tokenPHID` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_all` (`objectPHID`,`authorPHID`),
  KEY `key_author` (`authorPHID`),
  KEY `key_token` (`tokenPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_given`
--

LOCK TABLES `token_given` WRITE;
/*!40000 ALTER TABLE `token_given` DISABLE KEYS */;
/*!40000 ALTER TABLE `token_given` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token_token`
--

DROP TABLE IF EXISTS `token_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `flavor` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `builtinKey` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `creatorPHID` varbinary(64) NOT NULL,
  `tokenImagePHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_builtin` (`builtinKey`),
  KEY `key_creator` (`creatorPHID`,`dateModified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_token`
--

LOCK TABLES `token_token` WRITE;
/*!40000 ALTER TABLE `token_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_user`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_user` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_user`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phabricator_session`
--

DROP TABLE IF EXISTS `phabricator_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phabricator_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sessionKey` binary(40) NOT NULL,
  `sessionStart` int(10) unsigned NOT NULL,
  `sessionExpires` int(10) unsigned NOT NULL,
  `highSecurityUntil` int(10) unsigned DEFAULT NULL,
  `isPartial` tinyint(1) NOT NULL DEFAULT '0',
  `signedLegalpadDocuments` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sessionKey` (`sessionKey`),
  KEY `key_identity` (`userPHID`,`type`),
  KEY `key_expires` (`sessionExpires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phabricator_session`
--

LOCK TABLES `phabricator_session` WRITE;
/*!40000 ALTER TABLE `phabricator_session` DISABLE KEYS */;
INSERT INTO `phabricator_session` VALUES (1,'PHID-USER-q5wjhmvxv4zslcvjko2a','web','3bcbc93f3c2d6a9714ad6677995d1109c93b8536',1478565097,1481157097,NULL,0,1);
/*!40000 ALTER TABLE `phabricator_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `userName` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `realName` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `passwordSalt` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `passwordHash` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  `conduitCertificate` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `isSystemAgent` tinyint(1) NOT NULL DEFAULT '0',
  `isDisabled` tinyint(1) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `isEmailVerified` int(10) unsigned NOT NULL,
  `isApproved` int(10) unsigned NOT NULL,
  `accountSecret` binary(64) NOT NULL,
  `isEnrolledInMultiFactor` tinyint(1) NOT NULL DEFAULT '0',
  `availabilityCache` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `availabilityCacheTTL` int(10) unsigned DEFAULT NULL,
  `isMailingList` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userName` (`userName`),
  UNIQUE KEY `phid` (`phid`),
  KEY `realName` (`realName`),
  KEY `key_approved` (`isApproved`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'PHID-USER-q5wjhmvxv4zslcvjko2a','superadmin','Superadmin','8ffca9efc42a52ef32c9841bd7e264ce','bcrypt:$2y$11$tH6Q5TWpDavKyEkeLOilmuEU8FQDosgIkf/YURPVcCGHBkcTi/1te',1478565097,1478565097,NULL,'vonqj636c5lcqf2wjaevmwqdzqbu2fynqsr2mmqjc3jppn4rgshlcpsbrtxajyevakjtpnv5dsdbfckkqs7ssyo4v75xbrubj6jm6xssqxsxr5tmdnkt3xhfymrw7ire7z5miqgpikvhd7mldrlgfcldvxtstj7fnpfzwfrv2b5ml7ta36v3xtg3hyakaxdaxbmsbfupcjppmlfag2dw4ccg7mywvyeqkkmdmheaurxcp7gddxtvklqrsqvexqz',0,0,1,1,1,'jj2u63rprlkgcaqrkcagdrrl4ktc3fwk7ftci7efxvyzcyeribvxbaz5pkmu3355',0,'{\"until\":null,\"eventPHID\":null,\"availability\":null}',1478824297,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_authinvite`
--

DROP TABLE IF EXISTS `user_authinvite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_authinvite` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authorPHID` varbinary(64) NOT NULL,
  `emailAddress` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `verificationHash` binary(12) NOT NULL,
  `acceptedByPHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_address` (`emailAddress`),
  UNIQUE KEY `key_code` (`verificationHash`),
  UNIQUE KEY `key_phid` (`phid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_authinvite`
--

LOCK TABLES `user_authinvite` WRITE;
/*!40000 ALTER TABLE `user_authinvite` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_authinvite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_cache`
--

DROP TABLE IF EXISTS `user_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_cache` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `cacheIndex` binary(12) NOT NULL,
  `cacheKey` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `cacheData` longtext COLLATE utf8mb4_bin NOT NULL,
  `cacheType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_usercache` (`userPHID`,`cacheIndex`),
  KEY `key_cachekey` (`cacheIndex`),
  KEY `key_cachetype` (`cacheType`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_cache`
--

LOCK TABLES `user_cache` WRITE;
/*!40000 ALTER TABLE `user_cache` DISABLE KEYS */;
INSERT INTO `user_cache` VALUES (1,'PHID-USER-q5wjhmvxv4zslcvjko2a','y_FPXujT93U3','user.profile.image.uri.v1','AZWQW80MyLaG,https://can-kovai.cloudron.eu/res/phabricator/e132bb6a/rsrc/image/avatar.png','user.profile'),(2,'PHID-USER-q5wjhmvxv4zslcvjko2a','K_AbNYBYs5ZV','user.preferences.v1','[]','preferences'),(3,'PHID-USER-q5wjhmvxv4zslcvjko2a','ShljA1NUxUbb','user.message.count.v1','0','message.count'),(4,'PHID-USER-q5wjhmvxv4zslcvjko2a','rOGS6Fh944dc','user.notification.count.v1','0','notification.count');
/*!40000 ALTER TABLE `user_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_configuredcustomfieldstorage`
--

DROP TABLE IF EXISTS `user_configuredcustomfieldstorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_configuredcustomfieldstorage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `fieldIndex` binary(12) NOT NULL,
  `fieldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `objectPHID` (`objectPHID`,`fieldIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_configuredcustomfieldstorage`
--

LOCK TABLES `user_configuredcustomfieldstorage` WRITE;
/*!40000 ALTER TABLE `user_configuredcustomfieldstorage` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_configuredcustomfieldstorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_customfieldnumericindex`
--

DROP TABLE IF EXISTS `user_customfieldnumericindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_customfieldnumericindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`),
  KEY `key_find` (`indexKey`,`indexValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_customfieldnumericindex`
--

LOCK TABLES `user_customfieldnumericindex` WRITE;
/*!40000 ALTER TABLE `user_customfieldnumericindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_customfieldnumericindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_customfieldstringindex`
--

DROP TABLE IF EXISTS `user_customfieldstringindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_customfieldstringindex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectPHID` varbinary(64) NOT NULL,
  `indexKey` binary(12) NOT NULL,
  `indexValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_join` (`objectPHID`,`indexKey`,`indexValue`(64)),
  KEY `key_find` (`indexKey`,`indexValue`(64))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_customfieldstringindex`
--

LOCK TABLES `user_customfieldstringindex` WRITE;
/*!40000 ALTER TABLE `user_customfieldstringindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_customfieldstringindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_email`
--

DROP TABLE IF EXISTS `user_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `isVerified` tinyint(1) NOT NULL DEFAULT '0',
  `isPrimary` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address` (`address`),
  KEY `userPHID` (`userPHID`,`isPrimary`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_email`
--

LOCK TABLES `user_email` WRITE;
/*!40000 ALTER TABLE `user_email` DISABLE KEYS */;
INSERT INTO `user_email` VALUES (1,'PHID-USER-q5wjhmvxv4zslcvjko2a','admin@server.test',1,1,'axgx62scqr35z2vvlgolampp',1478565097,1478565097);
/*!40000 ALTER TABLE `user_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_externalaccount`
--

DROP TABLE IF EXISTS `user_externalaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_externalaccount` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `userPHID` varbinary(64) DEFAULT NULL,
  `accountType` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `accountDomain` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `accountSecret` longtext COLLATE utf8mb4_bin,
  `accountID` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `displayName` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `realName` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `emailVerified` tinyint(1) NOT NULL,
  `accountURI` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_details` (`accountType`,`accountDomain`,`accountID`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_user` (`userPHID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_externalaccount`
--

LOCK TABLES `user_externalaccount` WRITE;
/*!40000 ALTER TABLE `user_externalaccount` DISABLE KEYS */;
INSERT INTO `user_externalaccount` VALUES (1,'PHID-XUSR-gmbzmjuo4kr6f2br4vjt','PHID-USER-q5wjhmvxv4zslcvjko2a','password','self','pzrft5elysgwt2gmqgucof2subdojzkj','PHID-USER-q5wjhmvxv4zslcvjko2a',NULL,1478565097,1478565097,NULL,NULL,NULL,0,NULL,NULL,'[]');
/*!40000 ALTER TABLE `user_externalaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_log`
--

DROP TABLE IF EXISTS `user_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actorPHID` varbinary(64) DEFAULT NULL,
  `userPHID` varbinary(64) NOT NULL,
  `action` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `details` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `remoteAddr` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `session` binary(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `actorPHID` (`actorPHID`,`dateCreated`),
  KEY `userPHID` (`userPHID`,`dateCreated`),
  KEY `action` (`action`,`dateCreated`),
  KEY `dateCreated` (`dateCreated`),
  KEY `remoteAddr` (`remoteAddr`,`dateCreated`),
  KEY `session` (`session`,`dateCreated`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_log`
--

LOCK TABLES `user_log` WRITE;
/*!40000 ALTER TABLE `user_log` DISABLE KEYS */;
INSERT INTO `user_log` VALUES (1,'PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-USER-q5wjhmvxv4zslcvjko2a','create','null','\"admin@server.test\"','{\"host\":\"ca96c5a35ee9\",\"user_agent\":\"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/51.0.2704.106 Safari\\/537.36\"}',1478565097,1478565097,'172.18.0.1',NULL),(2,'PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-USER-q5wjhmvxv4zslcvjko2a','change-password','null','null','{\"host\":\"ca96c5a35ee9\",\"user_agent\":\"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/51.0.2704.106 Safari\\/537.36\"}',1478565097,1478565097,'172.18.0.1',NULL),(3,'PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-USER-q5wjhmvxv4zslcvjko2a','admin','false','true','{\"host\":\"ca96c5a35ee9\",\"user_agent\":\"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/51.0.2704.106 Safari\\/537.36\"}',1478565097,1478565097,'172.18.0.1',NULL),(4,NULL,'PHID-USER-q5wjhmvxv4zslcvjko2a','login-partial','null','null','{\"session_type\":\"web\",\"host\":\"ca96c5a35ee9\",\"user_agent\":\"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/51.0.2704.106 Safari\\/537.36\"}',1478565097,1478565097,'172.18.0.1','3bcbc93f3c2d6a9714ad6677995d1109c93b8536'),(5,'PHID-USER-q5wjhmvxv4zslcvjko2a','PHID-USER-q5wjhmvxv4zslcvjko2a','login-full','null','null','{\"host\":\"ca96c5a35ee9\",\"user_agent\":\"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/51.0.2704.106 Safari\\/537.36\"}',1478565098,1478565098,'172.18.0.1','3bcbc93f3c2d6a9714ad6677995d1109c93b8536');
/*!40000 ALTER TABLE `user_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_nametoken`
--

DROP TABLE IF EXISTS `user_nametoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_nametoken` (
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  KEY `token` (`token`(128)),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_nametoken`
--

LOCK TABLES `user_nametoken` WRITE;
/*!40000 ALTER TABLE `user_nametoken` DISABLE KEYS */;
INSERT INTO `user_nametoken` VALUES ('superadmin',1);
/*!40000 ALTER TABLE `user_nametoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) DEFAULT NULL,
  `preferences` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `phid` varbinary(64) NOT NULL,
  `builtinKey` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_builtin` (`builtinKey`),
  UNIQUE KEY `key_user` (`userPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferencestransaction`
--

DROP TABLE IF EXISTS `user_preferencestransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferencestransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferencestransaction`
--

LOCK TABLES `user_preferencestransaction` WRITE;
/*!40000 ALTER TABLE `user_preferencestransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_preferencestransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userPHID` varbinary(64) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `blurb` longtext COLLATE utf8mb4_bin NOT NULL,
  `profileImagePHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `icon` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userPHID` (`userPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_transaction`
--

DROP TABLE IF EXISTS `user_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_transaction`
--

LOCK TABLES `user_transaction` WRITE;
/*!40000 ALTER TABLE `user_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_worker`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_worker` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_worker`;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `src` varbinary(64) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `dst` varbinary(64) NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `seq` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`src`,`type`,`dst`),
  UNIQUE KEY `key_dst` (`dst`,`type`,`src`),
  KEY `src` (`src`,`type`,`dateCreated`,`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edgedata`
--

DROP TABLE IF EXISTS `edgedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edgedata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edgedata`
--

LOCK TABLES `edgedata` WRITE;
/*!40000 ALTER TABLE `edgedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `edgedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lisk_counter`
--

DROP TABLE IF EXISTS `lisk_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lisk_counter` (
  `counterName` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `counterValue` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`counterName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lisk_counter`
--

LOCK TABLES `lisk_counter` WRITE;
/*!40000 ALTER TABLE `lisk_counter` DISABLE KEYS */;
INSERT INTO `lisk_counter` VALUES ('worker_activetask',7);
/*!40000 ALTER TABLE `lisk_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_activetask`
--

DROP TABLE IF EXISTS `worker_activetask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_activetask` (
  `id` int(10) unsigned NOT NULL,
  `taskClass` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `leaseOwner` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `leaseExpires` int(10) unsigned DEFAULT NULL,
  `failureCount` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned DEFAULT NULL,
  `failureTime` int(10) unsigned DEFAULT NULL,
  `priority` int(10) unsigned NOT NULL,
  `objectPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dataID` (`dataID`),
  KEY `leaseExpires` (`leaseExpires`),
  KEY `leaseOwner` (`leaseOwner`(16)),
  KEY `key_failuretime` (`failureTime`),
  KEY `taskClass` (`taskClass`),
  KEY `leaseOwner_2` (`leaseOwner`,`priority`,`id`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_activetask`
--

LOCK TABLES `worker_activetask` WRITE;
/*!40000 ALTER TABLE `worker_activetask` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_activetask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_archivetask`
--

DROP TABLE IF EXISTS `worker_archivetask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_archivetask` (
  `id` int(10) unsigned NOT NULL,
  `taskClass` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `leaseOwner` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `leaseExpires` int(10) unsigned DEFAULT NULL,
  `failureCount` int(10) unsigned NOT NULL,
  `dataID` int(10) unsigned NOT NULL,
  `result` int(10) unsigned NOT NULL,
  `duration` bigint(20) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `objectPHID` varbinary(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dateCreated` (`dateCreated`),
  KEY `leaseOwner` (`leaseOwner`,`priority`,`id`),
  KEY `key_object` (`objectPHID`),
  KEY `key_modified` (`dateModified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_archivetask`
--

LOCK TABLES `worker_archivetask` WRITE;
/*!40000 ALTER TABLE `worker_archivetask` DISABLE KEYS */;
INSERT INTO `worker_archivetask` VALUES (3,'PhabricatorSearchWorker','46:1478565097:ca96c5a35ee9:53',1478572297,0,1,0,138037,1478565097,1478565097,4000,NULL),(4,'PhabricatorSearchWorker','46:1478565097:ca96c5a35ee9:54',1478572297,0,2,0,2219,1478565097,1478565097,4000,NULL),(5,'PhabricatorSearchWorker','46:1478565097:ca96c5a35ee9:55',1478572297,0,3,0,2366,1478565097,1478565097,4000,NULL),(6,'PhabricatorApplicationTransactionPublishWorker','46:1478565137:ca96c5a35ee9:95',1478572337,0,4,0,12499,1478565137,1478565137,1000,'PHID-AUTH-bakr7fx2mwkl6xno6n4z'),(7,'PhabricatorApplicationTransactionPublishWorker','46:1478565170:ca96c5a35ee9:129',1478572370,0,5,0,14811,1478565170,1478565170,1000,'PHID-AUTH-cayoqyeiyw2m7zfvclvs');
/*!40000 ALTER TABLE `worker_archivetask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_bulkjob`
--

DROP TABLE IF EXISTS `worker_bulkjob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_bulkjob` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `jobTypeKey` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `parameters` longtext COLLATE utf8mb4_bin NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_type` (`jobTypeKey`),
  KEY `key_author` (`authorPHID`),
  KEY `key_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_bulkjob`
--

LOCK TABLES `worker_bulkjob` WRITE;
/*!40000 ALTER TABLE `worker_bulkjob` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_bulkjob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_bulkjobtransaction`
--

DROP TABLE IF EXISTS `worker_bulkjobtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_bulkjobtransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `authorPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `viewPolicy` varbinary(64) NOT NULL,
  `editPolicy` varbinary(64) NOT NULL,
  `commentPHID` varbinary(64) DEFAULT NULL,
  `commentVersion` int(10) unsigned NOT NULL,
  `transactionType` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `oldValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `newValue` longtext COLLATE utf8mb4_bin NOT NULL,
  `contentSource` longtext COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_bulkjobtransaction`
--

LOCK TABLES `worker_bulkjobtransaction` WRITE;
/*!40000 ALTER TABLE `worker_bulkjobtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_bulkjobtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_bulktask`
--

DROP TABLE IF EXISTS `worker_bulktask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_bulktask` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bulkJobPHID` varbinary(64) NOT NULL,
  `objectPHID` varbinary(64) NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key_job` (`bulkJobPHID`,`status`),
  KEY `key_object` (`objectPHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_bulktask`
--

LOCK TABLES `worker_bulktask` WRITE;
/*!40000 ALTER TABLE `worker_bulktask` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_bulktask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_taskdata`
--

DROP TABLE IF EXISTS `worker_taskdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_taskdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_taskdata`
--

LOCK TABLES `worker_taskdata` WRITE;
/*!40000 ALTER TABLE `worker_taskdata` DISABLE KEYS */;
INSERT INTO `worker_taskdata` VALUES (1,'{\"documentPHID\":\"PHID-USER-q5wjhmvxv4zslcvjko2a\",\"parameters\":[]}'),(2,'{\"documentPHID\":\"PHID-USER-q5wjhmvxv4zslcvjko2a\",\"parameters\":[]}'),(3,'{\"documentPHID\":\"PHID-USER-q5wjhmvxv4zslcvjko2a\",\"parameters\":[]}'),(4,'{\"objectPHID\":\"PHID-AUTH-bakr7fx2mwkl6xno6n4z\",\"actorPHID\":\"PHID-USER-q5wjhmvxv4zslcvjko2a\",\"xactionPHIDs\":[],\"state\":{\"parentMessageID\":null,\"disableEmail\":null,\"isNewObject\":false,\"heraldEmailPHIDs\":[],\"heraldForcedEmailPHIDs\":[],\"heraldHeader\":null,\"mailToPHIDs\":[],\"mailCCPHIDs\":[],\"feedNotifyPHIDs\":[],\"feedRelatedPHIDs\":[],\"excludeMailRecipientPHIDs\":[],\"custom\":[],\"custom.encoding\":[]}}'),(5,'{\"objectPHID\":\"PHID-AUTH-cayoqyeiyw2m7zfvclvs\",\"actorPHID\":\"PHID-USER-q5wjhmvxv4zslcvjko2a\",\"xactionPHIDs\":[\"PHID-XACT-AUTH-adgj3m5crwndvby\",\"PHID-XACT-AUTH-usn5ry3jzrc46h3\",\"PHID-XACT-AUTH-g32lp472vv6hfld\",\"PHID-XACT-AUTH-f5lwkux6gtuoajy\",\"PHID-XACT-AUTH-fjdp2gvxtrshsch\",\"PHID-XACT-AUTH-w43bdbt776llkzr\",\"PHID-XACT-AUTH-fgqhlor73wmxgey\",\"PHID-XACT-AUTH-i7r3l4fheu2u4bx\",\"PHID-XACT-AUTH-6dazcebbnavyhgx\",\"PHID-XACT-AUTH-42d7yob3y3n5eup\",\"PHID-XACT-AUTH-pda5t6mwxx57nfm\"],\"state\":{\"parentMessageID\":null,\"disableEmail\":null,\"isNewObject\":false,\"heraldEmailPHIDs\":[],\"heraldForcedEmailPHIDs\":[],\"heraldHeader\":null,\"mailToPHIDs\":[],\"mailCCPHIDs\":[],\"feedNotifyPHIDs\":[],\"feedRelatedPHIDs\":[],\"excludeMailRecipientPHIDs\":[],\"custom\":[],\"custom.encoding\":[]}}');
/*!40000 ALTER TABLE `worker_taskdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_trigger`
--

DROP TABLE IF EXISTS `worker_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_trigger` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phid` varbinary(64) NOT NULL,
  `triggerVersion` int(10) unsigned NOT NULL,
  `clockClass` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `clockProperties` longtext COLLATE utf8mb4_bin NOT NULL,
  `actionClass` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `actionProperties` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_phid` (`phid`),
  UNIQUE KEY `key_trigger` (`triggerVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_trigger`
--

LOCK TABLES `worker_trigger` WRITE;
/*!40000 ALTER TABLE `worker_trigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_triggerevent`
--

DROP TABLE IF EXISTS `worker_triggerevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_triggerevent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `triggerID` int(10) unsigned NOT NULL,
  `lastEventEpoch` int(10) unsigned DEFAULT NULL,
  `nextEventEpoch` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_trigger` (`triggerID`),
  KEY `key_next` (`nextEventEpoch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_triggerevent`
--

LOCK TABLES `worker_triggerevent` WRITE;
/*!40000 ALTER TABLE `worker_triggerevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `worker_triggerevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_xhpast`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_xhpast` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_xhpast`;

--
-- Table structure for table `xhpast_parsetree`
--

DROP TABLE IF EXISTS `xhpast_parsetree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xhpast_parsetree` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authorPHID` varbinary(64) DEFAULT NULL,
  `input` longtext COLLATE utf8mb4_bin NOT NULL,
  `returnCode` int(10) NOT NULL,
  `stdout` longtext COLLATE utf8mb4_bin NOT NULL,
  `stderr` longtext COLLATE utf8mb4_bin NOT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xhpast_parsetree`
--

LOCK TABLES `xhpast_parsetree` WRITE;
/*!40000 ALTER TABLE `xhpast_parsetree` DISABLE KEYS */;
/*!40000 ALTER TABLE `xhpast_parsetree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `dbprefixgoeshere_xhprof`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dbprefixgoeshere_xhprof` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `dbprefixgoeshere_xhprof`;

--
-- Table structure for table `xhprof_sample`
--

DROP TABLE IF EXISTS `xhprof_sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xhprof_sample` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filePHID` varbinary(64) NOT NULL,
  `sampleRate` int(10) unsigned NOT NULL,
  `usTotal` bigint(20) unsigned NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `requestPath` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `userPHID` varbinary(64) DEFAULT NULL,
  `dateCreated` int(10) unsigned NOT NULL,
  `dateModified` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filePHID` (`filePHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xhprof_sample`
--

LOCK TABLES `xhprof_sample` WRITE;
/*!40000 ALTER TABLE `xhprof_sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `xhprof_sample` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-08  0:33:20
