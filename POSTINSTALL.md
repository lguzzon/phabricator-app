This Phabricator app integrates with Cloudron User management.
However, the steps below need to be done to complete the installation.

### Completing the installation

The admin credentials are:

`username`: superadmin

`password`: changeme123

The application is already setup for Cloudron LDAP.

Post installation, you are presented with two login forms:

* LDAP form for the Cloudron users
* Username/login form for the `superadmin` user above

You can disable the username/login form and use LDAP exclusively as follows:

* Login as `superadmin` and link the account to your LDAP account. See 
`People` -> `Edit Settings` -> `External Accounts`).
* Once linked, uncheck 'Login Allowed' from the username/password auth 
provider.

### Uploading large files

* How can I upload large files?

This app is configured to accept files upto 512MB. Note that large files need to be
dragged and dropped (instead of the file upload button).

See https://secure.phabricator.com/Q216.


